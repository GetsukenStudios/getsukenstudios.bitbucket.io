import { ToDo } from '../_interface/todo';
import { LineToLineMappedSource } from 'webpack-sources';

export interface EventPing {
    label: string;
    object: ToDo;
}
