import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToDo } from '../_interface/todo';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private serverUrl = 'http://localhost:3000';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  public getToDo(): Observable<ToDo[]> {
    return this.http.get<ToDo[]>(`${this.serverUrl}/todos`, this.httpOptions);
  }

  public postToDo(object: ToDo): Observable<ToDo> {
    return this.http.post<ToDo>(`${this.serverUrl}/todos`, object, this.httpOptions);
  }

  public deleteToDo(object: ToDo): Observable<ToDo> {
    return this.http.delete<ToDo>(`${this.serverUrl}/todos/${object.id}`, this.httpOptions);
  }

  public putToDo(object: ToDo): Observable<ToDo> {
    return this.http.put<ToDo>(`${this.serverUrl}/todos/${object.id}`, object, this.httpOptions);
  }
}
