const cards = document.querySelectorAll('.memory-card');

let hasFlippedCard = false;
let lockBoard = false;
let firstCard, secondCard;
let foundPairs = 0;
let maxPairs = cards.length / 2;

initBoard();

function flipCard() {
    if (lockBoard) return;
    if (this === firstCard) return;

    this.classList.toggle('flip');

    if (!hasFlippedCard) {
        hasFlippedCard = true;
        firstCard = this;
    } else {
        secondCard = this;

        checkForMatch();
    }
}

function checkForMatch() {
    let isMatch = firstCard.dataset.char === secondCard.dataset.char;

    isMatch ? disablePair() : unflipPair();
}

function disablePair() {
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);

    foundPairs++;
    resetBoard();
    setTimeout(checkIfGameWon, 2000);
}

function unflipPair() {
    lockBoard = true;

    setTimeout(() => {
        firstCard.classList.remove('flip');
        secondCard.classList.remove('flip');

        resetBoard();
    }, 1000);
}

function unflipAllCards() {
    lockBoard = true;

    cards.forEach(card => {
        card.classList.remove('flip');
        lockBoard = false;
    });
}

function resetBoard() {
    hasFlippedCard = false;
    lockBoard = false;
    firstCard = null;
    secondCard = null;
}

function checkIfGameWon() {
    if (foundPairs === maxPairs) {
        lockBoard = true;
        foundPairs = 0;

        unflipAllCards();
        setTimeout(() => {
            initBoard();
            lockBoard = false;
        }, 1000);
    }
}

function shuffleBoard() {
    cards.forEach(card => {
        let randPos = Math.floor(Math.random() * 12);
        card.style.order = randPos;
    });
}

function initBoard() {
    cards.forEach(card => {
        card.addEventListener('click', flipCard);
    });
    shuffleBoard();
}