const squares = document.querySelectorAll(".square");
const colorDisplay = document.querySelector("#colorDisplay");
const messageDisplay = document.querySelector("#message");
const resetButton = document.querySelector("#reset");
const easyBtn = document.querySelector("#easyBtn");
const hardBtn = document.querySelector("#hardBtn");
let colors = generateRandomColors(6);
let pickedColor = pickColor();
let difficulty = "hard";
const difficultyTable = {
    easy: 3,
    hard: 6,
};

function compareColors() {
    var squareColor = this.style.backgroundColor;
    if (squareColor === pickedColor) {
        messageDisplay.textContent = "Correct!";
        resetButton.textContent = "Play Again";
        colorDisplay.style.color = pickedColor;
        changeColors(squareColor);
    } else {
        this.style.backgroundColor = "#232323";
        messageDisplay.textContent = "Try Again!";
        colorDisplay.style.color = "white";
    }
}

function changeColors(color) {
    for (let i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = color;
    }
}

function resetColors() {
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = colors[i];
    }
}

function generateRandomColors(num) {
    let randomColors = new Array(num).fill("");

    for (let i = 0; i < num; i++) {
        let r = Math.floor(Math.random() * 256);
        let g = Math.floor(Math.random() * 256);
        let b = Math.floor(Math.random() * 256);
        let str = `rgb(${r}, ${g}, ${b})`;

        randomColors[i] = str;
    }

    return randomColors;
}

function pickColor() {
    let len = colors.length;
    let num = Math.floor(Math.random() * len);

    return colors[num];
}

function resetGame() {
    messageDisplay.textContent = "";
    resetButton.textContent = "New Colors";
    curNumSquares = difficultyTable[difficulty];
    colors = generateRandomColors(curNumSquares);
    resetColors();
    pickedColor = pickColor();
    colorDisplay.textContent = pickedColor;
    colorDisplay.style.color = "white";
}

for (var i = 0; i < squares.length; i++) {
    squares[i].addEventListener("click", compareColors);
}

resetButton.addEventListener("click", () => {
    resetGame();
});

easyBtn.addEventListener("click", () => {
    difficulty = "easy";
    easyBtn.classList.add("selected");
    hardBtn.classList.remove("selected");
    resetGame();

    for (let i = 0; i < squares.length; i++) {
        if (!colors[i]) {
            squares[i].style.display = "none";
        } else {
            squares[i].style.display = "block";
        }
    }
});

hardBtn.addEventListener("click", () => {
    difficulty = "hard";
    easyBtn.classList.remove("selected");
    hardBtn.classList.add("selected");
    resetGame();

    for (let i = 0; i < squares.length; i++) {
        if (!colors[i]) {
            squares[i].style.display = "none";
        } else {
            squares[i].style.display = "block";
        }
    }
});

// start game
resetGame();