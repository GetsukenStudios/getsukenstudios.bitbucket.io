const Logger = require('./logger');
const path = require('path');
const os = require('os');
const fs = require('fs');
const EventEmitter = require('events');
const http = require('http');

// ------

var totalMem = os.totalmem();
var freeMem = os.freemem();

// ------

const filesSync = fs.readdirSync('./');

// fs.readdir('./', function (err, files) {
//     if (err) console.log('Error', err);
//     else console.log('Result', files);
// });

// ------

const logger = new Logger();

// logger.on('messageLogged', (e) => {
//     console.log('Listener called!', e);
// });

// logger.log('message');

// ------

const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.write('Hello World!');
        res.end();
    }

    if (req.url === '/api/courses') {
        res.write(JSON.stringify([1, 2, 3]));
        res.end();
    }
});

server.listen(3003);

console.log('Listening on port 3003...');