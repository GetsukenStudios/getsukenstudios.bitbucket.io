# Final Fantasy XV: Game Toy Pocket Edition \[Unofficial Fangame\]

This is a fan-made demake of *Final Fantasy XV's* first chapter in the style of the old Game Boy title *Final Fantasy Legend* from 1989. 
It's developed in JavaScript and uses the Phaser 3 framework. It's currently still work-in-progress. 
What's playable here is just a prototype test level with some of the basic features that are working already.  
    
You can play the current prototype here: https://getsukenstudios.bitbucket.io/ffxvprev.html



## Tools used
* Code
	* JavaScript
	* Phaser 3 framework \(https://phaser.io/phaser3\) 
	* Phaser 3 + ES6 + Webpack bootstrap project \(https://github.com/lean/phaser-es6-webpack/tree/phaser3\)
	* BrowserSync plugin for Webpack
* Music 
	* FL Studio
	* Nintendo Gameboy PAPU VST Plugin \(https://socalabs.com/synths/papu/\)
	* Thanks to Ichigo's Sheet Music for the FFXV Midis that served as a good starting point for my Chiptune versions of FFXV's songs \(https://ichigos.com/sheets/814\)
* Art
	* GraphicsGale
	
## Run application
* npm install
* npm start
* runs on localhost:3000