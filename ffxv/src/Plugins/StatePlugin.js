import StateMachine from "../State/StateMachine";
import {
  isDefinedAndNotNull
} from './../Base/Utils';

export default class StatePlugin extends Phaser.Plugins.BasePlugin {

  constructor(pluginManager) {
    super(pluginManager);
  }

  start() {
    console.log('StatePlugin start');

    var eventEmitter = this.game.events;
    eventEmitter.on('shutdown', this.stop, this);
    eventEmitter.on('destroy', this.destroy, this);

    //does it make sense to create state of the game inside an plugin?
    // if not remove this class
    this.stateMachine = new StateMachine('StatePlugin', this.events);
  }

  stop() {
    if (isDefinedAndNotNull(this.state)) {
      this.state.stop();
    }
  }

  destroy() {
    this.stop();
  }
}