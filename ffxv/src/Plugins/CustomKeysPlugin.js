import CustomKeys from "./../Base/CustomKeys";

export default class CustomKeysPlugin extends Phaser.Plugins.ScenePlugin {

    constructor(scene, pluginManager) {
        super(scene, pluginManager);
        this.customKeys = null;
    }

    isInitialized() {
        return this.customKeys != null && this.customKeys !== undefined && this.customKeys.isInitialized();
    }

    boot() {
        console.log('CustomKeysPlugin boot');
        this.customKeys = new CustomKeys(null, this.scene.input.keyboard, this);
    }

    start() {
        console.log('CustomKeysPlugin start');
    }

    update(options) {
        // this.customKeys.updateKeys(options);
    }

    stop() {

    }

    destroy() {
        this.stop();
    }
}