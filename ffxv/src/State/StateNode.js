import {
    isDefinedAndNotNull,
    isDefinedAndFunction
} from './../Base/Utils';

export default class StateNode {

    constructor(stateMachine, name, onEnter, onUpdate, onLeave) {
        this.name = name;
        this.parentStateMachine = stateMachine;
        this.onEnter = onEnter;
        this.onUpdate = onUpdate;
        this.onLeave = onLeave;

        this.transitions = [];

        this.parentStateMachine.addState(this);
    }

    setEventEmitter(eventEmitter) {
        this.eventEmitter = eventEmitter;
    }

    getParentStateMachine() {
        return this.parentStateMachine;
    }

    setParentStateMachine(stateMachine) {
        this.parentStateMachine = stateMachine;
    }

    addTransition(transition) {
        this.transitions.push(transition);
    }

    enter() {
        this.call(this.onEnter, ...arguments);
        this.fire('State_Enter');
    }

    leave() {
        this.call(this.onLeave, ...arguments);
        this.fire('State_Leave');
    }

    update() {
        this.call(this.onUpdate, ...arguments);
        // this.fire('State_Update');
    }

    fire(eventName) {
        if (isDefinedAndNotNull(this.eventEmitter)) {
            this.eventEmitter.emit(eventName, this);
            // console.log('\n' + 'Event fired: ' + eventName);
            this.eventEmitter.emit(eventName + '_' + this.parentStateMachine.name +
                '_' + this.name, this);
            // console.log('Event fired: ' + eventName + '_' + this.parentStateMachine.name +  '_' + this.name);
        }
    }

    call(fn, ...restArgs) {
        if (isDefinedAndFunction(fn)) {
            fn(...restArgs);
            // console.log('called function: ' + fn.name);
        }
    }
}