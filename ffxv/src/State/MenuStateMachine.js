import 'phaser';
import {
    isDefinedAndNotNull
} from './../Base/Utils';
import StateMachine from './StateMachine';

export default class MenuStateMachine extends StateMachine {

    constructor(name, eventEmitter) {
        super(name, eventEmitter, 'Menu');
    }

    // startListener() {
    //     this.eventEmitter.on('Menu', this.onMenu.bind(this), this);
    //     this.eventEmitter.on('State_Enter', this.onStateEnter.bind(this), this);
    //     // this.eventEmitter.on('State_Update', this.onStateUpdate.bind(this), this);
    // }

    // stopListener() {
    //     this.eventEmitter.off('Menu', this.onMenu.bind(this), this);
    //     this.eventEmitter.off('State_Enter', this.onStateEnter.bind(this), this);
    //     // this.eventEmitter.off('State_Update', this.onStateUpdate.bind(this), this);
    // }

    // /* 
    //  * Handles events of kind 'Menu' and passes params to matching transition.
    //  * this.events.emit('Menu', 'OpenMainMenu', this, this.callbackCloseMainMenu);
    //  */
    // onMenu(action, params) {
    //     if (!isDefinedAndNotNull(action)) {
    //         return;
    //     }
    //     this.preCheck();
    //     if (this.currentState != null &&
    //         isDefinedAndNotNull(this.currentState.transitions) &&
    //         this.currentState.transitions.length > 0) {

    //         this.currentTransitions = this.currentState.transitions;

    //         for (let i = 0; i < this.currentTransitions.length; i++) {
    //             let transition = this.currentTransitions[i];
    //             if (isDefinedAndNotNull(transition) &&
    //                 transition.acceptsAction(action) &&
    //                 transition.isForState(this.currentState)) {
    //                 transition.execute(params);
    //                 // this.currentState = transition.newState;
    //             }
    //         }
    //     }
    // }
}