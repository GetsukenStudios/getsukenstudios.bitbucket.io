import {
    isDefinedAndNotNull,
    isDefinedAndFunction
} from './../Base/Utils';

export default class StateTransition {

    constructor(action, oldState, newState, fn) {
        // this.name = name;
        this.action = action;
        this.oldState = oldState;
        this.newState = newState;
        this.fn = fn;

        this.addTransitionToOldState();
    }

    acceptsAction(action) {
        if (isDefinedAndNotNull(this.action)) {
            return this.action == action;
        }
        return false;
    }

    isForState(state) {
        if (isDefinedAndNotNull(this.oldState)) {
            return this.oldState === state;
        }
        return false;
    }

    addTransitionToOldState() {
        if (isDefinedAndNotNull(this.oldState)) {
            this.oldState.addTransition(this);
        }
    }

    execute() {
        if (!isDefinedAndNotNull(this.oldState) || !isDefinedAndNotNull(this.newState)) {
            return;
        }

        if (this.oldState == this.newState) {
            // this.oldState.reenter(arguments);
        } else {
            this.oldState.leave(...arguments);
            this.newState.enter(...arguments);
        }
        if (isDefinedAndFunction(this.fn)) {
            this.fn(...arguments);
        }
    }
}