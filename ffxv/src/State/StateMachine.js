import 'phaser';
import {
    isDefinedAndNotNull
} from './../Base/Utils';

export default class StateMachine {

    constructor(name, eventEmitter, eventName) {
        this.name = name;
        this.eventEmitter = eventEmitter;
        this.eventName = eventName || "Action";

        this.stateNodes = [];

        this.startState = null;
        this.currentState = null;
        this.lastState = null;
        this.currentTransitions = null;
    }

    initialize(stateObject) {
        this.stateNodes = [];
        //TODO: analyse state object and create nodes, actions and transitions
        if (isDefinedAndNotNull(stateObject)) {

        }
        /*
        let blub = {start:{execute:function(){}},end:{name:'The End',execute:function(){}}}
        for(let prop in blub){
            let propObj = blub[prop];
            let text = prop + '(';
            for(let prop2 in propObj){
                let prop2Obj = propObj[prop2];
                text = text + prop2 + '[' + typeof prop2Obj + '],';
            }
            console.log(text + ')');
        }
        returns: 
        start(execute[function],)
        end(name[string],execute[function],)

        console.log(typeof {})
        returns: object
        */

        /* ultimate goal: create statemachine with an single object like:
        * {
        	start : {
        		moveRight : {
        			next : continue,
        			execute : function() {
        				this.menuCursor.setX(88);
        			}
        		},
        		enter : function() {
        			
        		}
        	}, 
        	continue : {
        		moveLeft : {
        			next : start,
        			execute : function() {
        				this.menuCursor.setX(8);
        			}
        		},
        		enter : function() {

        		}
        	}
          }
        */
    }

    getCurrentState() {
        return this.currentState;
    }

    setCurrentState(state) {
        this.currentState = state;
    }

    getCurrentStateName() {
        if (isDefinedAndNotNull(this.currentState)) {
            return this.currentState.name;
        }
    }

    getLastState() {
        return this.lastState;
    }

    setLastState(state) {
        this.lastState = state;
    }

    getLastStateName() {
        if (isDefinedAndNotNull(this.lastState)) {
            return this.lastState.name;
        }
    }

    addState(stateNode) {
        if (isDefinedAndNotNull(stateNode)) {
            this.checkStateNodes();
            this.stateNodes.push(stateNode);
            stateNode.setEventEmitter(this.eventEmitter);
        }
    }

    checkStateNodes() {
        if (!isDefinedAndNotNull(this.stateNodes)) {
            this.stateNodes = [];
        }
    }

    preCheck() {
        this.checkStateNodes();
        if (this.currentState == null && this.stateNodes.length > 0) {
            this.currentState = this.stateNodes[0];
            this.startState = this.currentState;
            this.lastState = null;
        }
    }

    start() {
        this.preCheck();
        this.startListener();
        if (this.currentState != null) {
            this.currentState.enter();
        }
    }

    startListener() {
        this.eventEmitter.on(this.eventName, this.onEvent, this);
        this.eventEmitter.on('State_Enter', this.onStateEnter, this);
        // this.eventEmitter.on('State_Update', this.onStateUpdate, this);
    }

    stop() {
        this.stopListener();
    }

    stopListener() {
        // TODO: off() doesn't work here! why!?
        this.eventEmitter.off(this.eventName, this.onEvent, this);
        this.eventEmitter.off('State_Enter', this.onStateEnter, this);
        // this.eventEmitter.off('State_Update', this.onStateUpdate, this);
    }

    reset() {
        if (isDefinedAndNotNull(this.startState)) {
            this.currentState = this.startState;
            this.lastState = null;
        }
    }

    /* 
     * Handles special events and passes params to matching transition.
     * this.events.emit('Action', 'KeyDown', this.keys.action);
     */
    onEvent(eventName, ...restArgs) {
        if (!isDefinedAndNotNull(eventName)) {
            return;
        }
        this.preCheck();
        if (this.currentState != null &&
            isDefinedAndNotNull(this.currentState.transitions) &&
            this.currentState.transitions.length > 0) {

            this.currentTransitions = this.currentState.transitions;

            for (let i = 0; i < this.currentTransitions.length; i++) {
                let transition = this.currentTransitions[i];
                if (isDefinedAndNotNull(transition) &&
                    transition.acceptsAction(eventName) &&
                    transition.isForState(this.currentState)) {
                    transition.execute(...restArgs);
                    // this.currentState = transition.newState;
                }
            }
        }
    }

    onStateEnter(newState) {
        let newStateMachine = newState.getParentStateMachine();
        if (newStateMachine === this) {
            this.lastState = this.currentState;
            this.currentState = newState;
        }
    }

    onStateUpdate() {

    }

    updateState() {
        this.currentState.update();
    }
}