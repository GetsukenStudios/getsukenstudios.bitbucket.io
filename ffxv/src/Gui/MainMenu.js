import {
    Item,
    Counter,
    CustomString
} from '../Game/GameObjects';
import {
    ListComponent
} from './ListComponent';
import {
    MainMenuWindow
} from './MainMenuWindow';
import {
    ItemListWindow
} from './ItemListWindow';
import {
    BaseMenu
} from './BaseMenu';
import {
    isDefinedAndNotNull,
    isUndefinedOrNull,
    getPropOrDefault,
    isDefinedAndFunction,
    isDefined,
    remove
} from '../Base/Utils';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';

export class MainMenu extends BaseMenu {

    constructor(scene) {
        super(scene);

        this.openMainMenuWindow();

        this.draw();
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        this.itemMenuState = new StateNode(stateMachine, 'ItemMenuState');
        this.charListMenuState = new StateNode(stateMachine, 'CharListMenuState');
        this.saveMenuState = new StateNode(stateMachine, 'SaveMenuState');

        new StateTransition('OpenItemMenu', this.startState, this.itemMenuState);
        new StateTransition('CloseItemMenu', this.itemMenuState, this.startState, this.closeItemMenu.bind(this));
        new StateTransition('OpenCharListMenu', this.startState, this.charListMenuState);
        new StateTransition('CloseCharListMenu', this.charListMenuState, this.startState);
        new StateTransition('OpenSaveMenu', this.startState, this.saveMenuState);
        new StateTransition('CloseSaveMenu', this.saveMenuState, this.startState);

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    resumeCallback() {
        super.resumeCallback();
    }

    openMainMenuWindow() {
        let entries;
        let newPos;

        this.mainMenuWindow = new MainMenuWindow('mainMenuWindow', this.scene, 1, 12);
        this.addWindow(this.mainMenuWindow);

        let testItem1 = new Item('test1', 'test1');
        let gear = this.stringDB.getCustomStringByKey('string-menu-gear');
        let item = this.stringDB.getCustomStringByKey('string-menu-item');
        let technique = this.stringDB.getCustomStringByKey('string-menu-technique');
        let save = this.stringDB.getCustomStringByKey('string-menu-save');
        entries = this.createEntries([gear, item, technique, save]);
        this.mainMenuList = new ListComponent('mainMenuList', this.scene, this,
            this.tileSizeMenu * 2, this.tileSizeMenu * 2, 3, 3, entries);
        this.mainMenuWindow.addComponent(this.mainMenuList);

        newPos = this.getOffsetFromMenuTilesToPixel(8, 4);
        let moneyText = this.stringDB.getCustomStringByKey('string-gil');
        let partyReg = this.scene.registry.get('party');
        let moneyValue = partyReg.getPartyData().money;
        let moneyCounter = new Counter('moneyCounter', moneyValue, moneyText.getFullName(), 6);
        entries = this.createEntries([moneyCounter]);
        this.moneyCounterComp = new ListComponent('moneyCounterComp', this.scene, this, newPos.x, newPos.y, 1, 1, entries);
        this.mainMenuWindow.addComponent(this.moneyCounterComp);

        this.mainMenuWindow.setActiveComponent(this.mainMenuList);
        this.setActiveWindow(this.mainMenuWindow);
    }

    closeMainMenu() {
        // check parent menu scene to know which specific Close event to fire 
        this.scene.game.events.emit('Menu', 'CloseMainMenu');
    }

    openItemMenu() {
        this.scene.events.emit('Action', 'OpenItemMenu');

        this.scene.game.events.emit('Menu', 'OpenItemMenu');
        // this.scene.game.events.emit('Menu', 'OpenItemMenuFromMainMenu', null, this.getCallbackForNextScene().bind(this));
    }

    closeItemMenu() {
        // console.log('\n successfully received event from ItemMenuScene!');
    }

    openCharListMenu(menuToSwitchTo) {
        this.scene.events.emit('Action', 'OpenCharListMenu', {
            nextMenu: menuToSwitchTo
        });
    }

    openSaveMenu() {
        this.scene.events.emit('Action', 'OpenSaveMenu');

        this.mainMenuList.setShowCursor1WhileNotActive(false);

        this.saveChoiceWindow = new MainMenuWindow('saveChoiceWindow', this.scene, 1, 12);
        this.addWindow(this.saveChoiceWindow);
        this.setActiveWindow(this.saveChoiceWindow);

        let saveChoiceTextString = this.stringDB.getCustomStringByKey('string-save-choice');
        let entries = this.createEntries([saveChoiceTextString]);
        this.saveChoiceTextComp = new ListComponent('saveChoiceTextComp', this.scene, this,
            this.tileSizeMenu * 2, this.tileSizeMenu * 2, 1, 1, entries);
        this.saveChoiceWindow.addComponent(this.saveChoiceTextComp);

        let saveChoiceString1 = this.stringDB.getCustomStringByKey('string-yes');
        let saveChoiceString2 = this.stringDB.getCustomStringByKey('string-no');
        entries = this.createEntries([saveChoiceString1, saveChoiceString2]);
        this.saveChoiceComp = new ListComponent('saveChoiceTextComp', this.scene, this,
            this.tileSizeMenu * 4, this.tileSizeMenu * 4, 2, 1, entries);
        this.saveChoiceComp.setEntryMarginInTiles(5, 1);
        this.saveChoiceWindow.addComponent(this.saveChoiceComp);
        this.setActiveComponent(this.saveChoiceComp);

        this.draw();

        // this.resumeCallback();
    }

    closeSaveMenu() {
        this.scene.events.emit('Action', 'CloseSaveMenu');

        this.removeWindow(this.saveChoiceWindow);
        this.setActiveWindow(this.mainMenuWindow);
        this.setActiveComponent(this.mainMenuList);

        this.draw();
    }

    updateSaveStateData() {
        let saveState = this.scene.registry.get('saveState');
        let gameSceneKey = this.scene.gameState.getCurrentSceneKey();
        let gameScene = this.scene.gameState.getCurrentScene();
        let playerSprite = gameScene.sprites.playerSprite;
        saveState.setDataByKey('currentSceneKey', gameSceneKey);
        saveState.setDataByKey('currentPosition', {
            x: playerSprite.x,
            y: playerSprite.y
        });
        saveState.setDataByKey('currentFaceDirection', playerSprite.direction);

        // console.log(saveState);
    }

    confirm() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            let currEntry = this.getActiveComponent().confirm().getKey();

            if (this.stateMachine.getCurrentStateName() === 'StartState') {
                switch (currEntry) {
                    case 'string-menu-gear':
                        this.openCharListMenu(currEntry);

                        break;
                    case 'string-menu-item':
                        this.openItemMenu();

                        break;
                    case 'string-menu-technique':
                        this.openCharListMenu(currEntry);

                        break;
                    case 'string-menu-save':
                        // this.resumeCallback(); // remove this when Save functionality works
                        this.openSaveMenu();

                        break;

                    default:
                        break;
                }
            } else if (this.stateMachine.getCurrentStateName() === 'SaveMenuState') {

                if (currEntry === 'string-yes') {
                    this.updateSaveStateData();

                    let gameDataManager = this.scene.registry.get('gameDataManager');
                    gameDataManager.saveGame();

                } else if (currEntry === 'string-no') {

                    // let gameDataManager = this.scene.registry.get('gameDataManager');
                    // gameDataManager.loadGame();
                }
                this.closeSaveMenu();
            }
        }
    }

    cancel() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {
            this.closeMainMenu();
        } else if (this.stateMachine.getCurrentStateName() === 'SaveMenuState') {
            this.closeSaveMenu();
        }
    }
}