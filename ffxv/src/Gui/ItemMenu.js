import {
    ItemListComponent
} from './ItemListComponent';
import {
    ItemListWindow
} from './ItemListWindow';
import {
    BaseMenu
} from './BaseMenu';
import {
    remove,
    isDefinedAndNotNull,
    prependWhitespaces
} from '../Base/Utils';
import {
    EmptyObject,
    Item,
    CustomImage,
    SpecialMenuObject,
    Character
} from '../Game/GameObjects';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';
import {
    BaseWindow
} from './BaseWindow';
import {
    ListComponent
} from './ListComponent';
import {
    CharacterSelectionRenderer
} from './CharacterSelectionRenderer';

export class ItemMenu extends BaseMenu {

    constructor(scene) {
        super(scene);

        this.initialInventoryItems = [];
        this.partyList = [];
        this.selectedItem = null;
        this.selectedItemIndex = 0;
        this.selectedItemEntry = null;
        this.selectedChar = null;

        this.initializeItemInventory();

        this.openItemListWindow();
    }

    createEntries(objects) {
        let entries = super.createEntries(objects);

        objects.forEach((obj, index) => {
            let entry = null;
            if (typeof obj === 'string') {
                if (obj === 'none') {
                    obj = new EmptyObject();

                    entry = this.createEntry(obj);
                }
            }

            if (isDefinedAndNotNull(entry)) {
                entries[index] = entry;
            }
        });
        return entries;
    }

    createRenderer(object) {
        let renderer;
        renderer = super.createRenderer(object);

        if (object instanceof Character) {
            renderer = new CharacterSelectionRenderer(this.scene, object, 'itemCharList');
        }

        return renderer;
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        this.swapModeState = new StateNode(stateMachine, 'SwapModeState');
        this.charListState = new StateNode(stateMachine, 'CharListState');

        new StateTransition('StartSwapMode', this.startState, this.swapModeState);
        new StateTransition('StopSwapMode', this.swapModeState, this.startState);
        new StateTransition('SelectChar', this.swapModeState, this.charListState);
        new StateTransition('SelectCharCancel', this.charListState, this.swapModeState);

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    initializeItemInventory() {
        let stringDB = this.scene.registry.get('stringDatabase');
        let itemDB = this.scene.registry.get('itemDatabase');
        let itemsInv = this.scene.registry.get('inventoryItems');

        this.initialInventoryItems = itemsInv.getInventoryItemObjects(stringDB, itemDB);

        this.initialInventoryItems.push('trash');
    }

    openItemListWindow() {
        this.itemListWindow = new ItemListWindow('itemListWindow', this.scene, 0, 0);
        this.addWindow(this.itemListWindow);

        let entries = this.createEntries(this.initialInventoryItems);
        this.itemListComp = new ItemListComponent('itemListComp', this.scene, this,
            this.tileSizeMenu * 2 - 4, this.tileSizeMenu * 2, 1, 8, entries);
        this.itemListComp.setShowCursor1WhileNotActive(false);
        this.itemListComp.setCursorOffset(1, 0);
        this.itemListWindow.addComponent(this.itemListComp);
        this.itemListWindow.setActiveComponent(this.itemListComp);
        this.setActiveWindow(this.itemListWindow);

        this.draw();
    }

    closeItemMenu() {
        this.getParentScene().events.emit('Action', 'CloseItemMenu');
        this.scene.game.events.emit('Menu', 'CloseItemMenu', {
            selectedItem: this.selectedItem
        }, this.getParentSceneCallback());
    }

    openCharListWindow() {
        this.charListWindow = new BaseWindow('charListWindow', this.scene, 11, 0, 9, 18);
        this.addWindow(this.charListWindow);

        let partyData = this.scene.registry.get('party');
        let stringDB = this.scene.registry.get('stringDatabase');
        this.partyList = [];
        this.partyList = partyData.getPartyCharObjects(stringDB);

        let entries = this.createEntries(this.partyList);
        this.charListComp = new ListComponent('charListComp', this.scene, this,
            this.tileSizeMenu, this.tileSizeMenu * 1, 1, 4, entries);
        this.charListComp.setShowCursor1WhileNotActive(false);
        this.charListComp.setCursorOffset(3, 0);
        this.charListWindow.addComponent(this.charListComp);

        this.draw();
    }

    closeCharListWindow() {
        this.removeWindow(this.charListWindow);
        this.charListWindow = null;
    }

    switchToCharListWindow() {
        this.setActiveWindow(this.charListWindow);
        this.setActiveComponent(this.charListComp);
        this.draw();
    }

    switchToItemListWindow() {
        this.setActiveWindow(this.itemListWindow);
        this.setActiveComponent(this.itemListComp);
        this.itemListComp.select(this.selectedItemIndex);
    }

    selectCharCancel() {
        this.scene.events.emit('Action', 'SelectCharCancel');
        this.switchToItemListWindow();
        this.closeCharListWindow();
        this.getActiveComponent().cancel();
    }

    applyItemToChar() {
        let gameSettings = this.scene.registry.get('gameSettings');
        let effectDB = this.scene.registry.get('effectDatabase');
        this.selectedItem.applyItemEffect(gameSettings, effectDB, this.selectedChar);
        if (this.selectedItem.quantity === 0) {
            this.itemListComp.clearEntry(this.selectedItemEntry);
            this.itemListComp.entries[this.selectedItemIndex] = this.createEntry(new EmptyObject());

            this.selectCharCancel();
        }

        this.draw();

        this.saveInventory();
        this.saveParty();
    }

    saveInventory() {
        let itemInventory = this.scene.registry.get('inventoryItems');
        let entryObjects = this.itemListComp.getEntryObjects();

        itemInventory.setDataFromItemObjects(entryObjects);
    }

    saveParty() {
        let newParty = this.scene.registry.get('party');

        newParty.setCharsFromCharObjects(this.partyList);
    }

    confirm() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            if (this.stateMachine.getCurrentStateName() === 'StartState') {
                this.selectedItem = this.getActiveComponent().confirm();
                this.selectedItemIndex = this.getActiveComponent().getCurrentIndex();
                this.selectedItemEntry = this.getActiveComponent().getCurrentEntry();

            } else if (this.stateMachine.getCurrentStateName() === 'SwapModeState') {
                let currIndex = this.getActiveComponent().getCurrentIndex();
                let effectDB = this.scene.registry.get('effectDatabase');
                let selectedItemEffect = this.selectedItem.effect || null;
                let effect = isDefinedAndNotNull(selectedItemEffect) ? effectDB.getEffectObjectByKey(selectedItemEffect) : null;
                if (this.selectedItem.type === 'consumable' &&
                    (effect && effect.type !== 'cure') &&
                    currIndex === this.selectedItemIndex
                ) {
                    this.openCharListWindow();
                    this.scene.events.emit('Action', 'SelectChar');
                    if (this.charListWindow) {
                        this.switchToCharListWindow();
                    }
                } else {
                    this.getActiveComponent().confirm();

                    if (this.charListWindow) {
                        this.closeCharListWindow();
                    }

                    this.saveInventory();
                }
            } else if (this.stateMachine.getCurrentStateName() === 'CharListState') {
                this.selectedChar = this.getActiveComponent().getCurrentEntryObject();

                this.applyItemToChar();

                this.saveInventory();
            }
        }
    }

    cancel() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            if (this.stateMachine.getCurrentStateName() === 'StartState') {
                this.closeItemMenu();
            } else if (this.stateMachine.getCurrentStateName() === 'SwapModeState') {
                this.getActiveComponent().cancel();

                if (this.charListWindow) {
                    this.closeCharListWindow();
                }
            } else if (this.stateMachine.getCurrentStateName() === 'CharListState') {
                this.selectCharCancel();
            }
        }
    }

    // pressRight() {
    //     if (isDefinedAndNotNull(this.getActiveComponent())) {
    //         if (this.stateMachine.getCurrentStateName() === 'SwapModeState') {
    //             this.scene.events.emit('Action', 'SelectChar');
    //             if (this.charListWindow) {
    //                 this.switchToCharListWindow();
    //             }
    //         }
    //     }
    // }

    // pressLeft() {
    //     if (isDefinedAndNotNull(this.getActiveComponent())) {
    //         if (this.stateMachine.getCurrentStateName() === 'CharListState') {
    //             this.scene.events.emit('Action', 'SelectCharCancel');
    //             this.switchToItemListWindow();
    //             this.closeCharListWindow();
    //             this.getActiveComponent().cancel();
    //         }
    //     }
    // }
}