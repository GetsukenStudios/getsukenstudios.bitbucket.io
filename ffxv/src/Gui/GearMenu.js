import {
    BaseMenu
} from './BaseMenu';
import {
    isDefinedAndNotNull,
    prependWhitespaces,
    capitalizeFirstLetter
} from '../Base/Utils';
import {
    EmptyObject,
    Item,
    SpecialMenuObject,
    CustomString,
    CustomImage,
    GameObject
} from '../Game/GameObjects';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';
import {
    GearMenuCharWindow
} from './GearMenuCharWindow';
import {
    ItemRenderer
} from './ItemRenderer';
import {
    ListComponent
} from './ListComponent';
import {
    BaseComponent
} from './BaseComponent';
import {
    GearChoiceInventoryWindow
} from './GearChoiceInventoryWindow';
import {
    BaseWindow
} from './BaseWindow';

export class GearMenu extends BaseMenu {

    constructor(scene, data) {
        super(scene);

        this.selectedChar = data.selectedChar;
        this.oldSelectedItem = null;
        this.oldSelectedItemIndex = null;
        this.newSelectedItem = null;
        this.newSelectedItemIndex = null;
        this.equippedWeapons = [];
        this.equippedAccs = [];
        this.inventoryWeapons = [];
        this.inventoryAccs = [];
        this.currStats = {};

        this.initializeInventories();

        this.openGearMenuCharWindow();
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        this.chooseNewWeaponState = new StateNode(stateMachine, 'ChooseNewWeaponState');
        this.chooseNewAccessoryState = new StateNode(stateMachine, 'ChooseNewAccessoryState');

        new StateTransition('ChooseNewWeapon', this.startState, this.chooseNewWeaponState, this.openNewInventory.bind(this));
        // new StateTransition('ChooseNewWeapon', this.startState, this.chooseNewWeaponState);
        new StateTransition('CancelWeaponChoice', this.chooseNewWeaponState, this.startState, this.closeNewInventory.bind(this));
        // new StateTransition('CancelWeaponChoice', this.chooseNewWeaponState, this.startState);
        new StateTransition('ChooseNewAccessory', this.startState, this.chooseNewAccessoryState, this.openNewInventory.bind(this));
        // new StateTransition('ChooseNewAccessory', this.startState, this.chooseNewAccessoryState);
        new StateTransition('CancelAccessoryChoice', this.chooseNewAccessoryState, this.startState, this.closeNewInventory.bind(this));
        // new StateTransition('CancelAccessoryChoice', this.chooseNewAccessoryState, this.startState);
        new StateTransition('WeaponChangeDone', this.chooseNewWeaponState, this.startState, this.weaponChangeDone.bind(this));
        // new StateTransition('WeaponChangeDone', this.chooseNewWeaponState, this.startState);
        new StateTransition('AccessoryChangeDone', this.chooseNewAccessoryState, this.startState, this.accessoryChangeDone.bind(this));
        // new StateTransition('AccessoryChangeDone', this.chooseNewAccessoryState, this.startState);

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    initializeInventories() {
        let stringDB = this.scene.registry.get('stringDatabase');
        let gearDB = this.scene.registry.get('gearDatabase');
        let partyData = this.scene.registry.get('party');
        let invWeaponsData = this.scene.registry.get('inventoryWeapons');
        let invAccsData = this.scene.registry.get('inventoryAccessories');
        let charKey = this.selectedChar.key;

        this.equippedWeapons = partyData.getEquippedWeaponObjects(charKey, stringDB, gearDB);
        this.equippedAccs = partyData.getEquippedAccObjects(charKey, stringDB, gearDB);
        this.inventoryWeapons = invWeaponsData.getInventoryWeaponObjects(stringDB, gearDB);
        this.inventoryAccs = invAccsData.getInventoryAccObjects(stringDB, gearDB);
    }

    createRenderer(object) {
        let renderer;
        renderer = super.createRenderer(object);

        if (object instanceof Item) {
            renderer = new ItemRenderer(this.scene, object, true);
        }

        return renderer;
    }

    openGearMenuCharWindow() {
        this.gearMenuCharWindow = new GearMenuCharWindow('gearMenuCharWindow', this.scene, 0, 0);
        this.addWindow(this.gearMenuCharWindow);

        this.charAvaComp = new BaseComponent('charAvaComp', this.scene, this,
            this.tileSizeMenu * 1, this.tileSizeMenu * 1);
        this.gearMenuCharWindow.addComponent(this.charAvaComp);

        this.charNameComp = new BaseComponent('charNameComp', this.scene, this,
            this.tileSizeMenu * 3, this.tileSizeMenu * 1);
        this.gearMenuCharWindow.addComponent(this.charNameComp);

        this.hpComp = new BaseComponent('hpComp', this.scene, this, this.tileSizeMenu * 3,
            this.tileSizeMenu * 2);
        this.gearMenuCharWindow.addComponent(this.hpComp);

        this.gearListComp = new ListComponent('gearListComp', this.scene, this,
            this.tileSizeMenu * 12, this.tileSizeMenu * 1, 1, 17);
        this.gearListComp.setEntryMargin(0, 0);
        this.gearMenuCharWindow.addComponent(this.gearListComp);

        this.gearListExpectedWeaponTypeComp = new ListComponent('gearListExpectedWeaponTypeComp', this.scene,
            this, this.tileSizeMenu * 12, this.tileSizeMenu * 3, 1, 2);
        this.gearMenuCharWindow.addComponent(this.gearListExpectedWeaponTypeComp);

        this.statsNamesComp = new ListComponent('statsNamesComp', this.scene, this, this.tileSizeMenu * 1,
            this.tileSizeMenu * 4, 1, 13, null, null, null, {
                width: 0,
                height: 0
            });
        this.gearMenuCharWindow.addComponent(this.statsNamesComp);


        this.statsValuesComp = new ListComponent('statsValuesComp', this.scene, this, this.tileSizeMenu * 7,
            this.tileSizeMenu * 4, 1, 13, null, null, null, {
                width: 0,
                height: 0
            });
        this.gearMenuCharWindow.addComponent(this.statsValuesComp);

        this.setActiveWindow(this.gearMenuCharWindow);
        this.gearMenuCharWindow.setActiveComponent(this.gearListComp);

        this.updateGearMenuCharWindow();
    }

    updateGearMenuCharWindow() {
        this.gearMenuCharWindow.clear();

        let imageFrame;
        switch (this.selectedChar.getKey()) {
            case 'char-noctis':
                imageFrame = 0;

                break;

            case 'char-gladio':
                imageFrame = 6;

                break;

            case 'char-ignis':
                imageFrame = 12;

                break;

            case 'char-prompto':
                imageFrame = 18;

                break;

            default:
                break;
        }

        let entries;

        entries = this.createEntries([new CustomImage('sprites-main-chars', imageFrame)]);
        this.charAvaComp.setEntries(entries, true);

        entries = this.createEntries([new CustomString(this.selectedChar.getKey(),
            this.selectedChar.getFullName(), this.selectedChar.getShortName(), true)]);
        this.charNameComp.setEntries(entries, true);

        let hpText = '' + prependWhitespaces(String(this.selectedChar.hp), 3) + '\/' +
            prependWhitespaces(String(this.selectedChar.maxHp.total), 3);
        entries = this.createEntries([hpText]);
        this.hpComp.setEntries(entries, true);

        let weapon1 = this.equippedWeapons[0];
        let weapon2 = this.equippedWeapons[1];
        let weapon3 = 'skip';
        let weapon4 = 'skip';
        if (this.selectedChar.key === 'char-noctis') {
            weapon3 = this.equippedWeapons[2];
            weapon4 = this.equippedWeapons[3];
        }
        let accessory1 = this.equippedAccs[0];
        let accessory2 = this.equippedAccs[1];

        let menuString = this.stringDB.getCustomStringByKey('string-menu-arms');
        menuString.setSkip(true);
        let accString = this.stringDB.getCustomStringByKey('string-menu-accessories');
        accString.setSkip(true);
        entries = this.createEntries([menuString, 'skip',
            weapon1, 'skip', weapon2, 'skip', weapon3, 'skip', weapon4, 'skip',
            'skip', accString, 'skip',
            accessory1, 'skip', accessory2, 'skip'
        ]);
        this.gearListComp.setEntries(entries, true);


        let weaponType1;
        let weaponType2;
        let currCharIsNotNoctis = false;
        if (this.selectedChar.key === 'char-gladio') {
            weaponType1 = new Item({
                key: '',
                type: 'weapon',
                subType: 'greatsword'
            });
            weaponType2 = new Item({
                key: '',
                type: 'weapon',
                subType: 'shield'
            });
            currCharIsNotNoctis = true;
        } else if (this.selectedChar.key === 'char-ignis') {
            weaponType1 = new Item({
                key: '',
                type: 'weapon',
                subType: 'daggers'
            });
            weaponType2 = new Item({
                key: '',
                type: 'weapon',
                subType: 'polearm'
            });
            currCharIsNotNoctis = true;
        } else if (this.selectedChar.key === 'char-prompto') {
            weaponType1 = new Item({
                key: '',
                type: 'weapon',
                subType: 'firearm'
            });
            weaponType2 = new Item({
                key: '',
                type: 'weapon',
                subType: 'machinery'
            });
            currCharIsNotNoctis = true;
        }
        if (currCharIsNotNoctis) {
            entries = this.createEntries([weaponType1, weaponType2]);
            this.gearListExpectedWeaponTypeComp.setEntries(entries);
        }


        entries = this.createEntries([
            this.stringDB.getCustomStringByKey('string-stat-stats'),
            '',
            this.stringDB.getCustomStringByKey('string-stat-attack'),
            this.stringDB.getCustomStringByKey('string-stat-defense'),
            this.stringDB.getCustomStringByKey('string-stat-magic'),
            this.stringDB.getCustomStringByKey('string-stat-spirit'),
            '',
            this.stringDB.getCustomStringByKey('string-stat-resistances'),
            '',
            this.stringDB.getCustomStringByKey('string-stat-fire-resistance'),
            this.stringDB.getCustomStringByKey('string-stat-ice-resistance'),
            this.stringDB.getCustomStringByKey('string-stat-lightning-resistance'),
            this.stringDB.getCustomStringByKey('string-stat-dark-resistance'),
        ]);
        this.statsNamesComp.setEntries(entries, true);


        let partyData = this.scene.registry.get('party');
        let gearDB = this.scene.registry.get('gearDatabase');
        this.currStats = partyData.calculateStatModsFromEquippedGear(this.selectedChar.key, gearDB);

        entries = this.createEntries([
            '',
            '',
            prependWhitespaces(this.currStats.attack.total.toString(), 3),
            prependWhitespaces(this.currStats.defense.total.toString(), 3),
            prependWhitespaces(this.currStats.magic.total.toString(), 3),
            prependWhitespaces(this.currStats.spirit.total.toString(), 3),
            '',
            '',
            '',
            prependWhitespaces(this.currStats.fireResistance.total.toString(), 3),
            prependWhitespaces(this.currStats.iceResistance.total.toString(), 3),
            prependWhitespaces(this.currStats.lightningResistance.total.toString(), 3),
            prependWhitespaces(this.currStats.darkResistance.total.toString(), 3),
        ]);
        this.statsValuesComp.setEntries(entries, true);


        this.saveStats();

        this.draw();
    }

    updateGearChoiceInventoryStatsWindow() {
        let newItem = this.getActiveComponent().getCurrentEntryObject();

        let attack = 0;
        let defense = 0;
        let magic = 0;
        let spirit = 0;
        let fireRes = 0;
        let iceRes = 0;
        let lightningRes = 0;
        let darkRes = 0;

        if (newItem instanceof Item) {
            if (newItem.hasOwnProperty('power')) {
                attack += newItem.power;
            }
            if (newItem.statModifiers.hasOwnProperty('strength')) {
                attack += newItem.statModifiers.strength;
            }
            if (newItem.statModifiers.hasOwnProperty('vitality')) {
                defense = newItem.statModifiers.vitality;
            }
            if (newItem.statModifiers.hasOwnProperty('magic')) {
                magic = newItem.statModifiers.magic;
            }
            if (newItem.statModifiers.hasOwnProperty('spirit')) {
                spirit = newItem.statModifiers.spirit;
            }
            if (newItem.statModifiers.hasOwnProperty('fireResistance')) {
                fireRes = newItem.statModifiers.fireResistance;
            }
            if (newItem.statModifiers.hasOwnProperty('iceResistance')) {
                iceRes = newItem.statModifiers.iceResistance;
            }
            if (newItem.statModifiers.hasOwnProperty('lightningResistance')) {
                lightningRes = newItem.statModifiers.lightningResistance;
            }
            if (newItem.statModifiers.hasOwnProperty('darkResistance')) {
                darkRes = newItem.statModifiers.darkResistance;
            }
        }

        let entries = this.createEntries([
            prependWhitespaces(attack.toString(), 3),
            prependWhitespaces(fireRes.toString(), 3),
            prependWhitespaces(defense.toString(), 3),
            prependWhitespaces(iceRes.toString(), 3),
            prependWhitespaces(magic.toString(), 3),
            prependWhitespaces(lightningRes.toString(), 3),
            prependWhitespaces(spirit.toString(), 3),
            prependWhitespaces(darkRes.toString(), 3),
        ]);
        this.gearChoiceInventoryStatsValuesComp.setEntries(entries, true);

        this.gearChoiceInventoryStatsWindow.draw();

        if (newItem.type === 'accessory' &&
            newItem.statModifiers.immunities && newItem.statModifiers.immunities.length > 0) {
            let dialogDB = this.scene.registry.get('dialogDatabase');
            let dlg1 = dialogDB.getDialogStringByKey('dlg-0028');
            let immunityStatusKey = newItem.statModifiers.immunities[0];
            let immunityString = this.stringDB.getCustomStringByKey(immunityStatusKey).getFullName().toUpperCase();
            entries = this.createEntries([dlg1.getText() + ' ' + immunityString]);

            this.gearChoiceInventoryAccDetailWindow.clearComponents();
            this.gearChoiceInventoryAccDetailComp.setEntries(entries, true);
            this.gearChoiceInventoryAccDetailWindow.setWindowVisible(true);
            this.gearChoiceInventoryAccDetailWindow.draw();
        } else if (newItem.type === 'weapon' &&
            newItem.statModifiers.statusAilments && newItem.statModifiers.statusAilments.length > 0) {
            let dialogDB = this.scene.registry.get('dialogDatabase');
            let dlg1 = dialogDB.getDialogStringByKey('dlg-0032').getText();
            dlg1 = capitalizeFirstLetter(dlg1);
            let ailmentStatusKey = newItem.statModifiers.statusAilments[0];
            let ailmentString = this.stringDB.getCustomStringByKey(ailmentStatusKey).getFullName().toUpperCase();
            entries = this.createEntries([dlg1 + ' ' + ailmentString]);

            this.gearChoiceInventoryAccDetailWindow.clearComponents();
            this.gearChoiceInventoryAccDetailComp.setEntries(entries, true);
            this.gearChoiceInventoryAccDetailWindow.setWindowVisible(true);
            this.gearChoiceInventoryAccDetailWindow.draw();
        } else {
            this.gearChoiceInventoryAccDetailWindow.setWindowVisible(false);
            this.gearChoiceInventoryAccDetailWindow.draw();
        }
    }

    saveStats() {
        let partyData = this.scene.registry.get('party');
        let charKey = this.selectedChar.key;
        let currChar = partyData.getCharDataByKey(charKey);

        partyData.setStatsFromObject(charKey, this.currStats);
    }

    saveInventoriesAndEquipment() {
        let gearDB = this.scene.registry.get('gearDatabase');
        let partyData = this.scene.registry.get('party');
        let invWeaponsData = this.scene.registry.get('inventoryWeapons');
        let invAccsData = this.scene.registry.get('inventoryAccessories');
        let charKey = this.selectedChar.key;

        invWeaponsData.setDataFromWeaponObjects(this.inventoryWeapons);
        invAccsData.setDataFromAccObjects(this.inventoryAccs);
        partyData.setEquippedWeaponsDataFromWeaponObjects(charKey, this.equippedWeapons);
        partyData.setEquippedAccsDataFromAccObjects(charKey, this.equippedAccs);
    }

    closeGearMenuCharWindow() {
        this.removeWindow(this.gearMenuCharWindow);
    }

    openNewInventory(data) {
        let type = data.type;
        let currentIndex = data.currentIndex;

        this.gearChoiceInventoryWindow = new GearChoiceInventoryWindow('gearChoiceInventoryWindow', this.scene,
            1, 0);
        this.addWindow(this.gearChoiceInventoryWindow);

        let filteredList = [];
        if (type === 'weapon') {
            this.inventoryWeapons.forEach(gear => {
                if (!gear.isEquipped) {
                    if ((this.selectedChar.key === 'char-noctis') ||
                        (currentIndex === 2 && this.selectedChar.key === 'char-gladio' && (gear.subType === 'greatsword')) ||
                        (currentIndex === 4 && this.selectedChar.key === 'char-gladio' && (gear.subType === 'shield')) ||
                        (currentIndex === 2 && this.selectedChar.key === 'char-prompto' && (gear.subType === 'firearm')) ||
                        (currentIndex === 4 && this.selectedChar.key === 'char-prompto' && (gear.subType === 'machinery')) ||
                        (currentIndex === 2 && this.selectedChar.key === 'char-ignis' && (gear.subType === 'daggers')) ||
                        (currentIndex === 4 && this.selectedChar.key === 'char-ignis' && (gear.subType === 'polearm'))
                    ) {
                        filteredList.push(gear);
                    }
                } else {
                    // filteredList.push('');
                }
            });
        } else if (type === 'accessory') {
            this.inventoryAccs.forEach(acc => {
                if (!acc.isEquipped) {
                    filteredList.push(acc);
                } else {
                    // filteredList.push('');
                }
            });
        }
        let unequipObject = new SpecialMenuObject('none');
        let trashCanObject = new SpecialMenuObject('trash');
        let entries;
        if (filteredList.length === 0) {
            entries = this.createEntries([unequipObject, 'skip', trashCanObject]);
        } else {
            entries = this.createEntries([unequipObject].concat(filteredList).concat([trashCanObject]));
        }
        let rows = (type === 'accessory') ? 4 : 4;
        this.gearChoiceInventoryComp = new ListComponent('gearChoiceInventoryComp', this.scene, this,
            this.tileSizeMenu * 1, this.tileSizeMenu * 2, 1, rows, entries);
        this.gearChoiceInventoryComp.setAlwaysHideScrollArrows(false);
        this.gearChoiceInventoryWindow.addComponent(this.gearChoiceInventoryComp);


        this.gearChoiceInventoryStatsWindow = new BaseWindow('gearChoiceInventoryStatsWindow', this.scene,
            0, 11, 20, 7);
        this.addWindow(this.gearChoiceInventoryStatsWindow);

        entries = this.createEntries([
            this.stringDB.getCustomStringByKey('string-stat-attack'),
            this.stringDB.getCustomStringByKey('string-stat-fire-resistance'),
            this.stringDB.getCustomStringByKey('string-stat-defense'),
            this.stringDB.getCustomStringByKey('string-stat-ice-resistance'),
            this.stringDB.getCustomStringByKey('string-stat-magic'),
            this.stringDB.getCustomStringByKey('string-stat-lightning-resistance'),
            this.stringDB.getCustomStringByKey('string-stat-spirit'),
            this.stringDB.getCustomStringByKey('string-stat-dark-resistance'),
        ]);
        this.gearChoiceInventoryStatsNamesComp = new ListComponent(
            'gearChoiceInventoryStatsNamesComp', this.scene, this, this.tileSizeMenu * 1, this.tileSizeMenu * 1.5,
            2, 4, entries, null, null, {
                width: this.tileSizeMenu * 6,
                height: 0
            });
        this.gearChoiceInventoryStatsWindow.addComponent(this.gearChoiceInventoryStatsNamesComp);

        this.gearChoiceInventoryStatsValuesComp = new ListComponent(
            'gearChoiceInventoryStatsValuesComp', this.scene, this, this.tileSizeMenu * 7, this.tileSizeMenu * 1.5,
            2, 4, null, null, null, {
                width: this.tileSizeMenu * 5,
                height: 0
            });
        this.gearChoiceInventoryStatsWindow.addComponent(this.gearChoiceInventoryStatsValuesComp);

        this.gearChoiceInventoryWindow.setActiveComponent(this.gearChoiceInventoryComp);
        this.setActiveWindow(this.gearChoiceInventoryWindow);


        this.gearChoiceInventoryAccDetailWindow = new BaseWindow('gearChoiceInventoryAccDetailWindow', this.scene, 1, 9, 18, 3);
        this.gearChoiceInventoryAccDetailComp = new ListComponent('gearChoiceInventoryAccDetailComp', this.scene, this, this.tileSizeMenu * 1, this.tileSizeMenu * 1, 1, 1);

        this.gearChoiceInventoryAccDetailWindow.addComponent(this.gearChoiceInventoryAccDetailComp);
        this.addWindow(this.gearChoiceInventoryAccDetailWindow);


        this.gearChoiceInventoryWindow.draw();
        this.gearChoiceInventoryStatsWindow.draw();
        this.updateGearChoiceInventoryStatsWindow();
    }

    closeNewInventory() {
        this.removeWindow(this.gearChoiceInventoryWindow);
        this.removeWindow(this.gearChoiceInventoryStatsWindow);
        this.removeWindow(this.gearChoiceInventoryAccDetailWindow);

        this.setActiveWindow(this.gearMenuCharWindow);
        this.updateGearMenuCharWindow();
    }

    closeGearMenu() {
        this.getParentScene().events.emit('Action', 'CloseGearMenu');
        this.scene.game.events.emit('Menu', 'CloseGearMenu');
    }

    weaponChangeDone() {
        this.removeWindow(this.gearChoiceInventoryWindow);
        this.removeWindow(this.gearChoiceInventoryStatsWindow);

        this.setActiveWindow(this.gearMenuCharWindow);
        this.updateGearMenuCharWindow();
    }

    accessoryChangeDone() {
        this.removeWindow(this.gearChoiceInventoryWindow);
        this.removeWindow(this.gearChoiceInventoryStatsWindow);

        this.setActiveWindow(this.gearMenuCharWindow);
        this.updateGearMenuCharWindow();
    }

    confirm() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            if (this.stateMachine.getCurrentStateName() === 'StartState') {
                this.oldSelectedItem = this.getActiveComponent().getCurrentEntryObject();
                this.oldSelectedItemIndex = this.getActiveComponent().getCurrentIndex();

                if (this.oldSelectedItem instanceof GameObject && !(this.oldSelectedItem instanceof EmptyObject)) {
                    this.getActiveComponent().confirm();

                    if (this.oldSelectedItemIndex === 13 || this.oldSelectedItemIndex === 15) {
                        // choose accessories

                        this.getActiveComponent().setShowCursor1WhileNotActive(false);

                        // this.openNewInventory('accessory');
                        this.scene.events.emit('Action', 'ChooseNewAccessory', {
                            type: 'accessory',
                            currentIndex: this.oldSelectedItemIndex
                        });
                    } else {
                        // choose weapons

                        this.getActiveComponent().setShowCursor1WhileNotActive(true);

                        // this.openNewInventory('weapon');
                        this.scene.events.emit('Action', 'ChooseNewWeapon', {
                            type: 'weapon',
                            currentIndex: this.oldSelectedItemIndex
                        });
                    }
                }


            } else if (this.stateMachine.getCurrentStateName() === 'ChooseNewWeaponState') {
                this.newSelectedItem = this.getActiveComponent().getCurrentEntryObject();
                this.newSelectedItemIndex = this.getActiveComponent().getCurrentIndex();

                // replace old gear with new gear 
                if (this.newSelectedItem instanceof GameObject) {
                    this.getActiveComponent().confirm();

                    let oldKey = this.oldSelectedItem.key;
                    let oldId = this.oldSelectedItem.id;
                    let newKey = this.newSelectedItem.key;
                    let newId = this.newSelectedItem.id;
                    for (let i = 0; i < this.inventoryWeapons.length; i++) {
                        let invWeapon = this.inventoryWeapons[i];
                        if (invWeapon.key === oldKey && invWeapon.id === oldId) {
                            if (newKey === 'trash') {
                                this.inventoryWeapons.splice(i, 1);
                            } else {
                                this.inventoryWeapons[i].isEquipped = false;
                            }
                        } else if (invWeapon.key === newKey && invWeapon.id === newId) {
                            this.inventoryWeapons[i].isEquipped = true;
                        }
                    }
                    for (let i = 0; i < this.equippedWeapons.length; i++) {
                        let equWeapon = this.equippedWeapons[i];
                        if (equWeapon.key === oldKey && equWeapon.id === oldId) {
                            if (this.newSelectedItem instanceof Item &&
                                this.newSelectedItem.type === 'weapon') {
                                this.newSelectedItem.isEquipped = true;
                                this.equippedWeapons[i] = this.newSelectedItem;
                            } else if (this.newSelectedItem instanceof SpecialMenuObject &&
                                this.newSelectedItem.key === 'none') {
                                this.equippedWeapons[i] = 'none';
                            } else if (this.newSelectedItem instanceof SpecialMenuObject &&
                                this.newSelectedItem.key === 'trash') {
                                this.equippedWeapons[i] = 'none';
                            }
                        }
                    }

                    if (oldKey === 'none') {
                        let oldIndex;
                        switch (this.oldSelectedItemIndex) {
                            case 2:
                                oldIndex = 0;
                                break;
                            case 4:
                                oldIndex = 1;
                                break;
                            case 6:
                                oldIndex = 2;
                                break;
                            case 8:
                                oldIndex = 3;
                                break;

                            default:
                                break;
                        }

                        if (newKey === 'none' || newKey === 'trash') {

                        } else {
                            this.equippedWeapons[oldIndex] = this.newSelectedItem;
                        }
                    }
                }

                this.saveInventoriesAndEquipment();

                // this.weaponChangeDone();
                this.scene.events.emit('Action', 'WeaponChangeDone');
            } else if (this.stateMachine.getCurrentStateName() === 'ChooseNewAccessoryState') {
                this.newSelectedItem = this.getActiveComponent().getCurrentEntryObject();
                this.newSelectedItemIndex = this.getActiveComponent().getCurrentIndex();

                // replace old accessory with new accessory 
                if (this.newSelectedItem instanceof GameObject) {
                    this.getActiveComponent().confirm();

                    let oldKey = this.oldSelectedItem.key;
                    let oldId = this.oldSelectedItem.id;
                    let newKey = this.newSelectedItem.key;
                    let newId = this.newSelectedItem.id;
                    for (let i = 0; i < this.inventoryAccs.length; i++) {
                        let invAcc = this.inventoryAccs[i];
                        if (invAcc.key === oldKey && invAcc.id === oldId) {
                            if (newKey === 'trash') {
                                this.inventoryAccs.splice(i, 1);
                            } else {
                                this.inventoryAccs[i].isEquipped = false;
                            }
                        } else if (invAcc.key === newKey && invAcc.id === newId) {
                            this.inventoryAccs[i].isEquipped = true;
                        }
                    }
                    for (let i = 0; i < this.equippedAccs.length; i++) {
                        let equAcc = this.equippedAccs[i];
                        if (equAcc.key === oldKey && equAcc.id === oldId) {
                            if (this.newSelectedItem instanceof Item &&
                                this.newSelectedItem.type === 'accessory') {
                                this.newSelectedItem.isEquipped = true;
                                this.equippedAccs[i] = this.newSelectedItem;
                            } else if (this.newSelectedItem instanceof SpecialMenuObject &&
                                this.newSelectedItem.key === 'none') {
                                this.equippedAccs[i] = 'none';
                            } else if (this.newSelectedItem instanceof SpecialMenuObject &&
                                this.newSelectedItem.key === 'trash') {
                                this.equippedAccs[i] = 'none';
                            }
                        }
                    }

                    if (oldKey === 'none') {
                        let oldIndex;
                        switch (this.oldSelectedItemIndex) {
                            case 13:
                                oldIndex = 0;
                                break;
                            case 15:
                                oldIndex = 1;
                                break;

                            default:
                                break;
                        }

                        if (newKey === 'none' || newKey === 'trash') {

                        } else {
                            this.equippedAccs[oldIndex] = this.newSelectedItem;
                        }
                    }
                }

                this.saveInventoriesAndEquipment();

                // this.accessoryChangeDone();
                this.scene.events.emit('Action', 'AccessoryChangeDone');
            }

        }
    }

    cancel() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            if (this.stateMachine.getCurrentStateName() === 'StartState') {
                this.closeGearMenu();
            } else if (this.stateMachine.getCurrentStateName() === 'ChooseNewWeaponState') {
                // this.closeNewInventory();
                this.scene.events.emit('Action', 'CancelWeaponChoice');
            } else if (this.stateMachine.getCurrentStateName() === 'ChooseNewAccessoryState') {
                // this.closeNewInventory();
                this.scene.events.emit('Action', 'CancelAccessoryChoice');
            }
        }
    }

    pressDown() {
        super.pressDown();
        if (this.stateMachine.getCurrentStateName() === 'ChooseNewWeaponState' ||
            this.stateMachine.getCurrentStateName() === 'ChooseNewAccessoryState') {
            this.updateGearChoiceInventoryStatsWindow();
        }
    }

    pressUp() {
        super.pressUp();
        if (this.stateMachine.getCurrentStateName() === 'ChooseNewWeaponState' ||
            this.stateMachine.getCurrentStateName() === 'ChooseNewAccessoryState') {
            this.updateGearChoiceInventoryStatsWindow();
        }
    }
}