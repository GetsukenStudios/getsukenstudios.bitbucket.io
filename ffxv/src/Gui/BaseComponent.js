import {
    isDefinedAndNotNull,
    isUndefined
} from '../Base/Utils';
import {
    EmptyObject
} from '../Game/GameObjects';
import {
    ListEntry
} from './ListEntry';

export class BaseComponent extends Phaser.GameObjects.Container {

    constructor(key, scene, parentMenu, xRel, yRel, entries) {
        super(scene);
        this.scene = scene;
        this.parentMenu = parentMenu;
        this.key = key;
        this.fullKey = this.constructor.name + '_In_' + this.parentMenu.fullKey;
        this.parentWindow = null;
        this.xRel = xRel;
        this.yRel = yRel;
        let settings = scene.registry.get('gameSettings');
        this.tileSizeMenu = settings.tileSizeMenu;
        this.entryMarginWidth = this.tileSizeMenu;
        this.entryMarginHeight = this.tileSizeMenu;
        this.entries = entries;
        this.entriesPerRow = 1;
        this.currentIndex = 0;
        this.isComponentVisible = true;
        this.isActive = false;
        this.wasActiveOnce = false;
        this.isConfirmed = false;
        this.cursorImage = null;
        this.cursorImage2 = null;
    }

    initializeIndeces() {

    }

    initialize() {

    }

    destroy() {
        super.destroy();
    }

    getPosition() {
        return {
            x: this.x,
            y: this.y
        };
    }

    getRelativeOffset() {
        return {
            xRel: this.xRel,
            yRel: this.yRel
        };
    }

    setRelativeOffset(xRel, yRel) {
        this.xRel = xRel;
        this.yRel = yRel;
    }

    setComponentPosition(xRel, yRel) {
        this.setRelativeOffset(xRel, yRel);
        if (isDefinedAndNotNull(this.parentWindow)) {
            this.setX(this.parentWindow.x + xRel);
            this.setY(this.parentWindow.y + yRel);
        }
    }

    updateComponentPosition() {
        this.setComponentPosition(this.xRel, this.yRel);
    }

    setEntryMargin(widthMargin, heightMargin) {
        this.entryMarginWidth = widthMargin;
        this.entryMarginHeight = heightMargin;
    }

    setEntryMarginInTiles(widthMargin, heightMargin) {
        this.entryMarginWidth = widthMargin * this.tileSizeMenu;
        this.entryMarginHeight = heightMargin * this.tileSizeMenu;
    }

    getCurrentIndex() {
        return this.currentIndex;
    }

    getEntries() {
        return this.entries;
    }

    setEntries(entries, keepCurrentIndex) {
        this.entries = entries;
        this.initializeIndeces(keepCurrentIndex);
    }

    getEntryObjects() {
        let objects = [];
        this.entries.forEach(entry => {
            objects.push(entry.object);
        });

        return objects;
    }

    getCurrentEntry() {
        if (isDefinedAndNotNull(this.entries) &&
            isDefinedAndNotNull(this.currentIndex)) {
            return this.entries[this.currentIndex];
        }
    }

    getCurrentEntryObject() {
        return this.getCurrentEntry().object;
    }

    getCurrentEntryObjectKey() {
        return this.getCurrentEntryObject().getKey();
    }

    getCurrentEntryObjectShortName() {
        return this.getCurrentEntryObject().getShortName();
    }

    getCurrentEntryObjectFullName() {
        return this.getCurrentEntryObject().getFullName();
    }

    setParentWindow(window) {
        this.parentWindow = window;
        this.updateComponentPosition();
    }

    getActive() {
        return this.isActive;
    }

    setActive(bool) {
        this.isActive = bool;

        if (bool) {
            if (this.wasActiveOnce) {
                this.resumeCallback();
            }

            this.wasActiveOnce = true;
        } else {
            this.loseActivity();
        }
    }

    getComponentVisible() {
        return this.isComponentVisible;
    }

    setComponentVisible(bool) {
        this.isComponentVisible = bool;
        if (bool) {

        } else {

        }
    }

    getIsConfirmed() {
        return this.isConfirmed;
    }

    setIsConfirmed(bool) {
        this.isConfirmed = bool;
    }

    setImageDepth(image, depth) {
        image.setDepth(depth);
    }

    loseActivity() {

    }

    resumeCallback() {

    }

    clearEntry(entry) {
        entry.clearRenderer();
    }

    clearComponent() {
        // this.removeAll(true);
        if (isDefinedAndNotNull(this.entries)) {
            this.entries.forEach(entry => {
                this.clearEntry(entry);
            });
        }
    }

    renderComponent() {
        if (isDefinedAndNotNull(this.entries)) {
            // console.log('\n' + 'Rendering Component: ' + this.key);

            let newX = 0;
            let newY = 0;
            for (let i = 0, count = 0; i < this.entries.length; i++, count++) {
                let marginWidth = 0;
                let marginHeight = 0;
                let lastIndex = Math.max(0, i - 1);
                if (isUndefined(this.entries[i])) {
                    break;
                }
                let lastRenderer = this.entries[lastIndex].getRenderer();
                let renderer = this.entries[i].getRenderer();


                if ((count % this.entriesPerRow) > 0) {
                    marginWidth = this.entryMarginWidth;
                }
                if (count >= this.entriesPerRow) {
                    marginHeight = this.entryMarginHeight;
                }

                if ((count % this.entriesPerRow) == 0) {
                    newX = 0;
                    if (count !== 0) {
                        newY += (lastRenderer.getRenderHeight() + marginHeight);
                    }
                } else {
                    newX += (lastRenderer.getRenderWidth() + marginWidth);
                }


                let renderObjects = renderer.render(this.x + newX, this.y + newY);
                renderObjects.forEach(obj => {
                    obj.setDepth(this.scene.registry.get('gameSettings').depthList.gui);
                    this.scene.add.existing(obj);
                });
            }
        }
    }

    drawCursor() {}

    draw() {
        if (isDefinedAndNotNull(this.entries)) {
            this.clearComponent();
            if (this.getComponentVisible()) {
                this.renderComponent();
            }
        }
    }

    pressUp() {}

    pressDown() {}

    pressLeft() {}

    pressRight() {}

    select(index) {}

    deselect() {}

    confirm() {}

    cancel() {}
}