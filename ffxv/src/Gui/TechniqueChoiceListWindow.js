import { BaseWindow } from './BaseWindow';
export class TechniqueChoiceListWindow extends BaseWindow {
    constructor(key, scene, xInTiles, yInTiles) {
        // x = 0, y = 0, w = 9, h = 18
        super(key, scene, xInTiles, yInTiles, 9, 12);
    }
}
