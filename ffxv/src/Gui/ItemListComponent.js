import {
    isDefinedAndNotNull,
    isUndefined,
    swap
} from '../Base/Utils';
import {
    ListComponent
} from './ListComponent';
import {
    SpecialMenuObject,
    EmptyObject
} from '../Game/GameObjects';
import {
    ListEntry
} from './ListEntry';

export class ItemListComponent extends ListComponent {

    constructor(key, scene, parentMenu, x, y, entriesPerRow, visibleRows, entries, startAtIndex, moveDirection) {
        super(key, scene, parentMenu, x, y, entriesPerRow, visibleRows, entries, startAtIndex = 0, moveDirection = 'vertical');

        this.alwaysHideScrollArrows = false;
        this.isInSwapMode = false;
        this.swapEntryOldIndex = 0;
    }

    confirm() {
        let currObj = this.getCurrentEntryObject();

        if (!this.isInSwapMode) {
            if ((currObj instanceof SpecialMenuObject && currObj.key === 'trash')) {

            } else {
                this.isInSwapMode = true;
                this.swapEntryOldIndex = this.currentIndex;

                this.scene.events.emit('Action', 'StartSwapMode', this);
            }
        } else {
            if ((currObj instanceof SpecialMenuObject && currObj.key === 'trash')) {
                this.isInSwapMode = false;

                let newObj = new EmptyObject();
                this.clearEntry(this.entries[this.swapEntryOldIndex]);

                this.entries[this.swapEntryOldIndex] = this.parentMenu.createEntry(newObj);

                this.scene.events.emit('Action', 'StopSwapMode', this);

            } else {
                this.isInSwapMode = false;

                swap(this.entries, this.swapEntryOldIndex, this.currentIndex);

                this.scene.events.emit('Action', 'StopSwapMode', this);
            }
        }

        this.draw();

        return currObj;
    }

    cancel() {
        if (this.isInSwapMode) {
            this.isInSwapMode = false;

            this.scene.events.emit('Action', 'StopSwapMode', this);
        } else {

        }

        this.draw();
    }

    pressLeft() {

    }

    pressRight() {

    }

    clearComponent() {
        super.clearComponent();

        this.menuScrollArrowDownImage.setVisible(false);
        this.menuScrollArrowUpImage.setVisible(false);
    }

    drawCursor2() {
        /*         if (!this.cursorImage2) {
                    this.cursorImage2 = this.scene.add.image(0, 0, 'sprites-menu-cursor').setOrigin(0, 0);
                    this.cursorImage2.setDepth(this.scene.registry.get('gameSettings').depthList.gui);
                    this.cursorImage2.setVisible(false);
                } */

        if (this.isInVisibleRange(this.swapEntryOldIndex)) {
            this.cursorImage2.setVisible(true);
            // this.cursorImage2.setDepth(this.scene.registry.get('gameSettings').depthList.guiCursor);
            this.cursorImage2.setX(this.entries[this.swapEntryOldIndex].renderer.getX() + this.cursorOffset.x - 11);
            this.cursorImage2.setY(this.entries[this.swapEntryOldIndex].renderer.getY() + this.cursorOffset.y);
            console.log('*Cursor2 drawn at Index=' + this.swapEntryOldIndex + ';  X=' + this.cursorImage2.x + ' , Y=' + this.cursorImage2.y);
        }
    }

    draw() {
        this.clearComponent();
        if (this.getComponentVisible()) {
            this.renderComponent();
            if (this.getActive()) {
                if (this.isInSwapMode) {
                    this.drawCursor2();
                }
                this.drawCursor();
            } else if (this.wasActiveOnce) {
                if (this.getShowCursor2WhileNotActive() && this.isInSwapMode) {
                    this.drawCursor2();
                }
                if (this.getShowCursor1WhileNotActive()) {
                    this.drawCursor();
                }
            }
            this.drawScrollArrows();
        }
    }
}