
import { BaseMenu } from './BaseMenu';
import { remove, isDefinedAndNotNull } from '../Base/Utils';
import { EmptyObject, Item, CustomImage, SpecialMenuObject, Character } from '../Game/GameObjects';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';
import { CharListWindow } from './CharListWindow';
import { CharacterSelectionRenderer } from './CharacterSelectionRenderer';
import { ListComponent } from './ListComponent';

export class CharListMenu extends BaseMenu {

    constructor(scene, data) {
        super(scene);

        this.nextMenu = data.nextMenu;

        this.openCharListWindow();

        this.draw();
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        this.gearMenuState = new StateNode(stateMachine, 'GearMenuState');
        this.techniqueMenuState = new StateNode(stateMachine, 'TechniqueMenuState');

        new StateTransition('OpenGearMenu', this.startState, this.gearMenuState);
        new StateTransition('CloseGearMenu', this.gearMenuState, this.startState);
        new StateTransition('OpenTechniqueMenu', this.startState, this.techniqueMenuState);
        new StateTransition('CloseTechniqueMenu', this.techniqueMenuState, this.startState);

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    charSelected(selectedChar) {

        switch (this.nextMenu) {
            case 'string-menu-gear':
                this.scene.events.emit('Action', 'OpenGearMenu');

                this.scene.game.events.emit('Menu', 'OpenGearMenu', { selectedChar: selectedChar });

                break;

            case 'string-menu-technique':
                this.scene.events.emit('Action', 'OpenTechniqueMenu');

                this.scene.game.events.emit('Menu', 'OpenTechniqueMenu', { selectedChar: selectedChar });

                break;

            default:
                break;
        }
    }

    openCharListWindow() {
        this.charListWindow = new CharListWindow('charListWindow', this.scene, 0, 0);
        this.addWindow(this.charListWindow);

        let partyData = this.scene.registry.get('party');
        let stringDB = this.scene.registry.get('stringDatabase');
        let partyList = [];
        partyList = partyData.getPartyCharObjects(stringDB);

        let entries = this.createEntries(partyList);
        this.charListComp = new ListComponent('charListComp', this.scene, this, this.tileSizeMenu * 2,
            this.tileSizeMenu * 2, 2, 2, entries);
        this.charListWindow.addComponent(this.charListComp);
        this.charListWindow.setActiveComponent(this.charListComp);

        this.setActiveWindow(this.charListWindow);

        this.draw();
    }

    closeCharListMenu() {
        this.scene.events.emit('Action', 'CloseCharListMenu');
    }

    confirm() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            if (this.stateMachine.getCurrentStateName() === 'StartState') {
                let selectedChar = this.getActiveComponent().confirm();

                this.charSelected(selectedChar);
            }
        }
    }

    cancel() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {
            this.closeCharListMenu();
        }
    }

}