import {
    BaseMenu
} from './BaseMenu';
import {
    isDefinedAndNotNull
} from '../Base/Utils';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';
import {
    BaseWindow
} from './BaseWindow';
import {
    BaseComponent
} from './BaseComponent';
import {
    ListComponent
} from './ListComponent';
import {
    CustomString,
    Item
} from '../Game/GameObjects';
import {
    ItemRenderer
} from './ItemRenderer';

export class ChestLootMenu extends BaseMenu {

    constructor(scene, data) {
        super(scene);

        this.lootDrop = data;
        this.openingSucceeded = false;

        this.playJingle();
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    createRenderer(object) {
        let renderer;
        renderer = super.createRenderer(object);

        if (object instanceof Item) {
            renderer = new ItemRenderer(this.scene, object, false);
        }

        return renderer;
    }

    playJingle() {
        let volume = this.registry.get('gameSettings').mainVolume;
        this.scene.getGameState().getCurrentScene().getCurrentBackgroundMusic().setVolume(0);
        let sfx = this.scene.sound.add('sfx-get-item');
        sfx.play();

        sfx.once('complete', () => {
            this.scene.getGameState().getCurrentScene().getCurrentBackgroundMusic().setVolume(volume);
            sfx.destroy();

            this.openChestLootWindow();
        });
    }

    openChestLootWindow() {

        this.chestLootWindow = new BaseWindow('chestLootWindow', this.scene, 0, 10, 20, 8);
        this.addWindow(this.chestLootWindow);

        let itemDB = this.scene.registry.get('itemDatabase');
        let dialogDB = this.scene.registry.get('dialogDatabase');
        let text1 = dialogDB.getDialogStringByKey('dlg-0000').getFullName();
        let text2 = dialogDB.getDialogStringByKey('dlg-0001').getFullName();

        // TODO: handle if more than one newItem in lootDrop (or should it be only one?)
        let firstItem = this.lootDrop[Object.keys(this.lootDrop)[0]];
        let newItem = itemDB.getItemObjectByKey(firstItem.key, stringDB);
        newItem.key = firstItem.key;
        newItem.quantity = firstItem.quantity;

        // add newItem to inventory if there's space
        // TODO: maybe push this to the item inventory data manager
        let inventory;

        if (newItem.type === 'consumable' || newItem.type === 'spell') {
            inventory = this.scene.registry.get('inventoryItems');

            let maxId = 0;
            for (let property in inventory.data) {
                let invItem = inventory.data[property];
                let invItemDB = itemDB.getDataByKey(invItem.key);
                if (inventory.data.hasOwnProperty(property)) {
                    if (invItem.key === newItem.key) {
                        maxId = Math.max(maxId, invItem.id);
                        if ((invItem.quantity + newItem.quantity <= invItemDB.maxQuantity)) {
                            inventory.data[property].quantity += newItem.quantity;

                            this.openingSucceeded = true;
                            break;
                        }
                    } else if (invItem === 'none') {
                        inventory.data[property] = {
                            key: newItem.key,
                            id: maxId + 1,
                            quantity: newItem.quantity,
                        };

                        this.openingSucceeded = true;
                        break;
                    }
                }
            }

        } else {
            // TODO: handle cases when newItem is a weapon/acc/etc.
        }

        let tmpList = [text1, newItem, '.'];
        if (!this.openingSucceeded) {
            tmpList.push(text2);
        }
        let entries = this.createEntries(tmpList);
        entries[0].renderer.setRenderWidthInTiles(7);
        entries[1].renderer.setRenderWidthInTiles(8);
        entries[2].renderer.setRenderWidthInTiles(1);
        this.chestLootComp = new ListComponent('chestLootComp', this.scene, this, 8, 16,
            3, 3, entries);
        this.chestLootWindow.addComponent(this.chestLootComp);

        this.setActiveWindow(this.chestLootWindow);

        this.draw();
    }

    closeChestLootMenu() {
        this.scene.game.events.emit('Menu', 'CloseChestLootMenu', {
            openingSucceeded: this.openingSucceeded
        }, this.getParentSceneCallback());
    }

    confirm() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {
            this.closeChestLootMenu();
        }
    }
}