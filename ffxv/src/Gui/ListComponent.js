import {
    isDefinedAndNotNull,
    isUndefined,
    isUndefinedOrNull
} from '../Base/Utils';
import {
    BaseComponent
} from './BaseComponent';
import {
    BattleActor,
    CustomMonsterImage
} from '../Game/GameObjects';

export class ListComponent extends BaseComponent {

    constructor(key, scene, parentMenu, x, y, entriesPerRow, visibleRows, entries, startAtIndex, moveDirection,
        entryMargin, alwaysResetIndexOnFocus) {
        super(key, scene, parentMenu, x, y, entries);
        this.entriesPerRow = entriesPerRow || 1;
        this.visibleRows = visibleRows || 8;
        this.maxVisibleEntries = (this.visibleRows * this.entriesPerRow);
        this.startAtIndex = startAtIndex || 0;
        this.currentIndex = 0;
        this.rangeIndex = 0; // start index of range of visible rows 
        this.moveDirection = moveDirection || 'both'; // 'horizontal', 'vertical'
        if (isDefinedAndNotNull(entryMargin)) {
            this.entryMarginWidth = entryMargin.width;
            this.entryMarginHeight = entryMargin.height;
        }
        this.cursorOffset = {
            x: 0,
            y: 0
        };
        this.showCursor1WhileNotActive = true;
        this.showCursor2WhileNotActive = true;
        this.alwaysHideCursors = false;
        this.alwaysHideScrollArrows = true;
        this.alwaysResetIndexOnFocus = alwaysResetIndexOnFocus ? alwaysResetIndexOnFocus : false;
        this.selectAllMode = false;
        this.selectAllModeCursors = [];

        this.initStateMachine();
        this.initialize();
    }

    initializeIndeces(keepCurrentIndex) {
        if (isDefinedAndNotNull(this.entries) && isDefinedAndNotNull(this.startAtIndex)) {
            let rows = this.visibleRows;
            let rowLen = this.entriesPerRow;
            let listLen = this.entries.length;

            if (this.startAtIndex < 0) {
                this.startAtIndex = listLen - 1;
                this.rangeIndex = ((listLen) - (listLen % rowLen)) - (rows * rowLen);
                if (this.rangeIndex < 0) {
                    this.rangeIndex = 0;
                }
            } else if (this.startAtIndex >= listLen) {
                this.startAtIndex = 0;
                this.rangeIndex = this.startAtIndex;
            } else {
                this.rangeIndex = ((this.startAtIndex) - (this.startAtIndex % rowLen));
            }

            if (!keepCurrentIndex) {
                this.currentIndex = this.startAtIndex;
            }

            if (this.entries[this.currentIndex].getObject().getSkip()) {
                if (this.moveDirection === 'vertical' || this.moveDirection === 'both') {
                    this.pressDown();
                } else if (this.moveDirection === 'horizontal') {
                    this.pressRight();
                }
            }
        }
    }

    initialize() {
        this.initializeIndeces();

        this.cursorImage2 = this.scene.add.image(0, 0, 'sprites-menu-cursor').setOrigin(0, 0);
        this.cursorImage2.setDepth(this.scene.registry.get('gameSettings').depthList.guiCursor);
        this.cursorImage2.setVisible(false);

        this.cursorImage = this.scene.add.image(0, 0, 'sprites-menu-cursor').setOrigin(0, 0);
        this.cursorImage.setDepth(this.scene.registry.get('gameSettings').depthList.guiCursor);
        this.cursorImage.setVisible(false);

        this.menuScrollArrowDownImage = this.scene.add.image(0, 0, 'sprites-menu-scroll-arrow').setOrigin(0, 0);
        this.menuScrollArrowDownImage.setDepth(this.scene.registry.get('gameSettings').depthList.guiCursor);
        this.menuScrollArrowDownImage.setVisible(false);

        this.menuScrollArrowUpImage = this.scene.add.image(0, 0, 'sprites-menu-scroll-arrow').setOrigin(0, 0);
        this.menuScrollArrowUpImage.setFlipY(true);
        this.menuScrollArrowUpImage.setDepth(this.scene.registry.get('gameSettings').depthList.guiCursor);
        this.menuScrollArrowUpImage.setVisible(false);
    }

    initStateMachine() {

    }

    setAlwaysHideScrollArrows(bool) {
        this.alwaysHideScrollArrows = bool;
    }

    getShowCursor1WhileNotActive() {
        return this.showCursor1WhileNotActive;
    }

    setShowCursor1WhileNotActive(bool) {
        this.showCursor1WhileNotActive = bool;
    }

    getShowCursor2WhileNotActive() {
        return this.showCursor2WhileNotActive;
    }

    setShowCursor2WhileNotActive(bool) {
        this.showCursor2WhileNotActive = bool;
    }

    getCursorOffset() {
        return this.cursorOffset;
    }

    setCursorOffset(xOffset, yOffset) {
        this.cursorOffset = {
            x: xOffset,
            y: yOffset
        };
    }

    getSelectAllMode() {
        return this.selectAllMode;
    }

    setSelectAllMode(bool) {
        this.selectAllMode = bool;
    }

    getVisibleRows() {
        return this.visibleRows;
    }

    isInVisibleRange(index) {
        if (index >= this.rangeIndex && index < (this.rangeIndex + (this.maxVisibleEntries))) {
            return true;
        } else {
            return false;
        }
    }

    isInLastVisibleRow() {
        let rowStartIndex = this.currentIndex - (this.currentIndex % this.entriesPerRow);
        if (rowStartIndex - this.rangeIndex === this.visibleRows - 1) {
            return true;
        } else {
            return false;
        }
    }

    isStillYScrollableDown() {
        return (this.rangeIndex + this.visibleRows) < (this.entries.length);
    }

    isStillYScrollableUp() {
        return this.rangeIndex > 0;
    }

    containsOnlySkippableEntries() {
        let nonSkippableEntries = [];
        let onlySkippableEntries = true;
        this.entries.forEach((entry) => {
            if (!entry.getObject().getSkip()) {
                onlySkippableEntries = false;
            }
        });
        if (!onlySkippableEntries) {
            return nonSkippableEntries;
        } else {
            return null;
        }
    }

    updateIndexAndRange(newIndex) {
        // maybe refactor selection code for all movement directions?
    }

    pressUp(doNotDraw) {
        return this.moveUpInList(doNotDraw);
    }

    moveUpInList(doNotDraw) {
        doNotDraw = doNotDraw || false;
        if (isDefinedAndNotNull(this.entries)) {
            if (this.moveDirection === 'horizontal') return null;

            this.deselect();
            let rows = this.visibleRows;
            let rowLen = this.entriesPerRow;
            let listLen = this.entries.length;
            let oldIndex = this.currentIndex;
            let newIndex = oldIndex - rowLen;
            let oldCol = oldIndex % rowLen;
            let lastRowIndex = ((listLen) - (listLen % rowLen));
            if (newIndex < 0) {
                this.currentIndex = lastRowIndex + oldCol;
                if (this.currentIndex >= listLen) {
                    this.currentIndex = this.currentIndex - rowLen;
                }
                this.rangeIndex = lastRowIndex - (rows * rowLen);
                if (this.rangeIndex < 0) {
                    this.rangeIndex = 0;
                }
            } else if (newIndex < this.rangeIndex) {
                this.currentIndex = newIndex;
                this.rangeIndex = this.rangeIndex - rowLen;
            } else {
                this.currentIndex = newIndex;
            }

            if (this.entries[this.currentIndex].getObject().getSkip()) {
                return this.pressUp();
            } else if (!doNotDraw) {
                this.draw();
            }
            return this.entries[this.currentIndex];
        }

    }

    pressDown(doNotDraw) {
        return this.moveDownInList(doNotDraw);
    }

    moveDownInList(doNotDraw) {
        doNotDraw = doNotDraw || false;
        if (isDefinedAndNotNull(this.entries)) {
            if (this.moveDirection === 'horizontal') return null;

            this.deselect();
            let rows = this.visibleRows;
            let rowLen = this.entriesPerRow;
            let listLen = this.entries.length;
            let oldIndex = this.currentIndex;
            let newIndex = oldIndex + rowLen;
            let oldCol = oldIndex % rowLen;
            if (newIndex >= listLen) {
                this.currentIndex = 0 + oldCol;
                this.rangeIndex = 0;
            } else if (newIndex >= this.rangeIndex + (rows * rowLen)) {
                this.currentIndex = newIndex;
                this.rangeIndex = this.rangeIndex + rowLen;
            } else {
                this.currentIndex = newIndex;
            }

            if (this.entries[this.currentIndex].getObject().getSkip()) {
                if (isUndefinedOrNull(this.containsOnlySkippableEntries())) {
                    return null;
                }
                return this.pressDown();
            } else if (!doNotDraw) {
                this.draw();
            }
            return this.entries[this.currentIndex];
        }
    }

    pressLeft(doNotDraw) {
        return this.moveLeftInList(doNotDraw);
    }

    moveLeftInList(doNotDraw) {
        doNotDraw = doNotDraw || false;
        if (isDefinedAndNotNull(this.entries)) {
            if (this.moveDirection === 'vertical') return null;

            this.deselect();
            let rows = this.visibleRows;
            let rowLen = this.entriesPerRow;
            let listLen = this.entries.length;
            let oldIndex = this.currentIndex;
            let newIndex = oldIndex - 1;
            let oldCol = oldIndex % rowLen;
            let oldRowIndex = oldIndex - oldCol;
            if (newIndex < oldRowIndex) {
                newIndex = oldRowIndex + (rowLen - 1);
                if (newIndex >= listLen) {
                    this.currentIndex = listLen - 1;
                } else {
                    this.currentIndex = newIndex;
                }
            } else {
                this.currentIndex = newIndex;
            }

            if (this.entries[this.currentIndex].getObject().getSkip()) {
                if (isUndefinedOrNull(this.containsOnlySkippableEntries())) {
                    return null;
                }
                return this.pressLeft();
            } else if (!doNotDraw) {
                this.draw();
            }
            return this.entries[this.currentIndex];
        }
    }

    pressRight(doNotDraw) {
        return this.moveRightInList(doNotDraw);
    }

    moveRightInList(doNotDraw) {
        doNotDraw = doNotDraw || false;
        if (isDefinedAndNotNull(this.entries)) {
            if (this.moveDirection === 'vertical') return null;

            this.deselect();
            let rows = this.visibleRows;
            let rowLen = this.entriesPerRow;
            let listLen = this.entries.length;
            let oldIndex = this.currentIndex;
            let newIndex = oldIndex + 1;
            let oldCol = oldIndex % rowLen;
            let oldRowIndex = oldIndex - oldCol;
            if (newIndex >= listLen) {
                this.currentIndex = oldRowIndex;
            } else if (newIndex >= oldRowIndex + rowLen) {
                this.currentIndex = oldRowIndex;
            } else {
                this.currentIndex = newIndex;
            }

            if (this.entries[this.currentIndex].getObject().getSkip()) {
                if (isUndefinedOrNull(this.containsOnlySkippableEntries())) {
                    return null;
                }
                return this.pressRight();
            } else if (!doNotDraw) {
                this.draw();
            }
            return this.entries[this.currentIndex];
        }
    }

    select(newIndex) {
        if (isDefinedAndNotNull(newIndex) && newIndex !== this.currentIndex) {
            let rows = this.visibleRows;
            let rowLen = this.entriesPerRow;
            let listLen = this.entries.length;
            let oldIndex = this.currentIndex;
            let oldCol = oldIndex % rowLen;
            if (!newIndex) {
                newIndex = 0;
                this.currentIndex = newIndex;
                this.rangeIndex = newIndex;
            } else if (newIndex < 0) {
                newIndex = listLen - 1;
                this.currentIndex = newIndex;
                this.rangeIndex = ((listLen) - (listLen % rowLen)) - (rows * rowLen);
                if (this.rangeIndex < 0) {
                    this.rangeIndex = 0;
                }
            } else if (newIndex >= listLen) {
                newIndex = 0;
                this.currentIndex = newIndex;
                this.rangeIndex = newIndex;
            } else if (newIndex < this.rangeIndex) {
                this.currentIndex = newIndex;
                this.rangeIndex = newIndex;
            } else if (newIndex >= this.rangeIndex + (rows * rowLen)) {
                this.currentIndex = newIndex;
                this.rangeIndex = (newIndex - (newIndex % rowLen)) - (rows * rowLen);
                if (this.rangeIndex < 0) {
                    this.rangeIndex = 0;
                }
            }
        }

        if (this.entries[this.currentIndex].getObject().getSkip()) {
            if (this.moveDirection === 'vertical' || this.moveDirection === 'both') {
                this.pressDown();
            } else if (this.moveDirection === 'horizontal') {
                this.pressRight();
            }
        }

        this.draw();
    }

    deselect() {

    }

    confirm() {
        this.setIsConfirmed(true);

        if (this.selectAllMode) {
            this.setSelectAllMode(false);
            this.draw();
            return this.getEntryObjects();
        } else {
            this.draw();
            return this.getCurrentEntryObject();
        }
    }

    cancel() {
        this.pushFrontCursorsDepth();

        this.setIsConfirmed(false);
        this.setSelectAllMode(false);
        this.draw();
    }

    loseActivity() {
        // this.pushBackCursorsDepth();

        let hideCursor1 = !this.getShowCursor1WhileNotActive();
        let hideCursor2 = !this.getShowCursor2WhileNotActive();
        this.hideCursors(hideCursor1, hideCursor2);
        this.pushBackScrollArrowsDepth();
        this.clearSelectAllCursors();
    }

    resumeCallback() {
        this.pushFrontCursorsDepth();
        this.pushFrontScrollArrowsDepth();

        this.setIsConfirmed(false);
        if (this.alwaysResetIndexOnFocus) this.select(0);
        this.draw();
    }

    pushBackCursorsDepth() {
        if (isDefinedAndNotNull(this.cursorImage)) {
            this.setImageDepth(this.cursorImage, this.scene.registry.get('gameSettings').depthList.gui);
        }
        if (isDefinedAndNotNull(this.cursorImage2)) {
            this.setImageDepth(this.cursorImage2, this.scene.registry.get('gameSettings').depthList.gui);
        }
    }

    pushFrontCursorsDepth() {
        if (isDefinedAndNotNull(this.cursorImage)) {
            this.setImageDepth(this.cursorImage, this.scene.registry.get('gameSettings').depthList.guiCursor);
        }
        if (isDefinedAndNotNull(this.cursorImage2)) {
            this.setImageDepth(this.cursorImage2, this.scene.registry.get('gameSettings').depthList.guiCursor);
        }
    }

    pushBackScrollArrowsDepth() {
        if (isDefinedAndNotNull(this.menuScrollArrowUpImage)) {
            this.menuScrollArrowUpImage.setDepth(this.scene.registry.get('gameSettings').depthList.gui);
        }
        if (isDefinedAndNotNull(this.menuScrollArrowDownImage)) {
            this.menuScrollArrowDownImage.setDepth(this.scene.registry.get('gameSettings').depthList.gui);
        }
    }

    pushFrontScrollArrowsDepth() {
        if (isDefinedAndNotNull(this.menuScrollArrowUpImage)) {
            this.setImageDepth(this.menuScrollArrowUpImage, this.scene.registry.get('gameSettings').depthList.guiCursor);
        }
        if (isDefinedAndNotNull(this.menuScrollArrowDownImage)) {
            this.setImageDepth(this.menuScrollArrowDownImage, this.scene.registry.get('gameSettings').depthList.guiCursor);
        }
    }

    hideScrollArrows() {
        if (isDefinedAndNotNull(this.menuScrollArrowUpImage)) {
            this.menuScrollArrowUpImage.setVisible(false);
        }
        if (isDefinedAndNotNull(this.menuScrollArrowDownImage)) {
            this.menuScrollArrowDownImage.setVisible(false);
        }

    }

    hideCursors(hideCursor1, hideCursor2) {
        if (isDefinedAndNotNull(this.cursorImage) && hideCursor1) {
            this.cursorImage.setVisible(false);
        }
        if (isDefinedAndNotNull(this.cursorImage2) && hideCursor2) {
            this.cursorImage2.setVisible(false);
        }
    }

    clearSelectAllCursors() {
        this.selectAllModeCursors.forEach((cursor) => {
            cursor.destroy();
        });
        this.selectAllModeCursors = [];
    }

    clearComponent() {
        super.clearComponent();

        this.hideScrollArrows();
        this.hideCursors(true, true);
        this.clearSelectAllCursors();
    }

    renderComponent() {
        if (isDefinedAndNotNull(this.entries)) {
            // console.log('\n' + 'Rendering Component: ' + this.key);

            let newX = 0;
            let newY = 0;
            for (let i = this.rangeIndex, count = 0; i < this.rangeIndex + (this.visibleRows * this.entriesPerRow); i++, count++) {
                if (isUndefined(this.entries[i])) {
                    break;
                }
                let lastIndex = Math.max(0, i - 1);
                let lastRenderer = this.entries[lastIndex].getRenderer();
                let renderer = this.entries[i].getRenderer();
                let marginWidth = 0;
                let marginHeight = 0;

                if ((count % this.entriesPerRow) > 0) {
                    marginWidth = this.entryMarginWidth;
                }
                if (count >= this.entriesPerRow) {
                    marginHeight = this.entryMarginHeight;
                }

                if ((count % this.entriesPerRow) == 0) {
                    newX = 0;
                    if (count !== 0) {
                        newY += (lastRenderer.getRenderHeight() + marginHeight);
                    }
                } else {
                    newX += (lastRenderer.getRenderWidth() + marginWidth);
                }


                let renderObjects = renderer.render(this.x + newX, this.y + newY);
                renderObjects.forEach(obj => {
                    obj.setDepth(this.scene.registry.get('gameSettings').depthList.gui);
                    this.scene.add.existing(obj);
                });
                // console.log('Rendered entry #' + [i] + ' at X=' + newX + ', Y=' + newY + '  [count=' + count + ']');
                // console.log('MarginWidth= ' + marginWidth + ', MarginHeight=' + marginHeight);
            }
        }
    }

    drawCursor() {
        if (isDefinedAndNotNull(this.entries) && !this.alwaysHideCursors) {
            if (this.selectAllMode) {
                for (let i = this.rangeIndex; i < this.rangeIndex + (this.visibleRows * this.entriesPerRow); i++) {
                    let currEntryObject = this.entries[i].getObject();
                    if ((currEntryObject instanceof CustomMonsterImage && !currEntryObject.getIsVisible())) continue;
                    let x = this.entries[i].renderer.getX() + this.cursorOffset.x - 16;
                    let y = this.entries[i].renderer.getY() + this.cursorOffset.y;
                    let newCursorImage = this.scene.add.image(x, y, 'sprites-menu-cursor').setOrigin(0, 0);
                    newCursorImage.setDepth(this.scene.registry.get('gameSettings').depthList.guiCursor);
                    this.selectAllModeCursors.push(newCursorImage);
                }

            } else {
                this.cursorImage.setVisible(true);

                if (!this.isConfirmed) {
                    this.cursorImage.setX(this.entries[this.currentIndex].renderer.getX() + this.cursorOffset.x - 16);
                    this.cursorImage.setY(this.entries[this.currentIndex].renderer.getY() + this.cursorOffset.y);
                } else {
                    this.cursorImage.setX(this.entries[this.currentIndex].renderer.getX() + this.cursorOffset.x - 12);
                    this.cursorImage.setY(this.entries[this.currentIndex].renderer.getY() + this.cursorOffset.y);
                }
                // console.log('*Cursor1 drawn at Index=' + this.currentIndex + ';  X=' + this.cursorImage.x + ' , Y=' + this.cursorImage.y);
            }
        }
    }

    drawScrollArrows() {
        if (!this.alwaysHideScrollArrows) {
            let scrollableYUp = this.isStillYScrollableUp();
            let scrollableYDown = this.isStillYScrollableDown();
            if (scrollableYUp || scrollableYDown) {
                this.menuScrollArrowUpImage.setVisible(true);
                this.menuScrollArrowUpImage.setX(this.parentWindow.x + ((this.parentWindow.widthInTiles - 1) * this.tileSizeMenu));
                this.menuScrollArrowUpImage.setY(this.parentWindow.y + (((Math.floor(this.parentWindow.heightInTiles * 0.5)) * this.tileSizeMenu)) - this.tileSizeMenu);

                this.menuScrollArrowDownImage.setVisible(true);
                this.menuScrollArrowDownImage.setX(this.parentWindow.x + ((this.parentWindow.widthInTiles - 1) * this.tileSizeMenu));
                this.menuScrollArrowDownImage.setY(this.parentWindow.y + (((Math.floor(this.parentWindow.heightInTiles * 0.5)) * this.tileSizeMenu)));
            }

            if (scrollableYUp) {
                this.menuScrollArrowUpImage.setFlipY(true);
                if (!scrollableYDown) this.menuScrollArrowDownImage.setFlipY(true);
            }
            if (scrollableYDown) {
                this.menuScrollArrowDownImage.setFlipY(false);
                if (!scrollableYUp) this.menuScrollArrowUpImage.setFlipY(false);
            }
        }
    }

    draw() {
        this.clearComponent();
        if (this.getComponentVisible()) {
            this.renderComponent();
            if (this.getActive() || (this.wasActiveOnce && this.getShowCursor1WhileNotActive())) {
                this.drawCursor();
            }
            this.drawScrollArrows();
        }
    }
}