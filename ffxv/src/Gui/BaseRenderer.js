import {
    isDefinedAndNotNull
} from '../Base/Utils';

export class BaseRenderer {
    constructor(scene, gameObject) {
        let settings = scene.registry.get('gameSettings');
        this.tileSizeMenu = settings.tileSizeMenu;
        this.scene = scene;
        this.startX = null;
        this.startY = null;
        this.object = gameObject;
        this.renderObjects = null;
        this.renderWidth = null;
        this.renderHeight = null;
    }

    getX() {
        return this.startX;
    }

    setStartX(startX) {
        this.startX = startX;
    }

    getY() {
        return this.startY;
    }

    setStartY(startY) {
        this.startY = startY;
    }

    getRenderWidth() {
        return this.renderWidth;
    }

    setRenderWidth(width) {
        this.renderWidth = width;
    }

    setRenderWidthInTiles(widthInTiles) {
        this.renderWidth = this.tileSizeMenu * widthInTiles;
    }

    getRenderHeight() {
        return this.renderHeight;
    }

    setRenderHeight(height) {
        this.renderHeight = height;
    }

    setRenderHeightInTiles(heightInTiles) {
        this.renderHeight = this.tileSizeMenu * heightInTiles;
    }

    render(x, y) {
        this.startX = x;
        this.startY = y;
    }

    clearRenderObjects() {
        if (isDefinedAndNotNull(this.renderObjects)) {
            this.renderObjects.forEach(obj => {
                obj.destroy();
            });
        }
    }
}