import {
    GameObject,
    Item,
    CustomString,
    Counter,
    CustomImage,
    SpecialMenuObject,
    EmptyObject,
    Character,
    Technique,
    DialogString,
    Monster,
    CustomMonsterImage
} from '../Game/GameObjects';
import {
    isDefinedAndNotNull,
    contains,
    remove,
    isUndefinedOrNull
} from '../Base/Utils';
import {
    ListEntry
} from './ListEntry';
import {
    SimpleStringRenderer
} from './SimpleStringRenderer';
import {
    ItemRenderer
} from './ItemRenderer';
import {
    CounterRenderer
} from './CounterRenderer';
import {
    SimpleImageRenderer
} from './SimpleImageRenderer';
import {
    CharacterSelectionRenderer
} from './CharacterSelectionRenderer';
import {
    SpecialMenuObjectRenderer
} from './SpecialMenuObjectRenderer';
import {
    TechniqueRenderer
} from './TechniqueRenderer';
import {
    MonsterRenderer
} from './MonsterRenderer';

export class BaseMenu {

    constructor(scene, data) {
        this.scene = scene;
        this.data = data;
        this.fullKey = this.constructor.name + '_In_' + this.scene.key;
        let settings = scene.registry.get('gameSettings');
        this.tileSizeMenu = settings.tileSizeMenu;
        this.stringDB = scene.registry.get('stringDatabase');

        this.windows = [];
        this.activeWindow = null;
        this.oldActiveWindow = null;

        this.stateMachine = null;

        this.isMenuVisible = true;
        this.isActive = false;

        this.initStateMachine();
    }

    initialize() {

    }

    initStateMachine() {

    }

    getCallbackForNextScene() {
        return this.scene.getCallbackForNextScene();
    }

    getParentSceneCallback() {
        return this.scene.getParentSceneCallback();
    }

    getParentScene() {
        return this.scene.getParentScene();
    }

    getParentSceneKey() {
        if (!isDefinedAndNotNull(this.getParentScene())) {
            return null;
        }
        return this.getParentScene().getKey();
    }

    getParentSceneEvents() {
        let scene = this.getParentScene();
        return scene.events;
    }

    createEntry(object) {
        let renderer = this.createRenderer(object);
        return new ListEntry(object, renderer);
    }

    createEntries(objects) {
        let entry;
        let entries = [];
        objects.forEach((obj, index) => {
            if (typeof obj === 'string') {
                if (obj === '') {
                    obj = new EmptyObject();
                } else if (obj === 'empty') {
                    obj = new EmptyObject();
                } else if (obj === 'skip') {
                    obj = new EmptyObject(true);
                } else if (obj === 'none') {
                    obj = new SpecialMenuObject('none');
                } else if (obj === 'trash') {
                    obj = new SpecialMenuObject('trash');
                } else {
                    obj = new CustomString(obj, obj, obj);
                }
            }
            if (typeof obj === 'number') {
                let string = obj.toString();
                obj = new CustomString(string, string, string);
            }
            entry = this.createEntry(obj);
            entries[index] = entry;
        });
        return entries;
    }

    createRenderer(object) {
        if (object instanceof CustomString) {
            return new SimpleStringRenderer(this.scene, object);
        } else if (object instanceof DialogString) {
            return new SimpleStringRenderer(this.scene, object);
        } else if (object instanceof CustomImage) {
            return new SimpleImageRenderer(this.scene, object);
        } else if (object instanceof CustomMonsterImage) {
            return new SimpleImageRenderer(this.scene, object);
        } else if (object instanceof EmptyObject) {
            return new SimpleStringRenderer(this.scene, object);
        } else if (object instanceof SpecialMenuObject) {
            return new SpecialMenuObjectRenderer(this.scene, object);
        } else if (object instanceof Character) {
            return new CharacterSelectionRenderer(this.scene, object);
        } else if (object instanceof Monster) {
            return new MonsterRenderer(this.scene, object);
        } else if (object instanceof Item) {
            return new ItemRenderer(this.scene, object);
        } else if (object instanceof Technique) {
            return new TechniqueRenderer(this.scene, object, 'baseMenu');
        } else if (object instanceof Counter) {
            return new CounterRenderer(this.scene, object);
        } else if (object instanceof GameObject) {
            return new SimpleStringRenderer(this.scene, object);
        } else {
            return new SimpleStringRenderer(this.scene, '???');
        }
    }

    draw() {
        if (isDefinedAndNotNull(this.windows)) {
            this.windows.forEach(win => {
                if (!win.getActive()
                    //&& win.getWindowVisible()
                ) {
                    win.draw();
                }
            });

            if (isDefinedAndNotNull(this.activeWindow)
                //&& this.activeWindow.getWindowVisible()
            ) {
                this.activeWindow.draw();
            }
        }
    }

    getOffsetFromMenuTilesToPixel(offsetXInMenuTiles, offsetYInMenuTiles) {
        let tileSize = this.tileSizeMenu;
        let newX = (offsetXInMenuTiles * tileSize);
        let newY = (offsetYInMenuTiles * tileSize);
        let newOffset = {
            x: newX,
            y: newY
        };

        return newOffset;
    }

    getActive() {
        return this.isActive;
    }

    setActive(bool) {
        this.isActive = bool;
        if (bool) {} else {}
    }

    getMenuVisible() {
        return this.isMenuVisible;
    }

    setMenuVisible(bool) {
        this.isMenuVisible = bool;
        this.windows.forEach(window => {
            window.setWindowVisible(bool);
        });

        if (bool) {

        } else {

        }
    }

    addWindow(win) {
        this.windows.push(win);
        // if (isUndefinedOrNull(this.activeWindow)) {
        //     this.setActiveWindow(win);
        // }
    }

    removeWindow(win) {
        if (isDefinedAndNotNull(win)) {
            win.reset();
            if (win === this.activeWindow) {
                this.activeWindow.setActiveComponent(null);
                this.activeWindow = null;
            }
            remove(this.windows, win);
            // TODO: only works if Windows objects' key is named after the variable referencing the Window object in the containing Menu
            this[win.key] = null;
            // thus this potential fallback method of returning null for the window to be assigned to in the menus
            return null;
        }
    }

    removeAllWindows() {
        this.windows.forEach(win => {
            this.removeWindow(win);
        });
    }

    destroyMenu() {
        this.removeAllWindows();
        if (isDefinedAndNotNull(this.stateMachine)) {
            this.stateMachine.stop();
        }
    }

    setActiveWindow(win) {
        if (contains(this.windows, win)) {
            if (isDefinedAndNotNull(this.activeWindow)) {
                this.activeWindow.setActive(false);
                this.oldActiveWindow = this.activeWindow;
                win.setActive(true);
                this.activeWindow = win;
            } else {
                win.setActive(true);
                this.oldActiveWindow = win;
                this.activeWindow = win;
            }
        }
    }

    getActiveWindow() {
        return this.activeWindow;
    }

    getOldActiveWindow() {
        return this.oldActiveWindow;
    }

    setActiveComponent(component) {
        this.activeWindow.setActiveComponent(component);
    }

    getActiveComponent() {
        if (isUndefinedOrNull(this.activeWindow)) return;
        return this.activeWindow.getActiveComponent();
    }

    getOldActiveComponent() {
        if (isUndefinedOrNull(this.activeWindow)) return;
        return this.activeWindow.oldActiveComponent;
    }

    getCurrentEntry() {
        if (isUndefinedOrNull(this.activeWindow) || isUndefinedOrNull(this.activeComponent)) return;
        return this.activeWindow.activeComponent.getCurrentEntry();
    }

    getCurrentEntryObject() {
        if (isUndefinedOrNull(this.activeWindow) || isUndefinedOrNull(this.activeComponent)) return;
        return this.activeWindow.activeComponent.getCurrentEntryObject();
    }

    switchToOldActiveWindowAndComponent() {
        this.setActiveWindow(this.getOldActiveWindow());
        this.setActiveComponent(this.getActiveComponent());

        this.draw();
    }

    closeActiveWindow() {

    }

    resumeCallback() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            this.getActiveComponent().resumeCallback();
        }
    }

    pressDown() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            this.getActiveComponent().pressDown();
        }
    }

    pressUp() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            this.getActiveComponent().pressUp();
        }
    }

    pressLeft() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            this.getActiveComponent().pressLeft();
        }
    }

    pressRight() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            this.getActiveComponent().pressRight();
        }
    }

    confirm() {

    }

    cancel() {

    }

    confirmReleased() {

    }

    cancelReleased() {

    }

    update(time, delta) {

    }
}