import { DefaultBitmapText } from './DefaultBitmapText';
import { BaseRenderer } from './BaseRenderer';
import { Counter } from '../Game/GameObjects';
import { prependWhitespaces, isDefinedAndNotNull } from '../Base/Utils';


export class CounterRenderer extends BaseRenderer {
    
    constructor(scene, object) {
        super(scene, object);
        this.renderWidth = this.tileSizeMenu * 4;
        this.renderHeight = this.tileSizeMenu * 1;
    }
    
    render(x, y) {
        super.render(x, y);
        
        let object = this.object;
        let value;
        let metric;
        let maxDigits;
        let prepend = false;

        if (object instanceof Counter) {
            value = object.value;
            metric = object.metric;
            maxDigits = object.maxDigits;
            prepend = object.prependMetric;
        }


        let text = '' + value;
        let length = text.length;

        if(length < maxDigits){
            text = prependWhitespaces(text, maxDigits);
        }
        
        if(isDefinedAndNotNull(metric)) {
            if(!prepend) {
                text += metric;
            } else {
                text = metric + text;
            }
        }

        let bitmapText = new DefaultBitmapText(this.scene, x, y, text);

        // change render width/height depending on maxDigits
        let metricLen = metric.length;
        let newRenderWidth = (maxDigits + metricLen);
        let newRenderHeight = 1;    //render height stays the same
        this.renderWidth = this.setRenderWidthInTiles(newRenderWidth);
        this.renderHeight = this.setRenderHeightInTiles(newRenderHeight);

        let result = [bitmapText];
        this.renderObjects = result;
        return result;
    }
}