import {
    BaseWindow
} from './BaseWindow';
export class CharListWindow extends BaseWindow {
    constructor(key, scene, xInTiles, yInTiles, widthInTiles = 20, heightInTiles = 13) {
        // x = 0, y = 0, w = 20, h = 13
        super(key, scene, xInTiles, yInTiles, widthInTiles, heightInTiles);
    }
}