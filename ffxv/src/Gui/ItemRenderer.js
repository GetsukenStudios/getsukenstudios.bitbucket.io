import { DefaultBitmapText } from './DefaultBitmapText';
import { BaseRenderer } from './BaseRenderer';
import { CustomString, Item } from '../Game/GameObjects';
import { prependWhitespaces, isDefinedAndNotNull } from '../Base/Utils';


export class ItemRenderer extends BaseRenderer {

    constructor(scene, object, hideQuantity) {
        super(scene, object);
        this.renderWidth = this.tileSizeMenu * 9;
        this.renderHeight = this.tileSizeMenu * 1;
        this.hideQuantity = hideQuantity || false;
    }

    render(x, y) {
        super.render(x, y);

        let settings = this.scene.registry.get('gameSettings');

        let shortName;
        let type;
        let subType;
        let quantity;

        if (this.object instanceof Item) {
            shortName = this.object.getShortName();
            type = this.object.type;
            subType = this.object.subType;
            if(this.object.quantity > 0) {
                quantity = '' + this.object.quantity;
                quantity = prependWhitespaces(quantity, 2);
            } else {
                quantity = ' -';
            }
        }

        // // not using image for icons, but encoded UTF-8 character in bitmap text
        // let spriteFrame = 0;
        // if (type === 'consumable') {
        //     spriteFrame = 0;
        // } else if (type === 'weapon') {
        //     switch (subType) {
        //         case 'sword':
        //             spriteFrame = 8;
        //             break;
        //         case 'greatsword':
        //             spriteFrame = 9;
        //             break;
        //         case 'polearm':
        //             spriteFrame = 10;
        //             break;
        //         case 'daggers':
        //             spriteFrame = 11;
        //             break;
        //         case 'firearm':
        //             spriteFrame = 12;
        //             break;
        //         case 'shield':
        //             spriteFrame = 13;
        //             break;
        //         case 'machinery':
        //             spriteFrame = 14;
        //             break;
        //         case 'royalArms':
        //             spriteFrame = 15;
        //             break;
        //         default:
        //             break;
        //     }
        // } else if (type === 'accessory') {
        //     spriteFrame = 2;
        // } else if (type === 'spell') {
        //     spriteFrame = 1;
        // } else if (type === 'treasure') {
        //     spriteFrame = 3;
        // }

        // let itemIcon = this.scene.add.image(x, y, 'sprites-item-icons', spriteFrame).setOrigin(0, 0);
        
        let iconChar = this.object.getTypeToChar(settings);
        let itemIconText = new DefaultBitmapText(this.scene, x, y, iconChar);
        let nameText = new DefaultBitmapText(this.scene, x + this.tileSizeMenu, y, shortName);
        let quantityText = new DefaultBitmapText(this.scene, x + 7 * this.tileSizeMenu, y, quantity);
        let result = [itemIconText, nameText];
        if(!this.hideQuantity) {
            result.push(quantityText);
        }

        this.renderObjects = result;
        return result;
    }
}
