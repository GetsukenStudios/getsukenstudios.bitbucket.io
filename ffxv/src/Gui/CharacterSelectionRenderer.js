import {
    BaseRenderer
} from './BaseRenderer';
import {
    CustomImage,
    Character
} from '../Game/GameObjects';
import {
    DefaultBitmapText
} from './DefaultBitmapText';
import {
    prependWhitespaces
} from '../Base/Utils';

export class CharacterSelectionRenderer extends BaseRenderer {

    constructor(scene, object, mode) {
        super(scene, object);
        this.mode = mode || 'default';
        this.renderWidth = this.tileSizeMenu * 8;
        this.renderHeight = this.tileSizeMenu * 5;
    }

    render(x, y) {
        super.render(x, y);

        let objKey;
        let charName;
        let charTp;
        let charHp;
        let charMaxHp;
        let charMp;
        let charMaxMp;
        let imageKey;
        let imageFrame;
        if (this.object instanceof Character) {
            objKey = this.object.getKey();
            charName = this.object.getFullName();
            charTp = Math.floor(this.object.tp);
            charHp = this.object.hp;
            charMaxHp = this.object.maxHp.total;
            // charMp = this.object.mp;
            // charMaxMp = this.object.maxMp.total;
        }

        imageKey = 'sprites-main-chars';

        switch (objKey) {
            case 'char-noctis':
                imageFrame = 0;

                break;

            case 'char-gladio':
                imageFrame = 6;

                break;

            case 'char-ignis':
                imageFrame = 12;

                break;

            case 'char-prompto':
                imageFrame = 18;

                break;

            default:
                break;
        }

        let result;
        if (this.mode === 'default') {
            let ava = this.scene.add.image(x, y - 8, imageKey, imageFrame).setOrigin(0, 0);
            let hpText = '' + prependWhitespaces(String(charHp), 3) + '\/' + prependWhitespaces(String(charMaxHp), 3);
            let hpCounter = new DefaultBitmapText(this.scene, x + 8, y + 8, hpText);
            let name = new DefaultBitmapText(this.scene, x, y + 24, charName);

            result = [ava, hpCounter, name];
        } else if (this.mode === 'itemCharList') {
            let hpText = '' + prependWhitespaces(String(charHp), 3) + '\/' + prependWhitespaces(String(charMaxHp), 3);
            let hpCounter = new DefaultBitmapText(this.scene, x, y + 8, hpText);
            let name = new DefaultBitmapText(this.scene, x, y, charName);

            this.setRenderWidthInTiles(7);
            this.setRenderHeightInTiles(3);

            result = [name, hpCounter];
        } else if (this.mode === 'battleMenu-PartyTurnState' || this.mode === 'battleMenu-SelectPlayerState') {
            let stringDB = this.scene.registry.get('stringDatabase');
            let ava = this.scene.add.image(x, y, imageKey, imageFrame).setOrigin(0, 0);
            let hpText = `${prependWhitespaces(String(charHp), 3)}\n\/${prependWhitespaces(String(charMaxHp), 3)}`;
            let hpCounter = new DefaultBitmapText(this.scene, x + this.tileSizeMenu * 2, y, hpText);
            let tpLabel = stringDB.getCustomStringByKey('string-tech-point').getShortName().toUpperCase();
            let tpText = '';
            if (objKey === 'char-noctis') {
                tpText = `${tpLabel}\n${prependWhitespaces(String(charTp), 2)}`;
            } else {
                tpText = `\n${prependWhitespaces(String(charTp), 2)}`;
            }
            let tpCounter = new DefaultBitmapText(this.scene, x + this.tileSizeMenu * 7, y, tpText);

            this.setRenderWidthInTiles(8);
            this.setRenderHeightInTiles(2);

            result = [ava, hpCounter, tpCounter];
        } else if (this.mode === 'battleMenu-PlayerTurnState') {
            let stringDB = this.scene.registry.get('stringDatabase');
            let name = new DefaultBitmapText(this.scene, x, y, charName);
            let ava = this.scene.add.image(x, y + this.tileSizeMenu * 1.5, imageKey, imageFrame).setOrigin(0, 0);
            let hpText = `${prependWhitespaces(String(charHp), 3)}\n\/${prependWhitespaces(String(charMaxHp), 3)}`;
            let hpCounter = new DefaultBitmapText(this.scene, x + this.tileSizeMenu * 3, y + this.tileSizeMenu * 1.5, hpText);
            let tpLabel = stringDB.getCustomStringByKey('string-tech-point').getShortName().toUpperCase();
            let tpText = `${tpLabel}${prependWhitespaces(String(charTp), 2)}`;
            let tpCounter = new DefaultBitmapText(this.scene, x, y + this.tileSizeMenu * 4, tpText);

            this.setRenderWidthInTiles(7);
            this.setRenderHeightInTiles(5);

            result = [name, ava, hpCounter, tpCounter];
        }

        this.renderObjects = result;
        return result;
    }
}