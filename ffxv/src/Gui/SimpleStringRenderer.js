import { DefaultBitmapText } from './DefaultBitmapText';
import { BaseRenderer } from './BaseRenderer';
import { CustomString } from '../Game/GameObjects';

export class SimpleStringRenderer extends BaseRenderer {
    
    constructor(scene, object, useFullName) {
        super(scene, object);
        this.renderWidth = this.tileSizeMenu * 4;
        this.renderHeight = this.tileSizeMenu * 1;
        this.useFullName = useFullName || object.useFullName || false;
    }

    render(x, y) {
        super.render(x, y);

        let text;
        if (this.object instanceof CustomString) {
            // text = this.object.getShortName();
        }
        if (this.useFullName === false) {
            text = this.object.shortName;
        }
        else {
            text = this.object.fullName;
        }
        let bitmapText = new DefaultBitmapText(this.scene, x, y, text);
        // let result = this.scene.add.existing(bitmapText);
        let result = [bitmapText];
        this.renderObjects = result;
        return result;
    }
}
