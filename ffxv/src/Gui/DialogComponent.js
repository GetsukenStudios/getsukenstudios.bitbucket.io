import {
    isDefinedAndNotNull,
    isUndefinedOrNull,
    sleep,
    clearArray
} from '../Base/Utils';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';
import {
    ListComponent
} from './ListComponent';
import {
    DialogString,
    EmptyObject
} from '../Game/GameObjects';

export class DialogComponent extends ListComponent {

    constructor(key, scene, parentMenu, visibleRows, data, callback) {

        super(key, scene, parentMenu, 0, parentMenu.tileSizeMenu * 1, 1, visibleRows,
            null, 0, 'vertical', {
                width: 0,
                height: 0
            });

        // this.initStateMachine();
        // this.initialize();

        this.alwaysHideCursors = true;

        if (isDefinedAndNotNull(data)) {
            this.setConfig(data.config);
            this.parseDialogData(data.lines);
            this.startDrawingText(callback);
        }

        this.draw();

        // console.dir(this.parsedLinesArray);
    }

    initialize() {
        super.initialize();

        this.stateMachine.reset();

        this.config = null;

        let settings = this.scene.registry.get('gameSettings');
        this.maxLineWidth = settings.maxLineWidthInChars; // 16 chars per line max
        this.indentTab = settings.indentString;
        this.linesData = [];
        this.initStrings = ['empty'];
        this.callbackAfterTextDrawDone = null;
        this.callbacksForParsedLines = [];
        this.parsedLinesArray = [];
        this.currIdxParsedLinesArray = 0;
        this.currIdxCompStrings = this.initStrings.length;
        this.currCharCompString = 0;
        this.compEntries = this.parentMenu.createEntries(this.initStrings);
        this.clearComponent();
        this.setEntries(this.compEntries);

        this.textDrawDone = true;
        this.waitingForInputToScroll = false;
        this.pressToFinish = false;

        // config
        this.animateText = true;
        this.elapsedTime = 0;
        this.maxDefaultTextSpeed = 8;
        this.textSpeed = 5;
        this.textSpeedScale = 3.0;
        this.defaultTextSpeedScale = 3.0;
        this.turboTextSpeedScale = 15.0;
        this.timePerChar = 0;
        this.setTextSpeed(settings.textSpeed);
        this.setTextSpeedScale(this.defaultTextSpeedScale);
    }

    destroy() {
        super.destroy();

        this.callbackAfterTextDrawDone = null;
        clearArray(this.callbacksForParsedLines);
    }

    setConfig(config) {
        if (isUndefinedOrNull(config)) return;
        this.config = config;

        if (isDefinedAndNotNull(config.animateText)) {
            this.animateText = config.animateText;
        }
        if (isDefinedAndNotNull(config.textSpeed)) {
            this.setTextSpeed(config.textSpeed);
        }
    }

    reset(newConfig) {
        this.initialize();
        if (isDefinedAndNotNull(newConfig)) {
            this.setConfig(newConfig);
        }
    }

    initStateMachine() {
        super.initStateMachine();

        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        this.dialogEndState = new StateNode(stateMachine, 'DialogEndState');

        new StateTransition('TextDrawDone', this.startState, this.dialogEndState);
        new StateTransition('StartTextDraw', this.dialogEndState, this.startState);

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    setTextSpeed(textSpeed) {
        this.textSpeed = Math.min(this.maxDefaultTextSpeed, textSpeed);

        let baseDefault = 3.0;
        let baseTurbo = 15.0;
        if (this.textSpeed > 5) {
            this.defaultTextSpeedScale = baseDefault + 2.0;
            this.turboTextSpeedScale = baseTurbo + 4.0;
        } else if (this.textSpeed > 3) {
            this.defaultTextSpeedScale = baseDefault;
            this.turboTextSpeedScale = baseTurbo;
        } else if (this.textSpeed > 0) {
            this.defaultTextSpeedScale = baseDefault + 3.0;
            this.turboTextSpeedScale = baseTurbo + 24.0;
        }
    }

    setTextSpeedScale(scale) {
        this.textSpeedScale = scale;
        this.calcTimePerChar();
    }

    calcTimePerChar() {
        this.timePerChar = (1000 / this.textSpeed) * (1 / this.textSpeedScale);
    }

    parseDialogLine(line) {
        let speaker = line.speaker;
        let options = line.options;
        let indent = null;
        if (isDefinedAndNotNull(options)) {
            indent = options.indent;
        }
        let indentInChars = 0;
        let indentString = '';
        let text = line.text;
        let textArray = [];

        if (isDefinedAndNotNull(text) && text !== '') {
            // calculate indent for current line in data
            if (isDefinedAndNotNull(speaker)) {
                indentInChars = this.indentTab.length * 2;
            } else if (isDefinedAndNotNull(indent)) {
                if (typeof indent === 'string') {
                    let idx = indent.indexOf('char');
                    if (idx > -1) {
                        let num = parseInt(indent.substring(0, idx));
                        indentInChars = !num.isNan() ? num : 0;
                    }
                } else if (typeof indent === 'number') {
                    indentInChars = indent * this.indentTab.length;
                }
            }
            let count = indentInChars;
            while (count > 0) {
                indentString += ' ';
                count--;
            }

            // prepare raw text from data by splitting into words and separating by newlines

            // text = text.replace(/^\s*[\r\n]/gm, '\n');   // when using JS multiline-string syntax in dialog database
            text = text.replace(/\n/gm, ' \n ');
            text = text.replace(/ +(?= )/g, '');
            textArray = text.split(' ');

            // check for each word in textArray if it fits in current parsedLine with maxLineWidth
            for (let i = 0; i < textArray.length; i++) {
                if (isUndefinedOrNull(this.parsedLinesArray[this.currIdxParsedLinesArray])) {
                    this.parsedLinesArray[this.currIdxParsedLinesArray] = '';
                }
                if (textArray[i] !== '\n' && textArray[i] !== '') {

                    let parsedLine = this.parsedLinesArray[this.currIdxParsedLinesArray];
                    let parsedLineLen = parsedLine.length;
                    let currWordLen = textArray[i].length;

                    if (parsedLineLen === 0) {
                        if (isDefinedAndNotNull(speaker) && i === 0) {
                            this.parsedLinesArray[this.currIdxParsedLinesArray] += speaker + ': ';
                        } else if (indentString !== '') {
                            this.parsedLinesArray[this.currIdxParsedLinesArray] += indentString;
                        }
                    }


                    parsedLineLen = parsedLine.length;
                    let totalLen = parsedLineLen + currWordLen;

                    if (totalLen <= this.maxLineWidth) {
                        this.parsedLinesArray[this.currIdxParsedLinesArray] += textArray[i];
                        if (totalLen < this.maxLineWidth) {
                            this.parsedLinesArray[this.currIdxParsedLinesArray] += ' ';
                        }
                    } else {
                        this.currIdxParsedLinesArray++;
                        this.parsedLinesArray[this.currIdxParsedLinesArray] = '';
                        parsedLine = this.parsedLinesArray[this.currIdxParsedLinesArray];
                        parsedLineLen = parsedLine.length;

                        if (parsedLineLen === 0 && indentString !== '') {
                            this.parsedLinesArray[this.currIdxParsedLinesArray] += indentString;
                        }

                        this.parsedLinesArray[this.currIdxParsedLinesArray] += textArray[i];

                        if (currWordLen < this.maxLineWidth) {
                            this.parsedLinesArray[this.currIdxParsedLinesArray] += ' ';
                        }
                    }
                } else if (textArray[i] === '\n') {
                    if (isDefinedAndNotNull(textArray[i + 1]) && textArray[i + 1] !== '') {
                        this.currIdxParsedLinesArray++;
                    }
                }
            }

            this.currIdxParsedLinesArray++;

            // console.dir(textArray);
        }
    }

    parseDialogData(data, callback, config = {}) {
        if (isUndefinedOrNull(data)) return;

        const oldEndIdx = Math.max(0, this.linesData.length);
        this.linesData.push(...data);


        for (let i = oldEndIdx; isDefinedAndNotNull(this.linesData[i]); i++) {
            let line = this.linesData[i];
            this.parseDialogLine(line);
        }
        if (isDefinedAndNotNull(callback)) {
            this.callbacksForParsedLines.push({
                parsedLineIndex: this.currIdxParsedLinesArray - 1,
                callback: callback,
                config: config
            });
        }


    }

    startDrawingText(callback, pressToFinish = false) {
        this.scene.events.emit('Action', 'StartTextDraw');
        this.textDrawDone = false;
        this.pressToFinish = pressToFinish;

        if (isDefinedAndNotNull(callback)) this.callbackAfterTextDrawDone = callback;
    }

    drawText(time, delta) {
        if (!this.textDrawDone && !this.waitingForInputToScroll) {
            if (isUndefinedOrNull(this.compEntries[this.currIdxCompStrings])) {
                let newObj;
                this.currCharCompString = 0;
                if (((this.currIdxCompStrings - 1) % 2) === 0) {
                    newObj = new DialogString('compString-' + this.currIdxCompStrings, '');
                    this.compEntries[this.currIdxCompStrings] = this.parentMenu.createEntry(newObj);

                    this.moveDownInList(true);
                    // console.log('new dialog line' + ' - comp index: ' + this.getCurrentIndex());
                } else {
                    newObj = new EmptyObject();
                    this.compEntries[this.currIdxCompStrings] = this.parentMenu.createEntry(newObj);

                    this.currIdxCompStrings++;
                    this.moveDownInList(true);
                    // console.log('new empty line' + ' - comp index: ' + this.getCurrentIndex());

                    return;
                }
            }

            let idxParsedArray = Math.floor((this.currIdxCompStrings - 1) / 2);

            if (isDefinedAndNotNull(this.parsedLinesArray[idxParsedArray])) {
                if (this.animateText) {
                    if (this.currCharCompString < this.parsedLinesArray[idxParsedArray].length) {
                        if (isDefinedAndNotNull(this.compEntries[this.currIdxCompStrings])) {
                            let obj = this.compEntries[this.currIdxCompStrings].getObject();
                            let string = obj.getFullName() + this.parsedLinesArray[idxParsedArray].charAt(this.currCharCompString);
                            obj.setFullName(string);
                        }

                        this.currCharCompString++;
                    } else {
                        this.currCharCompString = 0;
                        this.currIdxCompStrings++;

                        idxParsedArray = Math.floor((this.currIdxCompStrings - 1) / 2);
                        if (idxParsedArray >= (this.currIdxParsedLinesArray - 1)) {
                            this.textDrawDone = true;

                            if (this.pressToFinish) {
                                this.waitingForInputToScroll = true;
                            } else {
                                if (isDefinedAndNotNull(this.callbackAfterTextDrawDone)) {
                                    this.callbackAfterTextDrawDone();
                                    this.callbackAfterTextDrawDone = null;
                                }
                            }
                            this.scene.events.emit('Action', 'TextDrawDone');

                        } else if (((this.currIdxCompStrings) % this.getVisibleRows()) === 0) {
                            // this.waitingForInputToScroll = true;
                        }

                    }

                    this.draw();
                } else {
                    idxParsedArray = Math.floor((this.currIdxCompStrings - 1) / 2);

                    if (isDefinedAndNotNull(this.compEntries[this.currIdxCompStrings])) {
                        let obj = this.compEntries[this.currIdxCompStrings].getObject();
                        let string = this.parsedLinesArray[idxParsedArray];
                        obj.setFullName(string);
                    }

                    this.currIdxCompStrings++;
                    if (idxParsedArray >= (this.currIdxParsedLinesArray - 1)) {
                        this.textDrawDone = true;

                        this.draw();

                        if (this.pressToFinish) {
                            this.waitingForInputToScroll = true;
                        } else {
                            if (isDefinedAndNotNull(this.callbackAfterTextDrawDone)) {
                                this.callbackAfterTextDrawDone();
                                this.callbackAfterTextDrawDone = null;
                            }
                        }
                        this.scene.events.emit('Action', 'TextDrawDone');

                    } else if (((this.currIdxCompStrings) % this.getVisibleRows()) === 0) {
                        // this.waitingForInputToScroll = true;
                    }
                }

                idxParsedArray = Math.floor((this.currIdxCompStrings - 2) / 2);
                let callbackArrayIndex = null;
                let callbackForCurrParsedLine = this.callbacksForParsedLines.find((el, index) => {
                    let res = (isDefinedAndNotNull(el) && el.parsedLineIndex === idxParsedArray);
                    if (res) {
                        callbackArrayIndex = index;
                    }
                    return res;
                });
                let callback;
                let callbackConfig;
                if (isDefinedAndNotNull(callbackForCurrParsedLine)) {
                    callback = callbackForCurrParsedLine.callback;
                    callbackConfig = callbackForCurrParsedLine.config;
                }
                if (isDefinedAndNotNull(callback)) {
                    callback();
                    if (isDefinedAndNotNull(callbackArrayIndex)) this.callbacksForParsedLines[callbackArrayIndex] = null;

                    if (isDefinedAndNotNull(callbackConfig)) {
                        this.textDrawDone = true;
                        sleep(callbackConfig.delay).then(() => this.textDrawDone = false);
                    }
                }
            }
        }
    }

    clearComponent() {
        super.clearComponent();
    }

    pressUp() {

    }

    pressDown() {

    }

    pressLeft() {

    }

    pressRight() {

    }

    confirm() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {
            this.setTextSpeedScale(this.turboTextSpeedScale);
        } else if (this.stateMachine.getCurrentStateName() === 'DialogEndState') {
            if (this.waitingForInputToScroll) {
                this.waitingForInputToScroll = false;
                this.pressToFinish = false;

                if (isDefinedAndNotNull(this.callbackAfterTextDrawDone)) {
                    this.callbackAfterTextDrawDone();
                    this.callbackAfterTextDrawDone = null;
                }
            }
        }
    }

    confirmReleased() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {
            this.setTextSpeedScale(this.defaultTextSpeedScale);
        }
    }

    cancel() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {

        }
    }

    update(time, delta) {
        if (!this.textDrawDone && this.animateText) {
            this.elapsedTime = (this.elapsedTime + delta) % 1000;

            if (this.elapsedTime >= this.timePerChar) {
                this.elapsedTime = 0;

                this.drawText(time, delta);
            }
        } else if (!this.textDrawDone && !this.animateText) {
            this.drawText(time, delta);
        }
    }

}