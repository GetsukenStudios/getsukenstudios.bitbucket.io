import { isDefinedAndNotNull } from '../Base/Utils';
export class ListEntry {
    constructor(object, renderer) {
        this.object = object;
        this.renderer = renderer;
        this.isDirty = false;
    }

    getObject() {
        return this.object;
    }

    setObject(object) {
        this.object = object;
    }

    getRenderer() {
        return this.renderer;
    }

    setRenderer() {
        this.renderer = renderer;
    }

    clearRenderer() {
        if (isDefinedAndNotNull(this.renderer)) {
            this.renderer.clearRenderObjects();
        }
    }
}
