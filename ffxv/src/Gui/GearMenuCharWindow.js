import { BaseWindow } from './BaseWindow';
export class GearMenuCharWindow extends BaseWindow {
    constructor(key, scene, xInTiles, yInTiles) {
        // x = 0, y = 0, w = 20, h = 18
        super(key, scene, xInTiles, yInTiles, 20, 18);
    }
}
