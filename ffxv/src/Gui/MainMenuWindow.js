import { BaseWindow } from './BaseWindow';
export class MainMenuWindow extends BaseWindow {
    constructor(key, scene, xInTiles, yInTiles) {
        // x = 1, y = 12, w = 18, h = 6
        super(key, scene, xInTiles, yInTiles, 18, 6);
    }
}
