import {
    BaseMenu
} from './BaseMenu';
import {
    remove,
    isDefinedAndNotNull,
    prependWhitespaces
} from '../Base/Utils';
import {
    EmptyObject,
    Item,
    SpecialMenuObject,
    CustomString,
    CustomImage,
    GameObject,
    Technique
} from '../Game/GameObjects';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import {
    ItemRenderer
} from './ItemRenderer';
import {
    ListComponent
} from './ListComponent';
import {
    BaseComponent
} from './BaseComponent';
import {
    BaseWindow
} from './BaseWindow';
import {
    TechniqueRenderer
} from './TechniqueRenderer';
import {
    DialogComponent
} from './DialogComponent';

export class TechniqueMenu extends BaseMenu {

    constructor(scene, data) {
        super(scene);

        this.selectedChar = data.selectedChar;
        this.equippedWeapons = [];
        this.equippedAccs = [];
        this.unlockedTechniques = [];

        this.initializeInventories();

        this.openTechniqueMenuCharWindow();
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    initializeInventories() {
        let partyReg = this.scene.registry.get('party');
        let techDB = this.scene.registry.get('techniqueDatabase');
        let stringDB = this.scene.registry.get('stringDatabase');
        let charKey = this.selectedChar.key;
        this.unlockedTechniques = partyReg.getUnlockedTechniquesObjects(charKey, stringDB, techDB);
    }

    createRenderer(object) {
        let renderer;
        renderer = super.createRenderer(object);

        if (object instanceof Item) {
            renderer = new ItemRenderer(this.scene, object, true);
        }
        if (object instanceof Technique) {
            renderer = new TechniqueRenderer(this.scene, object, 'techniqueMenu');
        }

        return renderer;
    }

    openTechniqueMenuCharWindow() {
        this.techniqueMenuCharWindow = new BaseWindow('armsMenuCharWindow', this.scene, 0, 0, 20, 11);
        this.addWindow(this.techniqueMenuCharWindow);

        this.charAvaComp = new BaseComponent('charAvaComp', this.scene, this,
            this.tileSizeMenu * 1, this.tileSizeMenu * 1);
        this.techniqueMenuCharWindow.addComponent(this.charAvaComp);

        this.charNameComp = new BaseComponent('charNameComp', this.scene, this,
            this.tileSizeMenu * 3, this.tileSizeMenu * 1);
        this.techniqueMenuCharWindow.addComponent(this.charNameComp);

        this.hpComp = new BaseComponent('hpComp', this.scene, this, this.tileSizeMenu * 3,
            this.tileSizeMenu * 2);
        this.techniqueMenuCharWindow.addComponent(this.hpComp);

        this.levelComp = new BaseComponent('levelComp', this.scene, this, this.tileSizeMenu * 12,
            this.tileSizeMenu * 1);
        this.techniqueMenuCharWindow.addComponent(this.levelComp);

        // this.expComp = new BaseComponent('expComp', this.scene, this, this.tileSizeMenu * 12,
        //     this.tileSizeMenu * 2);
        // this.techniqueMenuCharWindow.addComponent(this.expComp);

        // this.techniquesComp = new BaseComponent('techniquesComp', this.scene, this,
        //     this.tileSizeMenu * 2, this.tileSizeMenu * 4);
        // this.techniqueMenuCharWindow.addComponent(this.techniquesComp);

        this.techniqueListComp = new ListComponent('techniqueListComp', this.scene, this,
            this.tileSizeMenu * 2, this.tileSizeMenu * 5, 1, 5, null, null, null, {
                width: Math.floor(this.tileSizeMenu * 0.5),
                height: Math.floor(this.tileSizeMenu * 0.5)
            });
        this.techniqueMenuCharWindow.addComponent(this.techniqueListComp);

        this.techniqueDescriptionWindow = new BaseWindow('techniqueDescriptionWindow', this.scene,
            0, 11, 20, 7);
        this.addWindow(this.techniqueDescriptionWindow);

        this.techniqueDescriptionComp = new DialogComponent('techniqueDescriptionComp', this.scene, this, 6);
        this.techniqueDescriptionComp.setComponentPosition(this.tileSizeMenu * 1, (this.tileSizeMenu / 2) - 3);
        this.techniqueDescriptionWindow.addComponent(this.techniqueDescriptionComp);

        /*         this.armsListComp = new ListComponent('armsListComp', this.scene, this,
                    this.tileSizeMenu * 12, this.tileSizeMenu * 10, 1, 7);
                this.armsListComp.setEntryMargin(0, 0);
                this.techniqueMenuCharWindow.addComponent(this.armsListComp); */

        /*         this.statsNamesComp = new ListComponent('statsNamesComp', this.scene, this, this.tileSizeMenu * 1,
                    this.tileSizeMenu * 12, 2, 5, null, null, null, { width: this.tileSizeMenu * 6, height: 0 });
                this.techniqueMenuCharWindow.addComponent(this.statsNamesComp);
        
                this.statsValuesComp = new ListComponent('statsValuesComp', this.scene, this, this.tileSizeMenu * 7,
                    this.tileSizeMenu * 12, 2, 5, null, null, null, { width: this.tileSizeMenu * 5, height: 0 });
                this.techniqueMenuCharWindow.addComponent(this.statsValuesComp); */

        this.setActiveWindow(this.techniqueMenuCharWindow);
        this.setActiveComponent(this.techniqueListComp);

        this.updateTechniqueMenuCharWindow();
    }

    closeTechniqueMenuCharWindow() {
        this.removeWindow(this.techniqueMenuCharWindow);
        this.removeWindow(this.techniqueDescriptionWindow);
    }

    updateTechniqueMenuCharWindow() {
        this.techniqueMenuCharWindow.clear();
        // this.techniqueDescriptionWindow.clear();

        let imageFrame;
        switch (this.selectedChar.getKey()) {
            case 'char-noctis':
                imageFrame = 0;

                break;

            case 'char-gladio':
                imageFrame = 6;

                break;

            case 'char-ignis':
                imageFrame = 12;

                break;

            case 'char-prompto':
                imageFrame = 18;

                break;

            default:
                break;
        }

        let entries;

        entries = this.createEntries([new CustomImage('sprites-main-chars', imageFrame)]);
        this.charAvaComp.setEntries(entries, true);

        entries = this.createEntries([new CustomString(this.selectedChar.getKey(),
            this.selectedChar.getFullName(), this.selectedChar.getShortName(), true)]);
        this.charNameComp.setEntries(entries, true);

        let hpText = '' + prependWhitespaces(String(this.selectedChar.hp), 3) + '\/' +
            prependWhitespaces(String(this.selectedChar.maxHp.total), 3);
        entries = this.createEntries([hpText]);
        this.hpComp.setEntries(entries, true);

        let levelText = this.stringDB.getCustomStringByKey('string-level').getShortName().toUpperCase() + '  ' +
            prependWhitespaces(String(this.selectedChar.level), 2);
        entries = this.createEntries([levelText]);
        this.levelComp.setEntries(entries, true);

        // let expText = this.stringDB.getCustomStringByKey('string-experience').getShortName().toUpperCase() +
        //     prependWhitespaces(String(this.selectedChar.exp), 5);
        // entries = this.createEntries([expText]);
        // this.expComp.setEntries(entries, true);

        // let techniquesText = this.stringDB.getCustomStringByKey('string-menu-techniques').getFullName();
        // entries = this.createEntries([techniquesText]);
        // this.techniquesComp.setEntries(entries);

        entries = this.createEntries(this.unlockedTechniques);
        this.techniqueListComp.setEntries(entries, true);

        let currObj = this.getActiveComponent().getCurrentEntryObject();
        let description = '';
        if (currObj instanceof Technique) {
            description = currObj.description;
        }
        // entries = this.createEntries([description]);
        // this.techniqueDescriptionComp.setEntries(entries);

        this.updateTechDescription(description);

        this.draw();
    }

    closeTechniqueMenu() {
        this.getParentScene().events.emit('Action', 'CloseTechniqueMenu');
        this.scene.game.events.emit('Menu', 'CloseTechniqueMenu');
    }

    updateTechDescription(descriptionText) {
        if (isDefinedAndNotNull(this.techniqueDescriptionComp)) {
            let config = {
                animateText: false,
            };
            let data = [{
                options: {},
                text: descriptionText,
            }];

            this.techniqueDescriptionComp.reset(config);
            this.techniqueDescriptionComp.parseDialogData(data);
            this.techniqueDescriptionComp.startDrawingText();
        }
    }

    confirm() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {

        }
    }

    cancel() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            if (this.stateMachine.getCurrentStateName() === 'StartState') {
                this.closeTechniqueMenu();
            }
        }
    }

    pressDown() {
        super.pressDown();

        this.updateTechniqueMenuCharWindow();
    }

    pressUp() {
        super.pressUp();

        this.updateTechniqueMenuCharWindow();
    }

    update(time, delta) {
        this.techniqueDescriptionComp.update(time, delta);
    }
}