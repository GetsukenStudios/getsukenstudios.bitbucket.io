export class DefaultBitmapText extends Phaser.GameObjects.BitmapText {
    constructor(scene, x, y, text) {
        super(scene, x, y, 'retroBitmap', text);
        this.setOrigin(0, 0);
    }
}
