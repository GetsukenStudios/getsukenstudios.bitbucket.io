import {
    DefaultBitmapText
} from './DefaultBitmapText';
import {
    BaseRenderer
} from './BaseRenderer';
import {
    CustomString
} from '../Game/GameObjects';

export class TechniqueRenderer extends BaseRenderer {

    constructor(scene, object, mode) {
        super(scene, object);
        this.renderWidth = this.tileSizeMenu * 16;
        this.renderHeight = this.tileSizeMenu * 1;
        this.useFullName = object.useFullName ? object.useFullName : false;
        this.mode = mode || '';
    }

    render(x, y) {
        super.render(x, y);

        let name;
        let cost;
        if (!this.useFullName) {
            name = this.object.getShortName();
        } else {
            name = this.object.getFullName();
        }
        if (this.mode === 'baseMenu') {
            name = this.object.getFullName();
        } else if (this.mode === 'techniqueMenu') {
            name = this.object.getFullName();
        } else if (this.mode === 'battleMenu-SelectTechState') {
            name = this.object.getShortName();
        }
        cost = this.object.cost;

        let stringDB = this.scene.registry.get('stringDatabase');

        let techNameText = new DefaultBitmapText(this.scene, x, y, name);
        let techCostText;
        if (this.mode === 'battleMenu-SelectTechState') {
            techCostText = new DefaultBitmapText(this.scene, x + this.tileSizeMenu * 8, y, '' + cost);
        } else {
            techCostText = new DefaultBitmapText(this.scene, x + this.tileSizeMenu * 13, y, '' + cost +
                stringDB.getCustomStringByKey('string-tech-point').getShortName().toUpperCase());
        }

        let result = [techNameText, techCostText];
        this.renderObjects = result;
        return result;
    }
}