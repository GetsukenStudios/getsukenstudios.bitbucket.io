import {
    BaseRenderer
} from './BaseRenderer';
import {
    CustomImage,
    SpecialMenuObject,
    CustomMonsterImage
} from '../Game/GameObjects';
import {
    DefaultBitmapText
} from './DefaultBitmapText';

export class SimpleImageRenderer extends BaseRenderer {

    constructor(scene, object) {
        super(scene, object);
        this.renderWidth = this.tileSizeMenu * 2;
        this.renderHeight = this.tileSizeMenu * 2;
        this.useImageSizeAsRenderSize = true;
    }

    render(x, y) {
        super.render(x, y);

        let result;
        let objKey;
        let imageKey;
        let imageFrame;
        let image;
        if (this.object instanceof CustomMonsterImage) {
            objKey = this.object.getKey();
            imageFrame = this.object.getFrame();
            imageKey = objKey;
            image = this.scene.add.image(x, y, imageKey, imageFrame);
            let visible = this.object.getIsVisible();
            image.setVisible(visible);
            this.useImageSizeAsRenderSize = false;
        } else if (this.object instanceof CustomImage) {
            objKey = this.object.getKey();
            imageFrame = this.object.getFrame();
            imageKey = objKey;
            image = this.scene.add.image(x, y, imageKey, imageFrame).setOrigin(0, 0);
        } else if (this.object instanceof SpecialMenuObject) {
            objKey = this.object.getKey();
            imageKey = objKey;
            image = this.scene.add.image(x, y, imageKey, imageFrame).setOrigin(0, 0);
        }
        result = [image];

        if (this.useImageSizeAsRenderSize) {
            this.setRenderWidth(image.width);
            this.setRenderHeight(image.height);
        }

        this.renderObjects = result;
        return result;
    }
}