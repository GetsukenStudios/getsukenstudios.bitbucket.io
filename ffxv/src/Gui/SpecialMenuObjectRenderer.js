
import { BaseRenderer } from './BaseRenderer';
import { CustomImage, SpecialMenuObject } from '../Game/GameObjects';
import { DefaultBitmapText } from './DefaultBitmapText';

export class SpecialMenuObjectRenderer extends BaseRenderer {

    constructor(scene, object) {
        super(scene, object);
        this.renderWidth = this.tileSizeMenu * 2;
        this.renderHeight = this.tileSizeMenu * 2;
    }

    render(x, y) {
        super.render(x, y);

        let result;
        let objKey;
        let imageKey;
        let imageFrame;
        let image;
        let text;
        if (this.object instanceof SpecialMenuObject) {
            objKey = this.object.getKey();
        }

        switch (objKey) {
            case 'trash':
                imageKey = 'sprites-trash-can';
                image = this.scene.add.image(x + (this.tileSizeMenu * 3), y - (this.tileSizeMenu) + 2, imageKey).setOrigin(0, 0);
                result = [image];

                this.setRenderWidth(image.width);
                this.setRenderHeight(image.height);
                break;
            case 'none':
                text = new DefaultBitmapText(this.scene, x, y, '   -  ');
                result = [text];

                this.setRenderWidthInTiles(4);
                this.setRenderHeightInTiles(1);
                break;
            default:
                imageKey = objKey;
                image = this.scene.add.image(x, y, imageKey, imageFrame).setOrigin(0, 0);
                result = [image];

                this.setRenderWidth(image.width);
                this.setRenderHeight(image.height);
                break;
        }

        this.renderObjects = result;
        return result;
    }
}