import {
    DefaultBitmapText
} from './DefaultBitmapText';
import {
    BaseRenderer
} from './BaseRenderer';

export class MonsterRenderer extends BaseRenderer {

    constructor(scene, object, useFullName) {
        super(scene, object);
        this.renderWidth = this.tileSizeMenu * 9;
        this.renderHeight = this.tileSizeMenu * 1;
        this.useFullName = useFullName || object.useFullName || false;
    }

    render(x, y) {
        super.render(x, y);

        let name;
        if (this.useFullName === false) {
            name = this.object.getShortName();
        } else {
            name = this.object.getFullName();
        }

        let monsterNameText = new DefaultBitmapText(this.scene, x, y, name);

        let result = [monsterNameText];
        this.renderObjects = result;
        return result;
    }
}