import {
    isDefinedAndNotNull,
    contains,
    remove
} from '../Base/Utils';

export class BaseWindow {
    constructor(key, scene, xInTiles, yInTiles, widthInTiles, heightInTiles, options = {}) {
        this.scene = scene;
        this.key = key;
        let settings = scene.registry.get('gameSettings');
        this.tileSizeMenu = settings.tileSizeMenu;
        this.x = xInTiles * this.tileSizeMenu;
        this.y = yInTiles * this.tileSizeMenu;
        this.widthInTiles = widthInTiles;
        this.heightInTiles = heightInTiles;
        this.windowBackground = []; // list of Images that are added in drawWindow()
        this.components = [];
        this.activeComponent = null;
        this.oldActiveComponent = null;
        this.isWindowsVisible = true;
        this.isActive = false;
        this.doNotRenderBackground = options.doNotRenderBackground || false;
        this.screenShakeActiveElapsedTime = -1;
        this.screenShakeActiveFrame = -1;
    }

    addComponent(component) {
        component.setParentWindow(this);
        this.components.push(component);
    }

    removeComponent(component) {
        remove(this.components, component);
        component.destroy();
        component = null;
    }

    removeAllComponents() {
        this.components.forEach(comp => {
            this.removeComponent(comp);
        });
    }

    reset() {
        this.clear();
        this.removeAllComponents();
        this.activeComponent = null;
        this.oldActiveComponent = null;
    }

    getOldActiveComponent() {
        return this.oldActiveComponent;
    }

    setOldActiveComponent(component) {
        this.oldActiveComponent = component;
    }

    getActiveComponent() {
        return this.activeComponent;
    }

    setActiveComponent(component) {
        if (!component || (component && contains(this.components, component))) {
            if (isDefinedAndNotNull(this.activeComponent)) {
                this.activeComponent.setActive(false);
                this.oldActiveComponent = this.activeComponent;
                if (component) component.setActive(true);
                this.activeComponent = component;
            } else {
                if (component) component.setActive(true);
                this.oldActiveComponent = component;
                this.activeComponent = component;
            }
        }
    }

    getActive() {
        return this.isActive;
    }

    setActive(bool) {
        this.isActive = bool;
        if (isDefinedAndNotNull(this.activeComponent)) {
            this.activeComponent.setActive(bool);
        }
        if (bool) {

        } else {

        }
    }

    getWindowVisible() {
        return this.isWindowsVisible;
    }

    setWindowVisible(bool) {
        this.isWindowsVisible = bool;
        this.windowBackground.forEach(image => {
            image.setVisible(bool);
        });
        this.components.forEach(comp => {
            comp.setComponentVisible(bool);
        });
        if (bool) {

        } else {

        }
    }

    getWindowPosition() {
        return {
            x: this.x,
            y: this.y
        };
    }

    setWindowPosition(x, y) {
        this.x = x;
        this.y = y;
        this.components.forEach(comp => {
            comp.updateComponentPosition();
        });
    }

    moveWindowPositionBy(xOffset, yOffset) {
        let newX = this.x + xOffset;
        let newY = this.y + yOffset;
        this.setWindowPosition(newX, newY);
    }

    isScreenShakeActive() {
        return this.screenShakeActiveElapsedTime >= 0;
    }

    startShakeEffect() {
        this.screenShakeActiveElapsedTime = 0;
        this.screenShakeActiveFrame = 0;
    }

    stopShakeEffect() {
        this.screenShakeActiveElapsedTime = -1;
        this.screenShakeActiveFrame = -1;
    }

    updateShakeEffectFrame(time, delta) {
        const ts = this.tileSizeMenu;
        // const animationFrameYOffsets = [-ts, 2 * ts, -2 * ts, 1.5 * ts, -ts, ts, -0.5 * ts];
        const animationFrameYOffsets = [ts, -2 * ts, 2 * ts, -ts];
        const frameDuration = 1000 / (animationFrameYOffsets.length / 0.5);

        if (this.screenShakeActiveElapsedTime === 0 || this.screenShakeActiveElapsedTime >= frameDuration) {
            if (this.screenShakeActiveElapsedTime > 0) {
                this.screenShakeActiveElapsedTime = 0;
                this.screenShakeActiveFrame++;
            }

            this.moveWindowPositionBy(0, animationFrameYOffsets[this.screenShakeActiveFrame]);
            this.draw();

            if (this.screenShakeActiveFrame >= animationFrameYOffsets.length - 1) {
                this.stopShakeEffect();
                return;
            }
        }

        this.screenShakeActiveElapsedTime = (this.screenShakeActiveElapsedTime + delta);
    }

    clear() {
        this.clearWindowBackground();
        this.clearComponents();
    }

    clearWindowBackground() {
        this.windowBackground.forEach(image => {
            image.destroy(true);
        });
        this.windowBackground = [];
    }

    clearComponents() {
        this.components.forEach(comp => {
            comp.clearComponent();
        });
    }

    draw() {
        this.clear();
        if (this.isWindowsVisible) {
            if (!this.doNotRenderBackground) {
                this.drawWindow(this.x, this.y, this.widthInTiles, this.heightInTiles);
            }
            this.components.forEach(comp => {
                comp.draw();
            });
        }
    }

    drawWindow(x, y, widthInTiles, heightInTiles) {
        // var image;
        for (let j = 0; j < heightInTiles; j++) {
            for (let i = 0; i < widthInTiles; i++) {
                let image;
                if (j == 0) {
                    if (i == 0) {
                        // draw top-left corner tile
                        image = this.scene.add.image(x, y, 'sprites-menu-tiles', 0).setOrigin(0, 0);
                    } else if (i == widthInTiles - 1) {
                        // draw top-right corner tile
                        image = this.scene.add.image(x + i * this.tileSizeMenu, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 2).setOrigin(0, 0);
                    } else {
                        // draw top border filler tiles
                        image = this.scene.add.image(x + i * this.tileSizeMenu, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 1).setOrigin(0, 0);
                    }
                } else if (j == heightInTiles - 1) {
                    if (i == 0) {
                        // draw bottom-left corner tile
                        image = this.scene.add.image(x, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 6).setOrigin(0, 0);
                    } else if (i == widthInTiles - 1) {
                        // draw bottom-right corner tile
                        image = this.scene.add.image(x + i * this.tileSizeMenu, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 8).setOrigin(0, 0);
                    } else {
                        // draw bottom border filler tiles
                        image = this.scene.add.image(x + i * this.tileSizeMenu, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 7).setOrigin(0, 0);
                    }
                } else {
                    if (i == 0) {
                        // draw center-left border tile
                        image = this.scene.add.image(x, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 3).setOrigin(0, 0);
                    } else if (i == widthInTiles - 1) {
                        // draw center-right border tile
                        image = this.scene.add.image(x + i * this.tileSizeMenu, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 5).setOrigin(0, 0);
                    } else {
                        // draw center filler tiles
                        image = this.scene.add.image(x + i * this.tileSizeMenu, y + j * this.tileSizeMenu, 'sprites-menu-tiles', 4).setOrigin(0, 0);
                    }
                }
                if (typeof image === 'object' && image.type == 'Image') {
                    image.setDepth(this.scene.registry.get('gameSettings').depthList.gui);
                    this.windowBackground.push(image);
                }
            }
        }
    }

    update(time, delta) {
        if (this.isScreenShakeActive()) this.updateShakeEffectFrame(time, delta);
    }
}