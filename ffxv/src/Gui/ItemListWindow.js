import { BaseWindow } from './BaseWindow';
export class ItemListWindow extends BaseWindow {
    constructor(key, scene, xInTiles, yInTiles) {
        // x = 7, y = 0, w = 12, h = 18
        super(key, scene, xInTiles, yInTiles, 12, 18);
    }
}
