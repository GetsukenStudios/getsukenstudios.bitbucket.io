import {
    BaseMenu
} from './BaseMenu';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';
import {
    BaseWindow
} from './BaseWindow';
import {
    DialogComponent
} from './DialogComponent';

export class DialogMenu extends BaseMenu {

    constructor(scene, data) {
        super(scene);

        this.initialize();

        this.openDialogMenu(data);

        this.draw();
    }

    initialize() {
        super.initialize();

        this.stateMachine.reset();
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        this.dialogEndState = new StateNode(stateMachine, 'DialogEndState');

        new StateTransition('TextDrawDone', this.startState, this.dialogEndState);

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    openDialogMenu(data) {
        this.dialogWindow = new BaseWindow('dialogWindow', this.scene, 0, 10, 20, 8);
        this.addWindow(this.dialogWindow);
        this.setActiveWindow(this.dialogWindow);

        this.dialogTextComp = new DialogComponent('dialogTextComp', this.scene, this, 6, data);
        this.dialogWindow.addComponent(this.dialogTextComp);
        // this.dialogWindow.setActiveComponent(this.dialogTextComp);
    }

    closeDialogMenu() {
        this.scene.game.events.emit('Menu', 'CloseDialogMenu', {}, this.getParentSceneCallback());
    }

    confirm() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {
            this.dialogTextComp.confirm();
        } else if (this.stateMachine.getCurrentStateName() === 'DialogEndState') {
            this.closeDialogMenu();
        }

    }

    confirmReleased() {
        if (this.stateMachine.getCurrentStateName() === 'StartState') {
            this.dialogTextComp.confirmReleased();
        }
    }

    cancel() {
        if (this.stateMachine.getCurrentStateName() === 'DialogEndState') {
            this.closeDialogMenu();
        }
    }

    update(time, delta) {
        this.dialogTextComp.update(time, delta);
    }

}