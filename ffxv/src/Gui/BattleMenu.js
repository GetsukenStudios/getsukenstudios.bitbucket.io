import {
    Item,
    Character,
    Technique,
    EmptyObject
} from '../Game/GameObjects';
import {
    ListComponent
} from './ListComponent';
import {
    BaseMenu
} from './BaseMenu';
import {
    isDefinedAndNotNull,
    isUndefinedOrNull,
    filterObject,
    isEmpty,
    prependZeroes,
    appendSpecialWhitespaces,
    prependSpecialWhitespaces,
    randomNumber,
    isRandomValueInRange
} from '../Base/Utils';
import StateMachine from '../State/StateMachine';
import StateNode from '../State/StateNode';
import StateTransition from '../State/StateTransition';
import {
    CharListWindow
} from './CharListWindow';
import {
    CharacterSelectionRenderer
} from './CharacterSelectionRenderer';
import {
    BaseWindow
} from './BaseWindow';
import {
    DialogComponent
} from './DialogComponent';
import {
    ItemRenderer
} from './ItemRenderer';
import {
    TechniqueRenderer
} from './TechniqueRenderer';

export class BattleMenu extends BaseMenu {

    constructor(scene, data) {
        super(scene, data);

        this.partySize = 4;
    }

    createEntries(objects) {
        let entries = super.createEntries(objects);

        if (this.stateMachine.getCurrentStateName() === 'SelectItemState') {
            objects.forEach((obj, index) => {
                let entry = null;
                if (typeof obj === 'string') {
                    if (obj === 'none') {
                        obj = new EmptyObject();

                        entry = this.createEntry(obj);
                    }
                }

                if (isDefinedAndNotNull(entry)) {
                    entries[index] = entry;
                }
            });
        }

        return entries;
    }

    createRenderer(object) {
        let renderer;
        renderer = super.createRenderer(object);

        if (this.stateMachine.getCurrentStateName() === 'PartyTurnState') {
            if (object instanceof Character) {
                renderer = new CharacterSelectionRenderer(this.scene, object, 'battleMenu-' + 'PartyTurnState');
            }
        } else if (this.stateMachine.getCurrentStateName() === 'PlayerTurnState') {
            if (object instanceof Character) {
                renderer = new CharacterSelectionRenderer(this.scene, object, 'battleMenu-' + 'PlayerTurnState');
            }
        } else if (this.stateMachine.getCurrentStateName() === 'SelectAttackState') {
            if (object instanceof Item) {
                return new ItemRenderer(this.scene, object, true);
            }
        } else if (this.stateMachine.getCurrentStateName() === 'SelectTechState') {
            if (object instanceof Technique) {
                return new TechniqueRenderer(this.scene, object, 'battleMenu-SelectTechState');
            }
        } else if (this.stateMachine.getCurrentStateName() === 'SelectPlayerState') {
            if (object instanceof Character) {
                renderer = new CharacterSelectionRenderer(this.scene, object, 'battleMenu-' + 'SelectPlayerState');
            }
        }

        return renderer;
    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.fullKey, this.scene.events);

        this.startState = new StateNode(stateMachine, 'StartState', this.enterStartState.bind(this));
        this.partyTurnState = new StateNode(stateMachine, 'PartyTurnState');
        this.prePlayerTurnState = new StateNode(stateMachine, 'PrePlayerTurnState');
        this.playerTurnState = new StateNode(stateMachine, 'PlayerTurnState');
        this.enemyTurnState = new StateNode(stateMachine, 'EnemyTurnState');
        this.selectAttackState = new StateNode(stateMachine, 'SelectAttackState');
        this.selectTechState = new StateNode(stateMachine, 'SelectTechState');
        this.selectItemState = new StateNode(stateMachine, 'SelectItemState');
        this.selectDefendState = new StateNode(stateMachine, 'SelectDefendState');
        this.selectRunState = new StateNode(stateMachine, 'SelectRunState');
        this.selectPlayerState = new StateNode(stateMachine, 'SelectPlayerState');
        this.selectEnemyState = new StateNode(stateMachine, 'SelectEnemyState');
        this.battleResultsState = new StateNode(stateMachine, 'BattleResultsState');

        new StateTransition('EnterStartState', this.partyTurnState, this.startState, this.enterStartStateFromPartyTurnState.bind(this));
        new StateTransition('EnterPartyTurnState', this.startState, this.partyTurnState, this.enterPartyTurnStateFromStartState.bind(this));
        new StateTransition('EnterPartyTurnState', this.playerTurnState, this.partyTurnState, this.enterPartyTurnStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterPartyTurnState', this.enemyTurnState, this.partyTurnState, this.enterPartyTurnStateFromEnemyTurnState.bind(this));
        new StateTransition('EnterPrePlayerTurnState', this.partyTurnState, this.prePlayerTurnState, this.enterPrePlayerTurnStateFromPartyTurnState.bind(this));
        new StateTransition('EnterPrePlayerTurnState', this.playerTurnState, this.prePlayerTurnState, this.enterPrePlayerTurnStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterPrePlayerTurnState', this.selectPlayerState, this.prePlayerTurnState, this.enterPrePlayerTurnStateFromSelectPlayerState.bind(this));
        new StateTransition('EnterPrePlayerTurnState', this.selectEnemyState, this.prePlayerTurnState, this.enterPrePlayerTurnStateFromSelectEnemyState.bind(this));
        new StateTransition('EnterPrePlayerTurnState', this.selectDefendState, this.prePlayerTurnState, this.enterPrePlayerTurnStateFromSelectDefendState.bind(this));
        new StateTransition('EnterPrePlayerTurnState', this.prePlayerTurnState, this.prePlayerTurnState, this.enterPrePlayerTurnStateFromPrePlayerTurnState.bind(this));
        new StateTransition('EnterPlayerTurnState', this.prePlayerTurnState, this.playerTurnState, this.enterPlayerTurnStateFromPrePlayerTurnState.bind(this));
        new StateTransition('EnterPlayerTurnState', this.playerTurnState, this.playerTurnState, this.enterPlayerTurnStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterPlayerTurnState', this.selectAttackState, this.playerTurnState, this.enterPlayerTurnStateFromSelectAttackState.bind(this));
        new StateTransition('EnterPlayerTurnState', this.selectTechState, this.playerTurnState, this.enterPlayerTurnStateFromSelectTechState.bind(this));
        new StateTransition('EnterPlayerTurnState', this.selectItemState, this.playerTurnState, this.enterPlayerTurnStateFromSelectItemState.bind(this));
        new StateTransition('EnterSelectEnemyState', this.selectAttackState, this.selectEnemyState, this.enterSelectEnemyStateFromSelectAttackState.bind(this));
        new StateTransition('EnterSelectEnemyState', this.selectTechState, this.selectEnemyState, this.enterSelectEnemyStateFromSelectTechState.bind(this));
        new StateTransition('EnterSelectEnemyState', this.selectItemState, this.selectEnemyState, this.enterSelectEnemyStateFromSelectItemState.bind(this));
        new StateTransition('EnterSelectPlayerState', this.selectItemState, this.selectPlayerState, this.enterSelectPlayerStateFromSelectItemState.bind(this));
        new StateTransition('EnterSelectAttackState', this.playerTurnState, this.selectAttackState, this.enterSelectAttackStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterSelectAttackState', this.selectEnemyState, this.selectAttackState, this.enterSelectAttackStateFromSelectEnemyState.bind(this));
        new StateTransition('EnterSelectTechState', this.playerTurnState, this.selectTechState, this.enterSelectTechStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterSelectTechState', this.selectEnemyState, this.selectTechState, this.enterSelectTechStateFromSelectEnemyState.bind(this));
        new StateTransition('EnterSelectItemState', this.playerTurnState, this.selectItemState, this.enterSelectItemStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterSelectItemState', this.selectEnemyState, this.selectItemState, this.enterSelectItemStateFromSelectEnemyState.bind(this));
        new StateTransition('EnterSelectItemState', this.selectPlayerState, this.selectItemState, this.enterSelectItemStateFromSelectPlayerState.bind(this));
        new StateTransition('EnterSelectDefendState', this.playerTurnState, this.selectDefendState, this.enterSelectDefendStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterSelectRunState', this.partyTurnState, this.selectRunState, this.enterSelectRunState.bind(this));
        new StateTransition('EnterEnemyTurnState', this.selectRunState, this.enemyTurnState, this.enterEnemyTurnStateFromSelectRunState.bind(this));
        new StateTransition('EnterEnemyTurnState', this.prePlayerTurnState, this.enemyTurnState, this.enterEnemyTurnStateFromPlayerTurnState.bind(this));
        new StateTransition('EnterBattleResultsState', this.prePlayerTurnState, this.battleResultsState, this.enterBattleResultsStateFromPrePlayerTurnState.bind(this));
        new StateTransition('EnterBattleResultsState', this.enemyTurnState, this.battleResultsState, this.enterBattleResultsStateFromEnemyTurnState.bind(this));

        this.stateMachine = stateMachine;
        this.stateMachine.start();

        this.scene.events.emit('Action', 'EnterPartyTurnState');
        // this.scene.events.emit('Action', 'EnterPrePlayerTurnState'); // TODO: delete later
    }

    resumeCallback() {
        super.resumeCallback();
    }

    getCurrChar() {
        return this.party.charList[this.playerTurnCharIdx];
    }

    getCurrCharKey() {
        return this.getCurrChar() ? this.getCurrChar().getKey() : null;
    }

    enterStartState() {
        this.playerTurnCharIdx = 0;
        this.playerTurnSelectedEntry = null;
        this.playerTurnSelectedEntryIndex = null;
        this.playerTurnSelectedTargets = [];
        this.playerTurnSelectedTargetIndex = null;
        this.party = {
            charList: [],
            items: [],
        };
        this.mob = {
            monsterList: []
        };

        this.gameSettings = this.scene.registry.get('gameSettings');
        this.partyRegData = this.scene.registry.get('party');
        this.inventoryItemsRegData = this.scene.registry.get('inventoryItems');
        this.stringDB = this.scene.registry.get('stringDatabase');
        this.dialogDB = this.scene.registry.get('dialogDatabase');
        this.itemDB = this.scene.registry.get('itemDatabase');
        this.gearDB = this.scene.registry.get('gearDatabase');
        this.techDB = this.scene.registry.get('techniqueDatabase');
        this.effectDB = this.scene.registry.get('effectDatabase');
        this.monsterDB = this.scene.registry.get('monsterDatabase');
        this.monsterMobDB = this.scene.registry.get('monsterMobDatabase');

        this.party.charList = this.partyRegData.getPartyCharObjects(this.stringDB);
        this.party.items = this.inventoryItemsRegData.getInventoryItemObjects(this.stringDB, this.itemDB);
        this.mob.data = this.monsterMobDB.getDataByKey(this.data.monsterMob);
        this.mob.monsterList = this.monsterMobDB.getMonsterObjects(this.data.monsterMob, true, this.stringDB, this.monsterDB);
        this.mob.monsterImageList = this.monsterMobDB.getMonsterImageObjectsFromMonsterObjects(this.mob.monsterList, this.monsterDB);


        let entries;

        this.monsterImageListWindow = new BaseWindow('monsterImageListWindow', this.scene, 0, 0, 8, 20, {
            doNotRenderBackground: true
        });
        this.addWindow(this.monsterImageListWindow);

        this.monsterImageListComp = null;
        entries = this.createEntries(this.mob.monsterImageList);
        let nrOfMonsters = this.mob.monsterList.length;
        if (nrOfMonsters === 1) {
            entries[0].renderer.setRenderWidthInTiles(4);
            this.monsterImageListComp = new ListComponent('monsterImageListComp', this.scene, this, this.tileSizeMenu * 10, this.tileSizeMenu * 4, 1, 1, entries, null, 'horizontal', {
                width: 0,
                height: 0
            }, true);
        } else if (nrOfMonsters === 2) {
            entries[0].renderer.setRenderWidthInTiles(9);
            entries[1].renderer.setRenderWidthInTiles(9);
            this.monsterImageListComp = new ListComponent('monsterImageListComp', this.scene, this, this.tileSizeMenu * 5, this.tileSizeMenu * 4, 2, 1, entries, null, 'horizontal', {
                width: 0,
                height: 0
            }, true);
        } else if (nrOfMonsters === 3) {
            entries[0].renderer.setRenderWidthInTiles(6.5);
            entries[1].renderer.setRenderWidthInTiles(6.5);
            entries[2].renderer.setRenderWidthInTiles(6.5);
            this.monsterImageListComp = new ListComponent('monsterImageListComp', this.scene, this, this.tileSizeMenu * 3.5, this.tileSizeMenu * 4, 3, 1, entries, null, 'horizontal', {
                width: 0,
                height: 0
            }, true);
        }
        this.monsterImageListComp.setShowCursor1WhileNotActive(false);
        // this.monsterImageListComp.setSelectAllMode(true); //DEBUG: for debug only
        this.monsterImageListWindow.addComponent(this.monsterImageListComp);

        this.battleDialogWindow = new BaseWindow('battleDialogWindow', this.scene, 0, 8, 20, 10);
        this.addWindow(this.battleDialogWindow);
        this.battleDialogComp = new DialogComponent('battleDialogComp', this.scene, this, 8);
        this.battleDialogWindow.addComponent(this.battleDialogComp);
        this.battleDialogWindow.setWindowVisible(false);

        this.draw();
    }

    enterStartStateFromPartyTurnState() {

    }

    enterPartyTurnState() {
        this.playerTurnCharIdx = 0;
        this.party.charList.forEach((char) => {
            if (char.getBattleState() !== 'dead') {
                char.setBattleState('wait');
            }
        });

        //DEBUG: debug only, DELETE!!!!!!
        if (!this.debug1) {
            this.debug1 = true;
            // this.party.charList[0].inflictStatusAilment('status-poison-01');

            //this.party.charList.forEach(char => char.setBattleState('dead'));
            // this.party.charList[1].setIsDead(true);
            // this.playerTurnCharIdx = 4;

            // this.enterBattleResultsState();
        }


        let entries;

        this.partyTurnMonsterListWindow = new BaseWindow('partyTurnMonsterListWindow', this.scene, 10, 7, 10, 8);
        this.addWindow(this.partyTurnMonsterListWindow);

        let livingMonstersList = this.mob.monsterList.map((monster) => {
            return !monster.getIsDead() ? monster : 'empty';
        });
        entries = this.createEntries(livingMonstersList);
        this.partyTurnMonsterListComp = new ListComponent('partyTurnMonsterListComp', this.scene, this, this.tileSizeMenu * 1.5, this.tileSizeMenu * 1, 1, 3, entries);
        this.partyTurnMonsterListWindow.addComponent(this.partyTurnMonsterListComp);


        this.partyTurnCharListWindow = new CharListWindow('partyTurnCharListWindow', this.scene, 0, 8, 11, 10);
        this.addWindow(this.partyTurnCharListWindow);

        entries = this.createEntries(this.party.charList);
        this.partyTurnCharListComp = new ListComponent('partyTurnCharListComp', this.scene, this, this.tileSizeMenu * 1, this.tileSizeMenu * 1, 1, 4, entries, null, null, {
            width: 0,
            height: 0
        });
        this.partyTurnCharListWindow.addComponent(this.partyTurnCharListComp);


        this.partyTurnActionListWindow = new BaseWindow('partyTurnActionListWindow', this.scene, 10, 13, 9, 5);
        this.addWindow(this.partyTurnActionListWindow);

        let actionChoiceFight = this.stringDB.getCustomStringByKey('string-battle-action-fight');
        actionChoiceFight.toUpperCaseName();
        let actionChoiceRun = this.stringDB.getCustomStringByKey('string-battle-action-run');
        actionChoiceRun.toUpperCaseName();
        entries = this.createEntries([actionChoiceFight, actionChoiceRun]);
        this.partyTurnActionListComp = new ListComponent('partyTurnActionListComp', this.scene, this, this.tileSizeMenu * 2, this.tileSizeMenu * 1, 1, 2, entries);
        this.partyTurnActionListComp.setShowCursor1WhileNotActive(false);
        this.partyTurnActionListWindow.addComponent(this.partyTurnActionListComp);
        this.partyTurnActionListWindow.setActiveComponent(this.partyTurnActionListComp);

        this.setActiveWindow(this.partyTurnActionListWindow);

        this.draw();
    }

    enterPartyTurnStateFromStartState() {
        this.enterPartyTurnState();
    }

    enterPartyTurnStateFromPlayerTurnState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.playerTurnActionListWindow);

        this.enterPartyTurnState();
    }

    enterPartyTurnStateFromEnemyTurnState() {
        // TODO: remove windows

        this.enterPartyTurnState();
    }

    enterPrePlayerTurnState() {
        //DEBUG: debug only, DELETE!!!!!!
        if (!this.debug2) {
            this.debug2 = true;
            // this.getCurrChar().inflictStatusAilment('status-sleep-01');
            // this.getCurrChar().inflictStatusAilment('status-poison-01');
            // this.getCurrChar().inflictStatusAilment('status-deprotect-01');
            // this.getCurrChar().inflictStatusAilment('status-protect-01');

            //this.party.charList.forEach(char => char.setBattleState('dead'));
            // this.party.charList[0].setIsDead(true);
            // this.playerTurnCharIdx = 4;
            // this.scene.events.emit('Action', 'EnterBattleResultsState');
            // return;
        }

        if (this.areAllEnemiesDead()) {
            this.scene.events.emit('Action', 'EnterBattleResultsState');
            return;
        }


        if (this.isWholePartyDead()) {
            let config = {
                animateText: true,
            };
            this.battleDialogComp.reset(config);
            this.setActiveWindow(this.battleDialogWindow);
            this.setActiveComponent(this.battleDialogComp);
            this.battleDialogWindow.setWindowVisible(true);

            let newText1 = this.dialogDB.getDialogStringByKey('dlg-0023');

            let callback = () => {
                setTimeout(() => {}, 1000);
            };
            let data = [];
            data.push({
                options: {
                    indent: 0,
                },
                text: newText1.getText(),
            }, {
                text: '\n',
            });

            this.battleDialogComp.parseDialogData(data, callback.bind(this));

            this.draw();

            let callback2 = () => {
                setTimeout(() => {
                    this.battleDialogWindow.setWindowVisible(false);
                    this.setActiveWindow(null);

                    this.quitToTitleScreen();
                }, 1000);
            };
            this.battleDialogComp.startDrawingText(callback2, true);
            this.scene.changeBackgroundMusicAndPlay('bgm-game-over');

            return;
        }
        if (this.getCurrChar() && this.getCurrChar().getBattleState() === 'dead') {
            this.playerTurnCharIdx++;
            this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
            return;
        }
        if (this.playerTurnCharIdx >= this.partySize) {
            this.scene.events.emit('Action', 'EnterEnemyTurnState');
            return;
        }

        this.draw();

        if (this.getCurrChar() && this.getCurrChar().getBattleState() !== 'dead') {

            this.getCurrChar().decrementStatusAilmentsTurnCounts();

            if (this.getCurrChar().getStatusAilments().length > 0) {
                let appliedStatusAilmentResults = this.getCurrChar().applyCurrentStatusAilmentsForThisTurn();
                let skipRestOfDialogBecauseDeathByAilment = false;

                let config = {
                    animateText: true,
                };
                this.battleDialogComp.reset(config);
                this.setActiveWindow(this.battleDialogWindow);
                this.setActiveComponent(this.battleDialogComp);
                this.battleDialogWindow.setWindowVisible(true);

                appliedStatusAilmentResults.every(result => {
                    let statusKey = result.statusKey;

                    let newText1 = null;
                    if (result.type === 'debuff') {
                        newText1 = this.dialogDB.getDialogStringByKey('dlg-0021');
                    } else if (result.type === 'buff') {
                        newText1 = this.dialogDB.getDialogStringByKey('dlg-0024');
                    }
                    let newText2 = this.stringDB.getCustomStringByKey(statusKey);

                    let data = [];
                    data.push({
                        options: {
                            indent: 0,
                        },
                        text: this.getCurrChar().getFullName() + ' ' + newText1.getText(),
                    }, {
                        options: {
                            indent: 1
                        },
                        text: newText2.getShortName().toUpperCase(),
                    }, {
                        text: '\n',
                    });

                    if (statusKey === 'status-poison-01') {
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0013');
                        let damage = result.damage;

                        data.push({
                            options: {
                                indent: 1
                            },
                            text: damage + ' ' + text1.getText(),
                        });
                    } else if (statusKey === 'status-sleep-01') {
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0022');

                        data.push({
                            options: {},
                            text: this.getCurrChar().getFullName() + ' ' + text1.getText(),
                        });
                    } else if (statusKey === 'status-deprotect-01') {
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0025');
                        let text2 = this.stringDB.getCustomStringByKey('string-stat-defense');
                        let text3 = this.dialogDB.getDialogStringByKey('dlg-0027');
                        let value = result.value * 100;

                        data.push({
                            options: {},
                            text: text2.getShortName().toUpperCase() + ' ' + text1.getText(),
                        }, {
                            options: {
                                indent: 1
                            },
                            text: value + text3.getText(),
                        });
                    } else if (statusKey === 'status-protect-01') {
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0026');
                        let text2 = this.stringDB.getCustomStringByKey('string-stat-defense');
                        let text3 = this.dialogDB.getDialogStringByKey('dlg-0027');
                        let value = result.value * 100;

                        data.push({
                            options: {},
                            text: text2.getShortName().toUpperCase() + ' ' + text1.getText(),
                        }, {
                            options: {
                                indent: 1
                            },
                            text: value + text3.getText(),
                        });
                    }

                    data.push({
                        text: '\n',
                    });


                    if (result.killedActor === true) {
                        skipRestOfDialogBecauseDeathByAilment = true;
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0014');

                        data.push({
                            options: {},
                            text: this.getCurrChar().getFullName() + ' ' + text1.getText(),
                        });
                        data.push({
                            text: '\n',
                        });

                        if (this.isWholePartyDead()) {
                            let text1 = this.dialogDB.getDialogStringByKey('dlg-0023');

                            data.push({
                                options: {},
                                text: text1.getText() + '\n',
                            });
                        }
                    }


                    let callback = () => {
                        setTimeout(() => {}, 1000);
                    };

                    this.battleDialogComp.parseDialogData(data, callback.bind(this));

                    this.draw();

                    if (skipRestOfDialogBecauseDeathByAilment) {
                        return false;
                    } else {
                        return true;
                    }
                });

                let callback = () => {
                    setTimeout(() => {
                        this.battleDialogWindow.setWindowVisible(false);
                        this.setActiveWindow(null);


                        if (this.isWholePartyDead()) {
                            this.quitToTitleScreen();
                        } else if (this.getCurrChar().getBattleState() === 'dead') {
                            this.playerTurnCharIdx++;
                            this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
                        } else if (this.getCurrChar().isAfflictedBy('status-sleep-01')) {
                            this.playerTurnCharIdx++;
                            this.getCurrChar().setBattleState('wait');

                            this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
                        } else {
                            this.scene.events.emit('Action', 'EnterPlayerTurnState');
                        }
                    }, 1000);
                };
                let partyDead = false;
                if (this.isWholePartyDead()) {
                    this.scene.changeBackgroundMusicAndPlay('bgm-game-over');
                    partyDead = true;
                }
                this.battleDialogComp.startDrawingText(callback, partyDead);
            } else {
                this.scene.events.emit('Action', 'EnterPlayerTurnState');
            }
        }
    }

    enterPrePlayerTurnStateFromPartyTurnState() {
        this.removeWindow(this.partyTurnMonsterListWindow);
        this.removeWindow(this.partyTurnCharListWindow);
        this.removeWindow(this.partyTurnActionListWindow);

        this.enterPrePlayerTurnState();
    }

    enterPrePlayerTurnStateFromPrePlayerTurnState() {

        this.enterPrePlayerTurnState();
    }

    enterPrePlayerTurnStateFromPlayerTurnState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.playerTurnActionListWindow);

        this.enterPrePlayerTurnState();
    }

    enterPrePlayerTurnStateFromSelectPlayerState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.playerTurnActionListWindow);
        this.removeWindow(this.selectItemStateItemListWindow);

        this.enterPrePlayerTurnState();
    }

    enterPrePlayerTurnStateFromSelectEnemyState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.playerTurnActionListWindow);
        this.removeWindow(this.selectAttackStateWeaponListWindow);
        this.removeWindow(this.selectItemStateItemListWindow);
        this.removeWindow(this.selectTechStateTechListWindow);

        this.enterPrePlayerTurnState();
    }

    enterPrePlayerTurnStateFromSelectDefendState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.playerTurnActionListWindow);
        this.removeWindow(this.playerTurnActionListWindow);

        this.enterPrePlayerTurnState();
    }

    enterPlayerTurnState() {
        this.getCurrChar().setBattleState('choose');

        let entries;
        this.playerTurnCharListWindow = new BaseWindow('playerTurnCharListWindow', this.scene, 0, 8, 9, 7);
        this.addWindow(this.playerTurnCharListWindow);

        entries = this.createEntries([this.party.charList[this.playerTurnCharIdx]]);
        this.playerTurnCharListComp = new ListComponent('playerTurnCharListComp', this.scene, this, this.tileSizeMenu * 1, this.tileSizeMenu * 1, 1, 1, entries);
        this.playerTurnCharListWindow.addComponent(this.playerTurnCharListComp);


        if (isUndefinedOrNull(this.playerTurnActionListWindow)) {
            this.playerTurnActionListWindow = new BaseWindow('playerTurnActionListWindow', this.scene, 8, 9, 9, 9);
            this.addWindow(this.playerTurnActionListWindow);

            let actionChoiceAttack = this.stringDB.getCustomStringByKey('string-battle-action-attack');
            actionChoiceAttack.toUpperCaseName();
            let actionChoiceTech = this.stringDB.getCustomStringByKey('string-battle-action-technique');
            actionChoiceTech.toUpperCaseName();
            let actionChoiceItem = this.stringDB.getCustomStringByKey('string-battle-action-item');
            actionChoiceItem.toUpperCaseName();
            let actionChoiceDefend = this.stringDB.getCustomStringByKey('string-battle-action-defend');
            actionChoiceDefend.toUpperCaseName();
            entries = this.createEntries([actionChoiceAttack, actionChoiceTech, actionChoiceItem, actionChoiceDefend]);
            this.playerTurnActionListComp = new ListComponent('playerTurnActionListComp', this.scene, this, this.tileSizeMenu * 2, this.tileSizeMenu * 1, 1, 4, entries);
            this.playerTurnActionListComp.setShowCursor1WhileNotActive(false);

            this.playerTurnActionListWindow.addComponent(this.playerTurnActionListComp);
        } else {
            this.playerTurnActionListWindow.setWindowVisible(true);
        }
        this.playerTurnActionListWindow.setActiveComponent(this.playerTurnActionListComp);

        this.setActiveWindow(this.playerTurnActionListWindow);

        this.draw();
    }

    enterPlayerTurnStateFromPlayerTurnState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.playerTurnActionListWindow);

        this.enterPlayerTurnState();
    }

    enterPlayerTurnStateFromPrePlayerTurnState() {
        this.enterPlayerTurnState();
    }

    enterPlayerTurnStateFromPartyTurnState() {
        this.removeWindow(this.partyTurnMonsterListWindow);
        this.removeWindow(this.partyTurnCharListWindow);
        this.removeWindow(this.partyTurnActionListWindow);

        this.enterPlayerTurnState();
    }

    enterPlayerTurnStateFromSelectAttackState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.selectAttackStateWeaponListWindow);

        this.enterPlayerTurnState();
    }

    enterPlayerTurnStateFromSelectTechState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.selectTechStateTechListWindow);

        this.enterPlayerTurnState();
    }

    enterPlayerTurnStateFromSelectItemState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.selectItemStateItemListWindow);

        this.enterPlayerTurnState();
    }

    enterSelectAttackState() {
        let entries;

        this.selectAttackStateWeaponListWindow = new BaseWindow('selectAttackStateWeaponListWindow', this.scene, 8, 8, 12, 10);
        this.addWindow(this.selectAttackStateWeaponListWindow);

        let currChar = this.getCurrChar();
        let currCharKey = this.getCurrCharKey();
        let charWeapons = [];
        charWeapons = this.partyRegData.getEquippedWeaponObjects(currCharKey, this.stringDB, this.gearDB)
            .filter(el => {
                return el !== 'none';
            });
        if (charWeapons.length <= 0) charWeapons.push('none');
        entries = this.createEntries(charWeapons);
        this.selectAttackStateWeaponListComp = new ListComponent('selectAttackStateWeaponListComp', this.scene, this, this.tileSizeMenu * 2, this.tileSizeMenu, 1, 4, entries);
        this.selectAttackStateWeaponListComp.setShowCursor1WhileNotActive(false);
        this.selectAttackStateWeaponListWindow.addComponent(this.selectAttackStateWeaponListComp);
        this.selectAttackStateWeaponListWindow.setActiveComponent(this.selectAttackStateWeaponListComp);


        this.setActiveWindow(this.selectAttackStateWeaponListWindow);

        this.draw();
    }

    enterSelectAttackStateFromPlayerTurnState() {
        this.playerTurnActionListWindow.setWindowVisible(false);

        this.enterSelectAttackState();
    }

    enterSelectAttackStateFromSelectEnemyState() {
        this.removeWindow(this.selectEnemyMonsterListWindow);
        this.setActiveWindow(this.selectAttackStateWeaponListWindow);

        this.draw();
    }

    enterSelectTechState() {
        let entries;

        this.selectTechStateTechListWindow = new BaseWindow('selectTechStateTechListWindow', this.scene, 8, 8, 12, 10);
        this.addWindow(this.selectTechStateTechListWindow);

        let tpLabel = this.stringDB.getCustomStringByKey('string-tech-point');
        tpLabel.toUpperCaseName();
        entries = this.createEntries([tpLabel]);
        this.selectTechStateTpLabelComp = new ListComponent('selectTechStateTpLabelComp', this.scene, this, this.tileSizeMenu * 9, this.tileSizeMenu, 1, 1, entries);
        this.selectTechStateTechListWindow.addComponent(this.selectTechStateTpLabelComp);

        let currChar = this.getCurrChar();
        let currCharKey = currChar.getKey();
        let charTechs = this.partyRegData.getUnlockedTechniquesObjects(currCharKey, this.stringDB, this.techDB);
        entries = this.createEntries(charTechs);
        this.selectTechStateTechListComp = new ListComponent('selectTechStateTechListComp', this.scene, this, this.tileSizeMenu * 2, this.tileSizeMenu * 3, 1, 3, entries);
        this.selectTechStateTechListComp.setShowCursor1WhileNotActive(false);
        this.selectTechStateTechListWindow.addComponent(this.selectTechStateTechListComp);
        this.selectTechStateTechListWindow.setActiveComponent(this.selectTechStateTechListComp);

        this.setActiveWindow(this.selectTechStateTechListWindow);

        this.draw();
    }

    enterSelectTechStateFromPlayerTurnState() {
        this.playerTurnActionListWindow.setWindowVisible(false);

        this.enterSelectTechState();
    }

    enterSelectTechStateFromSelectEnemyState() {

        this.removeWindow(this.selectEnemyMonsterListWindow);
        this.setActiveWindow(this.selectTechStateTechListWindow);

        this.draw();
    }

    enterSelectItemState() {
        let entries;

        this.selectItemStateItemListWindow = new BaseWindow('selectItemStateItemListWindow', this.scene, 8, 8, 12, 10);
        this.addWindow(this.selectItemStateItemListWindow);

        entries = this.createEntries(this.party.items);
        this.selectItemStateItemListComp = new ListComponent('selectItemStateItemListComp', this.scene, this,
            this.tileSizeMenu * 2 - 4, this.tileSizeMenu * 2, 1, 4, entries);
        this.selectItemStateItemListComp.setShowCursor1WhileNotActive(false);
        this.selectItemStateItemListComp.setAlwaysHideScrollArrows(false);
        this.selectItemStateItemListWindow.addComponent(this.selectItemStateItemListComp);
        this.selectItemStateItemListWindow.setActiveComponent(this.selectItemStateItemListComp);

        this.setActiveWindow(this.selectItemStateItemListWindow);

        this.draw();
    }

    enterSelectItemStateFromPlayerTurnState() {
        this.playerTurnActionListWindow.setWindowVisible(false);

        this.enterSelectItemState();
    }

    enterSelectItemStateFromSelectEnemyState() {
        this.removeWindow(this.selectEnemyMonsterListWindow);
        this.setActiveWindow(this.selectItemStateItemListWindow);

        this.draw();
    }

    enterSelectItemStateFromSelectPlayerState() {
        this.removeWindow(this.selectPlayerCharListWindow);
        this.setActiveWindow(this.selectItemStateItemListWindow);

        this.draw();
    }

    enterSelectDefendState() {
        this.getCurrChar().useDefense();


        let text = this.dialogDB.getDialogStringByKey('dlg-0010');

        let config = {
            animateText: true,
        };
        let data = [{
            options: text.getOptions(),
            text: '' + this.getCurrChar().getFullName() + ' ' + text.getText(),
        }];

        let callback = function () {
            setTimeout(() => {
                this.battleDialogWindow.setWindowVisible(false);
                this.setActiveWindow(null);

                this.playerTurnCharIdx++;

                this.scene.events.emit('Action', 'EnterPrePlayerTurnState');

                this.draw();
            }, 1000);
        };

        this.setActiveWindow(this.battleDialogWindow);
        this.setActiveComponent(this.battleDialogComp);
        this.battleDialogWindow.setWindowVisible(true);
        this.battleDialogComp.reset(config);
        this.battleDialogComp.parseDialogData(data, callback.bind(this));
        this.battleDialogComp.startDrawingText();

        this.draw();
    }

    enterSelectDefendStateFromPlayerTurnState() {
        // this.removeWindow(this.playerTurnActionListWindow);
        this.playerTurnActionListWindow.setWindowVisible(false);

        this.enterSelectDefendState();
    }

    enterSelectEnemyState() {
        let entries;

        this.selectEnemyMonsterListWindow = new BaseWindow('selectEnemyMonsterListWindow', this.scene, 0, 7, 10, 8);
        this.addWindow(this.selectEnemyMonsterListWindow);

        let livingMonstersList = this.mob.monsterList.map((monster) => {
            return !monster.getIsDead() ? monster : 'empty';
        });
        entries = this.createEntries(livingMonstersList);
        this.selectEnemyMonsterListComp = new ListComponent('selectEnemyMonsterListComp', this.scene, this, this.tileSizeMenu * 2, this.tileSizeMenu * 1, 1, 3, entries);
        this.selectEnemyMonsterListWindow.addComponent(this.selectEnemyMonsterListComp);

        this.monsterImageListWindow.setActiveComponent(this.monsterImageListComp);
        this.setActiveWindow(this.monsterImageListWindow);

        this.draw();
    }

    enterSelectEnemyStateFromSelectAttackState() {
        this.enterSelectEnemyState();
    }

    enterSelectEnemyStateFromSelectTechState() {
        this.enterSelectEnemyState();
    }

    enterSelectEnemyStateFromSelectItemState() {
        this.enterSelectEnemyState();
    }

    enterSelectPlayerState() {
        let entries;

        this.selectPlayerCharListWindow = new CharListWindow('selectPlayerCharListWindow', this.scene, 0, 7, 11, 10);
        this.addWindow(this.selectPlayerCharListWindow);

        entries = this.createEntries(this.party.charList);
        this.selectPlayerCharListComp = new ListComponent('selectPlayerCharListComp', this.scene, this, this.tileSizeMenu * 1, this.tileSizeMenu * 1, 1, 4, entries, null, null, {
            width: 0,
            height: 0
        }, true);
        this.selectPlayerCharListComp.setCursorOffset(6, 0);
        this.selectPlayerCharListWindow.addComponent(this.selectPlayerCharListComp);
        this.selectPlayerCharListWindow.setActiveComponent(this.selectPlayerCharListComp);

        this.setActiveWindow(this.selectPlayerCharListWindow);

        this.draw();
    }

    enterSelectPlayerStateFromSelectItemState() {
        this.enterSelectPlayerState();
    }

    enterSelectRunState() {
        let rand = Math.random();

        this.removeWindow(this.partyTurnCharListWindow);
        this.removeWindow(this.partyTurnActionListWindow);
        this.removeWindow(this.partyTurnMonsterListWindow);

        this.setActiveWindow(this.battleDialogWindow);
        this.setActiveComponent(this.battleDialogComp);
        this.battleDialogWindow.setWindowVisible(true);

        let runText = '';
        if (rand <= this.gameSettings.battleEscapeProbability) {
            runText = this.dialogDB.getDialogStringByKey('dlg-0007');

            let config = {
                animateText: true,
            };
            let data = [{
                options: runText.getOptions(),
                text: runText.getText(),
            }];

            let callback = function () {
                setTimeout(this.closeBattleMenu.bind(this), 1000);
            };

            this.battleDialogComp.reset(config);
            this.battleDialogComp.parseDialogData(data, callback.bind(this));
            this.battleDialogComp.startDrawingText();

        } else {
            runText = this.dialogDB.getDialogStringByKey('dlg-0008');
            let runText2 = this.dialogDB.getDialogStringByKey('dlg-0009');

            let config = {
                animateText: true,
            };
            let data = [{
                    options: runText.getOptions(),
                    text: runText.getText(),
                }, {
                    text: '\n',
                },
                {
                    options: runText2.getOptions(),
                    text: runText2.getText(),
                }, {
                    text: '\n',
                },
            ];

            let callback = function () {
                setTimeout(() => {
                    this.scene.events.emit('Action', 'EnterEnemyTurnState');
                }, 1000);
            };

            this.battleDialogComp.reset(config);
            this.battleDialogComp.parseDialogData(data, callback.bind(this));
            this.battleDialogComp.startDrawingText();
        }

        this.draw();
    }

    enterEnemyTurnState() {
        // TODO: implement EnemyTurnState       

        let mob = this.mob.monsterList;
        let party = this.party.charList;

        //DEBUG: debug only, DELETE!!!!!!
        if (!this.debug3) {
            this.debug3 = true;
            // mob[0].inflictStatusAilment('status-sleep-01');
            // mob[1].inflictStatusAilment('status-poison-01');
            // this.getCurrChar().inflictStatusAilment('status-deprotect-01');
            // this.getCurrChar().inflictStatusAilment('status-protect-01');

            //this.party.charList.forEach(char => char.setBattleState('dead'));
            // this.party.charList[0].setIsDead(true);
            // this.playerTurnCharIdx = 4;
        }

        let config = {
            animateText: true,
        };
        this.battleDialogComp.reset(config);
        this.setActiveWindow(this.battleDialogWindow);
        this.setActiveComponent(this.battleDialogComp);
        this.battleDialogWindow.setWindowVisible(true);

        this.draw();

        for (let i = 0; i < mob.length; i++) {
            let currEnemy = mob[i];
            let currTarget = null;
            if (currEnemy.getIsDead()) continue;

            currTarget = currEnemy.chooseNewTarget(party);
            let chosenAction = currEnemy.chooseNewAction();
            let chosenActionKey = chosenAction.effect;
            let chosenActionPower = chosenAction.power;

            let afflictedWithSleep = false;

            currEnemy.decrementStatusAilmentsTurnCounts();

            if (currEnemy.getStatusAilments().length > 0) {
                let appliedStatusAilmentResults = currEnemy.applyCurrentStatusAilmentsForThisTurn();
                let skipRestOfDialogBecauseDeathByAilment = false;

                appliedStatusAilmentResults.every((result, index) => {
                    let statusKey = result.statusKey;

                    let newText1 = null;
                    if (result.type === 'debuff') {
                        newText1 = this.dialogDB.getDialogStringByKey('dlg-0021');
                    } else if (result.type === 'buff') {
                        newText1 = this.dialogDB.getDialogStringByKey('dlg-0024');
                    }
                    let newText2 = this.stringDB.getCustomStringByKey(statusKey);

                    let data = [];
                    data.push({
                        options: {
                            indent: 0,
                        },
                        text: currEnemy.getFullName() + ' ' + newText1.getText(),
                    }, {
                        options: {
                            indent: 1
                        },
                        text: newText2.getShortName().toUpperCase(),
                    }, {
                        text: '\n',
                    });

                    if (statusKey === 'status-poison-01') {
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0013');
                        let damage = result.damage;

                        data.push({
                            options: {
                                indent: 1
                            },
                            text: damage + ' ' + text1.getText(),
                        });
                    } else if (statusKey === 'status-sleep-01') {
                        afflictedWithSleep = true;

                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0022');

                        data.push({
                            options: {},
                            text: currEnemy.getFullName() + ' ' + text1.getText(),
                        });
                    } else if (statusKey === 'status-deprotect-01') {
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0025');
                        let text2 = this.stringDB.getCustomStringByKey('string-stat-defense');
                        let text3 = this.dialogDB.getDialogStringByKey('dlg-0027');
                        let value = result.value * 100;

                        data.push({
                            options: {},
                            text: text2.getShortName().toUpperCase() + ' ' + text1.getText(),
                        }, {
                            options: {
                                indent: 1
                            },
                            text: value + text3.getText(),
                        });
                    } else if (statusKey === 'status-protect-01') {
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0026');
                        let text2 = this.stringDB.getCustomStringByKey('string-stat-defense');
                        let text3 = this.dialogDB.getDialogStringByKey('dlg-0027');
                        let value = result.value * 100;

                        data.push({
                            options: {},
                            text: text2.getShortName().toUpperCase() + ' ' + text1.getText(),
                        }, {
                            options: {
                                indent: 1
                            },
                            text: value + text3.getText(),
                        });
                    }

                    data.push({
                        text: '\n',
                    });

                    let data2 = [];
                    if (result.killedActor === true) {
                        skipRestOfDialogBecauseDeathByAilment = true;
                        let text1 = this.dialogDB.getDialogStringByKey('dlg-0014');

                        data2.push({
                            options: {},
                            text: currEnemy.getFullName() + ' ' + text1.getText(),
                        });
                        data2.push({
                            text: '\n',
                        });
                    }


                    let callback = () => {
                        setTimeout(() => {}, 0);
                    };

                    let callback2 = () => {
                        setTimeout((monsterIdx) => {
                            this.hideKilledMonsterImageListComp(monsterIdx);
                            this.draw();

                        }, 0, i);
                    };

                    this.battleDialogComp.parseDialogData(data, callback.bind(this));
                    if (data2.length > 0) this.battleDialogComp.parseDialogData(data2, callback2.bind(this));

                    if (skipRestOfDialogBecauseDeathByAilment) {
                        return false;
                    } else {
                        return true;
                    }
                });

                let callback = () => {
                    setTimeout(() => {
                        this.battleDialogWindow.setWindowVisible(false);
                        this.setActiveWindow(null);


                        if (this.areAllEnemiesDead()) {
                            this.scene.events.emit('Action', 'EnterBattleResultsState');
                        }
                    }, 1000);
                };
                let mobDead = false;
                if (this.areAllEnemiesDead()) {
                    mobDead = true;

                    this.battleDialogComp.startDrawingText(callback);

                    return;
                }
            }
            if (afflictedWithSleep) continue;

            if (chosenActionKey === 'attack') {
                console.log('currEnemy ' + currEnemy.getFullName() + i + ' attacks target ' + currTarget.getFullName());

                let resultObject = currEnemy.applyAttackDamageTo(currTarget, null, null, this.gameSettings);
                let resultValue = resultObject.resultValue;
                let inflictedStatusAilments = resultObject.inflictedStatusAilments;
                let targetWasKilled = currTarget.getIsDead();
                let targetWasDamaged = false;

                let resultDescription = '';
                let newText = this.dialogDB.getDialogStringByKey('dlg-0011');
                let newText2 = this.dialogDB.getDialogStringByKey('dlg-0012');
                let newText3 = '';
                let newText4 = '';

                if (resultValue > 0) {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0013').getText();
                    newText3 = resultValue + ' ' + resultDescription;

                    if (isDefinedAndNotNull(inflictedStatusAilments)) {
                        let inflictedByText = this.dialogDB.getDialogStringByKey('dlg-0032').getText();
                        let statusKey = inflictedStatusAilments[0];
                        let statusAilmentString = this.stringDB.getCustomStringByKey(statusKey).getShortName().toUpperCase();
                        newText3 += '\n' + inflictedByText + ' ' + statusAilmentString;
                    }

                    targetWasDamaged = true;
                } else if (resultValue === 0) {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0030').getText();
                    newText3 = resultDescription;

                } else {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0019').getText();
                    newText3 = Math.abs(resultValue) + ' ' + resultDescription;
                }

                let data1 = [{
                        options: newText.getOptions(),
                        text: currEnemy.getFullName() + ' ' + newText.getText(),
                    },
                    {
                        options: {
                            indent: 6,
                        },
                        text: currTarget.getFullName(),
                    },
                ];
                let data2 = [{
                        options: {
                            indent: 1,
                        },
                        text: newText3,
                    },
                    {
                        text: '\n',
                    },
                ];
                if (targetWasKilled) {
                    newText4 = this.dialogDB.getDialogStringByKey('dlg-0014');
                    data2.push({
                        text: currTarget.getFullName() + ' ' + newText4.getText()
                    }, {
                        text: '\n',
                    });
                }


                let callback1 = () => {
                    setTimeout(() => {
                        if (targetWasDamaged) {
                            this.startScreenShake();
                            //TODO: play sfx
                        }
                    }, 0);
                };
                let callback2 = () => {
                    setTimeout(() => {}, 1000);
                };

                this.battleDialogComp.parseDialogData(data1, callback1.bind(this), {
                    delay: 500
                });
                this.battleDialogComp.parseDialogData(data2, callback2.bind(this));

                this.draw();

            } else {
                //TODO: implement dialog for enemies using spells
                let targets = party.filter(char => !char.getIsDead());
                let effect = this.effectDB.getEffectObjectByKey(chosenActionKey);
                let spell = null;
                if (isDefinedAndNotNull(effect.spell)) spell = this.stringDB.getCustomStringByKey(effect.spell);
                if (isDefinedAndNotNull(chosenActionPower)) effect.power = chosenActionPower;
                let resultValue = currEnemy.applyMagicDamageTo(currTarget, effect, this.gameSettings);
                let targetWasKilled = currTarget.getIsDead();
                let targetWasDamaged = false;

                console.log('currEnemy ' + currEnemy.getFullName() + i + ' uses tech ' + spell.getFullName() + ' on target ' + currTarget.getFullName());

                let resultDescription = '';
                let newText = this.dialogDB.getDialogStringByKey('dlg-0017');
                let newText2 = this.dialogDB.getDialogStringByKey('dlg-0018');
                let newText3 = '';
                let newText4 = '';
                let data2 = [];
                let data3 = [];

                if (effect.type === 'spell') {
                    switch (effect.subType) {
                        case 'offensive':
                            if (resultValue > 0) {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0013').getText();
                                newText3 = resultValue + ' ' + resultDescription;

                                targetWasDamaged = true;
                            } else if (resultValue === 0) {
                                let element = this.stringDB.getCustomStringByKey('element-' + effect.element).getFullName().toUpperCase();
                                if (target.getElementalAffinityOf(effect.element) === 0) {
                                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0020').getText() + ' ' + element;
                                    newText3 = target.getFullName() + ' ' + resultDescription;
                                } else {
                                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0030').getText();
                                    newText3 = resultDescription;
                                }
                            } else {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0019').getText();
                                newText3 = Math.abs(resultValue) + ' ' + resultDescription;
                            }

                            break;
                        case 'heal':
                            resultDescription = this.dialogDB.getDialogStringByKey('dlg-0016').getText();
                            newText3 = resultStat + ' ' + resultDescription + ' ' + resultValue;
                            break;
                        case 'cure':
                            //not in game
                            break;
                        case 'inflict':
                            if (resultValue !== 'isImmune') {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0032').getText();
                                targetWasDamaged = true;
                            } else {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0033').getText();
                            }
                            let statusKey = effect.statusAilment;
                            let statusString = this.stringDB.getCustomStringByKey(statusKey).getShortName().toUpperCase();
                            newText3 = resultDescription + ' ' + statusString;

                            break;
                    }


                    let data1 = [{
                            options: newText.getOptions(),
                            text: currEnemy.getFullName(),
                        },
                        {
                            options: {
                                indent: 1,
                            },
                            text: newText.getText() + ' ' + spell.getFullName(),
                        }, {
                            text: '\n',
                        }
                    ];

                    data1.push({
                        options: {
                            indent: 1,
                        },
                        text: newText2.getText() + ' ' + currTarget.getFullName(),
                    });

                    data2.push({
                        options: {
                            indent: 1,
                        },
                        text: newText3,
                    }, {
                        text: '\n',
                    });


                    if (targetWasKilled) {
                        newText4 = this.dialogDB.getDialogStringByKey('dlg-0014');
                        data3.push({
                            text: currTarget.getFullName() + ' ' + newText4.getText()
                        }, {
                            text: '\n',
                        });
                    }

                    let callback1 = () => {
                        setTimeout(() => {
                            if (targetWasDamaged) {
                                this.startScreenShake();
                                //TODO: play sfx
                            }
                        }, 0);
                    };
                    let callback2 = () => {
                        setTimeout(() => {}, 1000);
                    };

                    let callback3 = () => {
                        setTimeout(() => {}, 0);
                    };

                    this.battleDialogComp.parseDialogData(data1, callback1.bind(this), {
                        delay: 500
                    });
                    this.battleDialogComp.parseDialogData(data2, callback2.bind(this));
                    if (data3.length > 0) this.battleDialogComp.parseDialogData(data3, callback3.bind(this));

                    this.draw();
                }

                //TODO: check if party already dead
                if (this.isWholePartyDead()) {
                    let newText1 = this.dialogDB.getDialogStringByKey('dlg-0023');

                    let data = [];

                    let callback = () => {
                        setTimeout(() => {
                            this.scene.changeBackgroundMusicAndPlay('bgm-game-over');
                        }, 0);
                    };

                    data.push({
                        options: {
                            indent: 0,
                        },
                        text: '',
                    });

                    this.battleDialogComp.parseDialogData(data, callback.bind(this));

                    let callback1 = () => {
                        setTimeout(() => {}, 1000);
                    };

                    data = [];
                    data.push({
                        options: {
                            indent: 0,
                        },
                        text: newText1.getText(),
                    }, {
                        text: '\n',
                    }, {
                        text: '\n',
                    });

                    this.battleDialogComp.parseDialogData(data, callback1.bind(this));

                    this.draw();

                    let callback2 = () => {
                        setTimeout(() => {
                            this.battleDialogWindow.setWindowVisible(false);
                            this.setActiveWindow(null);

                            this.quitToTitleScreen();
                        }, 1000);
                    };
                    this.battleDialogComp.startDrawingText(callback2, true);

                    return;
                }
            }
        }

        let callback = () => {
            setTimeout(() => {
                this.updateMonsterImageListComp();

                this.battleDialogWindow.setWindowVisible(false);
                this.setActiveWindow(null);

                console.log('*** enemies executed their turn! ***');
                this.scene.events.emit('Action', 'EnterPartyTurnState');
            }, 1000);
        };
        this.battleDialogComp.startDrawingText(callback);
    }

    enterEnemyTurnStateFromSelectRunState() {
        this.battleDialogWindow.setWindowVisible(false);
        this.setActiveWindow(null);

        this.enterEnemyTurnState();
    }

    enterEnemyTurnStateFromPlayerTurnState() {
        this.enterEnemyTurnState();
    }

    enterBattleResultsState() {
        //TODO: clean up and save relevant persistent data

        //TODO: (depending on my approach for buffs/debuffs) remove all status ailments so potential buffs/debuffs are removed from chars before storing them to db
        this.removeAllStatusAilmentsOfParty();

        this.partyRegData.setCharsFromCharObjects(this.party.charList);
        this.inventoryItemsRegData.setDataFromItemObjects(this.party.items);

        const filterDropData = (drop) => {
            const rand = randomNumber();
            const prob = drop.chance || -1;

            return isRandomValueInRange(rand, 0, prob);
        };
        const createItemObjectsFromDropData = (drop) => {
            const key = drop.item;
            const quantity = drop.quantity;
            const items = [];
            const item = this.itemDB.getItemObjectByKey(key, this.stringDB);
            item.setQuantity(quantity);

            return item;
        };

        const mobDropsData = this.mob.data.lootDrop;
        const gainedMobDropsData = mobDropsData.filter(filterDropData);
        const gainedMobDropItemObjects = gainedMobDropsData.map(createItemObjectsFromDropData);

        const monstersPrimaryDropsData = this.mob.monsterList.map((monster) => {
            const drop = monster.getPrimaryDrop();
            return drop;
        });
        const gainedMonstersPrimaryDropData = monstersPrimaryDropsData.filter(filterDropData);
        const gainedMonstersPrimaryDropItemObjects = gainedMonstersPrimaryDropData.map(createItemObjectsFromDropData);

        const monstersElementDropsData = this.mob.monsterList.map((monster) => {
            const drop = monster.getElementDrop();
            return drop;
        });
        const gainedMonstersElementDropData = monstersElementDropsData.filter(filterDropData);
        const gainedMonstersElementDropItemObjects = gainedMonstersElementDropData.map(createItemObjectsFromDropData);

        const gainedDropItemObjects = [...gainedMobDropItemObjects, ...gainedMonstersPrimaryDropItemObjects, ...gainedMonstersElementDropItemObjects];

        gainedDropItemObjects.forEach((newItem) => {
            this.inventoryItemsRegData.addNewItemToInventory(newItem, this.itemDB);
        });


        //TODO: exp
        //
        const monstersExpDropData = this.mob.monsterList.map((monster) => {
            const expDrop = monster.getExpDrop();
            return expDrop;
        });
        const monstersExpDropSum = monstersExpDropData.reduce((prev, expDrop) => {
            return prev + expDrop;
        }, 0);

        let onlyLivingChars = true;
        this.partyRegData.addGainedExpToParty(monstersExpDropSum, onlyLivingChars);
        const levelUpPartyResults = this.partyRegData.levelUpParty(onlyLivingChars);
        this.partyRegData.setStatsFromEquippedGearForAllChars(this.gearDB);


        this.setActiveWindow(this.battleDialogWindow);
        this.setActiveComponent(this.battleDialogComp);
        this.battleDialogWindow.setWindowVisible(true);

        let newText = this.dialogDB.getDialogStringByKey('dlg-0015');
        let newText2 = this.dialogDB.getDialogStringByKey('dlg-0041');

        let string1 = this.stringDB.getCustomStringByKey('string-experience');

        let config = {
            animateText: true,
        };
        let data = [{
            options: newText.getOptions(),
            text: newText.getText(),
        }, {
            text: '\n',
        }, {
            text: newText2.getText() + ' ' + monstersExpDropSum + ' ' + string1.getShortName().toUpperCase(),
        }, {
            text: '\n',
        }, ];


        //TODO: dialog for when levelled up 
        //
        levelUpPartyResults.filter((result) => {
            return result.didLevelUp;
        }).map((result) => {
            let charName = this.stringDB.getCustomStringByKey(result.charKey).getFullName();
            let text1 = this.dialogDB.getDialogStringByKey('dlg-0042').getText();
            let string1 = this.stringDB.getCustomStringByKey('string-level').getShortName().toUpperCase();
            let newLevel = result.newLevel;
            let unlockedTechsKeys = result.unlockedTechniques;

            data.push({
                text: charName + ' ' + text1,
            }, {
                options: {
                    indent: 2,
                },
                text: string1 + ' ' + newLevel
            }, {
                text: '\n',
            });

            if (isDefinedAndNotNull(unlockedTechsKeys)) {
                unlockedTechsKeys.forEach((key) => {
                    let text2 = this.dialogDB.getDialogStringByKey('dlg-0043').getText();
                    let string2 = this.stringDB.getCustomStringByKey(key).getShortName().toUpperCase();

                    data.push({
                        text: charName + ' ' + text2,
                    }, {
                        options: {
                            indent: 2,
                        },
                        text: string2
                    }, {
                        text: '\n',
                    });

                });

            }
        });


        //TODO: dialog for gained loot drops
        //                    
        let text1 = this.dialogDB.getDialogStringByKey('dlg-0000').getText();
        if (gainedDropItemObjects.length > 0) {
            data.push({
                text: text1
            });
        }
        gainedDropItemObjects.forEach((newItem) => {
            let string1 = newItem.getTypeToChar(this.gameSettings) + this.stringDB.getCustomStringByKey(newItem.key).getFullName().toUpperCase();

            data.push({
                options: {
                    indent: 2
                },
                text: string1
            });
        });

        let callback = function () {
            setTimeout(() => {
                this.closeBattleMenu();
            }, 1000);
        };

        this.battleDialogComp.parseDialogData(data, callback.bind(this));
        this.battleDialogComp.startDrawingText();

        this.draw();
    }

    enterBattleResultsStateFromPrePlayerTurnState() {
        this.removeWindow(this.playerTurnCharListWindow);
        this.removeWindow(this.playerTurnActionListWindow);

        this.enterBattleResultsState();
    }

    enterBattleResultsStateFromEnemyTurnState() {
        //TODO: remove unwanted windows

        this.enterBattleResultsState();
    }

    useSelectedItemOnSelectedTargets() {
        let char = this.getCurrChar();
        let item = this.party.items[this.playerTurnSelectedEntryIndex];
        let targets = this.playerTurnSelectedTargets;
        let effect = this.effectDB.getEffectObjectByKey(item.effect);

        let config = {
            animateText: true,
        };
        this.battleDialogComp.reset(config);

        for (let i = 0; i < targets.length; i++) {
            let target = targets[i];
            let resultValue;
            let targetWasKilled;

            if (item.type === 'consumable') {
                resultValue = item.applyItemEffect(this.gameSettings, this.effectDB, target);
            } else if (item.type === 'spell' && effect.type === 'spell' && effect.subType === 'offensive') {
                resultValue = item.applyItemEffect(this.gameSettings, this.effectDB, target, char);
                targetWasKilled = target.getIsDead();
            }
            if (isUndefinedOrNull(resultValue) && effect.type !== 'cure') {
                this.getCurrChar().setBattleState('choose');
                return null;
            }

            //TODO: refactor this check into a "updateItemObjects()"
            if (item.quantity <= 0) {
                this.party.items[this.playerTurnSelectedEntryIndex] = 'none';
            }
            //

            this.removeWindow(this.selectPlayerCharListWindow);
            this.removeWindow(this.selectEnemyMonsterListWindow);

            this.setActiveWindow(this.battleDialogWindow);
            this.setActiveComponent(this.battleDialogComp);
            this.battleDialogWindow.setWindowVisible(true);

            let resultStat = effect.stat || 'none';
            resultStat = resultStat.toUpperCase();
            let resultDescription = '';

            let newText = this.dialogDB.getDialogStringByKey('dlg-0017');
            let newText2 = this.dialogDB.getDialogStringByKey('dlg-0018');
            let newText3 = '';
            let newText4 = '';

            switch (effect.type) {
                case 'heal':
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0016').getText();
                    newText3 = resultStat + ' ' + resultDescription + ' ' + resultValue;
                    break;
                case 'cure':
                    if (isUndefinedOrNull(resultValue)) {
                        resultDescription = this.dialogDB.getDialogStringByKey('dlg-0031').getText();
                        newText3 = resultDescription;
                        break;
                    }
                    if (effect.statusAilment === 'status-all') {
                        resultValue.forEach((statusAilment) => {
                            resultDescription = this.dialogDB.getDialogStringByKey('dlg-0029').getText();
                            let curedAilment = this.stringDB.getCustomStringByKey(statusAilment.statusKey).getShortName().toUpperCase();
                            newText3 += resultDescription + ' ' + curedAilment + '\n\n';
                        });
                    } else {
                        resultDescription = this.dialogDB.getDialogStringByKey('dlg-0029').getText();
                        let curedAilment = this.stringDB.getCustomStringByKey(effect.statusAilment).getShortName().toUpperCase();
                        newText3 = resultDescription + ' ' + curedAilment;
                    }
                    break;
                case 'spell':
                    switch (effect.subType) {
                        case 'offensive':
                            if (resultValue > 0) {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0013').getText();
                                newText3 = resultValue + ' ' + resultDescription;
                            } else if (resultValue === 0) {
                                let element = this.stringDB.getCustomStringByKey('element-' + effect.element).getFullName().toUpperCase();
                                if (target.elementalAffinities[effect.element] === 0) {
                                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0020').getText() + ' ' + element;
                                    newText3 = target.getFullName() + ' ' + resultDescription;
                                } else {
                                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0030').getText();
                                    newText3 = resultDescription;
                                }
                            } else {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0019').getText();
                                newText3 = Math.abs(resultValue) + ' ' + resultDescription;
                            }
                            break;
                    }
                    break;
            }

            let data = [{
                    options: newText.getOptions(),
                    text: this.getCurrChar().getFullName(),
                },
                {
                    options: {
                        indent: 1,
                    },
                    text: newText.getText() + ' ' + item.getFullName(),
                },
                {
                    options: {
                        indent: 1,
                    },
                    text: newText2.getText() + ' ' + target.getFullName(),
                },
                {
                    text: '\n',
                },
                {
                    options: {
                        indent: 1,
                    },
                    text: newText3,
                },
                {
                    text: '\n',
                },
            ];
            if (targetWasKilled) {
                newText4 = this.dialogDB.getDialogStringByKey('dlg-0014');
                data.push({
                    text: target.getFullName() + ' ' + newText4.getText()
                }, {
                    text: '\n',
                });
            }


            let callback = () => {
                setTimeout(() => {}, 1000);
            };

            this.battleDialogComp.parseDialogData(data, callback.bind(this));

            this.draw();
        }

        let callback = () => {
            setTimeout(() => {
                this.updateMonsterImageListComp();

                this.battleDialogWindow.setWindowVisible(false);
                this.setActiveWindow(null);

                this.playerTurnCharIdx++;
                this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
            }, 1000);
        };
        this.battleDialogComp.startDrawingText(callback);

        this.getCurrChar().setBattleState('wait');
    }

    useSelectedAttackOnSelectedEnemies() {
        let char = this.getCurrChar();
        let weapon = this.playerTurnSelectedEntry;
        let enemies = this.playerTurnSelectedTargets;

        let config = {
            animateText: true,
        };
        this.battleDialogComp.reset(config);

        enemies.forEach((enemy, index) => {
            let resultObject = char.applyAttackDamageTo(enemy, weapon, null, this.gameSettings);
            let resultValue = resultObject.resultValue;
            let inflictedStatusAilments = resultObject.inflictedStatusAilments;
            let enemyWasKilled = enemy.getIsDead();
            let enemyWasDamaged = false;


            this.removeWindow(this.selectEnemyMonsterListWindow);
            this.removeWindow(this.playerTurnCharListWindow);
            this.removeWindow(this.selectAttackStateWeaponListWindow);

            this.setActiveWindow(this.battleDialogWindow);
            this.setActiveComponent(this.battleDialogComp);
            this.battleDialogWindow.setWindowVisible(true);

            let resultDescription = '';
            let newText = this.dialogDB.getDialogStringByKey('dlg-0011');
            let newText2 = this.dialogDB.getDialogStringByKey('dlg-0012');
            let newText3 = '';
            let newText4 = '';

            if (resultValue > 0) {
                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0013').getText();
                newText3 = resultValue + ' ' + resultDescription;

                if (isDefinedAndNotNull(inflictedStatusAilments)) {
                    let inflictedByText = this.dialogDB.getDialogStringByKey('dlg-0032').getText();
                    let statusKey = inflictedStatusAilments[0];
                    let statusAilmentString = this.stringDB.getCustomStringByKey(statusKey).getShortName().toUpperCase();
                    newText3 += '\n' + inflictedByText + ' ' + statusAilmentString;
                }

                enemyWasDamaged = true;
            } else if (resultValue === 0) {
                if (enemy.elementalAffinities[weapon.element] === 0) {
                    let element = this.stringDB.getCustomStringByKey('element-' + weapon.element).getFullName().toUpperCase();
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0020').getText() + ' ' + element;
                    newText3 = enemy.getFullName() + ' ' + resultDescription;

                } else {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0030').getText();
                    newText3 = resultDescription;
                }
            } else {
                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0019').getText();
                newText3 = Math.abs(resultValue) + ' ' + resultDescription;
            }

            let data1 = [{
                    options: newText.getOptions(),
                    text: char.getFullName() + ' ' + newText.getText(),
                },
                {
                    options: {
                        indent: 6,
                    },
                    text: enemy.getFullName(),
                },
                {
                    options: {
                        indent: 1,
                    },
                    text: newText2.getText() + ' ' + weapon.getTypeToChar(this.gameSettings) + weapon.getShortName(),
                },
            ];
            let data2 = [{
                    options: {
                        indent: 1,
                    },
                    text: newText3,
                },
                {
                    text: '\n',
                },
            ];
            if (enemyWasKilled) {
                newText4 = this.dialogDB.getDialogStringByKey('dlg-0014');
                data2.push({
                    text: enemy.getFullName() + ' ' + newText4.getText()
                }, {
                    text: '\n',
                });
            }


            let callback1 = () => {
                setTimeout(() => {}, 0);
            };
            let callback2 = () => {
                setTimeout(() => {}, 1000);
            };

            this.battleDialogComp.parseDialogData(data1, callback1.bind(this));
            this.battleDialogComp.parseDialogData(data2, callback2.bind(this));

            this.draw();
        });

        let callback = () => {
            setTimeout(() => {
                this.updateMonsterImageListComp();

                this.battleDialogWindow.setWindowVisible(false);
                this.setActiveWindow(null);

                this.playerTurnCharIdx++;
                this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
            }, 1000);
        };
        this.battleDialogComp.startDrawingText(callback);

        this.getCurrChar().setBattleState('wait');
    }

    useSelectedTechOnSelectedTargets() {
        let char = this.getCurrChar();
        let tech = this.playerTurnSelectedEntry;
        let targets = this.playerTurnSelectedTargets;
        let effect = this.effectDB.getEffectObjectByKey(tech.effects[0]);

        let config = {
            animateText: true,
        };
        this.battleDialogComp.reset(config);


        let newText = this.dialogDB.getDialogStringByKey('dlg-0017');

        let data = [{
                options: newText.getOptions(),
                text: this.getCurrChar().getFullName(),
            },
            {
                options: {
                    indent: 1,
                },
                text: newText.getText() + ' ' + tech.getFullName(),
            }, {
                text: '\n',
            }
        ];

        let callback = () => {
            setTimeout(() => {}, 1000);
        };

        this.battleDialogComp.parseDialogData(data, callback.bind(this));

        let resultObjects = char.applyTechniqueTo(targets, tech, effect, this.gameSettings);

        for (let i = 0; i < resultObjects.length; i++) {
            let target = targets[i];
            let resultObject = resultObjects[i];
            let targetWasKilled = target.getIsDead();
            let targetWasDamaged = false;

            if (isUndefinedOrNull(resultObject)) {
                this.getCurrChar().setBattleState('choose');
                return null;
            }

            this.removeWindow(this.selectEnemyMonsterListWindow);
            this.removeWindow(this.selectPlayerCharListWindow);

            this.setActiveWindow(this.battleDialogWindow);
            this.setActiveComponent(this.battleDialogComp);
            this.battleDialogWindow.setWindowVisible(true);

            let resultDescription = '';
            let newText2 = this.dialogDB.getDialogStringByKey('dlg-0018');
            let newText3 = '';
            let newText4 = '';
            let data2 = [];
            let data3 = [];
            let extraLines = [];

            if (effect.type === 'attack') {
                let resultValue = resultObject.resultValue;

                if (resultValue > 0) {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0013').getText();
                    newText3 = resultValue + ' ' + resultDescription;

                    targetWasDamaged = true;
                } else if (resultValue === 0) {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0030').getText();
                    newText3 = resultDescription;

                } else {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0019').getText();
                    newText3 = Math.abs(resultValue) + ' ' + resultDescription;
                }
            } else if (effect.type === 'warp') {
                let resultValue = resultObject.resultValue;

                if (resultValue > 0) {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0013').getText();
                    newText3 = resultValue + ' ' + resultDescription;

                    targetWasDamaged = true;
                } else if (resultValue === 0) {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0030').getText();
                    newText3 = resultDescription;

                } else {
                    resultDescription = this.dialogDB.getDialogStringByKey('dlg-0019').getText();
                    newText3 = Math.abs(resultValue) + ' ' + resultDescription;
                }
            } else if (effect.type === 'spell') {
                let resultValue = resultObject;

                switch (effect.subType) {
                    case 'offensive':
                        if (resultValue > 0) {
                            resultDescription = this.dialogDB.getDialogStringByKey('dlg-0013').getText();
                            newText3 = resultValue + ' ' + resultDescription;

                            targetWasDamaged = true;
                        } else if (resultValue === 0) {
                            let element = this.stringDB.getCustomStringByKey('element-' + effect.element).getFullName().toUpperCase();
                            if (target.elementalAffinities[effect.element] === 0) {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0020').getText() + ' ' + element;
                                newText3 = target.getFullName() + ' ' + resultDescription;
                            } else {
                                resultDescription = this.dialogDB.getDialogStringByKey('dlg-0030').getText();
                                newText3 = resultDescription;
                            }
                        } else {
                            resultDescription = this.dialogDB.getDialogStringByKey('dlg-0019').getText();
                            newText3 = Math.abs(resultValue) + ' ' + resultDescription;
                        }

                        break;
                    case 'heal':
                        resultDescription = this.dialogDB.getDialogStringByKey('dlg-0016').getText();
                        newText3 = resultStat + ' ' + resultDescription + ' ' + resultValue;
                        break;
                    case 'cure':
                        //not in game
                        break;
                    case 'inflict':
                        if (resultValue !== 'isImmune') {
                            resultDescription = this.dialogDB.getDialogStringByKey('dlg-0032').getText();
                        } else {
                            resultDescription = this.dialogDB.getDialogStringByKey('dlg-0033').getText();
                        }
                        let statusKey = effect.statusAilment;
                        let statusString = this.stringDB.getCustomStringByKey(statusKey).getShortName().toUpperCase();
                        newText3 = resultDescription + ' ' + statusString;
                        break;
                    case 'special':
                        //TODO: implement other techs besides Libra
                        if (effect.spell === 'spell-libra-01') {
                            let labelHP = this.stringDB.getCustomStringByKey('string-health-point').getShortName().toUpperCase();
                            let labelLvl = this.stringDB.getCustomStringByKey('string-level').getShortName().toUpperCase();
                            let textHP = labelHP + ' ' + prependSpecialWhitespaces(target.hp.toString(), 3) + '/' + prependSpecialWhitespaces(target.maxHp.total.toString(), 3);
                            let textLvl = labelLvl + prependSpecialWhitespaces(target.level.toString(), 2);

                            extraLines.push({
                                text: '\n'
                            }, {
                                options: {
                                    indent: 1,
                                },
                                text: textLvl
                            }, {
                                options: {
                                    indent: 1,
                                },
                                text: textHP
                            }, {
                                text: '\n'
                            });

                            if (isDefinedAndNotNull(resultValue.immunities) && resultValue.immunities.length > 0) {
                                let text1 = this.dialogDB.getDialogStringByKey('dlg-0033').getText();

                                extraLines.push({
                                    options: {
                                        indent: 1,
                                    },
                                    text: text1
                                });

                                resultValue.immunities.forEach((immunity) => {
                                    let string = this.stringDB.getCustomStringByKey(immunity).getShortName().toUpperCase();

                                    extraLines.push({
                                        options: {
                                            indent: 3,
                                        },
                                        text: string + '\n'
                                    });
                                });
                                extraLines.push({
                                    text: '\n'
                                });
                            }
                            if (isDefinedAndNotNull(resultValue.weaponAffinities)) {
                                let text1 = this.dialogDB.getDialogStringByKey('dlg-0036').getText();
                                let text2 = this.dialogDB.getDialogStringByKey('dlg-0034').getText();
                                let text3 = this.dialogDB.getDialogStringByKey('dlg-0035').getText();
                                let text4 = this.dialogDB.getDialogStringByKey('dlg-0038').getText();
                                let text5 = this.dialogDB.getDialogStringByKey('dlg-0039').getText();


                                let filteredWeak = filterObject(resultValue.weaponAffinities, (value) => value > 1.0);
                                let filteredStrong = filterObject(resultValue.weaponAffinities, (value) => value < 1.0);
                                let filteredResistant = filterObject(resultValue.weaponAffinities, (value) => value === 0);
                                let filteredAbsorbs = filterObject(resultValue.weaponAffinities, (value) => value < 0);


                                if (!isEmpty(filteredWeak)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text2 + ' ' + text1
                                    });
                                    Object.keys(filteredWeak).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('weapon-type-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                                if (!isEmpty(filteredStrong)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text3 + ' ' + text1
                                    });
                                    Object.keys(filteredStrong).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('weapon-type-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                                if (!isEmpty(filteredResistant)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text4 + ' ' + text1
                                    });
                                    Object.keys(filteredResistant).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('weapon-type-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                                if (!isEmpty(filteredAbsorbs)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text5 + ' ' + text1
                                    });
                                    Object.keys(filteredAbsorbs).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('weapon-type-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                            }
                            if (isDefinedAndNotNull(resultValue.elementalAffinities)) {
                                let text1 = this.dialogDB.getDialogStringByKey('dlg-0037').getText();
                                let text2 = this.dialogDB.getDialogStringByKey('dlg-0034').getText();
                                let text3 = this.dialogDB.getDialogStringByKey('dlg-0035').getText();
                                let text4 = this.dialogDB.getDialogStringByKey('dlg-0038').getText();
                                let text5 = this.dialogDB.getDialogStringByKey('dlg-0039').getText();


                                let filteredWeak = filterObject(resultValue.elementalAffinities, (value) => value > 1.0);
                                let filteredStrong = filterObject(resultValue.elementalAffinities, (value) => value < 1.0);
                                let filteredResistant = filterObject(resultValue.elementalAffinities, (value) => value === 0);
                                let filteredAbsorbs = filterObject(resultValue.elementalAffinities, (value) => value < 0);


                                if (!isEmpty(filteredWeak)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text2 + ' ' + text1
                                    });
                                    Object.keys(filteredWeak).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('element-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                                if (!isEmpty(filteredStrong)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text3 + ' ' + text1
                                    });
                                    Object.keys(filteredStrong).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('element-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                                if (!isEmpty(filteredResistant)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text4 + ' ' + text1
                                    });
                                    Object.keys(filteredResistant).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('element-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                                if (!isEmpty(filteredAbsorbs)) {
                                    extraLines.push({
                                        options: {
                                            indent: 1,
                                        },
                                        text: text5 + ' ' + text1
                                    });
                                    Object.keys(filteredAbsorbs).forEach((affinity) => {
                                        let string = this.stringDB.getCustomStringByKey('element-' + affinity).getShortName().toUpperCase();

                                        extraLines.push({
                                            options: {
                                                indent: 3,
                                            },
                                            text: string + '\n'
                                        });
                                    });
                                    extraLines.push({
                                        text: '\n'
                                    });
                                }
                            }
                            if (isDefinedAndNotNull(resultValue.primaryDrop) && (!isEmpty(resultValue.primaryDrop)) || (isDefinedAndNotNull(resultValue.elementDrop) && !isEmpty(resultValue.elementDrop))) {
                                let text1 = this.dialogDB.getDialogStringByKey('dlg-0040').getText();

                                extraLines.push({
                                    options: {
                                        indent: 1,
                                    },
                                    text: text1
                                });
                            }
                            if (isDefinedAndNotNull(resultValue.primaryDrop) && !isEmpty(resultValue.primaryDrop)) {
                                let itemKey = resultValue.primaryDrop.item;
                                let itemQuantity = resultValue.primaryDrop.quantity;
                                let itemString = this.stringDB.getCustomStringByKey(itemKey).getShortName().toUpperCase();

                                extraLines.push({
                                    options: {
                                        indent: 3,
                                    },
                                    text: appendSpecialWhitespaces(itemString, 7) + prependZeroes('' + itemQuantity, 2) + '\n'
                                });
                            }
                            if (isDefinedAndNotNull(resultValue.elementDrop) && !isEmpty(resultValue.elementDrop)) {
                                let itemKey = resultValue.elementDrop.item;
                                let itemQuantity = resultValue.elementDrop.quantity;
                                let itemString = this.stringDB.getCustomStringByKey(itemKey).getShortName().toUpperCase();

                                extraLines.push({
                                    options: {
                                        indent: 3,
                                    },
                                    text: appendSpecialWhitespaces(itemString, 7) + prependZeroes('' + itemQuantity, 2) + '\n'
                                });
                            }

                        }

                        break;

                }
            }

            data2.push({
                options: {
                    indent: 1,
                },
                text: newText2.getText() + ' ' + target.getFullName(),
            });

            if (extraLines.length <= 0) {
                data2.push({
                    options: {
                        indent: 1,
                    },
                    text: newText3,
                }, {
                    text: '\n',
                });
            } else {
                data2.push(...extraLines, {
                    text: '\n',
                });
            }

            if (targetWasKilled) {
                newText4 = this.dialogDB.getDialogStringByKey('dlg-0014');
                data3.push({
                    text: target.getFullName() + ' ' + newText4.getText()
                }, {
                    text: '\n',
                });
            }

            let callback2 = () => {
                setTimeout(() => {}, 1000);
            };

            let callback3 = () => {
                setTimeout((monsterIdx) => {
                    this.hideKilledMonsterImageListComp(monsterIdx);
                    // this.updateMonsterImageListComp();
                    this.draw();

                }, 0, i);
            };

            this.battleDialogComp.parseDialogData(data2, callback2.bind(this));
            if (data3.length > 0) this.battleDialogComp.parseDialogData(data3, callback3.bind(this));

            this.draw();
        }

        let callback4 = () => {
            setTimeout(() => {
                this.updateMonsterImageListComp();

                this.battleDialogWindow.setWindowVisible(false);
                this.setActiveWindow(null);

                this.playerTurnCharIdx++;
                this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
            }, 1000);
        };
        this.battleDialogComp.startDrawingText(callback4);

        this.getCurrChar().setBattleState('wait');


    }

    updateMonsterImageListComp() {
        this.monsterImageListWindow.clear();

        this.mob.monsterImageList = this.monsterMobDB.getMonsterImageObjectsFromMonsterObjects(this.mob.monsterList, this.monsterDB);
        let entries = this.createEntries(this.mob.monsterImageList);
        let nrOfMonsters = this.mob.monsterList.length;
        if (nrOfMonsters === 1) {
            entries[0].renderer.setRenderWidthInTiles(4);
            this.monsterImageListComp.setEntries(entries);
        } else if (nrOfMonsters === 2) {
            entries[0].renderer.setRenderWidthInTiles(9);
            entries[1].renderer.setRenderWidthInTiles(9);
            this.monsterImageListComp.setEntries(entries);
        } else if (nrOfMonsters === 3) {
            entries[0].renderer.setRenderWidthInTiles(6.5);
            entries[1].renderer.setRenderWidthInTiles(6.5);
            entries[2].renderer.setRenderWidthInTiles(6.5);
            this.monsterImageListComp.setEntries(entries);
        }
    }

    areAllEnemiesDead() {
        let res = true;
        this.mob.monsterList.forEach((enemy) => {
            if (!enemy.getIsDead()) res = false;
        });
        return res;
    }

    isWholePartyDead() {
        return this.party.charList.every(char => char.getBattleState() === 'dead');
    }

    removeAllStatusAilmentsOfParty() {
        this.party.charList.forEach(char => {
            char.cureAllStatusAilments();
        });
    }

    hideKilledMonsterImageListComp(monsterListIndex) {
        this.monsterImageListComp.getEntries()[monsterListIndex].getObject().setIsVisible(false);
    }

    startScreenShake() {
        this.battleDialogWindow.startShakeEffect();
    }

    closeBattleMenu() {

        this.scene.game.events.emit('Menu', 'CloseBattleMenu', {
            dontResumeParentScene: this.data.dontResumeParentScene,
            // TODO: battle results have to be passed here back to the parentSceneCallback 

        }, this.getParentSceneCallback());
        // this.getParentScene().flashScreen();
    }

    quitToTitleScreen() {
        this.scene.game.events.emit('Menu', 'CloseBattleMenu', {}, this.getParentSceneCallback());
        this.getParentScene().flashScreen();
        this.scene.game.events.emit('Game', 'ExitToTitleScreen');
    }

    confirm() {
        if (isDefinedAndNotNull(this.getActiveComponent())) {
            let currEntries = this.getActiveComponent().getEntryObjects();
            let currEntry = this.getActiveComponent().getCurrentEntryObject();
            let currEntryKey = currEntry ? currEntry.getKey() : null;
            let currEntryIndex = this.getActiveComponent().getCurrentIndex();
            let currSelectAllMode = this.getActiveComponent().getSelectAllMode();

            this.getActiveComponent().confirm();


            if (this.getActiveComponent() === this.battleDialogComp) return;
            if (this.stateMachine.getCurrentStateName() === 'PartyTurnState') {
                if (currEntryKey === 'string-battle-action-fight') {
                    this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
                } else if (currEntryKey === 'string-battle-action-run') {
                    this.scene.events.emit('Action', 'EnterSelectRunState');
                }
            } else if (this.stateMachine.getCurrentStateName() === 'PlayerTurnState') {
                if (currEntryKey === 'string-battle-action-attack') {
                    this.scene.events.emit('Action', 'EnterSelectAttackState');
                } else if (currEntryKey === 'string-battle-action-technique') {
                    this.scene.events.emit('Action', 'EnterSelectTechState');
                } else if (currEntryKey === 'string-battle-action-item') {
                    this.scene.events.emit('Action', 'EnterSelectItemState');
                } else if (currEntryKey === 'string-battle-action-defend') {
                    this.scene.events.emit('Action', 'EnterSelectDefendState');
                }
            } else if (this.stateMachine.getCurrentStateName() === 'SelectAttackState') {
                this.playerTurnSelectedEntry = currEntry;
                if (currEntryKey !== 'none' && currEntryKey !== 'empty') {
                    this.scene.events.emit('Action', 'EnterSelectEnemyState');
                    this.getActiveComponent().setSelectAllMode(false);
                } else {
                    this.getActiveComponent().cancel();
                }

                this.draw();
            } else if (this.stateMachine.getCurrentStateName() === 'SelectTechState') {
                if (currEntryKey !== 'none' && currEntryKey !== 'empty') {
                    this.playerTurnSelectedEntry = currEntry;
                    this.playerTurnSelectedEntryIndex = currEntryIndex;

                    if (this.getCurrChar().getTp() < currEntry.cost) {
                        this.getActiveComponent().cancel();
                    } else if (isDefinedAndNotNull(currEntry.getTargetModeEnemies())) {
                        this.scene.events.emit('Action', 'EnterSelectEnemyState');
                        if (currEntry.getTargetModeEnemies() === 'all') {
                            this.getActiveComponent().setSelectAllMode(true);
                        } else {
                            this.getActiveComponent().setSelectAllMode(false);
                        }
                    } else {
                        this.scene.events.emit('Action', 'EnterSelectPlayerState');
                        this.getActiveComponent().setSelectAllMode(true);
                    }
                    this.draw();
                } else {
                    this.getActiveComponent().cancel();
                }
            } else if (this.stateMachine.getCurrentStateName() === 'SelectItemState') {
                if (currEntryKey !== 'none' && currEntryKey !== 'empty') {
                    this.playerTurnSelectedEntry = currEntry;
                    this.playerTurnSelectedEntryIndex = currEntryIndex;

                    if (currEntry.type === 'spell') {
                        this.scene.events.emit('Action', 'EnterSelectEnemyState');
                        this.getActiveComponent().setSelectAllMode(false);
                    } else if (currEntry.type === 'consumable') {
                        this.scene.events.emit('Action', 'EnterSelectPlayerState');
                        this.getActiveComponent().setSelectAllMode(false);
                    }
                    this.draw();
                } else {
                    this.getActiveComponent().cancel();
                }
            } else if (this.stateMachine.getCurrentStateName() === 'SelectPlayerState') {

                if (currSelectAllMode) {
                    this.playerTurnSelectedTargets = this.party.charList.filter((char) => {
                        return !char.getIsDead();
                    });
                } else if (currEntryKey !== 'none' && currEntryKey !== 'empty') {
                    let currEntryCharList = this.party.charList[currEntryIndex];
                    this.playerTurnSelectedTargets = [currEntryCharList];
                    this.playerTurnSelectedTargetIndex = currEntryIndex;
                }

                if (this.getCurrChar().getBattleState() === 'choose') {
                    if (this.stateMachine.getLastStateName() === 'SelectItemState') {
                        let success = this.useSelectedItemOnSelectedTargets();
                        if (!success) {
                            this.getActiveComponent().cancel();
                        }
                    } else if (this.stateMachine.getLastStateName() === 'SelectTechState') {
                        let success = this.useSelectedTechOnSelectedTargets();
                        if (!success) {
                            this.getActiveComponent().cancel();
                        }
                    }
                }

            } else if (this.stateMachine.getCurrentStateName() === 'SelectEnemyState') {

                if (currSelectAllMode) {
                    this.playerTurnSelectedTargets = this.mob.monsterList.filter((monster) => {
                        return !monster.getIsDead();
                    });
                } else {
                    if (currEntryKey !== 'none' && currEntryKey !== 'empty') {
                        let currEntryIndexMonsterImageList = currEntryIndex;
                        let currEntryMonsterList = this.mob.monsterList[currEntryIndexMonsterImageList];
                        this.playerTurnSelectedTargets = [currEntryMonsterList];
                    }
                }


                if (this.getCurrChar().getBattleState() === 'choose') {
                    if (this.stateMachine.getLastStateName() === 'SelectAttackState') {
                        this.useSelectedAttackOnSelectedEnemies();
                    } else if (this.stateMachine.getLastStateName() === 'SelectTechState') {
                        let success = this.useSelectedTechOnSelectedTargets();
                        if (!success) {
                            this.getActiveComponent().cancel();
                        }
                    } else if (this.stateMachine.getLastStateName() === 'SelectItemState') {
                        let success = this.useSelectedItemOnSelectedTargets();
                        if (!success) {
                            this.getActiveComponent().cancel();
                        }
                    }
                }

            }
        }
    }

    confirmReleased() {
        if (isDefinedAndNotNull(this.getActiveComponent()) && this.getActiveComponent() === this.battleDialogComp) {
            this.getActiveComponent().confirmReleased();
        }
    }

    cancel() {
        if (this.getActiveComponent() === this.battleDialogComp) return;

        if (this.stateMachine.getCurrentStateName() === 'PartyTurnState') {
            this.closeBattleMenu();
        } else if (this.stateMachine.getCurrentStateName() === 'PlayerTurnState') {
            if (this.playerTurnCharIdx > 0) {
                //DEBUG: delete later
                // this.playerTurnCharIdx--;
                // this.scene.events.emit('Action', 'EnterPrePlayerTurnState');
                //
            } else {
                this.scene.events.emit('Action', 'EnterPartyTurnState');
            }
        } else if (this.stateMachine.getCurrentStateName() === 'SelectAttackState') {
            this.scene.events.emit('Action', 'EnterPlayerTurnState');
        } else if (this.stateMachine.getCurrentStateName() === 'SelectTechState') {
            this.scene.events.emit('Action', 'EnterPlayerTurnState');
        } else if (this.stateMachine.getCurrentStateName() === 'SelectItemState') {
            this.scene.events.emit('Action', 'EnterPlayerTurnState');
        } else if (this.stateMachine.getLastStateName() === 'SelectAttackState' &&
            this.stateMachine.getCurrentStateName() === 'SelectEnemyState') {
            this.scene.events.emit('Action', 'EnterSelectAttackState');
        } else if (this.stateMachine.getLastStateName() === 'SelectTechState' &&
            this.stateMachine.getCurrentStateName() === 'SelectEnemyState') {
            this.scene.events.emit('Action', 'EnterSelectTechState');
        } else if (this.stateMachine.getLastStateName() === 'SelectItemState' &&
            this.stateMachine.getCurrentStateName() === 'SelectEnemyState') {
            this.scene.events.emit('Action', 'EnterSelectItemState');
        } else if (this.stateMachine.getLastStateName() === 'SelectItemState' &&
            this.stateMachine.getCurrentStateName() === 'SelectPlayerState') {
            this.scene.events.emit('Action', 'EnterSelectItemState');
        }
    }

    pressUp() {
        super.pressUp();
    }

    pressDown() {
        super.pressDown();
    }

    pressRight() {
        super.pressRight();
    }

    pressLeft() {
        super.pressLeft();
    }

    update(time, delta) {
        this.battleDialogWindow.update(time, delta);
        this.battleDialogComp.update(time, delta);
    }
}