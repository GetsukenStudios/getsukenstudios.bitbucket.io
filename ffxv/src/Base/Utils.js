export function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

export function isDefined(obj) {
    if (typeof obj !== "undefined") {
        return true;
    }
    return false;
}

export function isUndefined(obj) {
    return !isDefined(obj);
}

export function isDefinedAndNotNull(obj) {
    return isDefined(obj) && obj != null;
}

export function isDefinedAndFunction(obj) {
    return isDefined(obj) && isFunction(obj);
}

export function isUndefinedOrNull(obj) {
    return !isDefined(obj) || obj == null;
}

export function isEmpty(obj) {
    for (var x in obj) {
        return false;
    }
    return true;
}

export function getPropOrDefault(obj, prop, defaultValue) {
    if (isDefined(obj) && isDefined(prop) && obj.hasOwnProperty(prop)) {
        return obj[prop];
    }
    return defaultValue;
}

export function clearArray(array) {
    while (array.length) {
        array.pop();
    }
}

export function contains(array, obj) {
    if (isDefinedAndNotNull(array) && isDefinedAndNotNull(obj)) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === obj) {
                return true;
            }
        }
    }
    return false;
}

export function remove(array, obj) {
    if (isDefinedAndNotNull(array) && isDefinedAndNotNull(obj)) {
        for (var i = array.length - 1; i >= 0; i--) {
            if (array[i] === obj) {
                return array.splice(i, 1);
            }
        }
    }
}

export function swap(array, index1, index2) {
    if (isDefinedAndNotNull(array) &&
        isDefinedAndNotNull(index1) && isDefinedAndNotNull(index2)) {
        let tmp = array[index2];
        array[index2] = array[index1];
        array[index1] = tmp;
    }
}

export function filterObject(obj, predicate) {
    return Object.keys(obj)
        .filter(key => predicate(obj[key]))
        .reduce((res, key) => (res[key] = obj[key], res), {});
}

export function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() +
        string.slice(1);
}

export function prependWhitespaces(string, maxLength) {
    if (isDefinedAndNotNull(string) &&
        isDefinedAndNotNull(maxLength)) {
        return string.padStart(maxLength, ' ');
    }
}

export function prependSpecialWhitespaces(string, maxLength) {
    if (isDefinedAndNotNull(string) &&
        isDefinedAndNotNull(maxLength)) {
        return string.padStart(maxLength, '½');
    }
}

export function prependZeroes(string, maxLength) {
    if (isDefinedAndNotNull(string) &&
        isDefinedAndNotNull(maxLength)) {
        return string.padStart(maxLength, '0');
    }
}

export function prependChar(string, char, maxLength) {
    if (isDefinedAndNotNull(string) &&
        isDefinedAndNotNull(maxLength)) {
        return string.padStart(maxLength, char);
    }
}

export function appendWhitespaces(string, maxLength) {
    if (isDefinedAndNotNull(string) &&
        isDefinedAndNotNull(maxLength)) {
        return string.padEnd(maxLength, ' ');
    }
}

export function appendSpecialWhitespaces(string, maxLength) {
    if (isDefinedAndNotNull(string) &&
        isDefinedAndNotNull(maxLength)) {
        return string.padEnd(maxLength, '½');
    }
}

export function extractNameFromNameHyphenNumberKey(string) {
    let regEx = /\-\d+$/;
    let name = string;
    let idx = name.search(regEx);
    if (idx >= 0) {
        name = name.slice(0, idx);
        return name;
    }

}

export function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function randomInteger(min = 0, max = 0) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomNumber(min = 0, max = 1) {
    return Math.random() * (max - min) + min;
}

export function isRandomValueInRange(randomValue, rangeStart = 0, rangeEnd) {
    if (isUndefinedOrNull(rangeEnd)) return false;

    return (rangeStart <= randomValue && randomValue <= rangeEnd);
}