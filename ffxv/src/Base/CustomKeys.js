import Phaser from 'phaser';
import KeyCodes from '../../node_modules/phaser/src/input/keyboard/keys/KeyCodes.js';
import Key from '../../node_modules/phaser/src/input/keyboard/keys/Key.js';
import { isDefinedAndNotNull } from './Utils.js';

export default class CustomKeys {
	constructor(options, keyboard, context) {
		this.keyboard = keyboard;
		// this = context;
		// if (context == null || context === undefined) {
		// 	this = this;
		// }

		this.keyMap = this.createKeyMap();
		this.updateKeys(options);
	}

	createKeyMap() {
		return {
			action: {
				keyName: 'F',
				keyCode: Phaser.Input.Keyboard.KeyCodes.F,
				key: null
			},
			cancel: {
				keyName: 'D',
				keyCode: Phaser.Input.Keyboard.KeyCodes.D,
				key: null
			},
			start: {
				keyName: 'SPACE',
				keyCode: Phaser.Input.Keyboard.KeyCodes.SPACE,
				key: null
			},
			select: {
				keyName: 'ESC',
				keyCode: Phaser.Input.Keyboard.KeyCodes.ESC,
				key: null
			},
			mute: {
				keyName: 'M',
				keyCode: Phaser.Input.Keyboard.KeyCodes.M,
				key: null
			},
			up: {
				keyName: 'UP',
				keyCode: Phaser.Input.Keyboard.KeyCodes.UP,
				key: null
			},
			down: {
				keyName: 'DOWN',
				keyCode: Phaser.Input.Keyboard.KeyCodes.DOWN,
				key: null
			},
			left: {
				keyName: 'LEFT',
				keyCode: Phaser.Input.Keyboard.KeyCodes.LEFT,
				key: null
			},
			right: {
				keyName: 'RIGHT',
				keyCode: Phaser.Input.Keyboard.KeyCodes.RIGHT,
				key: null
			}
		};
	}

	updateKeys(options) {
		if (options != null && options !== undefined) {
			options.forEach(option => {
				const keyName = option.keyName;
				if (this.keyMap.hasOwnProperty(keyName)) {
					let key = this.keyMap[keyName].key;
					this.removeKey(key);
					this.keyMap[keyName].keyName = option.key;
				}
			});
		}
		this.updateKeyMap();
		this.updateKeyProperties();
	}

	updateKeyMap() {
		for (var property in this.keyMap) {
			let keyName = this.keyMap[property].keyName;
			let code = KeyCodes[keyName.toUpperCase()];
			this.keyMap[property] = {
				keyName: keyName,
				keyCode: code,
				key: this.addKey(code)
			};
		}
	}

	removeKey(code) {
		if (isDefinedAndNotNull(this.keyboard)) {
			return this.keyboard.removeKey(code);
		}
	}

	addKey(code) {
		if (isDefinedAndNotNull(this.keyboard)) {
			return this.keyboard.addKey(code);
		}
	}

	updateKeyProperties() {
		this.action = this.getKey('action');
		this.cancel = this.getKey('cancel');
		this.start = this.getKey('start');
		this.select = this.getKey('select');
		this.mute = this.getKey('mute');

		this.up = this.getKey('up');
		this.down = this.getKey('down');
		this.left = this.getKey('left');
		this.right = this.getKey('right');
	}

	getKeyCode(keyName) {
		if (this.keyMap.hasOwnProperty(keyName)) {
			return this.keyMap[keyName].keyCode;
		}
		return null;
	}

	getKey(keyName) {
		if (this.keyMap.hasOwnProperty(keyName)) {
			return this.keyMap[keyName].key;
		}
		return null;
	}

	getKeyCodeFromKeyboard(key) {
		return Phaser.Input.Keyboard.KeyCodes[key];
	}

	hasKeyCode(keyName) {
		return this.keyMap.hasOwnProperty(keyName);
	}

	isInitialized() {
		return this != null && this != undefined &&
			this.action != null && this.action !== undefined &&
			this.cancel != null && this.cancel !== undefined &&
			this.start != null && this.start !== undefined &&
			this.select != null && this.select !== undefined &&
			this.mute != null && this.mute !== undefined &&
			this.left != null && this.left !== undefined &&
			this.right != null && this.right !== undefined &&
			this.up != null && this.up !== undefined &&
			this.down != null && this.down !== undefined;
	}

	resetAllKeys() {
		for (var property in this.keyMap) {
			this.keyMap[property].key.reset();
		}

	}
}