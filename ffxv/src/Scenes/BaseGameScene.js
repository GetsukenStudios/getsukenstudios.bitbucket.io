import BaseScene from "./BaseScene";
import {
    isDefinedAndNotNull,
    prependZeroes,
    isUndefinedOrNull
} from "../Base/Utils";
import WaterTileSprite from "../Game/Sprites/WaterTileSprite";
import ChestSprite from "../Game/Sprites/ChestSprite";
import PlayerSprite from "../Game/Sprites/PlayerSprite";
import ZoneObject from "../Game/Sprites/ZoneObject";
import NpcSprite from "../Game/Sprites/NpcSprite";
import NpcSpriteFactory from "../Game/Sprites/NpcSpriteFactory";

export default class BaseGameScene extends BaseScene {

    constructor(key) {
        super(key);

        this.mainCamera = null;
        this.sprites = {};
        this.terrainSprites = {};
        this.zoneObjects = [];
        this.tilemapObjects = {};
        this.map = null;
        this.tilesetKey = '';
        this.tilemapKey = '';
        this.belowLayer = null;
        this.worldLayer = null;
        this.aboveLayer = null;

        this.flashScreenDuration = 500;

        this.sceneWasCreated = false;
    }

    initialize() {
        super.initialize();

    }

    initStateMachine() {

    }

    startBackgroundMusic() {
        // play music 
        this.currentBackgroundMusic.play();
    }

    setupMainCamera() {
        this.mainCamera = this.cameras.main;
    }

    drawClearBackground() {
        this.sprites.screenBackground = [];

        let settings = this.registry.get('gameSettings');
        let tileSize = settings.tileSizeMenu;
        let widthInTiles = settings.gameWidthInMenuTiles;
        let heightInTiles = settings.gameHeightInMenuTiles;
        let cameraX = this.mainCamera.scrollX;
        let cameraY = this.mainCamera.scrollY;

        for (let i = 0; i <= widthInTiles; i++) {
            for (let j = 0; j <= heightInTiles; j++) {
                let newX = cameraX + (i * tileSize);
                let newY = cameraY + (j * tileSize);
                let sprite = this.add.image(newX, newY, 'sprites-menu-tiles', 4).setOrigin(0, 0);
                this.sprites.screenBackground.push(sprite);
            }
        }
    }

    flashScreen(data, callback) {
        this.sprites.screenFlashSprites = [];

        let settings = this.registry.get('gameSettings');
        let tileSize = settings.tileSizeMenu;
        let widthInTiles = settings.gameWidthInMenuTiles;
        let heightInTiles = settings.gameHeightInMenuTiles;
        let cameraX = this.mainCamera.scrollX;
        let cameraY = this.mainCamera.scrollY;

        for (let i = 0; i <= widthInTiles; i++) {
            for (let j = 0; j <= heightInTiles; j++) {
                let newX = cameraX + (i * tileSize);
                let newY = cameraY + (j * tileSize);
                let sprite = this.add.image(newX, newY, 'sprites-menu-tiles', 4).setOrigin(0, 0);
                sprite.setDepth(settings.depthList.screenFlash);
                this.sprites.screenFlashSprites.push(sprite);
            }
        }

        this.scene.pause();
        setTimeout(() => {
            this.sceneWasCreated = true;

            if (isUndefinedOrNull(data) || (isDefinedAndNotNull(data) && !data.dontResumeParentScene)) {
                this.scene.resume();
            }

            this.sprites.screenFlashSprites.forEach((sprite) => {
                sprite.destroy();
            });
            this.sprites.screenFlashSprites = [];

            if (isDefinedAndNotNull(callback)) {
                callback(data);
            }
        }, this.flashScreenDuration);
    }

    createTilemap(data) {
        this.map = this.make.tilemap({
            key: this.tilemapKey
        });

        const tileset = this.map.addTilesetImage(this.tilesetKey, this.tilesetKey);

        // Parameters: layer name (or index) from Tiled, tileset, x, y
        this.belowLayer = this.map.createStaticLayer('Below', tileset, 0, 0);
        this.worldLayer = this.map.createStaticLayer('World', tileset, 0, 0);
        this.aboveLayer = this.map.createStaticLayer('Above', tileset, 0, 0);

        this.worldLayer.setCollisionByProperty({
            collides: true
        });
        this.aboveLayer.setDepth(this.registry.get('gameSettings').depthList.above);

        // // draw debug collision graphics
        // const debugGraphics = this.add.graphics().setAlpha(0.75);
        // this.worldLayer.renderDebug(debugGraphics, {
        //   tileColor: null, // Color of non-colliding tiles
        //   collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
        //   faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
        // });


        // filter out objects from tilemap and create CustomSprites from them
        const spawnPoints = this.map.filterObjects('Objects', obj => obj.type === 'spawn-point');
        const zones = this.map.filterObjects('Objects', obj => obj.type === 'zone');
        const waterTiles = this.map.filterObjects('Objects', obj => obj.type === 'water');
        const chests = this.map.filterObjects('Objects', obj => obj.type === 'chest');
        const npcs = this.map.filterObjects('Objects', obj => obj.type === 'npc');
        this.tilemapObjects.spawnPoints = spawnPoints;
        this.tilemapObjects.zones = zones;
        this.tilemapObjects.waterTiles = waterTiles;
        this.tilemapObjects.chests = chests;
        this.tilemapObjects.npcs = npcs;

        // create ZoneObjects
        const zoneObjects = [];
        zones.forEach((obj, index) => {
            let key = obj.name;
            let newObject = new ZoneObject(this, key, obj.x, obj.y, obj.width, obj.height);
            zoneObjects.push(newObject);
        });
        this.zoneObjects = zoneObjects;

        // create WaterTileSprites
        const waterTileSprites = [];
        waterTiles.forEach((obj, index) => {
            let key = '' + obj.type + '-' + prependZeroes(index.toString(), 3);
            let newSprite = new WaterTileSprite(this, key, obj.x, obj.y);
            waterTileSprites.push(newSprite);
        });
        this.terrainSprites.waterTileSprites = waterTileSprites;

        // create ChestSprites
        const chestSprites = [];
        chests.forEach((obj, index) => {
            // let key = '' + obj.type + '-' + prependZeroes(index.toString(), 3);
            let key = obj.name;
            let newSprite = new ChestSprite(this, key, obj.x, obj.y);
            chestSprites.push(newSprite);

            // handle persistentData for this scene (e.g. already opened chests, which has to be saved for this scene)
            let scenePersistentData = this.registry.get('scenePersistentData');
            let sceneData = scenePersistentData.getDataByKey(this.key);
            for (let property in sceneData.chests) {
                if (sceneData.chests.hasOwnProperty(property) && property === newSprite.key) {
                    let isOpened = sceneData.chests[property].isOpened;
                    if (isOpened) {
                        newSprite.changeTextureToOpenChest();
                    }

                    let lootDrop = scenePersistentData.getDataByKey(this.key).chests[property].lootDrop;
                    newSprite.setLootDrop(lootDrop);
                }
            }
        });
        this.sprites.chestSprites = chestSprites;

        // create NPC Sprites
        const npcSprites = [];
        npcs.forEach((obj, index) => {
            let key = obj.name;
            let subType = obj.properties.find(o => o.name === 'subType').value;
            let scriptId = obj.properties.find(o => o.name === 'scriptId').value;
            let styleId = obj.properties.find(o => o.name === 'styleId').value;
            let newSprite = NpcSpriteFactory.createNpcSprite(this, key, obj.x, obj.y, subType, scriptId, styleId);
            npcSprites.push(newSprite);
        });
        this.sprites.npcSprites = npcSprites;

        // create PlayerSprite
        let newPosition;
        let playerSprite;
        if (isDefinedAndNotNull(spawnPoints)) {
            if (isDefinedAndNotNull(data) && isDefinedAndNotNull(data.spawnPoint)) {
                let key = 'spawn-point-' + data.spawnPoint;
                newPosition = spawnPoints.find(o => o.name === key);
            } else {
                newPosition = spawnPoints[0];
            }
        }
        if (isDefinedAndNotNull(data) && isDefinedAndNotNull(data.playerPosition)) {
            newPosition = data.playerPosition;
        }

        playerSprite = new PlayerSprite(this, 'player', newPosition.x, newPosition.y);
        if (isDefinedAndNotNull(data) && isDefinedAndNotNull(data.playerFaceDirection)) {
            playerSprite.setFaceDirection(data.playerFaceDirection);
        }
        this.sprites.playerSprite = playerSprite;

        // collision checks
        this.physics.add.collider(this.sprites.playerSprite, this.worldLayer, this.sprites.playerSprite.onCollideWithWorldLayer.bind(this.sprites.playerSprite));
        this.physics.add.collider(this.sprites.playerSprite, this.terrainSprites.waterTileSprites, this.sprites.playerSprite.onCollideWithWorldLayer.bind(this.sprites.playerSprite));
        this.physics.add.collider(this.sprites.playerSprite, this.sprites.chestSprites, this.sprites.playerSprite.onCollideWithWorldLayer.bind(this.sprites.playerSprite));
        this.physics.add.collider(this.sprites.playerSprite, this.sprites.npcSprites, this.sprites.playerSprite.onCollideWithWorldLayer.bind(this.sprites.playerSprite));
        // overlap checks
        this.physics.add.overlap(this.sprites.playerSprite, this.zoneObjects, this.onOverlapWithZones.bind(this));


        // setup camera to center on player
        if (this.sprites.playerSprite) {
            this.mainCamera.startFollow(this.sprites.playerSprite, true, 1, 1, -8, -8);
            let newX = this.sprites.playerSprite.getCenter().x - (this.mainCamera.displayWidth / 2);
            let newY = this.sprites.playerSprite.getCenter().y - (this.mainCamera.displayHeight / 2);
            this.mainCamera.setScroll(newX, newY);
        }
    }

    onOverlapWithZones(player, object) {

    }

    resetKeyboards() {
        super.resetKeyboards();

        if (isDefinedAndNotNull(this.sprites.playerSprite)) {
            this.sprites.playerSprite.resetKeyboards();
        }
    }

    pauseHandler(scene, data) {
        super.pauseHandler(scene, data);

    }

    resumeHandler(scene, data) {
        super.resumeHandler(scene, data);

    }

    shutdownHandler(scene, data) {
        super.shutdownHandler(scene, data);

        this.sceneWasCreated = false;

        this.mainCamera = null;
        // this.sprites = {};
        // this.zoneObjects = [];
        // this.tilemapObjects = {};
        // this.map = null;
        // this.belowLayer = null;
        // this.worldLayer = null;
        // this.aboveLayer = null;

        for (let group in this.sprites) {
            let arr = this.sprites[group];
            if (Array.isArray(arr)) {
                arr.forEach(sprite => {
                    sprite.destroy();
                });
            } else {
                arr.destroy();
            }
        }
        for (let group in this.terrainSprites) {
            let arr = this.terrainSprites[group];
            if (Array.isArray(arr)) {
                arr.forEach(sprite => {
                    sprite.destroy();
                });
            } else {
                arr.destroy();
            }
        }
    }

    init(data) {
        super.init(data);

    }

    preload(data) {
        super.preload(data);

    }

    create(data) {
        super.create(data);

        this.setupMainCamera();

        if (this.tilesetKey !== '' && this.tilemapKey !== '') {
            this.createTilemap(data);
        }

        this.flashScreen();
    }

    update(time, delta) {
        super.update(time, delta);

    }
}