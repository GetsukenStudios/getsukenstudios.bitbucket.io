import StateTransition from '../State/StateTransition';
import StateNode from '../State/StateNode';
import StateMachine from '../State/StateMachine';
import BaseScene from './BaseScene';
import { BaseWindow } from '../Gui/BaseWindow';
import { BaseComponent } from '../Gui/BaseComponent';
import { BaseMenu } from '../Gui/BaseMenu';
import { isDefinedAndNotNull, prependWhitespaces } from '../Base/Utils';
import BaseGameScene from './BaseGameScene';

export default class DisclaimerScene extends BaseGameScene {

    constructor(key) {
        super(key);
    }

    initStateMachine() {

        let stateMachine = new StateMachine(this.key, this.events);

        let startState = new StateNode(stateMachine, 'StartState');
        let titleScreenState = new StateNode(stateMachine, 'TitleScreen');

        // new StateTransition('OpenTitleScreen', startState, titleScreenState, this.openTitleScreen.bind(this));
        new StateTransition('OpenTitleScreen', startState, titleScreenState, this.openTitleScreen.bind(this));

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    openTitleScreen() {
        this.game.events.emit('Game', 'DisclaimerDone');
    }

    preload() {

    }

    create() {
        super.create();

        let settings = this.registry.get('gameSettings');
        let stringDB = this.registry.get('stringDatabase');
        this.tileSizeMenu = settings.tileSizeMenu;

        this.drawClearBackground();

        this.disclaimerMenu = new BaseMenu(this);
        this.disclaimerWindow = new BaseWindow('disclaimerWindow', this, 0, 0, 20, 15);
        this.disclaimerMenu.addWindow(this.disclaimerWindow);
        let text = stringDB.getCustomStringByKey('string-disclaimer-text').getFullName();

        // this.disclaimerText = this.add.bitmapText(8, 8, 'retroBitmap', text).setDepth(this.settings.depthList.gui);

        let entries = this.disclaimerMenu.createEntries([text]);
        this.disclaimerComp = new BaseComponent('disclaimerComp', this, this.disclaimerMenu, 8, 8, entries);
        this.disclaimerWindow.addComponent(this.disclaimerComp);

        this.buttonPromptWindow = new BaseWindow('buttonPromptWindow', this, 3, 15, 13, 3);
        this.buttonPromptWindow.moveWindowPositionBy(this.tileSizeMenu / 2, 0);
        // this.buttonPromptWindow.moveWindowPositionBy(0, this.tileSizeMenu / 2);
        this.disclaimerMenu.addWindow(this.buttonPromptWindow);

        let textPress = stringDB.getCustomStringByKey('string-press').getFullName();
        text = textPress + ' ' + this.keys.keyMap.start.keyName;
        text = prependWhitespaces(text, 9);

        // this.buttonPromptText = this.add.bitmapText(8, 8, 'retroBitmap', text);

        entries = this.disclaimerMenu.createEntries([text]);
        this.buttonPromptComp = new BaseComponent('disclaimerComp', this, this.disclaimerMenu, 8, 8, entries);
        this.buttonPromptWindow.addComponent(this.buttonPromptComp);

        this.disclaimerMenu.draw();
    }

    closeDisclaimerScene() {
        this.disclaimerMenu.destroyMenu();
        this.sprites.screenBackground.forEach((sprite) => {
            sprite.destroy();
        });
    }

    update() {
        super.update();

        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
                // console.log('Pressed Start key in ' + this.key);
                if (this.stateMachine.getCurrentStateName() == 'StartState') {
                    this.closeDisclaimerScene();
                    this.events.emit('Action', 'OpenTitleScreen');
                    this.openTitleScreen();
                }
            }
        }

        // console.log('still in ' + this.key + '!!!');
    }

}