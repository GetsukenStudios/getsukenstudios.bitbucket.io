import {
    isDefinedAndNotNull
} from '../Base/Utils';
import BaseGameScene from './BaseGameScene';

export default class LoadNewGameScene extends BaseGameScene {

    constructor(key) {
        super(key);


    }

    preload() {

    }

    create() {
        super.create();

        let gameDataManager = this.registry.get('gameDataManager');
        let partyData = this.registry.get('party');
        let gearDB = this.registry.get('gearDatabase');
        let gameSettings = this.registry.get('gameSettings');
        let appSettings = this.registry.get('appSettings');

        // apply gameSettings
        let newGameSettings = {
            // mute: false,
            // mute: true,
        };
        for (let prop in newGameSettings) {
            gameSettings[prop] = newGameSettings[prop];
        }
        // this.setMute(gameSettings.mute);

        // TODO: set party level to desired initial level
        //       calculate equipped gear's stat modifiers 
        let chars = partyData.getCharsData();
        chars.forEach((char) => {
            let desiredLevel = char.stats.level;
            let expNeededForDesiredInitialLevel = partyData.getExpNeededForLevel(desiredLevel);
            char.stats.exp = expNeededForDesiredInitialLevel;
        });
        partyData.levelUpParty(false);
        partyData.setStatsFromEquippedGearForAllChars(gearDB);


        let key;
        let data;
        if (!appSettings.debugMode) {
            key = 'TestScene2';
            data = {
                key: key,
                spawnPoint: '000',
                // playerFaceDirection: 'up',
            };
        } else {
            key = 'TestScene2';
            data = {
                key: key,
                spawnPoint: '000',
                // playerFaceDirection: 'up',
            };
        }

        this.game.events.emit('Game', 'StartNewGame', data);
    }

    update(time, delta) {
        super.update(time, delta);

    }
}