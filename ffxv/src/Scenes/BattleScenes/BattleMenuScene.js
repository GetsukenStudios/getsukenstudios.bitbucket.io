import StateTransition from '../../State/StateTransition';
import StateNode from '../../State/StateNode';
import StateMachine from '../../State/StateMachine';
import {
    isDefinedAndNotNull
} from '../../Base/Utils';
import {
    MainMenu
} from '../../Gui/MainMenu';
import BaseMenuScene from '../MenuScenes/BaseMenuScene';
import {
    BattleMenu
} from '../../Gui/BattleMenu';

export default class BattleMenuScene extends BaseMenuScene {

    constructor(key) {
        super(key);

        this.mainCamera = null;
        this.sprites = {};

        this.initializeBackgroundMusicKey('bgm-up-for-a-challenge');
    }

    initStateMachine() {
        super.initStateMachine();

        let stateMachine = new StateMachine(this.key, this.events);

        this.startState = new StateNode(stateMachine, 'StartState');

        new StateTransition('ExitBattleScene', this.startState, this.startState, this.closeBattleScene.bind(this));


        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    setupMainCamera() {
        this.mainCamera = this.cameras.main;
    }

    resumeHandler() {
        super.resumeHandler();

    }

    shutdownHandler() {
        super.shutdownHandler();

        this.mainCamera = null;

        for (let group in this.sprites) {
            let arr = this.sprites[group];
            if (Array.isArray(arr)) {
                arr.forEach(sprite => {
                    sprite.destroy();
                });
            } else {
                arr.destroy();
            }
        }

    }

    closeBattleScene() {

    }

    drawClearBackground() {
        this.sprites.screenBackground = [];

        let settings = this.registry.get('gameSettings');
        let tileSize = settings.tileSizeMenu;
        let widthInTiles = settings.gameWidthInMenuTiles;
        let heightInTiles = settings.gameHeightInMenuTiles;
        let cameraX = this.mainCamera.scrollX;
        let cameraY = this.mainCamera.scrollY;

        for (let i = 0; i <= widthInTiles; i++) {
            for (let j = 0; j <= heightInTiles; j++) {
                let newX = cameraX + (i * tileSize);
                let newY = cameraY + (j * tileSize);
                let sprite = this.add.image(newX, newY, 'sprites-menu-tiles', 4).setOrigin(0, 0);
                this.sprites.screenBackground.push(sprite);
            }
        }
    }

    flashScreen(data, callback) {
        this.sprites.screenFlashSprites = [];

        let settings = this.registry.get('gameSettings');
        let tileSize = settings.tileSizeMenu;
        let widthInTiles = settings.gameWidthInMenuTiles;
        let heightInTiles = settings.gameHeightInMenuTiles;
        let cameraX = this.mainCamera.scrollX;
        let cameraY = this.mainCamera.scrollY;

        for (let i = 0; i <= widthInTiles; i++) {
            for (let j = 0; j <= heightInTiles; j++) {
                let newX = cameraX + (i * tileSize);
                let newY = cameraY + (j * tileSize);
                let sprite = this.add.image(newX, newY, 'sprites-menu-tiles', 4).setOrigin(0, 0);
                sprite.setDepth(settings.depthList.screenFlash);
                this.sprites.screenFlashSprites.push(sprite);
            }
        }

        this.scene.pause();
        setTimeout((that) => {
            that.sceneWasCreated = true;

            that.scene.resume();

            that.sprites.screenFlashSprites.forEach((sprite) => {
                sprite.destroy();
            });
            that.sprites.screenFlashSprites = [];
        }, this.flashScreenDuration, this);
    }

    preload() {

    }

    create(data) {
        super.create(data);

        this.setupMainCamera();
        this.flashScreen();
        this.drawClearBackground();

        this.battleMenu = new BattleMenu(this, data);

        this.addMenus([
            this.battleMenu,
        ]);

        this.setCurrentMenu(this.battleMenu);
    }

    update(time, delta) {
        super.update(time, delta);

        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.down)) {
                console.log('\nDown Key pressed in ' + this.key);

                this.currentMenu.pressDown();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.up)) {
                console.log('\nUp Key pressed in ' + this.key);

                this.currentMenu.pressUp();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.left)) {
                console.log('\nLeft Key pressed in ' + this.key);

                this.currentMenu.pressLeft();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.right)) {
                console.log('\nRight Key pressed in ' + this.key);

                this.currentMenu.pressRight();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
                console.log('\nAction Key pressed in ' + this.key);
                this.currentMenu.confirm();

            } else if (Phaser.Input.Keyboard.JustUp(this.keys.action)) {
                // console.log('Action Key released in ' + this.key);

                this.currentMenu.confirmReleased();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
                console.log('\nCancel Key pressed in ' + this.key);
                this.currentMenu.cancel();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
                console.log('\nStart Key pressed in ' + this.key);

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.select)) {
                console.log('\nSelect Key pressed in ' + this.key);

            }
        }

        if (isDefinedAndNotNull(this.currentMenu)) {
            this.currentMenu.update(time, delta);
        }
    }
}