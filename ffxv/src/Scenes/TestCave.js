import BaseGameScene from './BaseGameScene';

export default class TestCave extends BaseGameScene {

    constructor(key) {
        super(key);

        this.tilesetKey = 'tileset-01';
        this.tilemapKey = 'tilemap-02';
        this.initializeBackgroundMusicKey('bgm-wanderlust');
    }

    initialize() {
        super.initialize();
    }

    onOverlapWithZones(player, object) {
        super.onOverlapWithZones(player, object);

        if (player.x === object.x && player.y === object.y) {
            // console.log('Player ' + `at [${player.x}, ${player.y}]` + ' overlapping with ' + object.key + ` at [${object.x}, ${object.y}]`);

            switch (object.key) {
                case 'zone-000':
                    let data = {
                        key: 'TestScene2',
                        spawnPoint: '001',
                        playerFaceDirection: 'down',
                        music: {
                            // keepPlayingMusic: true,
                        },
                    };
                    this.game.events.emit('Game', 'RunNewScene', data);

                    break;

                default:
                    break;
            }
        }
    }

    preload(data) {
        super.preload(data);

        this.load.image(this.tilesetKey, 'assets/tilesets/tileset-01.png');
        this.load.tilemapTiledJSON(this.tilemapKey, 'assets/tilemaps/' + this.tilemapKey + '.json');
    }

    create(data) {
        super.create(data);
    }

    update(time, delta) {
        super.update();

        // if(this.playerSprite) {
        // 	// this.playerSprite.update(time, delta);
        // }

        if (Phaser.Input.Keyboard.JustDown(this.keys.select)) {
            // console.log('Select Key pressed in ' + this.key);
            // this.game.events.emit('Game', 'ExitToTitleScreen');
        } else if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
            // console.log('Start Key pressed in ' + this.key);
        } else if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
            // console.log('Cancel Key pressed in ' + this.key);
        } else if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
            // console.log('Action Key pressed in ' + this.key);
        } else if (Phaser.Input.Keyboard.JustDown(this.keys.down)) {
            // console.log('Down Key pressed in ' + this.key);
        } else if (Phaser.Input.Keyboard.JustDown(this.keys.up)) {
            // console.log('Up Key pressed in ' + this.key);
        } else if (Phaser.Input.Keyboard.JustDown(this.keys.left)) {
            // console.log('Left Key pressed in ' + this.key);
        } else if (Phaser.Input.Keyboard.JustDown(this.keys.right)) {
            // console.log('Right Key pressed in ' + this.key);
        }
    }
}