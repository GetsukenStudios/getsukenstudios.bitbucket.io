import BaseGameScene from './BaseGameScene';

export default class TestScene2 extends BaseGameScene {

	constructor(key) {
		super(key);

		this.tilesetKey = 'tileset-01';
		this.tilemapKey = 'tilemap-01';
		this.initializeBackgroundMusicKey('bgm-wanderlust');
	}

	initialize() {
		super.initialize();
	}

	onOverlapWithZones(player, object) {
		super.onOverlapWithZones(player, object);

		if (player.x === object.x && player.y === object.y) {
			// console.log('Player ' + `at [${player.x}, ${player.y}]` + ' overlapping with ' + object.key + ` at [${object.x}, ${object.y}]`);

			switch (object.key) {
				case 'zone-000':
					let data = {
						key: 'TestCave',
						spawnPoint: '000',
						playerFaceDirection: 'up',
						music: {
							// keepPlayingMusic: true,
						},
					};
					this.game.events.emit('Game', 'RunNewScene', data);

					break;

				default:
					break;
			}
		}
	}

	startBattle() {
		// this.game.events.emit('Menu', 'OpenBattleMenu');
		this.game.events.emit('Menu', 'OpenBattleMenu', {
			monsterMob: 'monster-mob-02',
			playerSprite: {
				faceDirection: this.sprites.playerSprite.getFaceDirection(),
				frameCount: this.sprites.playerSprite.getFrameCount()
			},
			music: {
				pauseCurrentAudio: true,
			},
		}, this.callbackForAfterBattle.bind(this));
	}

	callbackForAfterBattle() {
		console.log('>>> callback after battle was called in ' + this.key + ' !!!');
	}

	preload(data) {
		super.preload(data);

		this.load.image(this.tilesetKey, 'assets/tilesets/tileset-01.png');
		this.load.tilemapTiledJSON(this.tilemapKey, 'assets/tilemaps/' + this.tilemapKey + '.json');
	}

	create(data) {
		super.create(data);

		// TODO: just for testing
		this.startedBattle = false;
	}

	update(time, delta) {
		super.update(time, delta);

		// TODO: just for testing
		if (!this.startedBattle && this.sprites.playerSprite) {
			let appSettings = this.registry.get('appSettings');
			if (appSettings.debugMode) this.startBattle();

			this.startedBattle = true;
		}

		if (Phaser.Input.Keyboard.JustDown(this.keys.select)) {
			console.log('Select Key pressed in ' + this.key);
			this.startBattle();
			this.startedBattle = true;
		} else if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
			// console.log('Start Key pressed in ' + this.key);
		} else if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
			// console.log('Cancel Key pressed in ' + this.key);
		} else if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
			// console.log('Action Key pressed in ' + this.key);
		} else if (Phaser.Input.Keyboard.JustDown(this.keys.down)) {
			// console.log('Down Key pressed in ' + this.key);
		} else if (Phaser.Input.Keyboard.JustDown(this.keys.up)) {
			// console.log('Up Key pressed in ' + this.key);
		} else if (Phaser.Input.Keyboard.JustDown(this.keys.left)) {
			// console.log('Left Key pressed in ' + this.key);
		} else if (Phaser.Input.Keyboard.JustDown(this.keys.right)) {
			// console.log('Right Key pressed in ' + this.key);
		}
	}
}