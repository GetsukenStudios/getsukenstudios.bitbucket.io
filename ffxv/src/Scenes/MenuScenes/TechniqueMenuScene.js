import StateTransition from '../../State/StateTransition';
import StateNode from '../../State/StateNode';
import StateMachine from '../../State/StateMachine';
import {
    Character,
    Monster
} from '../../Game/GameObjects';
import {
    isDefinedAndNotNull,
    isUndefinedOrNull,
    getPropOrDefault,
    isDefinedAndFunction,
    isDefined,
    remove
} from '../../Base/Utils';
import BaseMenuScene from './BaseMenuScene';
import {
    GearMenu
} from '../../Gui/GearMenu';
import {
    TechniqueMenu
} from '../../Gui/TechniqueMenu';
import {
    DialogMenu
} from '../../Gui/DialogMenu';

export default class TechniqueMenuScene extends BaseMenuScene {

    constructor(key) {
        super(key);

    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.key, this.events);

        this.startState = new StateNode(stateMachine, 'StartState');

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    preload() {

    }

    create(data) {
        super.create();

        this.selectedChar = data.selectedChar;

        this.techniqueMenu = new TechniqueMenu(this, data);

        this.addMenus([
            this.techniqueMenu,
        ]);

        this.setCurrentMenu(this.techniqueMenu);
    }

    update(time, delta) {
        super.update(time, delta);

        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.down)) {
                console.log('Down Key pressed in ' + this.key);

                this.currentMenu.pressDown();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.up)) {
                console.log('Up Key pressed in ' + this.key);

                this.currentMenu.pressUp();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.left)) {
                console.log('Left Key pressed in ' + this.key);

                this.currentMenu.pressLeft();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.right)) {
                console.log('Right Key pressed in ' + this.key);

                this.currentMenu.pressRight();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
                console.log('Action Key pressed in ' + this.key);

                this.currentMenu.confirm();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
                console.log('Cancel Key pressed in ' + this.key);

                this.currentMenu.cancel();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
                console.log('Start Key pressed in ' + this.key);

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.select)) {
                console.log('Select Key pressed in ' + this.key);

            }
        }

        if (isDefinedAndNotNull(this.currentMenu)) {
            this.currentMenu.update(time, delta);
        }
    }
}