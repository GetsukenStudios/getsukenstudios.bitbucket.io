import StateTransition from '../../State/StateTransition';
import StateNode from '../../State/StateNode';
import StateMachine from '../../State/StateMachine';
import CustomKeys from '../../Base/CustomKeys';
import {
    isDefinedAndNotNull,
    getPropOrDefault,
    isDefinedAndFunction
} from '../../Base/Utils';

export default class ListMenuScene extends Phaser.Scene {

    constructor(handle, parent, closeCallback) {
        super(handle);
        this.handle = handle;
        this.parent = parent;
        this.closeCallback = closeCallback;

        this.settings = {};
        this.keys = {};

        let config = {};
        this.width = getPropOrDefault(config, 'width', 60);
        this.height = getPropOrDefault(config, 'height', 60);
        this.maxVisibleItems = getPropOrDefault(config, 'maxVisibleItems', 3);
    }

    preload() {
        this.load.setBaseURL();
    }

    create() {
        // this.settings = this.registry.get('gameSettings');
        this.keys = this.registry.get('keys');
        // let options = null;
        // this.keys = new CustomKeys(options, this.input.keyboard);
        // this.input.keyboard.on("keydown", this.handleKeyDownEvent.bind(this), this);

        this.cameras.main.setViewport(0, 0, this.width, this.height);
        this.graphics = this.add.graphics();
    }

    init(data) {
        this.listItems = null;
        this.selectedItemIdx = 0;
        this.moveDir = 0;
        this.lastOffset = 0;
        this.isDirty = true;

        this.listItems = getPropOrDefault(data, 'items', null);
        //init test data if not set
        if (!isDefinedAndNotNull(this.listItems)) {
            this.listItems = ['item 1', 'item 2', 'item 3', 'item 4', 'item 5'];
            // this.listItems = [];
        }
        this.itemMap = [];
        this.setDirty();
    }

    handleKeyDownEvent(event) {
        console.log(event);

        if (isDefinedAndNotNull(this.keys)) {
            if (event.keyCode == this.keys.down.keyCode) {
                console.log('down key pressed in ' + this.handle);
                this.moveDown();
            } else if (event.keyCode == this.keys.up.keyCode) {
                console.log('up key pressed in ' + this.handle);
                this.moveUp();
            } else if (event.keyCode == this.keys.cancel.keyCode) {
                console.log('cancel key pressed in ' + this.handle);
                this.closeMenu();
            }
        }
    }

    closeMenu() {
        this.clearItemMap();
        if (isDefinedAndFunction(this.closeCallback)) {
            this.closeCallback();
        }
    }

    setDirty() {
        this.isDirty = true;
    }

    moveDown() {
        this.moveDir = 1;
        if (this.selectedItemIdx < this.listItems.length - 1) {
            this.selectedItemIdx = this.selectedItemIdx + this.moveDir;
        }
        this.setDirty();
    }

    moveUp() {
        this.moveDir = -1;
        if (this.selectedItemIdx > 0) {
            this.selectedItemIdx = this.selectedItemIdx + this.moveDir;
        }
        this.setDirty();
    }

    clearItemMap() {
        if (isDefinedAndNotNull(this.itemMap)) {
            for (let item in this.itemMap) {
                this.itemMap[item].destroy();
            }
            this.itemMap = [];
        } else {
            this.itemMap = [];
        }
    }

    update() {
        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.down)) {
                console.log('down key pressed in ' + this.handle);
                this.moveDown();
            } else if (Phaser.Input.Keyboard.JustDown(this.keys.up)) {
                console.log('up key pressed in ' + this.handle);
                this.moveUp();
            } else if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
                console.log('cancel key pressed in ' + this.handle);
                this.closeMenu();
            }
        }

        if (this.isDirty) {
            this.updateMenu();
        }
    }

    updateMenu() {
        this.isDirty = false;
        this.graphics.clear();
        this.clearItemMap();

        let x = 0;
        let y = 0;
        let w = this.width;
        let rowCount = this.maxVisibleItems;
        if (rowCount <= 0) {
            rowCount = 1;
        }
        let h = this.height / rowCount;

        let length = this.listItems.length;

        if (this.moveDir < 0) {
            if (this.selectedItemIdx < this.lastOffset) {
                this.lastOffset = this.selectedItemIdx;
            }
        } else if (this.moveDir > 0) {
            if (this.selectedItemIdx == this.lastOffset + this.maxVisibleItems &&
                this.selectedItemIdx > this.maxVisibleItems - 1) {
                this.lastOffset = this.selectedItemIdx - this.maxVisibleItems + 1;
            }
        }

        if (this.lastOffset > length - this.maxVisibleItems) {
            this.lastOffset = length - this.maxVisibleItems;
        }
        if (this.lastOffset < 0) {
            this.lastOffset = 0;
        }

        for (let i = 0; i < this.maxVisibleItems; i++) {
            let idx = i + this.lastOffset;
            let item = this.listItems[idx];

            if (idx == this.selectedItemIdx) {
                this.graphics.fillStyle(0xffff00, 1);
            } else {
                this.graphics.fillStyle(0x00ffff, 1);
            }
            let yi = y + i * h;
            this.graphics.fillRect(x, yi, this.width, h);
            let text = this.add.text(x, yi, item, {
                font: '11px Courier',
                fill: '#00ff00'
            }); //.setDepth(1000);
            this.itemMap.push(text);
        }

    }
}