import StateTransition from '../../State/StateTransition';
import StateNode from '../../State/StateNode';
import StateMachine from '../../State/StateMachine';
import CustomKeys from '../../Base/CustomKeys';
import {
    Character,
    Monster
} from '../../Game/GameObjects';
import {
    isDefinedAndNotNull,
    isUndefinedOrNull,
    getPropOrDefault,
    isDefinedAndFunction,
    isDefined,
    remove
} from '../../Base/Utils';
import {
    MainMenu
} from '../../Gui/MainMenu';
import {
    ItemMenu
} from '../../Gui/ItemMenu';
import BaseMenuScene from './BaseMenuScene';

export default class ItemMenuScene extends BaseMenuScene {

    constructor(key) {
        super(key);

    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.key, this.events);

        this.startState = new StateNode(stateMachine, 'StartState');

        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    closeAllMenus() {
        this.game.events.emit('Game', 'LeaveMenuToPlay');
    }

    preload() {

    }

    create() {
        super.create();

        this.itemMenu = new ItemMenu(this);

        this.addMenus([
            this.itemMenu,
        ]);

        this.setCurrentMenu(this.itemMenu);
    }

    update() {
        super.update();


        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.down)) {
                console.log('Down Key pressed in ' + this.key);

                this.currentMenu.pressDown();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.up)) {
                console.log('Up Key pressed in ' + this.key);

                this.currentMenu.pressUp();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.left)) {
                console.log('Left Key pressed in ' + this.key);

                this.currentMenu.pressLeft();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.right)) {
                console.log('Right Key pressed in ' + this.key);

                this.currentMenu.pressRight();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
                console.log('Action Key pressed in ' + this.key);

                this.currentMenu.confirm();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
                console.log('Cancel Key pressed in ' + this.key);

                this.currentMenu.cancel();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
                console.log('Start Key pressed in ' + this.key);

                // TODO: how to handle closing multiple menu scenes at once?
                //      maybe add "stop markers" in MenuState scene stack? 
                // this.closeAllMenus();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.select)) {
                console.log('Select Key pressed in ' + this.key);

            }
        }

    }
}