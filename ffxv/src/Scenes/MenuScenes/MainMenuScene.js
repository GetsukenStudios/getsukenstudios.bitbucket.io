import StateTransition from '../../State/StateTransition';
import StateNode from '../../State/StateNode';
import StateMachine from '../../State/StateMachine';
import {
    isDefinedAndNotNull,
    remove,
} from '../../Base/Utils';
import {
    MainMenu
} from '../../Gui/MainMenu';
import BaseMenuScene from './BaseMenuScene';
import {
    CharListMenu
} from '../../Gui/CharListMenu';

export default class MainMenuScene extends BaseMenuScene {

    constructor(key) {
        super(key);

    }

    initStateMachine() {
        super.initStateMachine();

        let stateMachine = new StateMachine(this.key, this.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        this.itemMenuState = new StateNode(stateMachine, 'ItemMenuState');
        // this.waitingForItemState = new StateNode(stateMachine, 'WaitingForItemState');
        this.charListMenuState = new StateNode(stateMachine, 'CharListMenuState');

        new StateTransition('ExitMainMenu', this.startState, this.startState, this.closeMainMenu.bind(this));
        new StateTransition('OpenItemMenu', this.startState, this.itemMenuState);
        // new StateTransition('OpenItemMenu', this.startState, this.waitingForItemState, this.waitingForItem.bind(this));
        new StateTransition('CloseItemMenu', this.itemMenuState, this.startState);
        // new StateTransition('ReceivedItem', this.waitingForItemState, this.startState);
        new StateTransition('OpenCharListMenu', this.startState, this.charListMenuState, this.openCharListMenu.bind(this));
        new StateTransition('CloseCharListMenu', this.charListMenuState, this.startState, this.closeCharListMenu.bind(this));


        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    waitingForItem() {
        this.setCallbackForNextScene(this.receivedItem.bind(this));
    }

    receivedItem(data) {
        console.log('MainMenu received selected item ' + data.selectedItem.getShortName() + ' from ItemMenu [callback works!]');

        this.events.emit('Action', 'ReceivedItem');
    }

    openCharListMenu(data) {
        this.charListMenu = new CharListMenu(this, data);
        this.addMenu(this.charListMenu);
        this.setCurrentMenu(this.charListMenu);
    }

    closeCharListMenu() {
        this.setCurrentMenu(this.mainMenu);
        this.removeMenu(this.charListMenu);
    }

    closeMainMenu() {
        this.game.events.emit('Menu', 'CloseMainMenu');
    }

    closeAllMenus() {
        this.game.events.emit('Game', 'LeaveMenuStateToPlayState');
    }

    resumeHandler() {
        super.resumeHandler();

    }

    shutdownHandler() {
        super.shutdownHandler();

    }

    preload() {

    }

    create() {
        super.create();

        this.mainMenu = new MainMenu(this);

        this.addMenus([
            this.mainMenu,
        ]);

        this.setCurrentMenu(this.mainMenu);
    }

    update() {
        super.update();


        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.down)) {
                console.log('\nDown Key pressed in ' + this.key);

                this.currentMenu.pressDown();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.up)) {
                console.log('\nUp Key pressed in ' + this.key);

                this.currentMenu.pressUp();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.left)) {
                console.log('\nLeft Key pressed in ' + this.key);

                this.currentMenu.pressLeft();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.right)) {
                console.log('\nRight Key pressed in ' + this.key);

                this.currentMenu.pressRight();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
                console.log('\nAction Key pressed in ' + this.key);

                this.currentMenu.confirm();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
                console.log('\nCancel Key pressed in ' + this.key);

                this.currentMenu.cancel();

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
                console.log('\nStart Key pressed in ' + this.key);

                // if(this.stateMachine.getCurrentStateName() === 'StartState') {
                //     this.closeMainMenu();
                // }

            } else if (Phaser.Input.Keyboard.JustDown(this.keys.select)) {
                console.log('\nSelect Key pressed in ' + this.key);

            }
        }

    }
}