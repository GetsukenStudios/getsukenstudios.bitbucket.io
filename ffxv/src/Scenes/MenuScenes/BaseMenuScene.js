import {
    isDefinedAndNotNull,
    remove,
    contains
} from '../../Base/Utils';
import BaseScene from '../BaseScene';

export default class BaseMenuScene extends BaseScene {

    constructor(key) {
        super(key);

        this.menus = [];
        this.oldMenu = null;
        this.currentMenu = null;
        this.stateMachine = null;
        this.isInitialized = false;

        this.callbackForNextScene = null;
        this.parentSceneCallback = null;

        this.flashScreenDuration = 500;
    }

    initialize() {
        super.initialize();
    }

    initStateMachine() {
        super.initStateMachine();
    }

    getCallbackForNextScene() {
        return this.callbackForNextScene;
    }

    setCallbackForNextScene(callback) {
        this.callbackForNextScene = callback;
    }

    getParentSceneCallback() {
        return this.parentSceneCallback;
    }

    setParentSceneCallback(callback) {
        this.parentSceneCallback = callback;
    }

    draw() {
        if (isDefinedAndNotNull(this.menus)) {
            this.menus.forEach(menu => {
                if (menu.getMenuVisible()) {
                    menu.draw();
                }
            });
        }
        // if (isDefinedAndNotNull(this.currentMenu) && this.currentMenu.getMenuVisible()) {
        //     this.currentMenu.draw();
        // }
    }

    addMenu(menu) {
        this.menus.push(menu);
        // if (isUndefinedOrNull(this.currentMenu)) {
        //     this.setCurrentMenu(menu);
        // }
    }

    addMenus(menus) {
        menus.forEach(menu => {
            this.addMenu(menu);
        });
    }

    removeMenu(menu) {
        menu.destroyMenu();
        remove(this.menus, menu);
    }

    removeAllMenus() {
        this.menus.forEach(menu => {
            this.removeMenu(menu);
        });
    }

    getOldMenu() {
        return this.oldMenu;
    }

    getCurrentMenu() {
        return this.currentMenu;
    }

    setCurrentMenu(menu) {
        if (contains(this.menus, menu)) {
            if (isDefinedAndNotNull(this.menus) &&
                isDefinedAndNotNull(menu)) {
                if (isDefinedAndNotNull(this.currentMenu)) {
                    this.currentMenu.setActive(false);
                    this.oldMenu = this.currentMenu;
                    menu.setActive(true);
                    this.currentMenu = menu;
                    this.currentMenu.resumeCallback();
                } else {
                    menu.setActive(true);
                    this.oldMenu = menu;
                    this.currentMenu = menu;
                }
            }
        }
    }

    switchToOldMenu() {
        this.setCurrentMenu(this.getOldMenu());
    }

    resumeHandler() {
        super.resumeHandler();

        this.getCurrentMenu().resumeCallback();

        // console.log('\nresumed ' + this.key);
    }

    shutdownHandler() {
        super.shutdownHandler();

        this.removeAllMenus();
        this.menus = [];
        this.currentMenu = null;
        this.oldMenu = null;

        this.callbackForNextScene = null;
        this.parentSceneCallback = null;

        // console.log('\nshutdown/stopped ' + this.key);
    }

    init(data) {
        if (!this.isInitialized) {
            this.initialize();
            this.initStateMachine();

            this.events.on('pause', this.pauseHandler.bind(this));
            this.events.on('resume', this.resumeHandler.bind(this));
            this.events.on('shutdown', this.shutdownHandler.bind(this));

            this.isInitialized = true;
            console.log('\ninitiated ' + this.key);
        }
    }

    preload(data) {
        super.preload(data);

    }

    create(data) {
        super.create(data);

        // this.initialize();
        // this.initStateMachine();
        console.log('\ncreated ' + this.key);
    }

    update(time, delta) {
        super.update(time, delta);

    }
}