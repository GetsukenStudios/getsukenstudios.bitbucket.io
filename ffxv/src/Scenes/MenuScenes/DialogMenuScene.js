import StateNode from '../../State/StateNode';
import StateMachine from '../../State/StateMachine';
import {
    isDefinedAndNotNull,
} from '../../Base/Utils';
import BaseMenuScene from './BaseMenuScene';
import { DialogMenu } from '../../Gui/DialogMenu';
import StateTransition from '../../State/StateTransition';

export default class DialogMenuScene extends BaseMenuScene {

    constructor(key) {
        super(key);

    }

    initialize() {
        super.initialize();

    }

    initStateMachine() {
        super.initStateMachine();
        
    }

    preload(data) {
        super.preload(data);

    }

    create(data) {
        super.create(data);

        this.dialogMenu = new DialogMenu(this, data);

        this.addMenus([
            this.dialogMenu,
        ]);

        this.setCurrentMenu(this.dialogMenu);
    }

    update(time, delta) {
        super.update(time, delta);

        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
                // console.log('Action Key pressed in ' + this.key);

                if (isDefinedAndNotNull(this.currentMenu)) {
                    this.currentMenu.confirm();
                }
            }
            if (Phaser.Input.Keyboard.JustUp(this.keys.action)) {
                // console.log('Action Key released in ' + this.key);

                if (isDefinedAndNotNull(this.currentMenu)) {
                    this.currentMenu.confirmReleased();
                }
            }
            if (Phaser.Input.Keyboard.JustDown(this.keys.cancel)) {
                // console.log('Cancel Key pressed in ' + this.key);

                if (isDefinedAndNotNull(this.currentMenu)) {
                    this.currentMenu.cancel();
                }
            }
        }

        if (isDefinedAndNotNull(this.currentMenu)) {
            this.currentMenu.update(time, delta);
        }
    }
}