import StateNode from '../../State/StateNode';
import StateMachine from '../../State/StateMachine';
import {
    isDefinedAndNotNull,
} from '../../Base/Utils';
import { ChestLootMenu } from '../../Gui/ChestLootMenu';
import BaseMenuScene from './BaseMenuScene';

export default class ChestLootMenuScene extends BaseMenuScene {

    constructor(key) {
        super(key);

    }

    initStateMachine() {
        let stateMachine = new StateMachine(this.key, this.events);

        this.startState = new StateNode(stateMachine, 'StartState');
        
        this.stateMachine = stateMachine;

        this.stateMachine.start();
    }

    preload() {
        
    }

    create(data) {
        super.create();

        this.chestLootMenu = new ChestLootMenu(this, data);

        this.addMenus([
            this.chestLootMenu,
        ]);

        this.setCurrentMenu(this.chestLootMenu);
    }

    update() {
        super.update();
        
        if (isDefinedAndNotNull(this.keys)) {
            if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
                // console.log('Action Key pressed in ' + this.key);
                
                this.currentMenu.confirm();

            } 
        }

    }
}