import CustomKeys from '../Base/CustomKeys';
import StateTransition from '../State/StateTransition';
import StateNode from '../State/StateNode';
import StateMachine from '../State/StateMachine';
import BaseScene from './BaseScene';
import {
	isDefinedAndNotNull
} from '../Base/Utils';
import BaseGameScene from './BaseGameScene';
import {
	SSL_OP_EPHEMERAL_RSA
} from 'constants';

export default class TitleScreenScene extends BaseGameScene {

	constructor(key) {
		super(key);

		this.initializeBackgroundMusicKey('bgm-somnus-main-theme');
	}

	initialize() {
		super.initialize();
	}

	initStateMachine() {

		let stateMachine = new StateMachine(this.key, this.events);

		let startState = new StateNode(stateMachine, 'StartState');
		let continueState = new StateNode(stateMachine, 'ContinueState');
		let startGameState = new StateNode(stateMachine, 'StartGameState');
		let loadGameState = new StateNode(stateMachine, 'LoadGameState');

		new StateTransition('PressRight', startState, continueState, this.stateEnterContinue.bind(this));
		// new StateTransition('PressRight', startState, continueState);
		new StateTransition('PressLeft', continueState, startState, this.stateEnterStart.bind(this));
		// new StateTransition('PressLeft', continueState, startState);
		new StateTransition('PressAction', startState, startGameState, this.stateEnterStartGame.bind(this));
		// new StateTransition('PressAction', startState, startGameState);
		new StateTransition('PressAction', continueState, loadGameState, this.stateEnterContinueGame.bind(this));
		// new StateTransition('PressAction', continueState, loadGameState);

		this.stateMachine = stateMachine;

		this.stateMachine.start();
	}

	stateEnterStart() {
		if (this.menuCursor) {
			this.menuCursor.setX(8);
		}
	}

	stateEnterContinue() {
		if (this.menuCursor) {
			this.menuCursor.setX(88);
		}
	}

	stateEnterStartGame() {
		this.game.events.emit('Game', 'LoadNewGame');
	}

	stateEnterContinueGame() {
		this.game.events.emit('Game', 'LoadSavedGame');
	}

	preload() {
		super.preload();

		this.load.setBaseURL();

		this.load.image('title-screen', 'assets/menu/title-screen/title-screen-01.png');
	}

	create() {
		super.create();

		this.settings = this.registry.get('gameSettings');
		let stringDB = this.registry.get('stringDatabase');

		// let options = null;
		// this.keys = new CustomKeys(options, this.input.keyboard);
		// this.input.keyboard.on("keydown", this.handleKeyDownEvent.bind(this), this);

		// add images
		this.titleScreen = this.add.image(this.settings.centerX, this.settings.centerY, 'title-screen');

		this.menuCursor = this.add.image(8, 110, 'sprites-menu-cursor');


		// add text 
		// let bitmap = this.cache.bitmapFont.get('retroBitmap');
		// let bitmap = Phaser.GameObjects.RetroFont.Parse(this, config);

		let textStartIcon = this.add.bitmapText(17, 102, 'retroBitmap', stringDB.getCustomStringByKey('string-start').getFullName().toUpperCase());
		let textContinueIcon = this.add.bitmapText(97, 102, 'retroBitmap', stringDB.getCustomStringByKey('string-continue').getFullName().toUpperCase());
		let textDemo = this.add.bitmapText(52, 80, 'retroBitmap', '[' + stringDB.getCustomStringByKey('string-demo').getFullName().toLowerCase() + ']');
		// let textCopyright = this.add.bitmapText(12, 128, 'retroBitmap', '©');
		let textDeveloper1 = this.add.bitmapText(25, 128, 'retroBitmap', stringDB.getCustomStringByKey('string-getsuken').getFullName() + ' ');
		let textDeveloper2 = this.add.bitmapText(93, 128, 'retroBitmap', stringDB.getCustomStringByKey('string-studios').getFullName());
	}

	update(time, delta) {
		super.update(time, delta);

		if (isDefinedAndNotNull(this.keys)) {
			// console.log('test');
			if (Phaser.Input.Keyboard.JustDown(this.keys.left)) {
				// console.log('Left Key pressed in ' + this.key);
				this.events.emit('Action', 'PressLeft');
				// this.stateEnterStart();
			}
			if (Phaser.Input.Keyboard.JustDown(this.keys.right)) {
				// console.log('Right Key pressed in ' + this.key);
				this.events.emit('Action', 'PressRight');
				// this.stateEnterContinue();
			}
			if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
				// console.log('Action Key pressed in ' + this.key);
				if (this.stateMachine.getCurrentStateName() == 'StartState') {
					this.events.emit('Action', 'PressAction');
					// this.stateEnterStartGame();
				} else if (this.stateMachine.getCurrentStateName() == 'ContinueState') {
					if (isDefinedAndNotNull(localStorage.getItem('ffxv-save-01'))) {
						this.events.emit('Action', 'PressAction');
						// this.stateEnterContinueGame();
					}
				}
			}
		}

		// console.log('still in ' + this.key + '!!!');
	}

}