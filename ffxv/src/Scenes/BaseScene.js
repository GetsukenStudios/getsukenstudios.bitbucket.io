import {
	isDefinedAndNotNull,
	isUndefinedOrNull
} from "../Base/Utils";
import CustomKeys from "../Base/CustomKeys";

export default class BaseScene extends Phaser.Scene {

	constructor(key) {
		super(key);

		this.parentScene = null;

		this.key = '';

		this.settings = {};
		this.keys = null;

		this.stateMachine = null;
		this.isInitialized = false;

		this.defaultBackgroundMusicKey = '';
		this.backgroundMusicKey = '';
		this.currentBackgroundMusic = null;

		this.gameState = null;
		this.menuState = null;

	}

	initialize() {
		this.key = this.sys.settings.key;

		// this.settings = this.registry.get('gameSettings');

		// sample override of custom keys:
		// let options = [{ keyName:'action', key : 'B'}, {}];
		this.keys = new CustomKeys(null, this.input.keyboard);

		let volume = this.registry.get('gameSettings').mainVolume;

		this.defaultAudioConfig = {
			mute: false,
			volume: volume,
			rate: 1,
			detune: 0,
			seek: 0,
			loop: true,
			delay: 0
		};

		this.gameState = this.game.gameState;
		this.menuState = this.game.menuState;
	}

	initStateMachine() {

	}

	getParentScene() {
		return this.parentScene;
	}

	setParentScene(scene) {
		this.parentScene = scene;
	}

	getKey() {
		return this.key;
	}

	setKey(key) {
		this.key = key;
	}

	getCurrentState() {
		return this.stateMachine.getCurrentState();
	}

	getCurrentStateName() {
		if (isDefinedAndNotNull(this.stateMachine)) {
			return this.stateMachine.getCurrentStateName();
		}
	}

	getGameState() {
		return this.gameState;
	}

	getMenuState() {
		return this.menuState;
	}

	getBackgroundMusicKey() {
		return this.backgroundMusicKey;
	}

	setBackgroundMusicKey(key) {
		this.backgroundMusicKey = key;
	}

	getDefaultBackgroundMusicKey() {
		return this.defaultBackgroundMusicKey;
	}

	setDefaultBackgroundMusicKey(key) {
		this.defaultBackgroundMusicKey = key;
	}

	setBackgroundMusicKeyToDefault() {
		this.backgroundMusicKey = this.defaultBackgroundMusicKey;
	}

	initializeBackgroundMusicKey(key) {
		this.setBackgroundMusicKey(key);
		this.setDefaultBackgroundMusicKey(key);
	}

	getCurrentBackgroundMusic() {
		return this.currentBackgroundMusic;
	}

	setCurrentBackgroundMusic(music) {
		this.currentBackgroundMusic = music;
	}

	playCurrentBackgroundMusic() {
		this.currentBackgroundMusic.play();
	}

	pauseCurrentBackgroundMusic() {
		this.currentBackgroundMusic.pause();
	}

	stopCurrentBackgroundMusic() {
		this.currentBackgroundMusic.stop();
	}

	resumeCurrentBackgroundMusic() {
		this.currentBackgroundMusic.resume();
	}

	changeBackgroundMusicAndPlay(newSongKey) {
		this.stopCurrentBackgroundMusic();
		this.setBackgroundMusicKey(newSongKey);
		this.currentBackgroundMusic = this.sound.add(this.getBackgroundMusicKey(), this.defaultAudioConfig);
		this.playCurrentBackgroundMusic();
	}

	setAudioVolume(volume) {
		this.sound.setVolume(volume);
	}

	setMute(mute) {
		if (mute) {
			this.sound.setVolume(0);
		} else {
			let volume = this.registry.get('gameSettings').mainVolume;
			this.sound.setVolume(volume);
		}
	}

	filterCanvasColor() {
		let canvas = document.getElementsByTagName('canvas')[0];
		let context = canvas.getContext('2d');
		if (context) {

			let imageData = context.getImageData(0, 0, canvas.width, canvas.height);
			let data = imageData.data;

			for (let i = 0; i < data.length; i++) {
				data[i] = 0;
			}

			// Update the canvas with the new data
			context.putImageData(imageData, 0, 0);
		}
	}

	resetKeyboards() {
		this.keys.resetAllKeys();
	}

	switchMute() {
		// let gameSettings = this.registry.get('gameSettings');
		let appSettings = this.registry.get('appSettings');
		// gameSettings.mute = !gameSettings.mute;
		appSettings.mute = !appSettings.mute;

		// // this.sound.setMute(gameSettings.mute);
		// this.setMute(gameSettings.mute);
		this.setMute(appSettings.mute);


		// instantly save app settings to localStorage so they're global and independent from saving/loading save states

		let appSettingsStorage = JSON.parse(localStorage.getItem('ffxv-app-save')) || {};
		appSettingsStorage = Object.assign(appSettingsStorage, {
			mute: appSettings.mute
		});
		localStorage.setItem('ffxv-app-save', JSON.stringify(appSettingsStorage));


		// console.log('Mute Key pressed in ' + this.key + ';  mute = ' + gameSettings.mute);
	}

	handleMusicPlaybackOnCreate(data) {
		let parentSceneMusic = null;
		this.setBackgroundMusicKeyToDefault();

		if (isDefinedAndNotNull(this.parentScene)) {
			parentSceneMusic = this.parentScene.getCurrentBackgroundMusic();
		}

		if (isDefinedAndNotNull(data) && isDefinedAndNotNull(data.music) && data.music.keepPlayingMusic &&
			isDefinedAndNotNull(parentSceneMusic)) {
			this.currentBackgroundMusic = parentSceneMusic;
		} else {
			if (isUndefinedOrNull(this.currentBackgroundMusic) && this.backgroundMusicKey !== '') {

				if (isDefinedAndNotNull(parentSceneMusic) && parentSceneMusic.key === this.backgroundMusicKey) {
					this.currentBackgroundMusic = parentSceneMusic;
				} else {
					this.currentBackgroundMusic = this.sound.add(this.backgroundMusicKey, this.defaultAudioConfig);
					this.playCurrentBackgroundMusic();
				}
			}
		}
	}

	handleMusicPlaybackOnPause(data) {
		if (isDefinedAndNotNull(data) && isDefinedAndNotNull(data.music)) {
			data.music.pauseCurrentAudio = data.music.pauseCurrentAudio || false;
		}

		if (isDefinedAndNotNull(data) && isDefinedAndNotNull(data.music) && data.music.pauseCurrentAudio &&
			isDefinedAndNotNull(this.currentBackgroundMusic) && this.currentBackgroundMusic.isPlaying) {
			this.pauseCurrentBackgroundMusic();
		}
	}

	handleMusicPlaybackOnResume(data) {
		if (isDefinedAndNotNull(this.currentBackgroundMusic) && this.currentBackgroundMusic.isPaused) {
			this.resumeCurrentBackgroundMusic();
		}
	}

	handleMusicPlaybackOnShutdown(data) {
		// TODO: compare current bgm key to next scene's bgm key
		if (isDefinedAndNotNull(this.currentBackgroundMusic) && isDefinedAndNotNull(data) &&
			this.getBackgroundMusicKey() === this.game.scene.getScene(data.key).getBackgroundMusicKey()) {
			return;
		}
		if (isDefinedAndNotNull(this.currentBackgroundMusic)) {
			this.stopCurrentBackgroundMusic();
			this.currentBackgroundMusic.destroy();
			this.setCurrentBackgroundMusic(null);
		}
	}

	pauseHandler(scene, data) {
		this.resetKeyboards();
		this.handleMusicPlaybackOnPause(data);

		console.log('\npaused ' + this.key);
	}

	resumeHandler(scene, data) {
		this.resetKeyboards();
		this.handleMusicPlaybackOnResume(data);

		console.log('\nresumed ' + this.key);
	}

	shutdownHandler(scene, data) {
		if (isDefinedAndNotNull(this.stateMachine)) {
			this.stateMachine.reset();
			// this.stateMachine.stop();
			// this.stateMachine = null;
		}

		this.resetKeyboards();
		this.handleMusicPlaybackOnShutdown(data);

		// TODO: possibly uncomment later if causes issues
		// 		had to comment out so flashScreen of parentScene when stopping BattleMenuScene works
		// this.parentScene = null;		

		console.log('\nshutdown/stopped ' + this.key);
	}

	init(data) {
		if (!this.isInitialized) {
			this.initialize();
			this.initStateMachine();

			this.events.on('pause', this.pauseHandler.bind(this));
			this.events.on('resume', this.resumeHandler.bind(this));
			this.events.on('shutdown', this.shutdownHandler.bind(this));

			this.isInitialized = true;
			console.log('\ninitiated ' + this.key);
		}
	}

	preload(data) {

	}

	create(data) {

		// play/continue background music depending on data passed by last scene
		this.handleMusicPlaybackOnCreate(data);
	}

	update(time, delta) {
		if (isDefinedAndNotNull(this.keys)) {
			if (Phaser.Input.Keyboard.JustDown(this.keys.mute)) {
				this.switchMute();
			}
		}

		// this.filterCanvasColor();
	}
}