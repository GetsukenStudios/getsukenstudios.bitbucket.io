import {
    isDefinedAndNotNull
} from '../Base/Utils';
import BaseGameScene from './BaseGameScene';

export default class LoadSavedGameScene extends BaseGameScene {

    constructor(key) {
        super(key);


    }

    preload() {

    }

    create(data) {
        super.create(data);

        let gameDataManager = this.registry.get('gameDataManager');
        gameDataManager.loadGame();


        //TODO: initialize game from the loaded save state
        let gameSettings = this.registry.get('gameSettings');
        let saveState = this.registry.get('saveState');

        // apply gameSettings
        // this.setMute(gameSettings.mute);     //mute is saved in appSettings now


        // apply saveState
        let key = saveState.getDataByKey('currentSceneKey');
        let position = saveState.getDataByKey('currentPosition');
        let direction = saveState.getDataByKey('currentFaceDirection');
        let newData = {
            key: key,
            playerPosition: position,
            playerFaceDirection: direction,
        };


        this.game.events.emit('Game', 'StartSavedGame', newData);
    }

    update(time, delta) {
        super.update(time, delta);

    }
}