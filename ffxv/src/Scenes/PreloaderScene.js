import CustomKeys from './../Base/CustomKeys';
import BaseScene from './BaseScene';
import {
    isDefinedAndNotNull
} from '../Base/Utils';
import BaseGameScene from './BaseGameScene';

export default class PreloaderScene extends BaseScene {

    constructor(key) {
        super(key);
    }

    getGameRatio() {
        if (isDefinedAndNotNull(this.gameSettings)) {
            return this.gameSettings.gameWidth / this.gameSettings.gameHeight;
        }
        return 1;
    }

    resize() {
        // let div = document.getElementById('phaser-app');
        // let canvas = document.getElementsByTagName('canvas')[0];

        // /* 	// simple 2*n scaling
        //     var n = 2;		
        //     canvas.style.width = (game._GAMEWIDTH * n) + "px";
        //     canvas.style.height = (game._GAMEHEIGHT * n) + "px"; */


        // let divWidth = 0;
        // let divHeight = 0;
        // let canvasWidth = 0;
        // let canvasHeight = 0;

        // let winWidth = window.innerWidth;
        // let winHeight = window.innerHeight;
        // let gameWidth = this.gameSettings.gameWidth;
        // let gameHeight = this.gameSettings.gameHeight;
        // let winRatio = winWidth / winHeight;
        // let gameRatio = this.getGameRatio();

        // let scale = 1;
        // let maxWidth = gameWidth * 3;
        // let maxHeight = gameHeight * 3;

        // if (winRatio <= gameRatio) {
        //     canvasWidth = winWidth;
        //     canvasHeight = (winWidth / gameRatio);
        // }
        // else {
        //     canvasWidth = (winHeight * gameRatio);
        //     canvasHeight = winHeight;
        // }

        // canvasWidth = Math.min(canvasWidth * scale, maxWidth);
        // canvasHeight = Math.min(canvasHeight * scale, maxHeight);
        // canvasWidth = Math.max(canvasWidth, gameWidth);
        // canvasHeight = Math.max(canvasHeight, gameHeight);

        // div.style.width = canvasWidth + "px";
        // div.style.height = canvasHeight + "px";

        // canvas.style.width = canvasWidth + "px";
        // canvas.style.height = canvasHeight + "px";


        // --- new method --- //        

        let div = document.getElementById('phaser-app');
        let canvas = document.getElementsByTagName('canvas')[0];

        let winWidth = window.innerWidth;
        let winHeight = window.innerHeight;
        let gameWidth = this.gameSettings.gameWidth;
        let gameHeight = this.gameSettings.gameHeight;
        let winRatio = winWidth / winHeight;
        let gameRatio = this.getGameRatio();

        let scale = 1;
        let maxWidth = gameWidth * 2;
        let maxHeight = gameHeight * 2;

        let divWidth = 0;
        let divHeight = 0;
        let canvasWidth = 0;
        let canvasHeight = 0;


        if (winRatio <= gameRatio) {
            divWidth = winWidth;
            divHeight = (winWidth / gameRatio);
        } else {
            divWidth = (winHeight * gameRatio);
            divHeight = winHeight;
        }

        // check messy device DPI
        let dpiW = divWidth / (canvas.width);
        let dpiH = divHeight / (canvas.height);

        if (winRatio <= gameRatio) {
            canvasWidth = winWidth * (dpiW / dpiH);
            canvasHeight = winWidth * (dpiW / dpiH) / gameRatio;
        } else {
            canvasWidth = winHeight * (dpiW / dpiH) * gameRatio;
            canvasHeight = winHeight * (dpiW / dpiH);
        }

        divWidth = Math.min(divWidth * scale, maxWidth);
        divHeight = Math.min(divHeight * scale, maxHeight);
        divWidth = Math.max(divWidth, gameWidth);
        divHeight = Math.max(divHeight, gameHeight);

        canvasWidth = Math.min(canvasWidth * scale, maxWidth);
        canvasHeight = Math.min(canvasHeight * scale, maxHeight);
        canvasWidth = Math.max(canvasWidth, gameWidth);
        canvasHeight = Math.max(canvasHeight, gameHeight);


        div.style.width = divWidth + "px";
        div.style.height = divHeight + "px";
        // div.style.width = canvasWidth + "px";
        // div.style.height = canvasHeight + "px";

        canvas.style.width = canvasWidth + "px";
        canvas.style.height = canvasHeight + "px";
    }

    filterCanvasColor() {
        const canvas = document.getElementsByTagName('canvas')[0];
        const context = canvas.getContext('2d');
        if (context) {

            let imageData = context.getImageData(0, 0, canvas.width, canvas.height);
            let data = imageData.data;

            for (let i = 0; i < data.length; i++) {
                data[i] = 255;
            }

            // Update the canvas with the new data
            context.putImageData(imageData, 0, 0);

            // context.refresh();
        }
    }

    preload() {
        window.addEventListener("resize", this.resize.bind(this));
        // window.addEventListener("resize", this.filterCanvasColor.bind(this));

        // font
        this.load.image('ff-retro-font', 'assets/fonts/final-fantasy-retrofont.png');

        // spritesheets & images
        this.load.spritesheet('sprites-menu-tiles', 'assets/menu/menu-tiles.png', {
            frameWidth: 8,
            frameHeight: 8
        });
        this.load.spritesheet('sprites-item-icons', 'assets/menu/inventory-icons.png', {
            frameWidth: 8,
            frameHeight: 8
        });
        this.load.image('sprites-menu-cursor', 'assets/menu/hand-cursor.png');
        this.load.image('sprites-menu-scroll-arrow', 'assets/menu/menu-scroll-arrow.png');
        this.load.image('sprites-trash-can', 'assets/menu/trash-bin-icon.png');
        this.load.spritesheet('sprites-terrain-01', 'assets/sprites/terrain/terrain-01.png', {
            frameWidth: 16,
            frameHeight: 16
        });
        this.load.spritesheet('sprites-props-01', 'assets/sprites/props/props-01.png', {
            frameWidth: 16,
            frameHeight: 16
        });

        // character spritesheets 
        this.load.spritesheet('sprites-main-chars', 'assets/sprites/characters/main-chars.png', {
            frameWidth: 16,
            frameHeight: 16
        });
        this.load.spritesheet('sprites-side-chars', 'assets/sprites/characters/side-chars.png', {
            frameWidth: 16,
            frameHeight: 16
        });
        this.load.spritesheet('sprites-monsters', 'assets/sprites/characters/monsters.png', {
            frameWidth: 16,
            frameHeight: 16
        });

        // monster sprites
        this.load.image('monster-goblin-01', 'assets/sprites/monsters/monster-goblin-01.png');

        // music
        this.load.audio('bgm-somnus-main-theme', 'assets/music/somnus-main-theme.mp3', 'assets/music/somnus-main-theme.ogg');
        this.load.audio('bgm-wanderlust', 'assets/music/wanderlust.mp3', 'assets/music/wanderlust.ogg');
        this.load.audio('bgm-up-for-a-challenge', 'assets/music/up-for-a-challenge.mp3', 'assets/music/up-for-a-challenge.ogg');
        this.load.audio('bgm-hunt-or-be-hunted', 'assets/music/hunt-or-be-hunted.mp3', 'assets/music/hunt-or-be-hunted.ogg');
        this.load.audio('bgm-game-over', 'assets/music/game-over.mp3', 'assets/music/game-over.ogg');

        // sfx
        this.load.audio('sfx-get-item', 'assets/sfx/get-item.mp3', 'assets/sfx/get-item.ogg');
    }

    create() {
        super.create();

        this.gameSettings = this.registry.get('gameSettings');

        this.loadingDone = false;

        // setup bitmapFont        
        let charSet = this.gameSettings.retroFontCharSet;
        let config = {
            image: 'ff-retro-font',
            width: 8,
            height: 8,
            charsPerRow: 13,
            spacing: {
                x: 1,
                y: 0
            },
            chars: charSet
        };

        let bitmap = this.cache.bitmapFont.add('retroBitmap', Phaser.GameObjects.RetroFont.Parse(this, config));

        // this.resize();
    }

    update() {
        if (!this.loadingDone) {
            this.loadingDone = true;
            // TODO: try getting filtering to work 
            this.resize();
            // this.filterCanvasColor();


            // load app-global save state data, but overwrite if data already exists in gameDataManager's appSettings (for debug inits) 
            let appSave = {};
            let appSettings = this.registry.get('appSettings');
            if (isDefinedAndNotNull(localStorage.getItem('ffxv-app-save'))) {
                appSave = JSON.parse(localStorage.getItem('ffxv-app-save'));

                // only selected settings in appSettings will overwrite the app settings in localStorage
                if (appSettings.mute) appSave.mute = appSettings.mute;
            }
            appSettings = Object.assign(appSettings, appSave);
            localStorage.setItem('ffxv-app-save', JSON.stringify(appSave));


            //// Phaser's mute doesn't work in Firefox, have to use my own method by global volume adjusting
            // this.sound.setMute(this.gameSettings.mute);
            this.setMute(appSettings.mute);


            if (!appSettings.debugMode) {
                this.game.events.emit('Game', 'ToDisclaimer');
                // this.game.events.emit('Game', 'ToTitleScreen');
            } else {
                // this.game.events.emit('Game', 'LoadTest', {
                //     spawnPoint: '000',
                //     // playerFaceDirection: 'up',
                // });
                this.game.events.emit('Game', 'LoadTest');
            }
        }
    }

}