import Phaser from 'phaser';
import GameState from './Game/GameState';
import MenuState from './Game/MenuState';
import GameDataManager from './Game/GameDataManager';
import {
	Character,
	Item,
	GameObject
} from './Game/GameObjects';



var config = {
	type: Phaser.AUTO,
	// canvas: document.querySelector('canvas'),
	parent: 'phaser-app',
	width: 160,
	height: 144,
	backgroundColor: '#ffffff',
	pixelArt: true,
	antialias: false,
	roundPixels: true,
	plugins: {
		global: [{}],
		/*scene: [{
			key: 'CustomKeysPlugin',
			plugin: CustomKeysPlugin,
			start: true,
			mapping: 'keys'
		}]*/
	},
	physics: {
		default: "arcade",
		arcade: {
			gravity: {
				y: 0
			},
			// debug: true,
		}
	}
	//scene: [TitleScreenScene],
};

var game = new Phaser.Game(config);
var gameDataManager = new GameDataManager(game);
let gameSettings = gameDataManager.gameData.gameSettings.data;

var element = document.getElementById('loading-banner-div');
if (element) {
	if (element.firstChild) {
		element.parentNode.removeChild(element);
	}
}

// add global data to registry

game.registry.set('gameDataManager', gameDataManager);

// game.registry.set('gameData', gameData);
game.registry.set('appSettings', gameDataManager.gameData.appSettings.data);
game.registry.set('gameSettings', gameDataManager.gameData.gameSettings.data);
game.registry.set('saveState', gameDataManager.gameData.saveState);
game.registry.set('party', gameDataManager.gameData.party);
game.registry.set('inventoryItems', gameDataManager.gameData.inventoryItems);
game.registry.set('inventoryWeapons', gameDataManager.gameData.inventoryWeapons);
game.registry.set('inventoryAccessories', gameDataManager.gameData.inventoryAccessories);
game.registry.set('inventoryTreasures', gameDataManager.gameData.inventoryTreasures);
game.registry.set('scenePersistentData', gameDataManager.gameData.scenePersistentData);

// game.registry.set('gameDatabases', gameDatabases);
game.registry.set('stringDatabase', gameDataManager.gameDatabases.stringDatabase);
game.registry.set('dialogDatabase', gameDataManager.gameDatabases.dialogDatabase);
game.registry.set('gearDatabase', gameDataManager.gameDatabases.gearDatabase);
game.registry.set('itemDatabase', gameDataManager.gameDatabases.itemDatabase);
game.registry.set('treasureDatabase', gameDataManager.gameDatabases.treasureDatabase);
game.registry.set('techniqueDatabase', gameDataManager.gameDatabases.techniqueDatabase);
game.registry.set('effectDatabase', gameDataManager.gameDatabases.effectDatabase);
game.registry.set('monsterMobDatabase', gameDataManager.gameDatabases.monsterMobDatabase);
game.registry.set('monsterDatabase', gameDataManager.gameDatabases.monsterDatabase);

console.log('\n' + '---- application started ----' + '\n');

game.gameState = new GameState(game);
game.menuState = new MenuState(game);
game.gameState.initialize();
game.menuState.initialize();
game.gameState.start();
game.menuState.start();

game.events.emit('Game', 'StartPreloading');