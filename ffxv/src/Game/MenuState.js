import StateTransition from '../State/StateTransition';
import StateNode from '../State/StateNode';
import StateMachine from '../State/StateMachine';
import {
    isDefinedAndNotNull,
    isDefinedAndFunction,
    isUndefinedOrNull
} from '../Base/Utils';
import MenuStateMachine from '../State/MenuStateMachine';
import MainMenuScene from '../Scenes/MenuScenes/MainMenuScene';
import {
    isNullOrUndefined
} from 'util';
import ItemMenuScene from '../Scenes/MenuScenes/ItemMenuScene';
import GearMenuScene from '../Scenes/MenuScenes/GearMenuScene';
import TechniqueMenuScene from '../Scenes/MenuScenes/TechniqueMenuScene';
import ChestLootMenuScene from '../Scenes/MenuScenes/ChestLootMenuScene';
import DialogMenuScene from '../Scenes/MenuScenes/DialogMenuScene';
import BattleMenuScene from '../Scenes/BattleScenes/BattleMenuScene';

export default class MenuState {

    constructor(game) {
        this.game = game;
        this.events = game.events;

        this.gameState = null;

        this.stateMachine = null;

        this.currentGameStateScene = null; // non-menu parent scene of current menus
        this.oldSceneKey = null;
        this.currentSceneKey = null;
        this.menuSceneKeyStack = [];

        this.initializeScenes();
    }

    getCurrentState() {
        return this.stateMachine.getCurrentState();
    }

    getCurrentStateName() {
        if (isDefinedAndNotNull(this.stateMachine)) {
            return this.stateMachine.getCurrentStateName();
        }
    }

    getMenuSceneKeyStack() {
        return this.menuSceneKeyStack;
    }

    getLastMenuSceneKeyFromStack() {
        if (isDefinedAndNotNull(this.menuSceneKeyStack)) {
            let len = this.menuSceneKeyStack.length;
            if (len > 0) {
                return this.menuSceneKeyStack[len - 1];
            }
        }
        return null;
    }

    getSecondToLastMenuSceneKeyFromStack() {
        if (isDefinedAndNotNull(this.menuSceneKeyStack)) {
            let len = this.menuSceneKeyStack.length;
            if (len > 1) {
                return this.menuSceneKeyStack[len - 2];
            }
        }
        return null;
    }

    pushToSceneKeyStack(key) {
        if (isDefinedAndNotNull(this.menuSceneKeyStack)) {
            this.menuSceneKeyStack.push(key);
        }
    }

    popFromSceneKeyStack() {
        if (isDefinedAndNotNull(this.menuSceneKeyStack)) {
            this.menuSceneKeyStack.pop();
        }
    }

    checkIfSceneStackEmpty() {
        if (isDefinedAndNotNull(this.menuSceneKeyStack) &&
            this.menuSceneKeyStack.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    getCurrentGameStateScene() {
        return this.currentGameStateScene;
    }

    setCurrentGameStateScene(scene) {
        this.currentGameStateScene = scene;
    }

    getCurrentSceneKey() {
        return this.currentSceneKey;
    }

    setCurrentSceneKey(key) {
        if (!isDefinedAndNotNull(key)) {
            key = null;
        }
        this.currentSceneKey = key;
    }

    getCurrentScene() {
        return this.game.scene.getScene(this.getCurrentSceneKey());
    }

    getOldSceneKey() {
        return this.oldSceneKey;
    }

    setOldSceneKey(key) {
        if (!isDefinedAndNotNull(key)) {
            key = null;
        }
        this.oldSceneKey = key;
    }

    getOldScene() {
        return this.game.scene.getScene(this.getOldSceneKey());
    }

    updateCurrentAndOldSceneKey() {
        this.setOldSceneKey(this.getSecondToLastMenuSceneKeyFromStack());
        this.setCurrentSceneKey(this.getLastMenuSceneKeyFromStack());
    }

    updateParentScene() {
        let curr = this.getCurrentScene();
        let old = this.getOldScene();
        if (isDefinedAndNotNull(curr) && isDefinedAndNotNull(old)) {
            curr.setParentScene(old);
        } else if (isDefinedAndNotNull(curr) && isUndefinedOrNull(old)) {
            this.setCurrentGameStateScene(this.gameState.getCurrentScene());
            let currGameStateScene = this.getCurrentGameStateScene();
            curr.setParentScene(currGameStateScene);
        }
    }

    startScene(key, data) {
        this.game.scene.start(key, data);
    }

    startSceneAndUpdateStack(key, data) {
        this.pauseCurrentScene(data);
        this.pushToSceneKeyStack(key);
        this.updateCurrentAndOldSceneKey();
        this.updateParentScene();
        this.startScene(key, data);
    }

    runScene(key, data) {
        this.game.scene.run(key, data);
    }

    runSceneAndUpdateStack(key, data) {
        this.pauseCurrentScene(data);
        this.pushToSceneKeyStack(key);
        this.updateCurrentAndOldSceneKey();
        this.updateParentScene();
        this.runScene(key, data);
    }

    resumeScene(key) {
        this.game.scene.resume(key);
    }

    wakeScene(key) {
        this.game.scene.wake(key);
    }

    stopCurrentScene(data) {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.game.scene.stop(this.getCurrentSceneKey(), data);
        }
    }

    stopCurrentSceneAndUpdateStack(data) {
        console.log('\n--- current scene that is stopped: ' + this.currentSceneKey);
        this.stopCurrentScene();

        this.popFromSceneKeyStack();

        if (this.checkIfSceneStackEmpty()) {
            this.reset();
            this.game.events.emit('Game', 'LeaveMenuToPlay');
        } else {
            this.updateCurrentAndOldSceneKey();
            console.log('--- current scene that is resumed: ' + this.currentSceneKey);
            this.resumeScene(this.getCurrentSceneKey(), data);
        }
    }

    pauseCurrentScene(data) {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.game.scene.pause(this.getCurrentSceneKey(), data);
        }
    }

    sleepCurrentScene() {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.game.scene.sleep(this.getCurrentSceneKey());
        }
    }

    stopMenuStack() {
        if (isDefinedAndNotNull(this.menuSceneKeyStack)) {
            this.menuSceneKeyStack.forEach(scene => {
                this.game.scene.stop(scene);
            });
        }
    }

    start() {
        this.stateMachine.start();

        this.setCurrentGameStateScene(this.gameState.getCurrentScene());
    }

    reset() {
        this.stateMachine.reset();
        this.stopMenuStack();
        this.menuSceneKeyStack = [];
        this.currentGameStateScene = null;
        this.oldSceneKey = null;
        this.currentSceneKey = null;
    }

    restart() {
        this.reset();
        this.start();
    }

    initializeScenes() {
        this.game.scene.add('MainMenuFromStartState', MainMenuScene);
        this.game.scene.add('ItemMenuFromMainMenu', ItemMenuScene);
        this.game.scene.add('GearMenuFromMainMenu', GearMenuScene);
        this.game.scene.add('TechniqueMenuFromMainMenu', TechniqueMenuScene);
        this.game.scene.add('ChestLootMenuFromStartState', ChestLootMenuScene);
        this.game.scene.add('DialogMenuFromStartState', DialogMenuScene);
        this.game.scene.add('BattleMenuFromStartState', BattleMenuScene);
    }

    initialize() {
        // reference to other state managers
        this.gameState = this.game.gameState;

        // initialize StateMachine
        let stateMachine = new MenuStateMachine('MenuState', this.events);

        let startState = new StateNode(stateMachine, 'StartState', this.enterStartState.bind(this), null, this.leaveStartState.bind(this));
        let mainMenuFromStartState = new StateNode(stateMachine, 'MainMenuFromStartState');
        let itemMenuFromMainMenuState = new StateNode(stateMachine, 'ItemMenuFromMainMenuState');
        let gearMenuFromMainMenuState = new StateNode(stateMachine, 'GearMenuFromMainMenuState');
        let techniqueMenuFromMainMenuState = new StateNode(stateMachine, 'TechniqueMenuFromMainMenuState');
        let chestLootMenuFromStartState = new StateNode(stateMachine, 'ChestLootMenuFromStartState');
        let dialogMenuFromStartState = new StateNode(stateMachine, 'DialogMenuFromStartState');
        let battleMenuFromStartState = new StateNode(stateMachine, 'BattleMenuFromStartState');

        new StateTransition('OpenMainMenu', startState, mainMenuFromStartState, this.openMainMenuFromStartState.bind(this));
        new StateTransition('CloseMainMenu', mainMenuFromStartState, startState, this.closeMainMenuToStartState.bind(this));
        new StateTransition('OpenItemMenu', mainMenuFromStartState, itemMenuFromMainMenuState, this.openItemMenuFromMainMenu.bind(this));
        new StateTransition('CloseItemMenu', itemMenuFromMainMenuState, mainMenuFromStartState, this.closeItemMenuToMainMenu.bind(this));
        new StateTransition('OpenGearMenu', mainMenuFromStartState, gearMenuFromMainMenuState, this.openGearMenuFromMainMenu.bind(this));
        new StateTransition('CloseGearMenu', gearMenuFromMainMenuState, mainMenuFromStartState, this.closeGearMenuToMainMenu.bind(this));
        new StateTransition('OpenTechniqueMenu', mainMenuFromStartState, techniqueMenuFromMainMenuState, this.openTechniqueMenuFromMainMenu.bind(this));
        new StateTransition('CloseTechniqueMenu', techniqueMenuFromMainMenuState, mainMenuFromStartState, this.closeTechniqueMenuToMainMenu.bind(this));
        new StateTransition('OpenChestLootMenu', startState, chestLootMenuFromStartState, this.openChestLootMenuFromStartState.bind(this));
        new StateTransition('CloseChestLootMenu', chestLootMenuFromStartState, startState, this.closeChestLootMenuToStartState.bind(this));
        new StateTransition('OpenDialogMenu', startState, dialogMenuFromStartState, this.openDialogMenuFromStartState.bind(this));
        new StateTransition('CloseDialogMenu', dialogMenuFromStartState, startState, this.closeDialogMenuToStartState.bind(this));
        new StateTransition('OpenBattleMenu', startState, battleMenuFromStartState, this.openBattleMenuFromStartState.bind(this));
        new StateTransition('CloseBattleMenu', battleMenuFromStartState, startState, this.closeBattleMenuToStartState.bind(this));

        this.stateMachine = stateMachine;
    }

    enterStartState() {

    }

    leaveStartState(data) {
        this.game.events.emit('Game', 'EnterMenuFromPlay', data);
    }

    openMainMenuFromStartState() {
        let key = 'MainMenuFromStartState';

        this.runSceneAndUpdateStack(key);
    }

    closeMainMenuToStartState(data) {
        this.stopCurrentSceneAndUpdateStack(data);
    }

    openGearMenuFromMainMenu(data) {
        let key = 'GearMenuFromMainMenu';

        this.runSceneAndUpdateStack(key, data);
    }

    closeGearMenuToMainMenu(data) {
        this.stopCurrentSceneAndUpdateStack(data);
    }

    openTechniqueMenuFromMainMenu(data) {
        let key = 'TechniqueMenuFromMainMenu';

        this.runSceneAndUpdateStack(key, data);
    }

    closeTechniqueMenuToMainMenu(data) {
        this.stopCurrentSceneAndUpdateStack(data);
    }


    /*     openItemMenuFromMainMenu() {
            let key = 'ItemMenuFromMainMenu';
    
            this.runSceneAndUpdateStack(key);
        } */

    openItemMenuFromMainMenu(data, callback) {
        let key = 'ItemMenuFromMainMenu';

        let menu = this.game.scene.getScene(key);
        if (isDefinedAndFunction(callback)) {
            menu.setParentSceneCallback(callback);
        }

        this.runSceneAndUpdateStack(key, data);
    }

    /*     closeItemMenuToMainMenu() {
            this.stopCurrentSceneAndUpdateStack();
        } */

    closeItemMenuToMainMenu(data, callback) {
        this.stopCurrentSceneAndUpdateStack();

        if (isDefinedAndFunction(callback) && isDefinedAndNotNull(item)) {
            callback(data);
        }
    }

    openChestLootMenuFromStartState(data, callback) {
        let key = 'ChestLootMenuFromStartState';

        let menu = this.game.scene.getScene(key);
        if (isDefinedAndFunction(callback)) {
            menu.setParentSceneCallback(callback);
        }

        this.runSceneAndUpdateStack(key, data);
    }

    closeChestLootMenuToStartState(data, callback) {

        this.stopCurrentSceneAndUpdateStack();

        if (isDefinedAndFunction(callback) && isDefinedAndNotNull(data)) {
            callback(data);
        }
    }

    openDialogMenuFromStartState(data, callback) {
        let key = 'DialogMenuFromStartState';

        let menu = this.game.scene.getScene(key);
        if (isDefinedAndFunction(callback)) {
            menu.setParentSceneCallback(callback);
        }

        this.runSceneAndUpdateStack(key, data);
    }

    closeDialogMenuToStartState(data, callback) {
        this.stopCurrentSceneAndUpdateStack();

        if (isDefinedAndFunction(callback) && isDefinedAndNotNull(data)) {
            callback(data);
        }
    }

    openBattleMenuFromStartState(data, callback) {
        let key = 'BattleMenuFromStartState';

        let menu = this.game.scene.getScene(key);
        if (isDefinedAndFunction(callback)) {
            menu.setParentSceneCallback(callback);
        }

        this.runSceneAndUpdateStack(key, data);
    }

    closeBattleMenuToStartState(data, callback) {
        this.stopCurrentSceneAndUpdateStack();

        if (isDefinedAndFunction(callback) && isDefinedAndNotNull(data)) {
            this.gameState.getCurrentScene().flashScreen(data, callback);
        }
    }
}