import {
    Item,
    Character,
    GameObject,
    Technique,
    CustomString,
    DialogString,
    Monster,
    CustomMonsterImage,
    Effect
} from "./GameObjects";
import {
    isNullOrUndefined
} from "util";
import {
    isDefinedAndNotNull,
    isUndefinedOrNull,
    prependZeroes
} from "../Base/Utils";

export default class GameDataManager {

    constructor(game) {

        this.game = game;

        this.gameDatabases = {

            stringDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },
                getCustomStringByKey(key) {
                    let data = this.data[key];
                    let obj = new CustomString(key, data.fullName, data.shortName);

                    return obj;
                },

                data: {
                    'char-noctis': {
                        fullName: 'Noctis',
                        shortName: 'Nct'
                    },
                    'char-gladio': {
                        fullName: 'Gladio',
                        shortName: 'Gld',
                    },
                    'char-ignis': {
                        fullName: 'Ignis',
                        shortName: 'Ign',
                    },
                    'char-prompto': {
                        fullName: 'Prompto',
                        shortName: 'Prm',
                    },

                    'item-potion-01': {
                        fullName: 'Potion',
                        shortName: 'Potion',
                    },
                    'item-phoenix-down-01': {
                        fullName: 'Phoenix Down',
                        shortName: 'PhDown',
                    },
                    'item-remedy-01': {
                        fullName: 'Remedy',
                        shortName: 'Remedy',
                    },
                    'item-antidote-01': {
                        fullName: 'Antidote',
                        shortName: 'Antido',
                    },

                    'spell-fire-01': {
                        fullName: 'Fire1',
                        shortName: 'Fire1',
                    },
                    'spell-ice-01': {
                        fullName: 'Ice1',
                        shortName: 'Ice1',
                    },
                    'spell-deprotect-01': {
                        fullName: 'Deprotect1',
                        shortName: 'Deprot1',
                    },
                    'spell-libra-01': {
                        fullName: 'Libra1',
                        shortName: 'Libra1',
                    },

                    'treasure-feathers': {
                        fullName: 'Feathers',
                        shortName: 'Fthrs'
                    },

                    'weapon-sword-01': {
                        fullName: 'Broad Sword',
                        shortName: 'Broad',
                    },
                    'weapon-greatsword-01': {
                        fullName: 'Great Sword',
                        shortName: 'Great',
                    },
                    'weapon-polearm-01': {
                        fullName: 'Lance',
                        shortName: 'Lance',
                    },
                    'weapon-daggers-01': {
                        fullName: 'Knives',
                        shortName: 'Knives',
                    },
                    'weapon-firearm-01': {
                        fullName: 'Gun',
                        shortName: 'Gun',
                    },
                    'weapon-shield-01': {
                        fullName: 'Shield',
                        shortName: 'Shield',
                    },
                    'weapon-machinery-01': {
                        fullName: 'Blaster',
                        shortName: 'Blastr',
                    },

                    'accessory-01': {
                        fullName: 'Bracelet',
                        shortName: 'Braclt',
                    },
                    'accessory-02': {
                        fullName: 'Anklet',
                        shortName: 'Anklet',
                    },

                    'technique-noctis-01': {
                        fullName: 'Warpstrike',
                        shortName: 'Warpstr',
                        description: 'Warpstrike an enemy.',
                    },
                    //TODO: come up with remaining techiques 2&3 for all chars
                    'technique-noctis-02': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },
                    'technique-noctis-03': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },
                    'technique-gladio-01': {
                        fullName: 'Tempest',
                        shortName: 'Tempest',
                        description: 'Attack all enemies.'
                    },
                    'technique-gladio-02': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },
                    'technique-gladio-03': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },
                    'technique-ignis-01': {
                        fullName: 'Libra',
                        shortName: 'Libra',
                        description: 'Analyze an enemy.'
                    },
                    'technique-ignis-02': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },
                    'technique-ignis-03': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },
                    'technique-prompto-01': {
                        fullName: 'Piercer',
                        shortName: 'Piercer',
                        description: 'Halve defense of all enemies for 3 turns.'
                    },
                    'technique-prompto-02': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },
                    'technique-prompto-03': {
                        fullName: '???',
                        shortName: '???',
                        description: '???',
                    },

                    'weapon-type-sword': {
                        fullName: 'Sword',
                        shortName: 'Sword',
                    },

                    'weapon-type-greatsword': {
                        fullName: 'Greatsword',
                        shortName: 'Greatsw',
                    },

                    'weapon-type-polearm': {
                        fullName: 'Polearm',
                        shortName: 'Polearm',
                    },

                    'weapon-type-daggers': {
                        fullName: 'Daggers',
                        shortName: 'Daggers',
                    },

                    'weapon-type-firearm': {
                        fullName: 'Firearm',
                        shortName: 'Firearm',
                    },

                    'weapon-type-shield': {
                        fullName: 'Shield',
                        shortName: 'Shield',
                    },

                    'weapon-type-machinery': {
                        fullName: 'Machinery',
                        shortName: 'Machine',
                    },

                    'weapon-type-royalarms': {
                        fullName: 'Royal Arms',
                        shortName: 'Royal',
                    },

                    'element-fire': {
                        fullName: 'Fire',
                        shortName: 'Fire',
                    },
                    'element-ice': {
                        fullName: 'Ice',
                        shortName: 'Ice',
                    },
                    'element-lightning': {
                        fullName: 'Lightning',
                        shortName: 'Lightng',
                    },
                    'element-dark': {
                        fullName: 'Dark',
                        shortName: 'Dark',
                    },
                    'element-light': {
                        fullName: 'Light',
                        shortName: 'Light',
                    },

                    'status-poison-01': {
                        fullName: 'Poison',
                        shortName: 'Poison',
                    },
                    'status-sleep-01': {
                        fullName: 'Sleep',
                        shortName: 'Sleep',
                    },
                    'status-deprotect-01': {
                        fullName: 'Deprotect1',
                        shortName: 'Deprot1',
                    },
                    'status-protect-01': {
                        fullName: 'Protect1',
                        shortName: 'Protec1',
                    },

                    'monster-goblin-01': {
                        fullName: 'Goblin',
                        shortName: 'Goblin',
                    },

                    'string-disclaimer-text': {
                        fullName: 'This material is\n' +
                            'not official.\n\n' +
                            'Some of the music,\n' +
                            'art, script etc.\n' +
                            'in this fangame \n' +
                            'are trademarks\/\n' +
                            'copyrighted works\n' +
                            'of Square Enix\n' +
                            'Holdings Co., Ltd.\n\n' +
                            'All rights reserv\-\n' +
                            'ed by Square Enix. \n'
                    },

                    'string-press': {
                        fullName: 'Press'
                    },
                    'string-start': {
                        fullName: 'Start'
                    },
                    'string-continue': {
                        fullName: 'Continue'
                    },
                    'string-demo': {
                        fullName: 'Demo'
                    },
                    'string-getsuken': {
                        fullName: 'Getsuken'
                    },
                    'string-studios': {
                        fullName: 'Studios'
                    },
                    'string-save-choice': {
                        fullName: 'Save game?'
                    },
                    'string-yes': {
                        fullName: 'Yes'
                    },
                    'string-no': {
                        fullName: 'No'
                    },

                    'string-gil': {
                        fullName: 'Gil'
                    },
                    'string-level': {
                        fullName: 'Level',
                        shortName: 'Lvl'
                    },
                    'string-experience': {
                        fullName: 'Experience',
                        shortName: 'Ep'
                    },
                    'string-health-point': {
                        fullName: 'Health Point',
                        shortName: 'Hp'
                    },
                    'string-health-point-max': {
                        fullName: 'Max Health Points',
                        shortName: 'Max Hp'
                    },
                    'string-tech-point': {
                        fullName: 'Tech Point',
                        shortName: 'Tp'
                    },

                    'string-menu-gear': {
                        fullName: 'Gear',
                        shortName: 'Gear',
                    },
                    'string-menu-arms': {
                        fullName: 'Arms',
                        shortName: 'Arms',
                    },
                    'string-menu-accessory': {
                        fullName: 'Accessory',
                        shortName: 'Acc',
                    },
                    'string-menu-accessories': {
                        fullName: 'Accessories',
                        shortName: 'Accs',
                    },
                    'string-menu-item': {
                        fullName: 'Item',
                        shortName: 'Item',
                    },
                    'string-menu-technique': {
                        fullName: 'Technique',
                        shortName: 'Tech',
                    },
                    'string-menu-techniques': {
                        fullName: 'Techniques',
                        shortName: 'Techs',
                    },
                    'string-menu-save': {
                        fullName: 'Save',
                        shortName: 'Save',
                    },

                    'string-battle-action-fight': {
                        fullName: 'Fight',
                        shortName: 'Fight',
                    },
                    'string-battle-action-run': {
                        fullName: 'Run',
                        shortName: 'Run',
                    },
                    'string-battle-action-attack': {
                        fullName: 'Attack',
                        shortName: 'Attack',
                    },
                    'string-battle-action-technique': {
                        fullName: 'Technique',
                        shortName: 'Tech',
                    },
                    'string-battle-action-item': {
                        fullName: 'Item',
                        shortName: 'Item',
                    },
                    'string-battle-action-defend': {
                        fullName: 'Defend',
                        shortName: 'Defend',
                    },

                    'string-stat-stat': {
                        fullName: 'Stat',
                        shortName: 'Stat',
                    },
                    'string-stat-stats': {
                        fullName: 'Stats',
                        shortName: 'Stats',
                    },
                    'string-stat-attack': {
                        fullName: 'Attack',
                        shortName: 'Atk',
                    },
                    'string-stat-defense': {
                        fullName: 'Defense',
                        shortName: 'Def',
                    },
                    'string-stat-magic': {
                        fullName: 'Magic Attack',
                        shortName: 'M.Atk',
                    },
                    'string-stat-spirit': {
                        fullName: 'Magic Defense',
                        shortName: 'M.Def',
                    },
                    'string-stat-resistance': {
                        fullName: 'Resistance',
                        shortName: 'Resist',
                    },
                    'string-stat-resistances': {
                        fullName: 'Resistances',
                        shortName: 'Resists',
                    },
                    'string-stat-fire-resistance': {
                        fullName: 'Fire Resistance',
                        shortName: 'Fire',
                    },
                    'string-stat-ice-resistance': {
                        fullName: 'Ice Resistance',
                        shortName: 'Ice',
                    },
                    'string-stat-lightning-resistance': {
                        fullName: 'Lightning Resistance',
                        shortName: 'Ltng',
                    },
                    'string-stat-dark-resistance': {
                        fullName: 'Dark Resistance',
                        shortName: 'Dark',
                    },
                }
            },

            dialogDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },
                getDialogStringByKey(key) {
                    let data = this.data[key];
                    let obj = new DialogString(key, data);

                    return obj;
                },

                data: {
                    'dlg-0000': {
                        text: 'You get'
                    },
                    'dlg-0001': {
                        text: 'But you can\'t carry any more items.'
                    },

                    'dlg-0002': {
                        text: 'This is a test dialog line!',
                    },
                    'dlg-0003': {
                        options: {

                        },
                        speaker: 'Speaker',
                        text: 'This is a test dialog line with a speaker name!\n'
                        // text: 
                        // `This is a test dialog line with a speaker name!
                        // This is a test dialog line with a speaker name!`
                    },
                    'dlg-0004': {
                        options: {

                        },
                        text: 'This is a test dialog line without a speaker name!\n'
                    },
                    'dlg-0005': {
                        text: 'Test indent'
                    },
                    'dlg-0006-1': {
                        speaker: 'Demon',
                        text: 'You gotta check out the battle system!'
                    },
                    'dlg-0006-2': {
                        speaker: 'Demon',
                        text: 'Wow, you\'re strong!'
                    },
                    'dlg-0007': {
                        text: 'You run.'
                    },
                    'dlg-0008': {
                        text: 'You can\'t run.'
                    },
                    'dlg-0009': {
                        text: 'Party does nothing.'
                    },
                    'dlg-0010': {
                        text: 'defends'
                    },
                    'dlg-0011': {
                        text: 'attacks'
                    },
                    'dlg-0012': {
                        text: 'by'
                    },
                    'dlg-0013': {
                        text: 'damage'
                    },
                    'dlg-0014': {
                        text: 'defeated'
                    },
                    'dlg-0015': {
                        text: 'Party won!!'
                    },
                    'dlg-0016': {
                        text: 'healed by'
                    },
                    'dlg-0017': {
                        text: 'uses'
                    },
                    'dlg-0018': {
                        text: 'on'
                    },
                    'dlg-0019': {
                        text: 'absorbed'
                    },
                    'dlg-0020': {
                        text: 'has resistance to'
                    },
                    'dlg-0021': {
                        text: 'is afflicted with'
                    },
                    'dlg-0022': {
                        text: 'can\'t act.'
                    },
                    'dlg-0023': {
                        text: 'The party is lost..'
                    },
                    'dlg-0024': {
                        text: 'is affected by'
                    },
                    'dlg-0025': {
                        text: 'reduced by'
                    },
                    'dlg-0026': {
                        text: 'increased by'
                    },
                    'dlg-0027': {
                        text: '%'
                    },
                    'dlg-0028': {
                        text: 'Resist'
                    },
                    'dlg-0029': {
                        text: 'cured'
                    },
                    'dlg-0030': {
                        text: 'No damage!'
                    },
                    'dlg-0031': {
                        text: 'Nothing to cure.'
                    },
                    'dlg-0032': {
                        text: 'inflicts'
                    },
                    'dlg-0033': {
                        text: 'immune to'
                    },
                    'dlg-0034': {
                        text: 'weak to'
                    },
                    'dlg-0035': {
                        text: 'strong against'
                    },
                    'dlg-0036': {
                        text: 'arms'
                    },
                    'dlg-0037': {
                        text: 'elements'
                    },
                    'dlg-0038': {
                        text: 'resistant to'
                    },
                    'dlg-0039': {
                        text: 'absorbs'
                    },
                    'dlg-0040': {
                        text: 'drops items'
                    },
                    'dlg-0041': {
                        text: 'Gained'
                    },
                    'dlg-0042': {
                        text: 'reached'
                    },
                    'dlg-0043': {
                        text: 'learned'
                    },
                },
            },

            itemDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },

                getItemObjectByKey(key, stringDB) {
                    let config = {};
                    let itemFromDB = this.getDataByKey(key);

                    config = Object.assign(config, JSON.parse(JSON.stringify(itemFromDB)));
                    config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(key))));
                    config.key = key;

                    return new Item(config);
                },

                data: {
                    'item-potion-01': {
                        type: 'consumable',
                        maxQuantity: 10,
                        price: 50,
                        effect: 'effect-item-potion-01',
                    },
                    'item-phoenix-down-01': {
                        type: 'consumable',
                        maxQuantity: 10,
                        price: 200,
                        effect: 'effect-item-phoenix-down-01',
                    },
                    'item-remedy-01': {
                        type: 'consumable',
                        maxQuantity: 10,
                        price: 50,
                        effect: 'effect-item-remedy-01',
                    },
                    'item-antidote-01': {
                        type: 'consumable',
                        maxQuantity: 10,
                        price: 50,
                        effect: 'effect-item-antidote-01',
                    },
                    'spell-fire-01': {
                        type: 'spell',
                        maxQuantity: 5,
                        price: null,
                        effect: 'effect-spell-fire-01'
                    },
                    'spell-ice-01': {
                        type: 'spell',
                        maxQuantity: 5,
                        price: null,
                        effect: 'effect-spell-ice-01'
                    },
                },
            },

            treasureDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },

                getItemObjectByKey(key, stringDB) {
                    let config = {};
                    let itemFromDB = this.getDataByKey(key);

                    config = Object.assign(config, JSON.parse(JSON.stringify(itemFromDB)));
                    config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(key))));
                    config.key = key;

                    return new Item(config);
                },

                data: {
                    'treasure-feathers': {
                        type: 'treasure',
                        maxQuantity: 99,
                        price: 5,
                    },
                },
            },

            gearDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },

                data: {
                    'weapon-sword-01': {
                        type: 'weapon',
                        subType: 'sword',
                        power: 5,
                        critical: 1.1,
                        element: 'fire', // TODO: just for test
                        price: null,
                        statModifiers: {
                            strength: 2,
                            vitality: 2,
                            statusAilments: ['status-poison-01']
                        },
                    },
                    'weapon-greatsword-01': {
                        type: 'weapon',
                        subType: 'greatsword',
                        power: 10,
                        critical: 1.3,
                        element: 'none',
                        price: null,
                        statModifiers: {
                            strength: 3,
                            vitality: 1,
                        },
                    },
                    'weapon-polearm-01': {
                        type: 'weapon',
                        subType: 'polearm',
                        power: 15,
                        critical: 1.2,
                        element: 'none',
                        price: null,
                        statModifiers: {
                            strength: 5,
                            magic: 2,
                            spirit: 2,
                        },
                    },
                    'weapon-daggers-01': {
                        type: 'weapon',
                        subType: 'daggers',
                        power: 20,
                        critical: 1.0,
                        element: 'none',
                        price: null,
                        statModifiers: {
                            vitality: 5,
                            spirit: 2,
                        },
                    },
                    'weapon-firearm-01': {
                        type: 'weapon',
                        subType: 'firearm',
                        power: 25,
                        critical: 1.2,
                        element: 'none',
                        price: null,
                        statModifiers: {
                            magic: 2,
                            spirit: 2,
                        },
                    },
                    'weapon-shield-01': {
                        type: 'weapon',
                        subType: 'shield',
                        power: 30,
                        critical: 1.1,
                        element: 'none',
                        price: null,
                        statModifiers: {
                            vitality: 15,
                            spirit: 5,
                        },
                    },
                    'weapon-machinery-01': {
                        type: 'weapon',
                        subType: 'machinery',
                        power: 35,
                        critical: 1.4,
                        element: 'none',
                        price: null,
                        statModifiers: {
                            strength: 4,
                            magic: 4,
                        },
                    },
                    'accessory-01': {
                        type: 'accessory',
                        subType: 'none',
                        price: null,
                        statModifiers: {
                            strength: 5,
                            fireResistance: 10,
                            lightningRes: 10,
                            immunities: []
                        },
                    },
                    'accessory-02': {
                        type: 'accessory',
                        subType: 'none',
                        price: null,
                        statModifiers: {
                            vitality: 5,
                            iceResistance: 10,
                            darkResistance: 10,
                            immunities: ['status-sleep-01']
                        },
                    },
                },
            },

            techniqueDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },

                data: {
                    'technique-noctis-01': {
                        cost: 1,
                        effects: ['effect-tech-noctis-01'],
                        targetModeEnemies: 'single',
                    },
                    'technique-noctis-02': {
                        cost: 1,
                        effects: ['effect-tech-noctis-02'],
                        targetModeEnemies: 'single',
                    },
                    'technique-noctis-03': {
                        cost: 1,
                        effects: ['effect-tech-noctis-03'],
                        targetModeEnemies: 'single',
                    },
                    'technique-gladio-01': {
                        cost: 1,
                        effects: ['effect-tech-gladio-01'],
                        targetModeEnemies: 'all',
                    },
                    'technique-gladio-02': {
                        cost: 1,
                        effects: ['effect-tech-gladio-02'],
                        targetModeEnemies: 'all',
                    },
                    'technique-gladio-03': {
                        cost: 1,
                        effects: ['effect-tech-gladio-03'],
                        targetModeEnemies: 'all',
                    },
                    'technique-ignis-01': {
                        cost: 1,
                        effects: ['effect-tech-ignis-01'],
                        targetModeEnemies: 'single',
                    },
                    'technique-ignis-02': {
                        cost: 1,
                        effects: ['effect-tech-ignis-02'],
                        targetModeEnemies: 'single',
                    },
                    'technique-ignis-03': {
                        cost: 1,
                        effects: ['effect-tech-ignis-03'],
                        targetModeEnemies: 'single',
                    },
                    'technique-prompto-01': {
                        cost: 1,
                        effects: ['effect-tech-prompto-01'],
                        targetModeEnemies: 'all',
                    },
                    'technique-prompto-02': {
                        cost: 1,
                        effects: ['effect-tech-prompto-02'],
                        targetModeEnemies: 'all',
                    },
                    'technique-prompto-03': {
                        cost: 1,
                        effects: ['effect-tech-prompto-03'],
                        targetModeEnemies: 'all',
                    },
                }
            },

            effectDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },
                getEffectObjectByKey(key) {
                    let config = {};
                    let effectFromDB = this.getDataByKey(key);

                    config = Object.assign(config, JSON.parse(JSON.stringify(effectFromDB)));
                    config.key = key;

                    return new Effect(config);
                },

                data: {
                    'effect-attack-01': {
                        type: 'attack',
                        power: 1.0,
                        targetMode: 'single',
                    },
                    'effect-item-potion-01': {
                        type: 'heal',
                        stat: 'hp',
                        value: 'absolute',
                        power: 50,
                        targetMode: 'single',
                    },
                    'effect-item-phoenix-down-01': {
                        type: 'revive',
                        stat: 'hp',
                        value: 'relative',
                        power: 0.33,
                        targetMode: 'single',
                    },
                    'effect-item-remedy-01': {
                        type: 'cure',
                        statusAilment: 'status-all',
                        targetMode: 'single',
                    },
                    'effect-item-antidote-01': {
                        type: 'cure',
                        statusAilment: 'status-poison-01',
                        targetMode: 'single',
                    },
                    'effect-spell-fire-01': {
                        spell: 'spell-fire-01',
                        type: 'spell',
                        subType: 'offensive',
                        power: 1.0,
                        element: 'fire',
                        targetMode: 'single',
                    },
                    'effect-spell-ice-01': {
                        spell: 'spell-ice-01',
                        type: 'spell',
                        subType: 'offensive',
                        power: 1.0,
                        element: 'ice',
                        targetMode: 'single',
                    },
                    'effect-spell-cure-01': {
                        spell: 'spell-cure-01',
                        type: 'spell',
                        subType: 'heal',
                        power: 1.25,
                        targetMode: 'single',
                    },
                    'effect-spell-poisona-01': {
                        spell: 'spell-poisona-01',
                        type: 'spell',
                        subType: 'cure',
                        statusAilment: 'status-poison-01',
                        power: 1.0,
                        targetMode: 'single',
                    },
                    'effect-spell-deprotect-01': {
                        spell: 'spell-deprotect-01',
                        type: 'spell',
                        subType: 'inflict',
                        statusAilment: 'status-deprotect-01',
                        power: 1.0,
                        targetMode: 'all',
                    },
                    'effect-spell-protect-01': {
                        spell: 'spell-protect-01',
                        type: 'spell',
                        subType: 'inflict',
                        statusAilment: 'status-protect-01',
                        power: 1.0,
                        targetMode: 'all',
                    },
                    'effect-spell-libra-01': {
                        spell: 'spell-libra-01',
                        type: 'spell',
                        subType: 'special',
                        power: 1.0,
                        targetMode: 'single',
                    },
                    'effect-tech-noctis-01': {
                        type: 'warp',
                        power: 1.8,
                        targetMode: 'single',
                    },
                    'effect-tech-noctis-02': {
                        type: 'warp',
                        power: 1.5,
                        targetMode: 'single',
                    },
                    'effect-tech-noctis-03': {
                        type: 'warp',
                        power: 1.5,
                        targetMode: 'single',
                    },
                    'effect-tech-gladio-01': {
                        type: 'attack',
                        power: 1.0,
                        targetMode: 'all',
                    },
                    'effect-tech-gladio-02': {
                        type: 'attack',
                        power: 1.0,
                        targetMode: 'all',
                    },
                    'effect-tech-gladio-03': {
                        type: 'attack',
                        power: 1.0,
                        targetMode: 'all',
                    },
                    'effect-tech-ignis-01': {
                        spell: 'spell-libra-01',
                        type: 'spell',
                        subType: 'special',
                        power: 1.0,
                        targetMode: 'single',
                    },
                    'effect-tech-ignis-02': {
                        spell: 'spell-libra-01',
                        type: 'spell',
                        subType: 'special',
                        power: 1.0,
                        targetMode: 'single',
                    },
                    'effect-tech-ignis-03': {
                        spell: 'spell-libra-01',
                        type: 'spell',
                        subType: 'special',
                        power: 1.0,
                        targetMode: 'single',
                    },
                    'effect-tech-prompto-01': {
                        spell: 'spell-deprotect-01',
                        type: 'spell',
                        subType: 'inflict',
                        statusAilment: 'status-deprotect-01',
                        power: 1.0,
                        targetMode: 'all',
                    },
                    'effect-tech-prompto-02': {
                        spell: 'spell-deprotect-01',
                        type: 'spell',
                        subType: 'inflict',
                        statusAilment: 'status-deprotect-01',
                        power: 1.0,
                        targetMode: 'all',
                    },
                    'effect-tech-prompto-03': {
                        spell: 'spell-deprotect-01',
                        type: 'spell',
                        subType: 'inflict',
                        statusAilment: 'status-deprotect-01',
                        power: 1.0,
                        targetMode: 'all',
                    },
                }
            },

            monsterMobDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },
                getMonsterObjects(key, numberizeSameMonsters = true, stringDB, monsterDB) {
                    let objects = [];
                    let monsterKeys = this.getDataByKey(key).monsters;
                    let sameMonsterCounts = {};
                    monsterKeys.forEach((key) => {
                        let obj = monsterDB.getMonsterObjectByKey(key, stringDB);

                        if (numberizeSameMonsters) {
                            let count = sameMonsterCounts[key] || 0;
                            count++;
                            sameMonsterCounts[key] = count;

                            if (count > 0) {
                                obj.shortName += '' + count;
                                obj.fullName += '' + count;
                            }
                        }

                        objects.push(obj);
                    });
                    return objects;
                },
                getMonsterImageObjectsFromMonsterMobKey(key, monsterDB) {
                    let objects = [];
                    let monsterKeys = this.getDataByKey(key).monsters;
                    monsterKeys.forEach((key) => {
                        let obj = monsterDB.getMonsterImageObjectByKey(key);
                        objects.push(obj);
                    });
                    return objects;
                },
                getMonsterImageObjectsFromMonsterObjects(monsterObjects, monsterDB) {
                    let imageObjects = [];
                    monsterObjects.forEach((monster) => {
                        let key = monster.key;
                        let isVisible = !monster.isDead;
                        let obj = monsterDB.getMonsterImageObjectByKey(key, isVisible);
                        imageObjects.push(obj);
                    });
                    return imageObjects;
                },

                data: {
                    'monster-mob-01': {
                        monsters: ['monster-goblin-01'],
                        lootDrop: [{
                            item: 'item-potion-01',
                            chance: 0.2,
                            quantity: 1
                        }],
                    },
                    'monster-mob-02': {
                        monsters: ['monster-goblin-01', 'monster-goblin-01'],
                        lootDrop: [{
                            item: 'item-potion-01',
                            chance: 0.2,
                            // chance: 1,
                            quantity: 2
                        }],
                    },
                    'monster-mob-03': {
                        monsters: ['monster-goblin-01', 'monster-goblin-01', 'monster-goblin-01'],
                        lootDrop: [{
                            item: 'item-potion-01',
                            chance: 0.2,
                            quantity: 3
                        }],
                    },
                }
            },

            monsterDatabase: {
                getDataByKey(key) {
                    return this.data[key];
                },
                getMonsterObjectByKey(key, stringDB) {
                    let config = {};
                    let monsterFromDB = this.getDataByKey(key);

                    config = Object.assign(config, JSON.parse(JSON.stringify(monsterFromDB)));
                    config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(key))));
                    config.key = key;

                    return new Monster(config);
                },
                getMonsterImageObjectByKey(key, isVisible) {
                    return new CustomMonsterImage(key, isVisible);
                },

                data: {
                    'monster-goblin-01': {
                        stats: {
                            level: 2,
                            expDrop: 10,
                            libra: 0.95,
                            maxHp: {
                                total: 200
                            },
                            attack: {
                                total: 5
                            },
                            defense: {
                                total: 10
                            },
                            spirit: {
                                total: 2
                            },
                            weaponAffinities: {
                                sword: 0.7,
                                greatsword: 0.0,
                                polearm: -1.0,
                                daggers: 1.2,
                                firearm: 1.0,
                                shield: 1.0,
                                machinery: 1.2,
                                royalArms: 1.0,
                            },
                            elementalAffinities: {
                                fire: 1.2,
                                ice: 0.0,
                                lightning: -1.0,
                                light: 2.0,
                            },
                            immunities: [],
                        },
                        primaryDrop: {
                            item: 'item-potion-01',
                            chance: 0.03,
                            // chance: 1.0,
                            quantity: 1,
                        },
                        appendage: {},
                        elementDrop: {
                            item: 'spell-ice-01',
                            chance: 0.3,
                            // chance: 1.0,
                            quantity: 2,
                        },
                        actions: [{
                                effect: 'effect-spell-ice-01',
                                power: 1.0,
                                quantity: 2, //-1 means infinite
                                probability: 0.5,
                            },
                            {
                                effect: 'effect-spell-deprotect-01',
                                quantity: 1, //-1 means infinite
                                probability: 0.025,
                            }
                        ]
                    },
                }
            },

        };

        this.gameData = {
            appSettings: {
                data: {
                    language: 'en',

                    // mute: true,
                    // debugMode: true,
                }
            },

            gameSettings: {
                getDataByKey(key) {
                    return this.data[key];
                },

                data: {
                    battleEscapeProbability: 0.5,
                    maxDamage: 999,
                    randomModifier: {
                        min: 0.94,
                        max: 1.06
                    },
                    weaponInflictStatusAilmentProbability: 1.05,
                    criticalHitProbability: 0.03,

                    gameWidth: 160,
                    gameHeight: 144,
                    centerX: (160 / 2),
                    centerY: (144 / 2),
                    tileSize: 16,
                    tileSizeMenu: 8,
                    gameWidthInTiles: 10,
                    gameHeightInTiles: 9,
                    gameWidthInMenuTiles: 20,
                    gameHeightInMenuTiles: 18,
                    mainVolume: 0.5,
                    textSpeed: 5, // 1-8
                    maxLineWidthInChars: 16,
                    indentWidthInChars: 1, // a tab
                    indentString: ' ',
                    depthList: {
                        world: 0,
                        above: 10,
                        gui: 100,
                        guiCursor: 101,
                        screenFlash: 10000,
                    },
                    itemTypeToCharMapping: {
                        consumable: '¢',
                        spell: '£',
                        accessory: '¤',
                        treasure: '¥',
                        sword: '¦',
                        greatsword: '§',
                        polearm: '¬',
                        daggers: '®',
                        firearm: '°',
                        shield: '±',
                        machinery: 'µ',
                        royalArms: '¼',

                        // ½¾«»
                    },
                    myFontStyle: {
                        fontFamily: 'fontFinalFantasy',
                        fontSize: 8,
                        color: '#000000',
                        fontStyle: ''
                    },
                    myFontStyleCourier: {
                        fontFamily: 'Courier',
                        fontSize: 16,
                        color: '#000000',
                        fontStyle: 'bold'
                    },
                    myFontStyleFinalFantasy: {
                        fontFamily: 'fontFinalFantasy',
                        fontSize: 16,
                        color: '#000000',
                        fontStyle: ''
                    },
                    retroFontCharSet: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,;:?!-\'"()[]/% ¢£¤¥¦§¬®°±µ¼½¾«»',

                }
            },

            saveState: {
                getDataByKey(key) {
                    return this.data[key];
                },
                setDataByKey(key, value) {
                    this.data[key] = value;
                },

                data: {
                    currentSceneKey: 'TestScene2',
                    currentPosition: {
                        x: 112,
                        y: 112
                    },
                    currentFaceDirection: 'down',

                    mainQuests: {
                        activeQuest: 'mq-000-00',
                        completedQuests: {

                        }
                    },
                    sideQuests: {
                        activeQuest: 'sq-000-00',
                        trackedQuests: {

                        },
                        completedQuests: {

                        },
                    }
                }
            },

            party: {
                getCharDataByKey(key) {
                    if (key !== 'party') {
                        let obj = this.data[key];
                        obj.key = key;
                        return obj;
                    }
                },

                getCharsData() {
                    let charKeys = this.getCharKeys();

                    return charKeys.map((key) => {
                        let obj = this.getCharDataByKey(key);
                        obj.key = key;
                        return obj;
                    });
                },

                getLivingCharsData() {
                    const chars = this.getCharsData();
                    return chars.filter((char) => {
                        return char.stats.hp > 0;
                    });
                },

                getPartyData() {
                    return this.data.party;
                },

                getCharKeys() {
                    let keys = [];
                    for (let prop in this.data) {
                        if (prop !== 'party') {
                            keys.push(prop);
                        }
                    }
                    return keys;
                },

                getPartyCharObjects(stringDB) {
                    let partyList = [];
                    for (let prop in this.data) {
                        if (this.data.hasOwnProperty(prop) && prop !== 'party') {
                            let config = {};
                            config = Object.assign(config, JSON.parse(JSON.stringify(this.getCharDataByKey(prop))));
                            config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(prop))));
                            config.key = prop;

                            let newChar = new Character(config);
                            partyList.push(newChar);
                        }
                    }

                    return partyList;
                },

                getUnlockedTechniquesObjects(charKey, stringDB, techDB) {
                    let objects = [];

                    for (let prop in this.data[charKey].unlockedTechniques) {
                        let config = {};
                        let tech = this.data[charKey].unlockedTechniques[prop];

                        if (tech !== 'none') {
                            let techFromDB = techDB.getDataByKey(tech);
                            config = Object.assign(config, JSON.parse(JSON.stringify(techFromDB)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(tech))));
                            config.key = tech;

                            let newObj = new Technique(config);
                            objects.push(newObj);
                        } else {
                            let newObj = 'none';
                            objects.push(newObj);
                        }
                    }

                    return objects;
                },

                unlockTechniques(charKey, newLevel) {
                    let char = this.getCharDataByKey(charKey);

                    let unlockedTechsKeys = [];
                    let charName = charKey.substring(5);
                    let techNrs = [];
                    let techNr = null;

                    const levelToUnlockTech01 = 1;
                    const levelToUnlockTech02 = 8;
                    const levelToUnlockTech03 = 14;

                    if (newLevel >= levelToUnlockTech01) {
                        techNr = '01';
                        techNrs.push(techNr);
                    }
                    if (newLevel >= levelToUnlockTech02) {
                        techNr = '02';
                        techNrs.push(techNr);
                    }
                    if (newLevel >= levelToUnlockTech03) {
                        techNr = '03';
                        techNrs.push(techNr);
                    }

                    techNrs.forEach((nr) => {
                        let propKey = 'tech' + nr.substring(1);
                        if (char.unlockedTechniques[propKey] === 'none') {
                            let unlockedTechKey = 'technique' + '-' + charName + '-' + nr;
                            unlockedTechsKeys.push(unlockedTechKey);
                            char.unlockedTechniques[propKey] = unlockedTechKey;
                        }
                    });

                    if (unlockedTechsKeys.length <= 0) return null;
                    else return unlockedTechsKeys;
                },

                getEquippedWeaponObjects(charKey, stringDB, gearDB) {
                    let objects = [];

                    for (let prop in this.data[charKey].equippedWeapons) {
                        let config = {};
                        let equWeapon = this.data[charKey].equippedWeapons[prop];

                        if (equWeapon !== 'none') {
                            let weaponFromDB = gearDB.getDataByKey(equWeapon.key);
                            config = Object.assign(config, JSON.parse(JSON.stringify(equWeapon)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(weaponFromDB)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(equWeapon.key))));

                            let newObj = new Item(config);
                            objects.push(newObj);
                        } else {
                            let newObj = 'none';
                            objects.push(newObj);
                        }
                    }

                    return objects;
                },

                getEquippedAccObjects(charKey, stringDB, gearDB) {
                    let objects = [];

                    for (let prop in this.data[charKey].equippedAccs) {
                        let config = {};
                        let equAcc = this.data[charKey].equippedAccs[prop];

                        if (equAcc !== 'none') {
                            let gearFromDB = gearDB.getDataByKey(equAcc.key);
                            config = Object.assign(config, JSON.parse(JSON.stringify(equAcc)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(gearFromDB)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(equAcc.key))));

                            let newObj = new Item(config);
                            objects.push(newObj);
                        } else {
                            let newObj = 'none';
                            objects.push(newObj);
                        }
                    }

                    return objects;
                },

                setEquippedWeaponsDataFromWeaponObjects(charKey, objects) {
                    this.getCharDataByKey(charKey).equippedWeapons = {};
                    let equ = this.getCharDataByKey(charKey).equippedWeapons;

                    objects.forEach((obj, index) => {
                        let propName = 'weapon' + index;
                        if (obj !== 'none' ||
                            obj instanceof GameObject && obj.key !== 'none') {
                            equ[propName] = {
                                key: obj.key,
                                id: obj.id,
                                isEquipped: obj.isEquipped,
                            };
                        } else {
                            equ[propName] = 'none';
                        }
                    });
                },

                setEquippedAccsDataFromAccObjects(charKey, objects) {
                    this.getCharDataByKey(charKey).equippedAccs = {};
                    let equ = this.getCharDataByKey(charKey).equippedAccs;

                    objects.forEach((obj, index) => {
                        let propName = 'accessory' + index;
                        if (obj !== 'none' ||
                            obj instanceof GameObject && obj.key !== 'none') {
                            equ[propName] = {
                                key: obj.key,
                                id: obj.id,
                                isEquipped: obj.isEquipped,
                            };
                        } else {
                            equ[propName] = 'none';
                        }
                    });
                },

                setCharsFromCharObjects(objects) {
                    objects.forEach((char) => {
                        this.data[char.key] = Object.assign(this.data[char.key], JSON.parse(JSON.stringify(char.createConfig())));
                    });
                },

                setStatsFromObject(charKey, stats) {
                    let charStats = this.getCharDataByKey(charKey).stats;

                    charStats.attack = Object.assign(charStats.attack, JSON.parse(JSON.stringify(stats.attack)));
                    charStats.defense = Object.assign(charStats.defense, JSON.parse(JSON.stringify(stats.defense)));
                    charStats.magic = Object.assign(charStats.magic, JSON.parse(JSON.stringify(stats.magic)));
                    charStats.spirit = Object.assign(charStats.spirit, JSON.parse(JSON.stringify(stats.spirit)));
                    charStats.fireResistance = Object.assign(charStats.fireResistance, JSON.parse(JSON.stringify(stats.fireResistance)));
                    charStats.iceResistance = Object.assign(charStats.iceResistance, JSON.parse(JSON.stringify(stats.iceResistance)));
                    charStats.lightningResistance = Object.assign(charStats.lightningResistance, JSON.parse(JSON.stringify(stats.lightningResistance)));
                    charStats.darkResistance = Object.assign(charStats.darkResistance, JSON.parse(JSON.stringify(stats.darkResistance)));
                    charStats.immunities = Object.assign(charStats.immunities, JSON.parse(JSON.stringify(stats.immunities)));
                },

                calculateStatModsFromEquippedGear(charKey, gearDB) {
                    let char = this.getCharDataByKey(charKey);

                    let statAttack = {
                        base: char.stats.attack.base,
                        mods: 0,
                        total: char.stats.attack.base,
                    };
                    let statDefense = {
                        base: char.stats.defense.base,
                        mods: 0,
                        total: char.stats.defense.base,
                    };
                    let statMagic = {
                        base: char.stats.magic.base,
                        mods: 0,
                        total: char.stats.magic.base,
                    };
                    let statSpirit = {
                        base: char.stats.spirit.base,
                        mods: 0,
                        total: char.stats.spirit.base,
                    };
                    let statFireRes = {
                        base: char.stats.fireResistance.base,
                        mods: 0,
                        total: char.stats.fireResistance.base,
                    };
                    let statIceRes = {
                        base: char.stats.iceResistance.base,
                        mods: 0,
                        total: char.stats.iceResistance.base,
                    };
                    let statLightningRes = {
                        base: char.stats.lightningResistance.base,
                        mods: 0,
                        total: char.stats.lightningResistance.base,
                    };
                    let statDarkRes = {
                        base: char.stats.darkResistance.base,
                        mods: 0,
                        total: char.stats.darkResistance.base,
                    };
                    let statImmunities = [];
                    for (let prop in char.equippedWeapons) {
                        let equ = char.equippedWeapons[prop];

                        if (equ !== 'none') {
                            let gearFromDB = gearDB.getDataByKey(equ.key);

                            statAttack.mods += gearFromDB.power;
                            statAttack.total = Math.min(255, statAttack.base + statAttack.mods);
                            if (gearFromDB.statModifiers.hasOwnProperty('strength')) {
                                statAttack.mods += gearFromDB.statModifiers.strength;
                                statAttack.total = Math.min(255, statAttack.base + statAttack.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('vitality')) {
                                statDefense.mods += gearFromDB.statModifiers.vitality;
                                statDefense.total = Math.min(255, statDefense.base + statDefense.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('magic')) {
                                statMagic.mods += gearFromDB.statModifiers.magic;
                                statMagic.total = Math.min(255, statMagic.base + statMagic.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('spirit')) {
                                statSpirit.mods += gearFromDB.statModifiers.spirit;
                                statSpirit.total = Math.min(255, statSpirit.base + statSpirit.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('fireResistance')) {
                                statFireRes.mods += gearFromDB.statModifiers.fireResistance;
                                statFireRes.total = Math.min(100, statFireRes.base + statFireRes.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('iceResistance')) {
                                statIceRes.mods += gearFromDB.statModifiers.iceResistance;
                                statIceRes.total = Math.min(100, statIceRes.base + statIceRes.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('lightningResistance')) {
                                statLightningRes.mods += gearFromDB.statModifiers.lightningResistance;
                                statLightningRes.total = Math.min(100, statLightningRes.base + statLightningRes.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('darkResistance')) {
                                statDarkRes.mods += gearFromDB.statModifiers.darkResistance;
                                statDarkRes.total = Math.min(100, statDarkRes.base + statDarkRes.mods);
                            }
                        }
                    }
                    for (let prop in char.equippedAccs) {
                        let equ = char.equippedAccs[prop];

                        if (equ !== 'none') {
                            let gearFromDB = gearDB.getDataByKey(equ.key);

                            if (gearFromDB.statModifiers.hasOwnProperty('strength')) {
                                statAttack.mods += gearFromDB.statModifiers.strength;
                                statAttack.total = Math.min(255, statAttack.base + statAttack.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('vitality')) {
                                statDefense.mods += gearFromDB.statModifiers.vitality;
                                statDefense.total = Math.min(255, statDefense.base + statDefense.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('magic')) {
                                statMagic.mods += gearFromDB.statModifiers.magic;
                                statMagic.total = Math.min(255, statMagic.base + statMagic.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('spirit')) {
                                statSpirit.mods += gearFromDB.statModifiers.spirit;
                                statSpirit.total = Math.min(255, statSpirit.base + statSpirit.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('fireResistance')) {
                                statFireRes.mods += gearFromDB.statModifiers.fireResistance;
                                statFireRes.total = Math.min(100, statFireRes.base + statFireRes.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('iceResistance')) {
                                statIceRes.mods += gearFromDB.statModifiers.iceResistance;
                                statIceRes.total = Math.min(100, statIceRes.base + statIceRes.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('lightningResistance')) {
                                statLightningRes.mods += gearFromDB.statModifiers.lightningResistance;
                                statLightningRes.total = Math.min(100, statLightningRes.base + statLightningRes.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('darkResistance')) {
                                statDarkRes.mods += gearFromDB.statModifiers.darkResistance;
                                statDarkRes.total = Math.min(100, statDarkRes.base + statDarkRes.mods);
                            }
                            if (gearFromDB.statModifiers.hasOwnProperty('immunities')) {
                                statImmunities.push(...gearFromDB.statModifiers.immunities);

                            }
                        }
                    }

                    return {
                        attack: statAttack,
                        defense: statDefense,
                        magic: statMagic,
                        spirit: statSpirit,
                        fireResistance: statFireRes,
                        iceResistance: statIceRes,
                        lightningResistance: statLightningRes,
                        darkResistance: statDarkRes,
                        immunities: statImmunities
                    };
                },

                calculateStatModsFromEquippedGearForAllChars(gearDb) {
                    let keys = this.getCharKeys();
                    let newStats = [];
                    keys.forEach(key => {
                        let stats = this.calculateStatModsFromEquippedGear(key, gearDb);
                        newStats.push(stats);
                    });

                    return newStats;
                },

                setStatsFromEquippedGearForAllChars(gearDB) {
                    let charKeys = this.getCharKeys();
                    let stats = this.calculateStatModsFromEquippedGearForAllChars(gearDB);

                    charKeys.forEach((key, index) => {
                        this.setStatsFromObject(key, stats[index]);
                    });
                },

                levelUpParty(onlyLivingChars = false) {
                    const maxLevel = 99;

                    let charsData = this.getCharsData();
                    let chars = charsData;
                    if (onlyLivingChars) chars = this.getLivingCharsData();
                    let resultObjects = [];

                    //TODO: add exp to each char and check if levelled up
                    chars.forEach((char) => {
                        const currLevel = char.stats.level;
                        let newLevel = currLevel;
                        const newExp = char.stats.exp;

                        if (currLevel < maxLevel) {
                            let i = currLevel;

                            while (true) {
                                const expNeededForNextLevel = this.getExpNeededForLevel(i + 1);
                                if (newExp < expNeededForNextLevel) break;
                                i++;
                            }

                            newLevel = i;
                        }
                        const leveledUp = currLevel < newLevel;
                        char.stats.level = newLevel;

                        const statIncreases = this.levelUpStats(char, newLevel);

                        const unlockedTechniques = this.unlockTechniques(char.key, newLevel);

                        resultObjects.push({
                            charKey: char.key,
                            oldLevel: currLevel,
                            newLevel: newLevel,
                            didLevelUp: leveledUp,
                            statIncreases: statIncreases,
                            unlockedTechniques: unlockedTechniques,
                        });
                    });

                    //TODO: return result object for each char with info on
                    // how much exp was gained and what level they reached

                    return resultObjects;
                },

                levelUpStats(charData, newLevel) {
                    const statMax = 255;
                    const maxHpScale = charData.levelingScales.maxHp * 1.5;
                    const attackScale = charData.levelingScales.attack;
                    const defenseScale = charData.levelingScales.defense;
                    const magicScale = charData.levelingScales.magic;
                    const spiritScale = charData.levelingScales.spirit;
                    const maxHpBase = 53;
                    const attackBase = 10;
                    const defenseBase = 10;
                    const magicBase = 10;
                    const spiritBase = 10;

                    const newMaxHp = Math.min(Math.floor(maxHpBase * maxHpScale + Math.pow(newLevel, maxHpScale)), statMax);
                    const newAttack = Math.min(Math.floor(attackBase * attackScale + Math.pow(newLevel, attackScale)), statMax);
                    const newDefense = Math.min(Math.floor(defenseBase * defenseScale + Math.pow(newLevel, defenseScale)), statMax);
                    const newMagic = Math.min(Math.floor(magicBase * magicScale + Math.pow(newLevel, magicScale)), statMax);
                    const newSpirit = Math.min(Math.floor(spiritBase * spiritScale + Math.pow(newLevel, spiritScale)), statMax);

                    const oldMaxHp = charData.stats.maxHp.base;
                    const oldAttack = charData.stats.attack.base;
                    const oldDefense = charData.stats.defense.base;
                    const oldMagic = charData.stats.magic.base;
                    const oldSpirit = charData.stats.spirit.base;

                    charData.stats.hp = newMaxHp;
                    charData.stats.maxHp.base = newMaxHp;
                    charData.stats.maxHp.total = newMaxHp;
                    charData.stats.attack.base = newAttack;
                    charData.stats.defense.base = newDefense;
                    charData.stats.magic.base = newMagic;
                    charData.stats.spirit.base = newSpirit;

                    return {
                        maxHp: Math.abs(newMaxHp - oldMaxHp),
                        attack: Math.abs(newAttack - oldAttack),
                        defense: Math.abs(newDefense - oldDefense),
                        magic: Math.abs(newMagic - oldMagic),
                        spirit: Math.abs(newSpirit - oldSpirit),
                    };
                },

                addGainedExpToParty(gainedExp, onlyLivingChars = false) {
                    let chars = this.getCharsData();
                    if (onlyLivingChars) chars = this.getLivingCharsData();

                    chars.forEach((char) => {
                        const newExp = char.stats.exp + gainedExp;
                        char.stats.exp = newExp;
                    });
                },

                getExpNeededForLevel(level) {
                    const constant = 2.7;

                    return Math.round(Math.pow(level, constant) - 1);
                },

                data: {
                    'party': {
                        money: 999,
                    },

                    'char-noctis': {
                        type: 'player',

                        stats: {
                            level: 1,
                            exp: 0,
                            tp: 0,
                            hp: 0,
                            maxHp: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            attack: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            defense: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            magic: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            spirit: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            fireResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            iceResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            lightningResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            darkResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            immunities: [],
                        },

                        levelingScales: {
                            maxHp: 0.6,
                            attack: 0.7,
                            defense: 0.55,
                            magic: 1.0,
                            spirit: 0.78,
                        },

                        unlockedTechniques: {
                            tech1: 'none',
                            tech2: 'none',
                            tech3: 'none'
                        },
                        equippedWeapons: {
                            weapon0: {
                                key: 'weapon-sword-01',
                                id: 0,
                                isEquipped: true,
                            },
                            weapon1: {
                                key: 'weapon-polearm-01',
                                id: 0,
                                isEquipped: true,
                            },
                            weapon2: 'none',
                            weapon3: 'none'
                        },
                        equippedAccs: {
                            accessory0: {
                                key: 'accessory-01',
                                id: 0,
                                isEquipped: true,
                            },
                            accessory1: 'none'
                        },
                    },
                    'char-gladio': {
                        type: 'player',

                        stats: {
                            level: 1,
                            exp: 0,
                            hp: 0,
                            tp: 0,
                            maxHp: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            attack: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            defense: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            magic: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            spirit: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            fireResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            iceResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            lightningResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            darkResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            immunities: [],
                        },

                        levelingScales: {
                            maxHp: 1.0,
                            attack: 1.0,
                            defense: 1.0,
                            magic: 0.1,
                            spirit: 0.7,
                        },

                        unlockedTechniques: {
                            tech1: 'none',
                            tech2: 'none',
                            tech3: 'none'
                        },
                        equippedWeapons: {
                            weapon0: {
                                key: 'weapon-greatsword-01',
                                id: 0,
                                isEquipped: true,
                            },
                            weapon1: {
                                key: 'weapon-shield-01',
                                id: 0,
                                isEquipped: true,
                            },
                            weapon2: 'none',
                            weapon3: 'none'
                        },
                        equippedAccs: {
                            accessory0: {
                                key: 'accessory-01',
                                id: 1,
                                isEquipped: true,
                            },
                            accessory1: 'none'
                        },
                    },
                    'char-ignis': {
                        type: 'player',

                        stats: {
                            level: 1,
                            exp: 0,
                            hp: 0,
                            tp: 0,
                            maxHp: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            attack: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            defense: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            magic: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            spirit: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            fireResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            iceResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            lightningResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            darkResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            immunities: [],
                        },

                        levelingScales: {
                            maxHp: 0.63,
                            attack: 0.62,
                            defense: 0.77,
                            magic: 0.38,
                            spirit: 1.0,
                        },

                        unlockedTechniques: {
                            tech1: 'none',
                            tech2: 'none',
                            tech3: 'none'
                        },
                        equippedWeapons: {
                            weapon0: {
                                key: 'weapon-daggers-01',
                                id: 0,
                                isEquipped: true,
                            },
                            weapon1: {
                                key: 'weapon-polearm-01',
                                id: 1,
                                isEquipped: true,
                            },
                            weapon2: 'none',
                            weapon3: 'none'
                        },
                        equippedAccs: {
                            accessory0: {
                                key: 'accessory-01',
                                id: 2,
                                isEquipped: true,
                            },
                            accessory1: 'none'
                        },
                    },
                    'char-prompto': {
                        type: 'player',

                        stats: {
                            level: 1,
                            exp: 0,
                            hp: 0,
                            tp: 1,
                            maxHp: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            attack: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            defense: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            magic: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            spirit: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            fireResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            iceResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            lightningResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            darkResistance: {
                                total: 0,
                                base: 0,
                                mods: 0,
                            },
                            immunities: [],
                        },

                        levelingScales: {
                            maxHp: 0.33,
                            attack: 0.77,
                            defense: 0.52,
                            magic: 0.17,
                            spirit: 0.74,
                        },

                        unlockedTechniques: {
                            tech1: 'none',
                            tech2: 'none',
                            tech3: 'none'
                        },
                        equippedWeapons: {
                            weapon0: {
                                key: 'weapon-firearm-01',
                                id: 0,
                                isEquipped: true,
                            },
                            weapon1: {
                                key: 'weapon-machinery-01',
                                id: 0,
                                isEquipped: true,
                            },
                            weapon2: 'none',
                            weapon3: 'none'
                        },
                        equippedAccs: {
                            accessory0: {
                                key: 'accessory-01',
                                id: 3,
                                isEquipped: true,
                            },
                            accessory1: 'none'
                        },
                    }
                }
            },

            inventoryWeapons: {
                getInventoryWeaponObjects(stringDB, gearDB) {
                    let list = [];
                    for (let prop in this.data) {
                        let config = {};
                        let invGear = this.data[prop];
                        let gearFromDB = gearDB.getDataByKey(invGear.key);
                        config = Object.assign(config, JSON.parse(JSON.stringify(invGear)));
                        config = Object.assign(config, JSON.parse(JSON.stringify(gearFromDB)));
                        config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(invGear.key))));

                        let newObj = new Item(config);
                        list.push(newObj);
                    }

                    return list;
                },

                setDataFromWeaponObjects(objects) {
                    this.data = {};
                    objects.forEach((obj, index) => {
                        let propName = 'weapon' + index;
                        this.data[propName] = {
                            key: obj.key,
                            id: obj.id,
                            isEquipped: obj.isEquipped,
                        };
                    });
                },

                getNewIdForItem(newItemKey) {
                    let newId = 0;
                    let foundSame;
                    while (newId >= 0) {
                        for (let prop in this.data) {
                            foundSame = false;
                            if (this.data.hasOwnProperty(prop)) {
                                let invItem = this.data[prop];
                                if ((invItem.key === newItemKey) && invItem.id === newId) {
                                    newId++;
                                    foundSame = true;
                                    break;
                                }
                            }
                        }
                        if (!foundSame) {
                            return newId;
                        }
                    }
                },

                addNewItemToInventory(newItem) {
                    for (let prop in this.data) {
                        if (this.data.hasOwnProperty(prop)) {
                            let invItem = this.data[prop];

                            if (invItem === 'none') {
                                let newId = this.getNewIdForItem(newItem.key);
                                this.data[prop] = {
                                    key: newItem.key,
                                    id: newId,
                                    isEquipped: newItem.isEquipped,
                                };

                                return true;
                            }
                        }
                    }
                    return false;
                },

                data: {
                    weapon0: {
                        key: 'weapon-sword-01',
                        id: 0,
                        isEquipped: true,
                    },
                    weapon1: {
                        key: 'weapon-greatsword-01',
                        id: 0,
                        isEquipped: true,
                    },
                    weapon2: {
                        key: 'weapon-polearm-01',
                        id: 0,
                        isEquipped: true,
                    },
                    weapon3: {
                        key: 'weapon-polearm-01',
                        id: 1,
                        isEquipped: true,
                    },
                    weapon4: {
                        key: 'weapon-daggers-01',
                        id: 0,
                        isEquipped: true,
                    },
                    weapon5: {
                        key: 'weapon-firearm-01',
                        id: 0,
                        isEquipped: true,
                    },
                    weapon6: {
                        key: 'weapon-shield-01',
                        id: 0,
                        isEquipped: true,
                    },
                    weapon7: {
                        key: 'weapon-machinery-01',
                        id: 0,
                        isEquipped: true,
                    },
                    weapon8: {
                        key: 'weapon-greatsword-01',
                        id: 1,
                    },
                    weapon9: {
                        key: 'weapon-daggers-01',
                        id: 1,
                    },
                    weapon10: {
                        key: 'weapon-firearm-01',
                        id: 1,
                    },
                    weapon11: {
                        key: 'weapon-shield-01',
                        id: 1,
                    },
                    weapon12: {
                        key: 'weapon-machinery-01',
                        id: 1,
                    },
                }
            },
            inventoryAccessories: {
                getInventoryAccObjects(stringDB, gearDB) {
                    let list = [];
                    for (let prop in this.data) {
                        let config = {};
                        let invGear = this.data[prop];
                        let gearFromDB = gearDB.getDataByKey(invGear.key);
                        config = Object.assign(config, JSON.parse(JSON.stringify(invGear)));
                        config = Object.assign(config, JSON.parse(JSON.stringify(gearFromDB)));
                        config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(invGear.key))));

                        let newObj = new Item(config);
                        list.push(newObj);
                    }

                    return list;
                },

                setDataFromAccObjects(objects) {
                    this.data = {};
                    objects.forEach((obj, index) => {
                        let propName = 'accessory' + index;
                        this.data[propName] = {
                            key: obj.key,
                            id: obj.id,
                            isEquipped: obj.isEquipped,
                        };
                    });
                },

                getNewIdForItem(newItemKey) {
                    let newId = 0;
                    let foundSame;
                    while (newId >= 0) {
                        for (let prop in this.data) {
                            foundSame = false;
                            if (this.data.hasOwnProperty(prop)) {
                                let invItem = this.data[prop];
                                if ((invItem.key === newItemKey) && invItem.id === newId) {
                                    newId++;
                                    foundSame = true;
                                    break;
                                }
                            }
                        }
                        if (!foundSame) {
                            return newId;
                        }
                    }
                },

                addNewItemToInventory(newItem) {
                    for (let prop in this.data) {
                        if (this.data.hasOwnProperty(prop)) {
                            let invItem = this.data[prop];

                            if (invItem === 'none') {
                                let newId = this.getNewIdForItem(newItem.key);
                                this.data[prop] = {
                                    key: newItem.key,
                                    id: newId,
                                    isEquipped: newItem.isEquipped,
                                };

                                return true;
                            }
                        }
                    }
                    return false;
                },

                data: {
                    accessory0: {
                        key: 'accessory-01',
                        id: 0,
                        isEquipped: true,
                    },
                    accessory1: {
                        key: 'accessory-01',
                        id: 1,
                        isEquipped: true,
                    },
                    accessory2: {
                        key: 'accessory-01',
                        id: 2,
                        isEquipped: true,
                    },
                    accessory3: {
                        key: 'accessory-01',
                        id: 3,
                        isEquipped: true,
                    },
                    accessory4: {
                        key: 'accessory-02',
                        id: 0,
                    },
                    accessory5: {
                        key: 'accessory-02',
                        id: 1,
                    },
                    accessory6: {
                        key: 'accessory-02',
                        id: 2,
                    },
                    accessory7: {
                        key: 'accessory-02',
                        id: 3,
                    },
                }
            },
            inventoryItems: {
                getInventoryItemObjects(stringDB, itemDB) {
                    let objects = [];
                    let data = this.data;

                    for (let prop in data) {
                        let config = {};
                        let itemInv = data[prop];
                        let itemFromDB = itemDB.getDataByKey(itemInv.key);

                        if (itemInv !== 'none') {
                            config = Object.assign(config, JSON.parse(JSON.stringify(itemInv)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(itemFromDB)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(itemInv.key))));

                            objects.push(new Item(config));
                        } else {
                            objects.push('none');
                        }
                    }


                    return objects;
                },

                setDataFromItemObjects(objects) {
                    this.data = {};
                    let data = this.data;
                    objects.forEach((obj, index) => {
                        let propName = 'item' + prependZeroes('' + index, 2);
                        if (obj instanceof GameObject && obj.key === 'trash') {
                            // ignore 'trash' SpecialMenuObjects
                        } else if ((typeof obj === 'string' && obj !== 'empty' && obj !== 'none') ||
                            (obj instanceof GameObject && obj.key !== 'empty' && obj.key !== 'none' && obj.quantity > 0)) {
                            data[propName] = {
                                key: obj.key,
                                quantity: obj.quantity,
                            };
                        } else {
                            data[propName] = 'none';
                        }
                    });
                },

                getNewIdForItem(newItemKey) {
                    let newId = 0;
                    let foundSame;
                    while (newId >= 0) {
                        for (let prop in this.data) {
                            foundSame = false;
                            if (this.data.hasOwnProperty(prop)) {
                                let invItem = this.data[prop];
                                if ((invItem.key === newItemKey) && invItem.id === newId) {
                                    newId++;
                                    foundSame = true;
                                    break;
                                }
                            }
                        }
                        if (!foundSame) {
                            return newId;
                        }
                    }
                },

                addNewItemToInventory(newItem, itemDB) {
                    let firstNoneObjectProp = null;
                    for (let prop in this.data) {
                        if (this.data.hasOwnProperty(prop)) {
                            let invItem = this.data[prop];
                            let invItemDB = itemDB.getDataByKey(invItem.key);

                            if (invItem.key === newItem.key) {
                                if ((invItem.quantity + newItem.quantity <= invItemDB.maxQuantity)) {
                                    this.data[prop].quantity += newItem.quantity;

                                    return true;
                                }
                            }

                            if (firstNoneObjectProp === null && invItem === 'none') {
                                firstNoneObjectProp = prop;
                            }
                        }
                    }
                    if (isDefinedAndNotNull(firstNoneObjectProp)) {
                        let newId = this.getNewIdForItem(newItem.key);
                        this.data[firstNoneObjectProp] = {
                            key: newItem.key,
                            id: newId,
                            quantity: newItem.quantity,
                        };

                        return true;
                    }

                    return false;
                },

                data: {
                    item00: {
                        key: 'item-potion-01',
                        id: 0,
                        quantity: 10,
                    },
                    item01: {
                        key: 'item-antidote-01',
                        id: 0,
                        quantity: 5,
                    },
                    item02: {
                        key: 'item-remedy-01',
                        id: 0,
                        quantity: 5,
                    },
                    item03: {
                        key: 'item-phoenix-down-01',
                        id: 0,
                        quantity: 5,
                    },
                    item04: {
                        key: 'spell-ice-01',
                        id: 0,
                        quantity: 5,
                    },
                    item05: {
                        key: 'spell-fire-01',
                        id: 0,
                        quantity: 5,
                    },
                    item06: 'none',
                    item07: 'none',
                    item08: 'none',
                    item09: 'none',
                    item10: 'none',
                    item11: 'none',
                    item12: 'none',
                    item13: 'none',
                    item14: 'none',
                }
            },
            inventoryTreasures: {
                getInventoryTreasureObjects(stringDB, treasureDB) {
                    let objects = [];
                    let data = this.data;

                    for (let prop in data) {
                        let config = {};
                        let itemInv = data[prop];
                        let itemFromDB = treasureDB.getDataByKey(itemInv.key);

                        if (itemInv !== 'none') {
                            config = Object.assign(config, JSON.parse(JSON.stringify(itemInv)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(itemFromDB)));
                            config = Object.assign(config, JSON.parse(JSON.stringify(stringDB.getDataByKey(itemInv.key))));

                            objects.push(new Item(config));
                        }
                    }


                    return objects;
                },

                setDataFromTreasureObjects(objects) {
                    this.data = {};
                    let data = this.data;
                    objects.forEach((obj, index) => {
                        let propName = 'item' + index;
                        if (obj instanceof GameObject && obj.key === 'trash') {
                            // ignore 'trash' SpecialMenuObjects
                        } else if ((typeof obj === 'string' && obj !== 'empty' && obj !== 'none') ||
                            (obj instanceof GameObject && obj.key !== 'empty' && obj.key !== 'none')) {
                            data[propName] = {
                                key: obj.key,
                                quantity: obj.quantity,
                                id: obj.id,
                            };
                        } else {
                            data[propName] = 'none';
                        }
                    });
                },

                getNewIdForItem(newItemKey) {
                    let newId = 0;
                    let foundSame;
                    while (newId >= 0) {
                        for (let prop in this.data) {
                            foundSame = false;
                            if (this.data.hasOwnProperty(prop)) {
                                let invItem = this.data[prop];
                                if ((invItem.key === newItemKey) && invItem.id === newId) {
                                    newId++;
                                    foundSame = true;
                                    break;
                                }
                            }
                        }
                        if (!foundSame) {
                            return newId;
                        }
                    }
                },

                addNewItemToInventory(newItem) {
                    for (let prop in this.data) {
                        if (this.data.hasOwnProperty(prop)) {
                            let invItem = this.data[prop];

                            if (invItem === 'none') {
                                let newId = this.getNewIdForItem(newItem.key);
                                this.data[prop] = {
                                    key: newItem.key,
                                    id: newId,
                                    quantity: newItem.quantity,
                                };

                                return true;
                            }
                        }
                    }
                    return false;
                },

                data: {

                }
            },

            scenePersistentData: {
                getDataByKey(key) {
                    return this.data[key];
                },
                setDataByKey(key, value) {
                    this.data[key] = value;
                },

                data: {
                    'TestScene2': {
                        chests: {
                            'chest-000': {
                                isOpened: false,
                                lootDrop: {
                                    item00: {
                                        key: 'item-potion-01',
                                        quantity: 5,
                                    },
                                },
                            },
                        },
                    },
                }
            },
        };
    }

    setGame(game) {
        this.game = game;
    }

    saveGame() {
        let gameSaveObject = {};
        gameSaveObject.gameSettings = {};
        gameSaveObject.saveState = {};
        gameSaveObject.party = {};
        gameSaveObject.inventoryItems = {};
        gameSaveObject.inventoryWeapons = {};
        gameSaveObject.inventoryAccessories = {};
        gameSaveObject.inventoryTreasures = {};
        gameSaveObject.scenePersistentData = {};

        gameSaveObject.gameSettings.data = this.gameData.gameSettings.data;
        gameSaveObject.saveState.data = this.gameData.saveState.data;
        gameSaveObject.party.data = this.gameData.party.data;
        gameSaveObject.inventoryItems.data = this.gameData.inventoryItems.data;
        gameSaveObject.inventoryWeapons.data = this.gameData.inventoryWeapons.data;
        gameSaveObject.inventoryAccessories.data = this.gameData.inventoryAccessories.data;
        gameSaveObject.inventoryTreasures.data = this.gameData.inventoryTreasures.data;
        gameSaveObject.scenePersistentData.data = this.gameData.scenePersistentData.data;

        localStorage.setItem('ffxv-save-01', JSON.stringify(gameSaveObject));

        // console.log(localStorage);
    }

    loadGame() {
        if (isNullOrUndefined(localStorage.getItem('ffxv-save-01'))) return;

        let gameSaveObject = JSON.parse(localStorage.getItem('ffxv-save-01'));
        Object.keys(gameSaveObject).forEach((prop1) => {
            Object.keys(this.gameData[prop1].data).forEach((prop2) => {
                delete this.gameData[prop1].data[prop2];
            });
            Object.assign(this.gameData[prop1].data, gameSaveObject[prop1].data);
        });

        // this.game.registry.set('gameSettings', this.gameData.gameSettings.data);
        // this.game.registry.set('saveState', this.gameData.saveState);
        // this.game.registry.set('party', this.gameData.party);
        // this.game.registry.set('inventoryItems', this.gameData.inventoryItems);
        // this.game.registry.set('inventoryWeapons', this.gameData.inventoryWeapons);
        // this.game.registry.set('inventoryAccessories', this.gameData.inventoryAccessories);
        // this.game.registry.set('inventoryTreasures', this.gameData.inventoryTreasures);
        // this.game.registry.set('scenePersistentData', this.gameData.scenePersistentData);

        // console.log(gameSaveObject);
    }
}