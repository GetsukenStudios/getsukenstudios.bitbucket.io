import {
    isDefinedAndNotNull
} from "../../Base/Utils";
import ActorSprite from "./ActorSprite";
import CustomSprite from "./CustomSprite";

export default class PlayerSprite extends ActorSprite {

    constructor(scene, key, x, y) {
        super(scene, key, x, y);

        this.isInteracting = false;

        this.initialize();
    }

    initialize() {
        super.initialize();

    }

    resetKeyboards() {
        // console.log("before reset - action key isDown: ", this.keys.action.isDown);
        // console.log("before reset  - action key isUp: ", this.keys.action.isUp);
        this.keys.resetAllKeys();
        // console.log("after reset - action key isDown: ", this.keys.action.isDown);
        // console.log("after reset  - action key isUp: ", this.keys.action.isUp);
    }

    openMainMenu() {
        // this.scene.game.events.emit('Game', 'EnterMenuFromPlay');

        this.scene.game.events.emit('Menu', 'OpenMainMenu');
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);

        if (this.scene.gameState.getCurrentStateName() == 'Play') {
            if (this.keys.down.isDown && !this.keys.up.isDown) {
                this.moveDown();

            }
            if (this.keys.up.isDown && !this.keys.down.isDown) {
                this.moveUp();

            }
            if (this.keys.left.isDown && !this.keys.right.isDown) {
                this.moveLeft();

            }
            if (this.keys.right.isDown && !this.keys.left.isDown) {
                this.moveRight();

            }
        }
        if (Phaser.Input.Keyboard.JustDown(this.keys.start)) {
            if (this.scene.gameState.getCurrentStateName() == 'Play' &&
                !this.isMoving) {
                this.openMainMenu();
            }
        }
        if (Phaser.Input.Keyboard.JustDown(this.keys.action)) {
            if (this.scene.gameState.getCurrentStateName() == 'Play') {
                if (!this.isInteracting) {
                    // console.log(this.scene.map.getTileAtWorldXY(this.x, this.y - 16, false, 
                    //     this.scene.mainCamera, 'World'));          

                    let posInFront;
                    if (this.direction === 'down') {
                        posInFront = {
                            x: this.x,
                            y: this.y + this.tileSize
                        };
                    } else if (this.direction === 'up') {
                        posInFront = {
                            x: this.x,
                            y: this.y - this.tileSize
                        };
                    } else if (this.direction === 'left') {
                        posInFront = {
                            x: this.x - this.tileSize,
                            y: this.y
                        };
                    } else if (this.direction === 'right') {
                        posInFront = {
                            x: this.x + this.tileSize,
                            y: this.y
                        };
                    }


                    let objectInFront;
                    let isInFront = function (obj) {
                        if (obj.x === posInFront.x && obj.y === posInFront.y) {
                            objectInFront = obj;
                        }
                    };
                    for (let property in this.scene.sprites) {
                        if (this.scene.sprites.hasOwnProperty(property) && Array.isArray(this.scene.sprites[property])) {
                            this.scene.sprites[property].forEach(isInFront);
                        }
                    }

                    if (isDefinedAndNotNull(objectInFront) && objectInFront instanceof CustomSprite) {
                        // console.log('Player interacted with object = ' + objectInFront.key);

                        objectInFront.interactedWith(this);
                        this.isInteracting = true;
                    }
                } else {
                    setTimeout(() => {
                        this.isInteracting = false;
                    }, 0);
                }
            }

        }

        // check if Sprite reached its target position
        if (this.isMoving) {
            if (Phaser.Math.Fuzzy.Equal(this.x, this.targetPos.x, this.fuzzyEpsilon) &&
                Phaser.Math.Fuzzy.Equal(this.y, this.targetPos.y, this.fuzzyEpsilon)) {
                this.body.setVelocity(0, 0);
                this.x = this.targetPos.x;
                this.y = this.targetPos.y;

                this.isMoving = false;
            }
        }


        // animations
        this.changeFaceDirectionTexture();

        // // create animations [doesn't work - using manual animation code in Sprite classes]
        // this.game.anims.create({
        //     key: "player-walk-left",
        //     frames: this.anims.generateFrameNumbers("main-chars", {
        //         start: 4,
        // 		end: 5,
        // 		first: 0,
        //     }),
        //     frameRate: 8,
        //     repeat: -1
        // });
        // this.game.anims.create({
        //     key: "player-walk-right",
        //     frames: this.anims.generateFrameNumbers("main-chars", {
        //         start: 4,
        //         end: 5,
        //     }),
        //     frameRate: 8,
        //     repeat: -1
        // });
        // this.game.anims.create({
        //     key: "player-walk-up",
        //     frames: this.anims.generateFrameNumbers("main-chars", {
        //         start: 2,
        //         end: 3,
        //     }),
        //     frameRate: 8,
        //     repeat: -1
        // });
        // this.game.anims.create({
        //     key: "player-walk-down",
        //     frames: this.anims.generateFrameNumbers("main-chars", {
        //         start: 0,
        //         end: 1,
        //     }),
        //     frameRate: 8,
        //     repeat: -1
        // });


        this.elapsedMs = (this.elapsedMs + delta) % 1000;
        this.frameCount = Math.floor(this.elapsedMs / (1000 / this.framePerSecond)) % 2;

        // console.log(this.isMoving);
    }

    update(time, delta) {
        super.update(time, delta);
    }


    // collision handlers
    onCollideWithWorldLayer(player, object) {
        // collision with World layer or Objects layer

        // if (object.key) {
        //     // collided with an Object
        //     console.log('PlayerSprite collided with ' + object.key + ' in ObjectLayer');
        // } else {
        //     // collided with a Tile
        //     console.log('PlayerSprite collided with ' + 'WorldLayer');
        // }

        this.body.setVelocity(0, 0);
        this.targetPos.x = Phaser.Math.Snap.To(this.x, this.tileSize);
        this.targetPos.y = Phaser.Math.Snap.To(this.y, this.tileSize);

        this.isMoving = false;
    }

    onCollideWithBelowLayer(player, object) {
        // if (object.key) {
        //     // collided with an Object
        //     console.log('PlayerSprite collided with ' + object.key + ' in ObjectLayer');
        // } else {
        //     // collided with a Tile
        //     console.log('PlayerSprite collided with ' + 'BelowLayer');
        // }


    }
}