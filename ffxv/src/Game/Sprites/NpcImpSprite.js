import CustomSprite from "./CustomSprite";
import {
    isDefinedAndNotNull
} from "../../Base/Utils";
import NpcSprite from "./NpcSprite";

export default class NpcImpSprite extends NpcSprite {

    constructor(scene, key, x, y, subType, scriptId, styleId) {
        super(scene, key, x, y, subType, scriptId, styleId);
    }

    initStateMachine() {
        super.initStateMachine();

        this.imageKey = 'sprites-monsters';
    }

    setTextureBySubTypeAndName() {
        let frame = this.startImageFrame;

        switch (this.styleId) {
            case 0:
                frame = 0;
                break;
        }

        this.startImageFrame = frame;

        this.setTexture(this.imageKey, this.startImageFrame);
    }

    interactedWith(playerSprite) {

        let dialogData = {
            config: {

            },
            lines: [

            ]
        };
        let callback = function () {

        };

        if (this.currentState === 'st0-000') {
            let key3 = 'dlg-0006-1';
            let dialogStringData = this.dialogDB.getDataByKey(key3);
            dialogData.lines.push(dialogStringData);
            dialogData.lines[0].options = {
                indent: 2,
            };

            callback = function () {
                this.scene.game.events.emit('Menu', 'OpenBattleMenu', {
                    monsterMob: 'monster-mob-02',
                    playerSprite: {
                        faceDirection: this.scene.sprites.playerSprite.getFaceDirection(),
                        frameCount: this.scene.sprites.playerSprite.getFrameCount()
                    },
                    music: {
                        pauseCurrentAudio: true,
                    },
                    dontResumeParentScene: true,
                }, callbackForAfterBattle.bind(this));
            };

            let callbackForAfterBattle = function () {
                // this.scene.flashScreen();

                let key3 = 'dlg-0006-2';
                let dialogStringData = this.dialogDB.getDataByKey(key3);
                dialogData.lines = [];
                dialogData.lines.push(dialogStringData);

                this.scene.game.events.emit('Menu', 'OpenDialogMenu', dialogData, null);
            };
        }

        this.scene.game.events.emit('Menu', 'OpenDialogMenu', dialogData, callback.bind(this));
    }

    update(time, delta) {
        super.update(time, delta);
    }
}