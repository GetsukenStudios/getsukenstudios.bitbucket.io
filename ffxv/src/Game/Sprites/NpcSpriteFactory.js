import { extractNameFromNameHyphenNumberKey } from "../../Base/Utils";
import NpcManSprite from "./NpcManSprite";
import NpcWomanSprite from "./NpcWomanSprite";
import NpcImpSprite from "./NpcImpSprite";

export default class NpcSpriteFactory {
    constructor() {

    }

    static createNpcSprite(scene, key, x, y, subType, scriptId, styleId) {
        let name = extractNameFromNameHyphenNumberKey(key);

        switch (name) {
            case 'man':
                return new NpcManSprite(scene, key, x, y, subType, scriptId, styleId);
            case 'woman':
                return new NpcWomanSprite(scene, key, x, y, subType, scriptId, styleId);
            case 'imp':
                return new NpcImpSprite(scene, key, x, y, subType, scriptId, styleId);
        }
    }
}