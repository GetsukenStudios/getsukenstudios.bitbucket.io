export default class ZoneObject extends Phaser.GameObjects.Zone {
    constructor(scene, key, x, y, width, height, isStaticBody) {
        super(scene, x, y, width, height);
        this.key = key || ''; 
        this.scene = scene;
        let isStatic = isStaticBody || true;

        // this.scene.physics.world.enable(this).setOrigin(0, 0);
        this.scene.physics.add.existing(this, isStatic).setOrigin(0, 0);
    }

    overlappedWith(){        
                
    }
}