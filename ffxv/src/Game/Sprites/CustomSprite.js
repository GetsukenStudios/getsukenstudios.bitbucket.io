export default class CustomSprite extends Phaser.GameObjects.Sprite {
    constructor(scene, key, x, y, texture, frame, isStaticBody) {
        super(scene, x, y, texture, frame);
        this.key = key || '';
        this.scene = scene;
        this.keys = scene.keys;
        let isStatic = isStaticBody || false;

        this.scene.add.existing(this).setOrigin(0, 0);
        // this.scene.physics.world.enable(this).setOrigin(0, 0);
        this.scene.physics.add.existing(this, isStatic).setOrigin(0, 0);

        this.initialize();
    }

    initialize() {

    }

    interactedWith(playerSprite) {

    }

    preUpdate(time, delta) {

    }

    update(time, delta) {

    }
}