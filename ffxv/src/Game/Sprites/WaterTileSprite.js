import CustomSprite from "./CustomSprite";

export default class WaterTileSprite extends CustomSprite {

    constructor(scene, key, x, y) {
        super(scene, key, x, y, 'sprites-terrain-01', 0, true);

        this.imageKey = 'sprites-terrain-01';
        this.startImageKey = 0;

        this.elapsedMs = 0;
        this.frameCount = 0;
        this.framePerSecond = 3;
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);

        this.setTexture(this.imageKey, 0 + this.frameCount);

        this.elapsedMs += delta;
        this.frameCount = Math.floor(this.elapsedMs / (1000 / this.framePerSecond)) % 4;
    }

    update(time, delta) {
        super.update(time, delta);
    }
}


