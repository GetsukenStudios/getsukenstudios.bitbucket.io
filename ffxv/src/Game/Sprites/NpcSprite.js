import {
    isDefinedAndNotNull,
    extractNameFromNameHyphenNumberKey,
} from "../../Base/Utils";
import ActorSprite from "./ActorSprite";

export default class NpcSprite extends ActorSprite {

    constructor(scene, key, x, y, subType, scriptId, styleId) {
        super(scene, key, x, y);

        this.name = key || '';
        this.type = 'npc';
        this.subType = subType || '';
        this.scriptId = scriptId || 0;
        this.styleId = styleId || 0;
        this.currentState = 'st0-000';

        this.body.setImmovable();

        this.initialize();
        this.initStateMachine();
    }

    initialize() {
        super.initialize();

        this.dialogDB = this.scene.registry.get('dialogDatabase');

        // extract name of the NPC from its key (e.g. "goblin-000" => "goblin")
        this.name = extractNameFromNameHyphenNumberKey(this.key);

        this.setTextureBySubTypeAndName();
    }

    initStateMachine() {
        this.currentState = 'st' + this.scriptId + '-' + '000';
    }

    setTextureBySubTypeAndName() {

    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);

        // animations
        this.changeFaceDirectionTexture();

        // check if Sprite reached its target position
        if (this.isMoving) {
            if (Phaser.Math.Fuzzy.Equal(this.x, this.targetPos.x, this.fuzzyEpsilon) &&
                Phaser.Math.Fuzzy.Equal(this.y, this.targetPos.y, this.fuzzyEpsilon)) {
                this.body.setVelocity(0, 0);
                this.x = this.targetPos.x;
                this.y = this.targetPos.y;

                this.isMoving = false;
            }
        }


        this.elapsedMs += delta;
        this.frameCount = Math.floor(this.elapsedMs / (1000 / this.framePerSecond)) % 2;

        // console.log(this.isMoving);
    }

    interactedWith(playerSprite) {

    }

    update(time, delta) {
        super.update(time, delta);
    }


}