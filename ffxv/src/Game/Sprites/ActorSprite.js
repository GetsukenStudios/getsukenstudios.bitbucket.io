import {
    isDefinedAndNotNull
} from "../../Base/Utils";
import CustomSprite from "./CustomSprite";

export default class ActorSprite extends CustomSprite {

    constructor(scene, key, x, y) {
        super(scene, key, x, y, 'sprites-main-chars', 0);

        this.imageKey = 'sprites-main-chars';
        this.startImageFrame = 0;

        let settings = this.scene.registry.get('gameSettings');
        this.tileSize = settings.tileSize;

        this.direction = 'down';
        this.isMoving = false;
        this.targetPos = {};
        this.speed = this.tileSize * 4;
        this.fuzzyEpsilon = 1.5;
        this.elapsedMs = 0;
        this.frameCount = 0;
        this.framePerSecond = 4;

        this.initialize();
    }

    initialize() {
        super.initialize();

    }

    getIsMoving() {
        return this.isMoving;
    }

    getFrameCount() {
        return this.frameCount;
    }

    setFrameCount(frameCount) {
        this.frameCount = frameCount;
    }

    getFaceDirection() {
        return this.direction;
    }

    setFaceDirection(direction) {
        this.direction = direction;

        this.changeFaceDirectionTexture();
    }

    getOppositeFaceDirection() {
        let oppDir = '';
        if (this.direction === 'down') {
            oppDir = 'up';
        } else if (this.direction === 'up') {
            oppDir = 'down';
        } else if (this.direction === 'left') {
            oppDir = 'right';
        } else if (this.direction === 'right') {
            oppDir = 'left';
        }
        return oppDir;
    }

    changeToOppositeFaceDirectionTexture() {
        this.setTexture(this.getOppositeFaceDirection());
    }

    changeFaceDirectionTexture() {
        if (this.direction === 'down') {
            this.setTexture(this.imageKey, this.startImageFrame + this.frameCount);
            this.setFlipX(false);
        } else if (this.direction === 'up') {
            this.setTexture(this.imageKey, this.startImageFrame + 2 + this.frameCount);
            this.setFlipX(false);
        } else if (this.direction === 'left') {
            this.setTexture(this.imageKey, this.startImageFrame + 4 + this.frameCount);
            this.setFlipX(false);
        } else if (this.direction === 'right') {
            this.setTexture(this.imageKey, this.startImageFrame + 4 + this.frameCount);
            this.setFlipX(true);
        }
    }

    moveDown() {
        if (!this.isMoving) {
            this.direction = 'down';
            this.isMoving = true;

            this.targetPos = {
                x: this.x,
                y: this.y + this.tileSize
            };
            this.body.setVelocityX(0);
            this.body.setVelocityY(this.speed);

            // this.anims.play("player-walk-down", true);
        }
    }

    moveUp() {
        if (!this.isMoving) {
            this.direction = 'up';
            this.isMoving = true;

            this.targetPos = {
                x: this.x,
                y: this.y - this.tileSize
            };
            this.body.setVelocityX(0);
            this.body.setVelocityY(-this.speed);

            // this.anims.play("player-walk-up", true);
        }
    }

    moveLeft() {
        if (!this.isMoving) {
            this.direction = 'left';
            this.isMoving = true;

            this.targetPos = {
                x: this.x - this.tileSize,
                y: this.y
            };
            this.body.setVelocityX(-this.speed);
            this.body.setVelocityY(0);

            // this.anims.play("player-walk-left", true);
        }
    }

    moveRight() {
        if (!this.isMoving) {
            this.direction = 'right';
            this.isMoving = true;

            this.targetPos = {
                x: this.x + this.tileSize,
                y: this.y
            };
            this.body.setVelocityX(this.speed);
            this.body.setVelocityY(0);

            // this.anims.play("player-walk-right", true);
        }
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);

    }

    update(time, delta) {
        super.update(time, delta);

    }


    // collision handlers
    onCollideWithWorldLayer(actor, object) {
        // collision with World layer or Objects layer

        // if (object.key) {
        //     // collided with an Object
        //     console.log('ActorSprite collided with ' + object.key + ' in ObjectLayer');
        // } else {
        //     // collided with a Tile
        //     console.log('ActorSprite collided with ' + 'WorldLayer');
        // }

        this.body.setVelocity(0, 0);
        this.targetPos.x = Phaser.Math.Snap.To(this.x, this.tileSize);
        this.targetPos.y = Phaser.Math.Snap.To(this.y, this.tileSize);

        this.isMoving = false;
    }

    onCollideWithBelowLayer(actor, object) {
        // if (object.key) {
        //     // collided with an Object
        //     console.log('ActorSprite collided with ' + object.key + ' in ObjectLayer');
        // } else {
        //     // collided with a Tile
        //     console.log('ActorSprite collided with ' + 'BelowLayer');
        // }


    }
}