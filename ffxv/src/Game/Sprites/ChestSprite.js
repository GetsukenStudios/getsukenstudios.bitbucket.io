import CustomSprite from "./CustomSprite";
import {
    appendWhitespaces,
    prependWhitespaces,
    prependSpecialWhitespaces
} from "../../Base/Utils";

export default class ChestSprite extends CustomSprite {

    constructor(scene, key, x, y) {
        super(scene, key, x, y, 'sprites-props-01', 0, true);

        this.imageKey = 'sprites-props-01';
        this.startImageKey = 0;

        this.isOpened = false;
        this.lootDrop = {};

        this.initialize();
    }

    initialize() {
        super.initialize();
    }

    changeTextureToOpenChest() {
        this.setTexture(this.imageKey, 1);
    }

    setLootDrop(lootDrop) {
        this.lootDrop = lootDrop;
    }

    openChest() {
        if (!this.isOpened) {
            let openingSucceeded = false;

            let itemDB = this.scene.registry.get('itemDatabase');
            let gearDB = this.scene.registry.get('gearDatabase');
            let treasureDB = this.scene.registry.get('treasureDatabase');
            let stringDB = this.scene.registry.get('stringDatabase');
            let dialogDB = this.scene.registry.get('dialogDatabase');
            let text1 = dialogDB.getDialogStringByKey('dlg-0000').getFullName();
            let text2 = dialogDB.getDialogStringByKey('dlg-0001').getFullName();

            // TODO: handle if more than one newItem in lootDrop (or should it be only one?)
            let firstItem = this.lootDrop[Object.keys(this.lootDrop)[0]];

            let newItem = itemDB.getItemObjectByKey(firstItem.key, stringDB);
            newItem.key = firstItem.key;
            newItem.quantity = firstItem.quantity;


            // add newItem to inventory if there's space
            let inventory;
            if (newItem.type === 'consumable' || newItem.type === 'spell') {
                inventory = this.scene.registry.get('inventoryItems');
                openingSucceeded = inventory.addNewItemToInventory(newItem, itemDB);
            } else if (newItem.type === 'weapon') {
                inventory = this.scene.registry.get('inventoryWeapons');
                openingSucceeded = inventory.addNewItemToInventory(newItem, gearDB);
            } else if (newItem.type === 'accessory') {
                inventory = this.scene.registry.get('inventoryAccessories');
                openingSucceeded = inventory.addNewItemToInventory(newItem, gearDB);
            } else if (newItem.type === 'treasure') {
                inventory = this.scene.registry.get('inventoryTreasures');
                openingSucceeded = inventory.addNewItemToInventory(newItem, treasureDB);
            }

            this.triedOpeningChest(openingSucceeded);

            let gameSettings = this.scene.registry.get('gameSettings');
            let chestMessage1 = '' + text1 + ' ' + '\n';
            let chestMessage2 = '' + appendWhitespaces(newItem.getTypeToChar(gameSettings) + newItem.getFullName(), 6) +
                prependSpecialWhitespaces(newItem.quantity.toString(), 2);
            let chestMessage3;

            let dialogMenuData = {
                config: {

                },
                lines: [{
                        options: {

                        },
                        text: chestMessage1
                    },
                    {
                        options: {
                            indent: 2,
                        },
                        text: chestMessage2
                    },
                    {
                        options: {

                        },
                        text: ''
                    },
                ]
            };

            if (!openingSucceeded) {
                chestMessage3 = text2;
                dialogMenuData.lines[1].text = chestMessage3;
            }

            this.scene.game.events.emit('Menu', 'OpenDialogMenu', dialogMenuData);

        }
    }

    triedOpeningChest(succeeded) {
        if (succeeded) {
            this.isOpened = true;
            this.changeTextureToOpenChest();
        } else {
            this.isOpened = false;
        }

        // update persistent scene data
        let persistentData = this.scene.registry.get('scenePersistentData');
        persistentData.getDataByKey(this.scene.key).chests[this.key] = {
            isOpened: this.isOpened,
        };
    }

    playJingle() {
        let volume = this.scene.registry.get('gameSettings').mainVolume;
        this.scene.getCurrentBackgroundMusic().setVolume(0);
        let sfx = this.scene.sound.add('sfx-get-item');
        sfx.play();

        this.openChest();

        sfx.once('complete', () => {
            this.scene.getCurrentBackgroundMusic().setVolume(volume);
            sfx.destroy();
        });
    }

    interactedWith(playerSprite) {
        if (!this.isOpened) {
            this.playJingle();
        }
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);
    }

    update(time, delta) {
        super.update(time, delta);
    }
}