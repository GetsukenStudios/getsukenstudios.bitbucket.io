import CustomSprite from "./CustomSprite";
import {
    isDefinedAndNotNull
} from "../../Base/Utils";
import NpcSprite from "./NpcSprite";

export default class NpcWomanSprite extends NpcSprite {

    constructor(scene, key, x, y, subType, scriptId, styleId) {
        super(scene, key, x, y, subType, scriptId, styleId);
    }

    initStateMachine() {
        super.initStateMachine();

        this.imageKey = 'sprites-side-chars';
    }

    setTextureBySubTypeAndName() {
        let frame = this.startImageFrame;

        switch (this.styleId) {
            case 0:
                frame = 0;
                break;
        }

        this.startImageFrame = frame;

        this.setTexture(this.imageKey, this.startImageFrame);
    }

    interactedWith(playerSprite) {
        let dialogDB = this.scene.registry.get('dialogDatabase');
        let sceneData = {
            config: {

            },
            lines: []
        };
        let callback = function () {

        };

        if (this.currentState === 'st0-000') {
            let key2 = 'dlg-0004';
            let dialogStringData2 = dialogDB.getDataByKey(key2);
            sceneData.lines.push(dialogStringData2);
            sceneData.lines[0].options = {
                // indent: 4,
            };

            callback = function () {

            };

            this.setFaceDirection(playerSprite.getOppositeFaceDirection());
            this.scene.game.events.emit('Menu', 'OpenDialogMenu', sceneData, callback);
        }

    }

    update(time, delta) {
        super.update(time, delta);
    }


}