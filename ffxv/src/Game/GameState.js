import StateTransition from '../State/StateTransition';
import StateNode from '../State/StateNode';
import GameStateMachine from '../State/GameStateMachine';
import {
    isDefinedAndNotNull,
    isUndefinedOrNull
} from '../Base/Utils';
import PreloaderScene from '../Scenes/PreloaderScene';
import TitleScreenScene from '../Scenes/TitleScreenScene';
import TestScene2 from '../Scenes/TestScene2';
import DisclaimerScene from '../Scenes/DisclaimerScene';
import TestCave from '../Scenes/TestCave';
import LoadSavedGameScene from '../Scenes/LoadSavedGameScene';
import LoadNewGameScene from '../Scenes/LoadNewGameScene';

export default class GameState {

    constructor(game) {
        this.game = game;
        this.events = game.events;

        this.menuState = null;

        this.stateMachine = null;

        this.currentSceneKey = null;
        this.oldSceneKey = null;

        this.initializeScenes();
    }

    getCurrentState() {
        return this.stateMachine.getCurrentState();
    }

    getCurrentStateName() {
        if (isDefinedAndNotNull(this.stateMachine)) {
            return this.stateMachine.getCurrentStateName();
        }
    }

    getCurrentSceneKey() {
        return this.currentSceneKey;
    }

    setCurrentSceneKey(key) {
        this.currentSceneKey = key;
    }

    getCurrentScene() {
        return this.game.scene.getScene(this.getCurrentSceneKey());
    }

    getOldSceneKey() {
        return this.oldSceneKey;
    }

    setOldSceneKey(key) {
        if (!isDefinedAndNotNull(key)) {
            key = null;
        }
        this.oldSceneKey = key;
    }

    getOldScene() {
        return this.game.scene.getScene(this.getOldSceneKey());
    }

    updateCurrentAndOldSceneKey(key) {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.setOldSceneKey(this.getCurrentSceneKey());
        }
        this.setCurrentSceneKey(key);
    }

    updateParentScene() {
        let curr = this.getCurrentScene();
        let old = this.getOldScene();
        if (isDefinedAndNotNull(curr) && isDefinedAndNotNull(old)) {
            curr.setParentScene(old);
        }
    }

    startScene(key, data) {
        this.game.scene.start(key, data);
    }

    launchScene(key, data) {
        this.game.scene.launch(key, data);
    }

    runScene(key, data) {
        this.game.scene.run(key, data);
    }

    runSceneAndUpdateKeys(key, data) {
        this.stopCurrentScene(data);
        this.updateCurrentAndOldSceneKey(key);
        this.updateParentScene();
        this.runScene(key, data);
    }

    resumeScene(key, data) {
        this.game.scene.resume(key, data);
    }

    wakeScene(key, data) {
        this.game.scene.wake(key, data);
    }

    stopScene(key, data) {
        this.game.scene.stop(key, data);
    }

    pauseScene(key, data) {
        this.game.scene.pause(key, data);
    }

    sleepScene(key, data) {
        this.game.scene.sleep(key, data);
    }

    stopCurrentScene(data) {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.game.scene.stop(this.getCurrentSceneKey(), data);
        }
    }

    pauseCurrentScene(data) {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.game.scene.pause(this.getCurrentSceneKey(), data);
        }
    }

    sleepCurrentScene(data) {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.game.scene.sleep(this.getCurrentSceneKey(), data);
        }
    }

    resumeCurrentScene(data) {
        if (isDefinedAndNotNull(this.getCurrentSceneKey())) {
            this.game.scene.resume(this.getCurrentSceneKey(), data);
        }
    }

    start() {
        this.stateMachine.start();
    }

    reset() {
        this.stateMachine.reset();
        this.currentSceneKey = null;
        this.oldSceneKey = null;
    }

    restart() {
        this.reset();
        this.start();
    }

    initializeScenes() {
        this.game.scene.add('PreloaderScene', PreloaderScene);
        this.game.scene.add('DisclaimerScene', DisclaimerScene);
        this.game.scene.add('TitleScreenScene', TitleScreenScene);
        this.game.scene.add('LoadSavedGameScene', LoadSavedGameScene);
        this.game.scene.add('LoadNewGameScene', LoadNewGameScene);
        this.game.scene.add('TestScene2', TestScene2);
        this.game.scene.add('TestCave', TestCave);
    }

    initialize() {
        // reference to other state managers
        this.menuState = this.game.menuState;

        // initialize StateMachine
        let stateMachine = new GameStateMachine('GameState', this.events);

        let start = new StateNode(stateMachine, 'Start', this.startState.bind(this));
        let preloading = new StateNode(stateMachine, 'Preloading', this.preloading.bind(this));
        let disclaimer = new StateNode(stateMachine, 'Disclaimer', this.disclaimer.bind(this));
        let titleScreen = new StateNode(stateMachine, 'Title', this.titleScreen.bind(this));
        let loadNewGame = new StateNode(stateMachine, 'LoadNewGame');
        let loadGame = new StateNode(stateMachine, 'LoadGame');
        let credits = new StateNode(stateMachine, 'Credits', this.showCredits.bind(this));
        let test = new StateNode(stateMachine, 'LoadTest');
        let play = new StateNode(stateMachine, 'Play', this.playGame.bind(this));
        let menu = new StateNode(stateMachine, 'Menu', this.enterMenuState.bind(this), null, this.leaveMenuState.bind(this));

        new StateTransition('StartPreloading', start, preloading);
        new StateTransition('ToDisclaimer', preloading, disclaimer);
        new StateTransition('DisclaimerDone', disclaimer, titleScreen);
        new StateTransition('ToTitleScreen', preloading, titleScreen);
        new StateTransition('SkipToGame', preloading, play);
        new StateTransition('LoadTest', preloading, test, this.loadTest.bind(this));
        new StateTransition('StartTestGame', test, play);
        new StateTransition('LoadSavedGame', titleScreen, loadGame, this.loadSavedGames.bind(this));
        new StateTransition('StartSavedGame', loadGame, play, this.startSavedGame.bind(this));
        new StateTransition('LoadNewGame', titleScreen, loadNewGame, this.loadNewGame.bind(this));
        new StateTransition('LoadNewGame', test, loadNewGame, this.loadNewGame.bind(this));
        new StateTransition('StartNewGame', loadNewGame, play, this.startNewGame.bind(this));
        new StateTransition('ExitToTitleScreen', loadGame, titleScreen);
        new StateTransition('ExitToTitleScreen', play, titleScreen);
        new StateTransition('GameFinished', play, credits);
        new StateTransition('EnterMenuFromPlay', play, menu, this.enterMenuFromPlay.bind(this));
        new StateTransition('LeaveMenuToPlay', menu, play, this.leaveMenuToPlay.bind(this));

        new StateTransition('RunNewScene', play, play, this.runNewScene.bind(this));

        this.stateMachine = stateMachine;
    }

    startState() {
        // this.setCurrentSceneKey('PreloaderScene');
    }

    preloading() {
        let key = 'PreloaderScene';

        this.runSceneAndUpdateKeys(key);
    }

    disclaimer() {
        let key = 'DisclaimerScene';

        this.runSceneAndUpdateKeys(key);
    }

    titleScreen() {
        let key = 'TitleScreenScene';

        this.runSceneAndUpdateKeys(key);
    }

    loadNewGame(data) {
        let key = 'LoadNewGameScene';

        this.runSceneAndUpdateKeys(key, data);
    }

    loadSavedGames(data) {
        let key = 'LoadSavedGameScene';

        this.runSceneAndUpdateKeys(key, data);
    }

    startNewGame(data) {
        this.runSceneAndUpdateKeys(data.key, data);
    }

    startSavedGame(data) {
        this.runSceneAndUpdateKeys(data.key, data);
    }

    playGame() {
        // if(this.game.scene.getScene(this.getCurrentSceneKey())){
        //     this.resumeScene(this.getCurrentSceneKey());
        // }

        // this.runScene(this.getCurrentSceneKey());
    }

    showCredits() {

    }

    loadTest(data) {
        this.game.events.emit('Game', 'LoadNewGame', data);
    }

    runNewScene(data) {
        this.runSceneAndUpdateKeys(data.key, data);
    }

    enterMenuState(data) {
        this.pauseCurrentScene(data);

        // this.menuState.restart();
    }

    leaveMenuState() {
        this.menuState.reset();

        this.resumeCurrentScene();
    }

    enterMenuFromPlay() {

    }

    leaveMenuToPlay() {

    }

}