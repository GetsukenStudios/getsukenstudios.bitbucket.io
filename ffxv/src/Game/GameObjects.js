import TestScene2 from "../Scenes/TestScene2";
import {
    isDefinedAndNotNull,
    isUndefinedOrNull,
    clamp,
    filterObject,
    randomNumber
} from "../Base/Utils";

export class GameObject {
    constructor(key, fullName, shortName) {
        this.key = key || 'default';
        this.fullName = fullName || ' ';
        this.shortName = shortName || fullName || ' ';

        this.skip = false; //skip this entry in a list when cursor is over it
    }

    getKey() {
        return this.key;
    }

    setKey(key) {
        this.key = key;
    }

    getFullName() {
        return this.fullName;
    }

    setFullName(fullName) {
        this.fullName = fullName;
    }

    getShortName() {
        return this.shortName;
    }

    setShortName(shortName) {
        this.shortName = shortName;
    }

    toUpperCaseName() {
        this.shortName = this.shortName.toUpperCase();
        this.fullName = this.fullName.toUpperCase();
    }

    getSkip() {
        return this.skip;
    }

    setSkip(skip) {
        this.skip = skip;
    }
}

export class BattleActor extends GameObject {
    constructor(config) {
        super(config.key, config.fullName, config.shortName);

        // wait, choose, defend, dead
        this.battleState = 'wait';
        this.isDead = false;
        this.statusAilments = []; // [{statusKey: 'status-poison-01', remainingTurns: 3}]
    }

    getIsDead() {
        return this.isDead;
    }

    setIsDead(bool) {
        this.isDead = bool;
        this.battleState = bool ? 'dead' : 'wait';
        this.hp = bool ? 0 : this.hp;
        this.tp = bool ? 0 : this.tp;
    }

    getBattleState() {
        return this.battleState;
    }

    setBattleState(state) {
        this.battleState = state;
        if (state === 'dead') this.setIsDead(true);
    }

    checkAndUpdateIfDead() {
        if (this.hp <= 0) {
            this.setIsDead(true);

            return true;
        } else {
            this.setIsDead(false);

            return false;
        }
    }

    receiveDamageHandler(damage) {}

    getStatusAilments() {
        return this.statusAilments;
    }

    setStatusAilments(ailments) {
        this.statusAilments = ailments;
    }

    addStatusAilment(ailment) {
        this.statusAilments.push(ailment);
    }

    filterOnlyStatusAilmentsWithRemainingTurnCount() {
        this.statusAilments = this.statusAilments.filter(status => status.remainingTurns >= 0);
    }

    inflictStatusAilment(statusKey) {
        if (this instanceof Character && this.immunities.includes(statusKey) || this instanceof Monster && this.immunities.includes(statusKey)) return 'isImmune';
        let existingAilment = this.statusAilments.find(status => status.statusKey === statusKey);
        let maxTurns = 3;
        if (isDefinedAndNotNull(existingAilment)) {
            existingAilment.remainingTurns = maxTurns;
        } else {
            this.addStatusAilment({
                statusKey: statusKey,
                remainingTurns: maxTurns
            });
        }

        return statusKey;
    }

    cureStatusAilment(statusKey) {
        let result = null;
        this.statusAilments.forEach(status => {
            if (status.statusKey === statusKey) {
                status.remainingTurns = -1;
                result = status;
            }
        });
        this.filterOnlyStatusAilmentsWithRemainingTurnCount();

        return result;
    }

    cureAllStatusAilments() {
        let results = [];
        this.statusAilments.forEach(status => {
            status.remainingTurns = -1;
            results.push(status.statusKey);
        });
        this.filterOnlyStatusAilmentsWithRemainingTurnCount();

        results = results.length <= 0 ? null : results;
        return results;
    }

    //TODO: call this method every turn for party and enemies
    decrementStatusAilmentsTurnCounts() {
        this.statusAilments.forEach(status => {
            status.remainingTurns = status.remainingTurns - 1;
        });
        this.filterOnlyStatusAilmentsWithRemainingTurnCount();
    }

    //TODO: apply effect of status ailment every turn for party and enemies
    applyCurrentStatusAilmentsForThisTurn() {
        let appliedStatusAilmentResults = [];

        this.statusAilments.forEach(status => {
            appliedStatusAilmentResults.push(this.getStatusAilmentEffectResult(status.statusKey));
        });

        return appliedStatusAilmentResults;
    }

    getStatusAilmentEffectResult(statusKey) {
        switch (statusKey) {
            case 'status-poison-01':
                let damage = Math.round(this.maxHp.total * 0.05);
                damage = Math.max(1, damage);
                this.hp = Math.max(0, this.hp - damage);
                let killedActor = this.checkAndUpdateIfDead();

                return {
                    statusKey: statusKey,
                        type: 'debuff',
                        damage: damage,
                        killedActor: killedActor,
                };
            case 'status-sleep-01':
                return {
                    statusKey: statusKey,
                        type: 'debuff',
                };
            case 'status-deprotect-01':
                return {
                    statusKey: statusKey,
                        type: 'debuff',
                        value: 0.5,
                };
            case 'status-protect-01':
                return {
                    statusKey: statusKey,
                        type: 'buff',
                        value: 2,
                };
            default:
                break;
        }
    }

    applyStatusAilmentsForDefenseCalculation(target) {
        let factor = 1.0;
        if (target.isAfflictedBy('status-deprotect-01')) {
            factor = this.getStatusAilmentEffectResult('status-deprotect-01').value;
        } else if (target.isAfflictedBy('status-protect-01')) {
            factor = this.getStatusAilmentEffectResult('status-protect-01').value;
        }
        factor = factor ? factor : 1.0;

        return factor;
    }

    isAfflictedBy(statusKey) {
        return isDefinedAndNotNull(this.statusAilments.find(status => status.statusKey === statusKey));
    }

    applyAttackDamageTo(target, weapon, effect, gameSettings) {}
    applyMagicDamageTo(target, effect, gameSettings) {}
    applyTechniqueTo(target, technique, gameSettings) {}

    applySpecialSpell(target, effect) {
        //TODO:  for special spells switch case on "spell" prop of effect object e.g. 'spell-libra-01'
        let result = null;
        switch (effect.spell) {
            case 'spell-libra-01':
                //TODO: filter out affinities != 1.0 to pass in the result object for BattleMenu dialog creation
                // let weaponAffinities = Object.fromEntries(Object.entries(target.weaponAffinities).filter());
                let weaponAffinities = filterObject(target.weaponAffinities, (value) => value !== 1);
                // let elementalAffinities = Object.fromEntries(Object.entries(target.elementalAffinities).filter(([key, value]) => value !== 1));
                let elementalAffinities = filterObject(target.elementalAffinities, (value) => value !== 1);
                let immunities = target.immunities;
                let primaryDrop = target.primaryDrop;
                let elementDrop = target.elementDrop;
                result = {
                    spellKey: effect.spell,
                    weaponAffinities: weaponAffinities,
                    elementalAffinities: elementalAffinities,
                    immunities: immunities,
                    primaryDrop: primaryDrop,
                    elementDrop: elementDrop,
                };

                return result;
        }
    }
}

export class Character extends BattleActor {

    constructor(config) {
        super(config);

        this.type = config.type || 'player'; // Player, NPC

        // stats
        this.level = config.stats.level || 1;
        this.exp = config.stats.exp || 0;
        this.tp = config.stats.tp || 0.75; //TODO: DEBUG: set to 0 later
        this.hp = config.stats.hp || 0;
        this.maxHp = config.stats.maxHp || {
            base: 999,
            mods: 0,
            total: 999
        };

        this.checkAndUpdateIfDead();

        this.attack = config.stats.attack || {
            base: 99,
            mods: 0,
            total: 99,
        }; // physical attack
        this.defense = config.stats.defense || {
            base: 99,
            mods: 0,
            total: 99,
        }; // physical defense 
        this.magic = config.stats.magic || {
            base: 99,
            mods: 0,
            total: 99,
        }; // magic attack 
        this.spirit = config.stats.spirit || {
            base: 99,
            mods: 0,
            total: 99,
        }; // magic defense

        this.fireResistance = config.stats.fireResistance || {
            base: 0,
            mods: 0,
            total: 0,
        };
        this.iceResistance = config.stats.iceResistance || {
            base: 0,
            mods: 0,
            total: 0,
        };
        this.lightningResistance = config.stats.lightningResistance || {
            base: 0,
            mods: 0,
            total: 0,
        };
        this.darkResistance = config.stats.darkResistance || {
            base: 0,
            mods: 0,
            total: 0,
        };
        // this.shotResistance = config.stats.shotResistance || { base: 0, mods: 0, total: 0, };    // not used in this game

        this.immunities = config.stats.immunities || [];

        // equipment
        this.equippedWeapons = {
            weapon0: config.equippedWeapons.weapon0 || 'none',
            weapon1: config.equippedWeapons.weapon1 || 'none',
            // Noctis has two more weapons than the others
            weapon2: config.equippedWeapons.weapon2 || 'none',
            weapon3: config.equippedWeapons.weapon3 || 'none',
        };
        this.equippedAccs = {
            accessory0: config.equippedAccs.accessory0 || 'none',
            accessory1: config.equippedAccs.accessory1 || 'none',
        };

        // techniques
        this.unlockedTechniques = config.unlockedTechniques;
    }

    createConfig() {
        return {
            // key: this.key,
            // fullName: this.fullName,
            // shortName: this.shortName,

            type: this.type,

            stats: {
                level: this.level,
                exp: this.exp,
                tp: this.tp,
                hp: this.hp,
                maxHp: this.maxHp,
                attack: this.attack,
                defense: this.defense,
                magic: this.magic,
                spirit: this.spirit,
                fireResistance: this.fireResistance,
                iceResistance: this.iceResistance,
                lightningResistance: this.lightningResistance,
                darkResistance: this.darkResistance,
                immunities: this.immunities,
            },
            unlockedTechniques: this.unlockedTechniques,
            equippedWeapons: this.equippedWeapons,
            equippedAccs: this.equippedAccs,
        };
    }

    getFullName() {
        return this.fullName.toUpperCase();
    }

    getShortName() {
        return this.shortName.toUpperCase();
    }

    getTp() {
        return this.tp;
    }

    setTp(tp) {
        this.tp = tp;
    }

    receiveDamageHandler(damage) {}

    increaseTpPerAction() {
        let increasePerAction = 0.5;
        this.tp = Math.min(3, this.tp + increasePerAction);
    }

    useDefense() {
        this.setBattleState('defend');
        this.increaseTpPerAction();
    }

    convertCharResistanceStatToPercent(elementalResistanceStat) {
        return (1.0 - (elementalResistanceStat / 255.0));
    }

    getElementalResistancesConvertedToAffinities() {
        let fire = this.convertCharResistanceStatToPercent(this.fireResistance.total);
        let ice = this.convertCharResistanceStatToPercent(this.iceResistance.total);
        let lightning = this.convertCharResistanceStatToPercent(this.lightningResistance.total);
        let dark = this.convertCharResistanceStatToPercent(this.darkResistance.total);
        return {
            fire: fire,
            ice: ice,
            lightning: lightning,
            dark: dark,
        };
    }

    getElementalAffinityOf(element) {
        let affinities = this.getElementalResistancesConvertedToAffinities();
        return affinities[element];
    }

    applyAttackDamageTo(target, weaponParam, effectParam, gameSettings) {
        /* 
        Damage = A × B × C × D × E

        These five factors (A to E) are detailed in the next five sections:

        A = Base Damage
        B = Target Defense
        C = Affinity Modifier
        D = Conditional Modifier(s)
        E = Random Modifier
        
        ***
        C = 
            This is a simple check against the affinity of the target to the damage type being dealt. These are the resistances or weaknesses to damage types that we reveal in the Bestiary for each monster:

            Check for nullification of the damage type. If true, then no damage is dealt. This corresponds to immunity ("IMM") in the Bestiary.

            Check the target's resistance to the damage type. This corresponds to the percentages in the Bestiary. If more than one type applies (e.g., swords and Fire with the Flame Tongue), multiply the resistances together to obtain a single percentage value.

            Limit this result to a value between 0 and 999.

            If the target absorbs this damage type ("ABS" in the Bestiary), convert the result into HP restoration.

            The value of C is the result of this procedure.

        D = Critical-hit chance of weapon
        E = random

        */

        let resultObject = {};
        let resultValue = 0;
        let maxDamage = gameSettings.maxDamage;
        let randMin = gameSettings.randomModifier.min;
        let randMax = gameSettings.randomModifier.max;
        let weaponInflictStatusAilmentProbability = gameSettings.weaponInflictStatusAilmentProbability;
        let criticalHitProbability = gameSettings.criticalHitProbability;
        let weapon = isDefinedAndNotNull(weaponParam) ? weaponParam : {
            power: 0,
            critical: 1.0
        }; //special case when weaponParam is null => attack calculated based on total char "attack" stat
        let effect = effectParam;
        let effectPowerScale = 1.0;
        let charLevel = this.level;
        let charStrength = this.attack.total - weapon.power;
        let weaponPower = weapon.power;
        let weaponCritical = weapon.critical;
        let weaponSubType = weapon.subType;
        weaponSubType = (isDefinedAndNotNull(effect) && effect.type === 'warp') ? 'firearm' : weaponSubType; //subtype 'warp' treated like 'firearm' (e.g. good against airborne enemies) 
        effectPowerScale = (isDefinedAndNotNull(effect) && isDefinedAndNotNull(effect.power)) ? effect.power : effectPowerScale;
        let weaponElement = weapon.element;
        let targetDefense = target.defense.total;
        let targetWeaponAffinities = target.weaponAffinities;
        let targetElementalAffinities = target.elementalAffinities;

        // Apply status ailments like buffs/debuffs
        let factor = this.applyStatusAilmentsForDefenseCalculation(target);
        targetDefense = targetDefense * factor;

        // A = Base Damage:     [Strength + Weapon Power + (Level × 3)] × (Attack Damage Modifier × 2)
        let baseDamageTotal = charStrength + weaponPower + (charLevel * 3);

        // B = Target Defense:  1 ÷ [1 + (Vitality ÷ 100)]  
        let targetDefenseTotal = 1.0 / (1.0 + (targetDefense / 10.0));

        // C = Affinity Modifier
        let affinityModifierTotal = 1.0;
        let targetWeaponAffinityModifier = 1.0;
        let targetElementalAffinityModifier = 1.0;
        if (isDefinedAndNotNull(targetWeaponAffinities[weaponSubType])) {
            targetWeaponAffinityModifier = targetWeaponAffinities[weaponSubType];
        }
        if (isDefinedAndNotNull(targetElementalAffinities[weaponElement])) {
            targetElementalAffinityModifier = targetElementalAffinities[weaponElement];
        }
        affinityModifierTotal = targetWeaponAffinityModifier * targetElementalAffinityModifier;

        // D = Conditional Modifier
        let criticalModifierTotal = 1.0;
        let randCrit = Math.random();
        if (randCrit <= (criticalHitProbability * weaponCritical)) criticalModifierTotal = 2.0;

        // E = Random Modifier
        let randomModifierTotal = Math.random() * (randMax - randMin) + randMin;


        resultValue = baseDamageTotal * targetDefenseTotal * effectPowerScale * affinityModifierTotal * criticalModifierTotal * randomModifierTotal;
        resultValue = Math.round(resultValue);
        resultValue = clamp(resultValue, -maxDamage, maxDamage);
        resultObject.resultValue = resultValue;

        target.hp = target.hp - resultValue;
        target.hp = clamp(target.hp, 0, target.maxHp.total);

        target.checkAndUpdateIfDead();

        //TODO: for now only one statusAilment can be inflicted
        let randInflict = Math.random();
        if (isDefinedAndNotNull(weapon.statModifiers) && isDefinedAndNotNull(weapon.statModifiers.statusAilments)) {
            let statusToInflict = weapon.statModifiers.statusAilments[0];
            if (randInflict <= weaponInflictStatusAilmentProbability) {
                target.inflictStatusAilment(statusToInflict);
                resultObject.inflictedStatusAilments = [statusToInflict];
            }
        }

        this.increaseTpPerAction();
        return resultObject;
    }

    applyMagicDamageTo(target, effect, gameSettings) {
        let result = 0;
        let maxDamage = gameSettings.maxDamage;
        let randMin = gameSettings.randomModifier.min;
        let randMax = gameSettings.randomModifier.max;
        let charLevel = this.level;
        let charMagic = this.magic.total;
        let spellPower = effect.power;
        let spellElement = effect.element;
        let targetSpirit = target.spirit.total;
        let targetElementalAffinities = target.elementalAffinities || {};

        //TODO: try out different calculations?
        // A = Base Damage:    (Magic + 100) × (Spell Power + 20) × (Level + 20) × (1 ÷ 200) 
        let baseDamageTotal = (charMagic + 100) * (spellPower * 20) * (charLevel + 20) * (1.0 / 200.0);

        // B = Target Defense:  1 ÷ [1 + (Spirit ÷ 100)]
        let targetDefenseTotal = 1.0 / (1.0 + (targetSpirit / 10.0));

        // C = Affinity Modifier
        let affinityModifierTotal = 1.0;
        let targetElementalAffinityModifier = 1.0;
        if (isDefinedAndNotNull(targetElementalAffinities[spellElement])) {
            targetElementalAffinityModifier = targetElementalAffinities[spellElement];
        }
        affinityModifierTotal = targetElementalAffinityModifier;

        // D = Conditional Modifier

        // E = Random Modifier
        let randomModifierTotal = Math.random() * (randMax - randMin) + randMin;


        result = baseDamageTotal * targetDefenseTotal * affinityModifierTotal * randomModifierTotal;
        if (effect.type === 'spell' && effect.subType === 'heal') result = baseDamageTotal * -1;
        result = Math.round(result);
        result = clamp(result, -maxDamage, maxDamage);

        target.hp = target.hp - result;
        target.hp = clamp(target.hp, 0, target.maxHp.total);

        target.checkAndUpdateIfDead();

        this.increaseTpPerAction();
        return result;
    }

    applyTechniqueTo(targets, technique, effect, gameSettings) {
        //TODO: implement

        let results = [];

        if ((this.tp <= 0) || (this.tp < technique.cost)) return [null];
        this.tp = Math.max(0, this.tp - technique.cost);

        for (let i = 0; i < targets.length; i++) {
            let target = targets[i];
            let result;

            if (effect.type === 'attack') {
                result = this.applyAttackDamageTo(target, null, null, gameSettings);
            } else if (effect.type === 'warp') {
                result = this.applyAttackDamageTo(target, null, effect, gameSettings);
            } else if (effect.type === 'spell') {
                switch (effect.subType) {
                    case 'offensive':
                        result = this.applyMagicDamageTo(target, effect, gameSettings);

                        break;
                    case 'heal':
                        result = this.applyMagicDamageTo(target, effect, gameSettings);

                        break;
                    case 'cure':
                        result = target.cureStatusAilment(effect.statusAilment);

                        break;
                    case 'inflict':
                        result = target.inflictStatusAilment(effect.statusAilment);

                        break;
                    case 'special':
                        result = this.applySpecialSpell(target, effect);

                        break;
                }
            }
            results.push(result);
        }

        return results;
    }
}

export class Monster extends BattleActor {
    constructor(config) {
        super(config);

        this.level = config.stats.level || 1;
        this.maxHp = config.stats.maxHp || {
            total: 60
        };
        this.hp = config.stats.hp || this.maxHp.total;

        this.libra = config.stats.libra || 0.75; // chance to analyze enemy per Libra spell - NOT USED

        this.attack = config.stats.attack || {
            total: 8
        };
        this.defense = config.stats.defense || {
            total: 3
        };
        this.spirit = config.stats.spirit || {
            total: 2
        };

        this.weaponAffinities = config.stats.weaponAffinities || {};
        this.elementalAffinities = config.stats.elementalAffinities || {};
        this.immunities = config.stats.immunities || {};

        this.expDrop = config.stats.expDrop || 20;
        this.primaryDrop = config.primaryDrop || {};
        this.secondaryDrop = config.secondaryDrop || {};
        this.appendage = config.appendage || {};
        this.elementDrop = config.elementDrop || {};

        this.actions = config.actions || [];
    }

    getFullName() {
        return this.fullName.toUpperCase();
    }

    getShortName() {
        return this.shortName.toUpperCase();
    }

    getExpDrop() {
        return this.expDrop;
    }

    getPrimaryDrop() {
        return this.primaryDrop;
    }

    getSecondaryDrop() {
        return this.secondaryDrop;
    }

    getElementDrop() {
        return this.elementDrop;
    }

    applyAttackDamageTo(target, weapon, effect, gameSettings) {
        let resultObject = {};
        let resultValue = 0;
        let maxDamage = gameSettings.maxDamage;
        let randMin = gameSettings.randomModifier.min;
        let randMax = gameSettings.randomModifier.max;
        let monsterLevel = this.level;
        let monsterStrength = this.attack.total;
        let targetDefense = target.defense.total;
        let enemyWeapon = weapon || null;

        // Apply status ailments like buffs/debuffs
        let factor = this.applyStatusAilmentsForDefenseCalculation(target);
        targetDefense = targetDefense * factor;

        // A = Base Damage
        let baseDamageTotal = monsterStrength + (monsterLevel * 3);

        // B = Target Defense
        let targetDefenseTotal = 1.0 / (1.0 + (targetDefense / 10.0));

        // C = Affinity Modifier
        //TODO: what about elemental affinity when attacking chars? or do enemies not have elemental attacks
        let affinityModifierTotal = 1.0;

        // D = Conditional Modifier
        let criticalModifierTotal = 1.0;

        // E = Random Modifier
        let randomModifierTotal = Math.random() * (randMax - randMin) + randMin;


        resultValue = baseDamageTotal * targetDefenseTotal * affinityModifierTotal * criticalModifierTotal *
            randomModifierTotal;
        resultValue = Math.round(resultValue);
        resultValue = clamp(resultValue, -maxDamage, maxDamage);
        resultObject.resultValue = resultValue;

        target.hp = target.hp - resultValue;
        target.hp = clamp(target.hp, 0, target.maxHp.total);

        target.checkAndUpdateIfDead();

        return resultObject;
    }

    applyMagicDamageTo(target, effect, gameSettings) {
        let result = 0;
        let maxDamage = gameSettings.maxDamage;
        let randMin = gameSettings.randomModifier.min;
        let randMax = gameSettings.randomModifier.max;
        let monsterLevel = this.level;
        let monsterStrength = this.attack.total;
        let spellPower = effect.power;
        let spellElement = effect.element;
        let targetSpirit = target.spirit.total;
        let targetElementalAffinities = target.getElementalResistancesConvertedToAffinities();

        // A = Base Damage
        let baseDamageTotal = (monsterStrength + (monsterLevel * 3)) * (spellPower * 10);

        // B = Target Defense
        let targetDefenseTotal = 1.0 / (1.0 + (targetSpirit / 10.0));

        // C = Affinity Modifier
        //TODO: consider chars' elemental resistances
        let affinityModifierTotal = 1.0;
        let targetElementalAffinityModifier = 1.0;
        if (isDefinedAndNotNull(targetElementalAffinities[spellElement])) {
            targetElementalAffinityModifier = targetElementalAffinities[spellElement];
        }
        affinityModifierTotal = targetElementalAffinityModifier;

        // D = Conditional Modifier

        // E = Random Modifier
        let randomModifierTotal = Math.random() * (randMax - randMin) + randMin;


        result = baseDamageTotal * targetDefenseTotal * affinityModifierTotal * randomModifierTotal;
        if (effect.type === 'spell' && effect.subType === 'heal') result = baseDamageTotal * -1;
        result = Math.round(result);
        result = clamp(result, -maxDamage, maxDamage);

        target.hp = target.hp - result;
        target.hp = clamp(target.hp, 0, target.maxHp.total);

        target.checkAndUpdateIfDead();

        return result;
    }

    chooseNewTarget(playerParty) {
        let players = playerParty.filter(char => !char.getIsDead());
        let countPlayers = players.length;
        let baseProb = 1.0 / players.length;
        let probChooseCharWithLowestHp = baseProb * 0.25;
        let mappedPlayerHp = players.map((p) => p.hp);
        let indexCharWithLowestHp = mappedPlayerHp.indexOf(Math.min(...mappedPlayerHp));

        let totalProbPerChar = Array.from({
            length: countPlayers
        }, (v, i) => {
            let res = baseProb;
            if (i === indexCharWithLowestHp) res += probChooseCharWithLowestHp;
            return {
                prob: res,
            };
        });
        totalProbPerChar.forEach((el, i) => {
            let prevStart = (i > 0) ? totalProbPerChar[i - 1].start : 0;
            let prevProb = (i > 0) ? totalProbPerChar[i - 1].prob : 0;
            totalProbPerChar[i].start = (i > 0) ? (prevStart + prevProb) : 0;
            totalProbPerChar[i].end = (totalProbPerChar[i].start + totalProbPerChar[i].prob);
        });

        let totalStart = totalProbPerChar[0].start;
        let totalEnd = totalProbPerChar[totalProbPerChar.length - 1].end;
        let rand = randomNumber(totalStart, totalEnd);
        let idx = 0;
        while (1) {
            idx = totalProbPerChar.findIndex((el) => {
                return ((el.start <= rand) && (rand < el.end));
            });
            if (idx > -1) {
                break;
            } else {
                rand = randomNumber(totalStart, totalEnd);
            }
        }

        return players[idx];
    }

    chooseNewAction() {
        let chosenAction = {
            effect: 'attack'
        };
        let possibleActions = this.actions.filter((action) => (action.quantity > 0 || action.quantity === -1));
        let noPossibleActions = possibleActions.length;
        if (noPossibleActions <= 0) return chosenAction;
        const probNormalAttack = 1.0;

        let totalSumProb = possibleActions.reduce(
            (acc, curr) => acc + curr.probability,
            0
        );
        totalSumProb = totalSumProb + probNormalAttack; //lower general prob of choosing actions instead of attacking
        let rand2 = Math.random() * totalSumProb;

        let currProbSum = 0.0;
        for (let i = 0; i < noPossibleActions; i++) {
            let currAction = possibleActions[i];
            let newCurrProbSum = currProbSum + currAction.probability;
            if (rand2 >= currProbSum && rand2 <= newCurrProbSum) {
                if (currAction.quantity !== -1) currAction.quantity = currAction.quantity - 1;
                chosenAction = currAction;

                return chosenAction;
            } else {
                currProbSum = newCurrProbSum;
            }
        }

        return chosenAction;
    }

    hasRemainingAction() {
        return this.actions.filter((action) => (action.quantity > 0 || action.quantity === -1)).length > 0;
    }
}

export class Item extends GameObject {

    constructor(config) {
        super(config.key, config.fullName, config.shortName);

        // Consumable, Weapon, Accessory, Spell, Treasure
        this.type = config.type || 'none';
        // Weapon: Sword, Greatsword, Polearm, Daggers, Firearm, Shield, Machinery, RoyalArms  
        this.subType = config.subType || 'none';
        this.price = config.price || 0;
        this.maxQuantity = config.maxQuantity || 10;
        this.quantity = config.quantity || -1; // -1 means it can be used indefinitely
        this.id = config.id || 0; // unique ID nr. if there are multiple copies of an item in inventory 
        this.isEquipped = config.isEquipped || false;

        // Consumables/Spells
        this.targetMode = config.targetMode || 'single'; //Single, All
        this.effect = config.effect || 'none';

        // Weapons
        this.power = config.power || 1;
        this.critical = config.critical || 1.0;
        this.element = config.element || 'none';
        this.statModifiers = config.statModifiers || {}; // Character stats
        this.conditionalModifiers = config.conditionalModifiers || {}; // stat boosts during special battle conditions
    }

    getFullName() {
        return this.fullName.toUpperCase();
    }

    getShortName() {
        return this.shortName.toUpperCase();
    }

    createConfig() {
        return {
            // key: this.key,
            // fullName: this.fullName,
            // shortName: this.shortName,
            type: this.type,
            subType: this.subType,
            price: this.price,
            maxQuantity: this.maxQuantity,
            quantity: this.quantity,
            id: this.id,
            isEquipped: this.isEquipped,
            targetMode: this.targetMode,
            effect: this.effect,
            power: this.power,
            critical: this.critical,
            element: this.element,
            statModifiers: this.statModifiers,
            conditionalModifiers: this.conditionalModifiers,
        };
    }

    getTypeToChar(gameSettings) {
        let settings = gameSettings;
        let iconChar = settings.itemTypeToCharMapping[this.type];
        if (isDefinedAndNotNull(this.subType) && this.subType !== 'none' && this.type !== 'spell') {
            iconChar = settings.itemTypeToCharMapping[this.subType];
        }

        return iconChar;
    }

    getIsEquipped() {
        return this.isEquipped;
    }

    setIsEquipped(isEquipped) {
        this.isEquipped = isEquipped;
    }

    getQuantity() {
        return this.quantity;
    }

    setQuantity(quantity) {
        this.quantity = quantity;
    }

    decreaseItemQuantity() {
        if (this.quantity > 0) {
            this.quantity--;
        }
    }

    // TODO: what about items with targetMode = 'all' ?
    applyItemEffect(gameSettings, effectDB, target, source) {
        if (target instanceof Character || target instanceof Monster) {
            let effect = effectDB.getDataByKey(this.effect);

            if (this.type === 'consumable') {
                if (this.quantity === 0) return null;

                if (source instanceof Character) source.increaseTpPerAction();

                if (effect.type === 'heal' || effect.type === 'revive') {
                    let stat = effect.stat;
                    let maxStat = null;
                    let result = null;
                    let typeIsRevive = effect.type === 'revive';
                    if (stat === 'hp') {
                        maxStat = 'maxHp';
                    } else {
                        return null;
                    }
                    let maxStatTotal = target[maxStat].total;

                    if (((!typeIsRevive && target[stat] > 0) ||
                            (typeIsRevive && target[stat] === 0)) &&
                        target[stat] < maxStatTotal) {
                        result = Effect.calcEffectPower(effect, maxStatTotal);

                        target[stat] += result;
                        target[stat] = Math.min(target[stat], maxStatTotal);

                        if (typeIsRevive && stat === 'hp' && target[stat] > 0) target.setIsDead(false);

                        this.decreaseItemQuantity();
                    } else {
                        //return null; //TODO: decide if "waste" of item should just be allowed instead of returning null 

                        this.decreaseItemQuantity();
                        result = 0;
                    }

                    return result;
                } else if (effect.type === 'cure') {
                    if (effect.statusAilment === 'status-all') {
                        //TODO: decide if Remedy cures all ailments or just (Poison, Stone, Confusion, Toad)
                        let result = target.cureAllStatusAilments();
                        this.decreaseItemQuantity(); //TODO: decide if "waste" of item should just be allowed instead of returning null 
                        return result;
                    } else {
                        let result = target.cureStatusAilment(effect.statusAilment);
                        this.decreaseItemQuantity(); //TODO: decide if "waste" of item should just be allowed instead of returning null 
                        return result;
                    }
                }
            } else if (this.type === 'spell') {
                if (this.quantity === 0) return null;

                if (effect.subType === 'offensive') {
                    let result = source.applyMagicDamageTo(target, effect, gameSettings);

                    this.decreaseItemQuantity();

                    return result;
                }
            }
        }
    }

}

export class Technique extends GameObject {
    constructor(config) {
        super(config.key, config.fullName, config.shortName);

        this.description = config.description;
        this.cost = config.cost;
        this.effects = config.effects;
        this.targetModeEnemies = config.targetModeEnemies || null;
    }

    getFullName() {
        return this.fullName.toUpperCase();
    }

    getShortName() {
        return this.shortName.toUpperCase();
    }

    getEffects() {
        return this.effects;
    }

    setEffects(effects) {
        this.effects = effects;
    }

    getTargetModeEnemies() {
        return this.targetModeEnemies;
    }

    setTargetModeEnemies(mode) {
        this.targetModeEnemies = mode;
    }
}

export class Effect extends GameObject {
    constructor(config) {
        super(config.key, config.fullName, config.shortName);

        this.type = config.type || 'none'; // attack, heal, cure, warp, spell ... 
        this.subType = config.subType || 'none'; // for type=spell: offensive, heal, cure, inflict, special (e.g. Libra)
        this.spell = config.spell || 'none'; // spell key 
        this.statusAilment = config.statusAilment || 'none'; // status ailment to cure
        this.stat = config.stat || 'none'; // stat to heal/buff/debuff
        this.element = config.element || 'none'; // element of attack/offensive spell
        this.value = config.value || 'relative'; // absolute, relative
        this.power = config.power || 1.0;
        this.targetMode = config.targetMode; // single, all
    }

    static calcEffectPower(effect, stat = 1) {
        if (effect.value === 'absolute') {
            return effect.power;
        } else if (effect.value === 'relative') {
            return Math.floor(stat * effect.power);
        }
    }
}

export class Counter extends GameObject {

    constructor(key, valueToCount, metric, maxDigits, prependMetric) {
        super(key, key);

        this.value = valueToCount;
        this.maxDigits = maxDigits || 2;
        this.metric = metric || null;
        this.prependMetric = prependMetric || false; //prepend instead of appending metric to value
    }
}

export class SpecialMenuObject extends GameObject {

    constructor(key) {
        super(key);
    }
}

export class EmptyObject extends SpecialMenuObject {

    constructor(skip) {
        super('empty');

        this.skip = skip || false;
    }
}

export class CustomImage extends GameObject {

    constructor(key, frame) {
        super(key);

        this.frame = frame;
    }

    getFrame() {
        return this.frame;
    }
}

export class CustomMonsterImage extends CustomImage {
    constructor(key, isVisible, skip) {
        super(key, 0);

        this.isVisible = isDefinedAndNotNull(isVisible) ? isVisible : true;
        this.skip = skip || !isVisible;
    }

    getIsVisible() {
        return this.isVisible;
    }

    setIsVisible(bool) {
        this.isVisible = bool;
    }
}

export class CustomString extends GameObject {

    constructor(key, fullName, shortName, useFullName, skip) {
        super(key, fullName, shortName);

        this.useFullName = useFullName || false;
        this.skip = skip || false;
    }

    getUseFullName() {
        return this.useFullName;
    }

    setUseFullName(useFullName) {
        this.useFullName = useFullName;
    }
}

export class DialogString extends CustomString {

    constructor(key, data = {}) {
        super(key, data.text, null, true);

        this.text = data.text || '???';
        this.speaker = data.speaker || '';
        this.options = data.options || {};
    }

    getText() {
        return this.text;
    }

    getSpeaker() {
        return this.speaker;
    }

    getOptions() {
        return this.options;
    }
}