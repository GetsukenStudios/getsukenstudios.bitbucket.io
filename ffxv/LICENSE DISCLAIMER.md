Copyright (c) 2018-2019 Getsuken Studios. All rights reserved.

Some of the materials such as character designs, lore, scripts, music etc. used in this fan-made project are trademarks and/or copyrighted works of Square Enix Holdings Co., Ltd. 
All rights reserved by Square Enix. This material is not official and is not endorsed by Square Enix. The developers do not seek to profit from this project.